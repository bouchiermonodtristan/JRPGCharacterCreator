# JRPG Character Creator ! A story driven character creator for your rpgs !

A small Unity game made by Bouchier Tristan & Demay Thibault to showcase the uses
of multiple choices in Unity to generate a character sheet for vampire the mascarade.

![Demo Scene](Docs/Images/CaptureDuJeu.PNG)

## Changelog

 - 12-02-2021
   - ...
 - 12-01-2021
   - First Version
   
## To play the game

To be able to simply play the game without bothering yourself you can just:

 1. Download the last build of the game (V1.1) [windows](https://cdn.discordapp.com/attachments/666238347559174146/889985744188342303/VampireV1.1.rar) - [linux](https://cdn.discordapp.com/attachments/666238347559174146/890163319204380682/VampireV1.1Linux.zip)
 2. Unzipp the files downloaded your computer.
 3. Enter the folder and double click on "VampireQuestionnaire.exe"
 4. The game should start. Enter a story seed in story json or just click on Create Character

![Unity Configuration](Docs/Images/Jeu.PNG)

At any time in the game you can click on the Character tab and it'll generate your character sheet
You can then process to come back to Story tab to keep playing and the character sheet will be updated.
Here is a view of the character sheet :

![Unity Configuration](Docs/Images/CharacterSheet.PNG)

## Configuration for Unity

To be able to use this project you'll have to set it up first:

 1. Download the original sources and unversionned files [from our drive](https://drive.google.com/file/d/1LU-nfLNPpprljcfcY2CptcZeEWHN9NJp/view?usp=sharing).
 2. Initiate this git repository somewhere on your computer.
 3. Unzipp the files downloaded from the drive (1) inside the git repository (2) initiated.
 4. Pull master inside the repository to erase the folder with the lasts modifications.
 5. Start unity hub, locate the folder and it should run.
 

## Convenient scripts

At any time while playing the game inside the unity editor, you can press H to generate the stats of the game
It shows the amount of different stats available through the choices of the whole story
It is advised to try to balance those stats when you create your story.
![Unity Configuration](Docs/Images/Stats.PNG)


## TODO
 
- ????