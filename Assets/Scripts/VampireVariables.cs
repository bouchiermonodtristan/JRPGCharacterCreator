using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Michsky.UI.ModernUIPack;
using System;

public enum Clans { Brujah = 0, Gangrel = 1, Malkavian = 2, Nosferatu = 3, Toreador = 4, Tremere = 5, Ventrue = 6, Lassombra = 7, Tzimisce = 8 }
public enum PowerRank { One = 1, Two = 2, Three = 3, Four = 4, Five = 5 }

public enum Disciplines { Abyssale = 0, Animalisme = 1, Auspex = 2, Celerite = 3, Domination = 4, ForceDame = 5, Occultation = 6, Presence = 7, Proteism = 8, Puissance = 9, SorcellerieDuSang = 10 }
public enum Attributs { Force = 0, Charisme = 1, Intelligence = 2, Dexterite = 3, Manipulation = 4, Astuce = 5, Vigueur = 6, SangFroid = 7, Resolution = 8}
public enum Specialite { Athletisme = 0, Bagarre = 1, Artisanat = 2, Conduite = 3, ArmesAfeu = 4, Melee = 5, Larcin = 6, Furtivite = 7,
    Survie = 8, Animaux = 9, Etiquette = 10, Empathie = 11, Intimidation = 12, Commandement = 13, Representation = 14, Persuasion = 15, 
    ExperienceDeLaRue = 16, Subterfuge = 17, Erudition = 18, Vigilance = 19, Finances = 20, Investigation = 21, Medecine = 22, Occultisme = 23, Politique = 24, Science = 25, Technologie = 26 }
public enum Avantage { Allie = 0, Majordhome = 1, Animal = 2 }

public class VampireVariables : MonoBehaviour
{
    public static VampireVariables current;

    public List<Power> vampirePowers;

    public int NombreAttributMax4 = 1;
    public int NombreAttributMax3 = 3;
    public int NombreAttributMax2 = 4;
    public int NombreAttributMax1 = 1;

    public bool isToucheTout = false;
    public bool isEquilibre = true;
    public bool isSpecialist = false;

    public int NombreSpecialiteMax4 = 0;
    public int NombreSpecialiteMax3 = 1;
    public int NombreSpecialiteMax2 = 8;
    public int NombreSpecialiteMax1 = 10;

    //La liste des answerNumbers r�alis� par le joueur.
    public List<int> ActionsDuJoueur = new List<int>();
    public int ActionPrecedente = 0;



    /**
     * Prenez un attribut � 4, trois attributs � 3, quatre attributs � 2 et un attribut � 1.
    Sant� = Vigueur + 3 ; Volont� = Sang-froid + R�solution.
    */
    [Header("Attributs")]
    public int Force = 1;
    public int Charisme = 1;
    public int Intelligence = 1;
    public int Dexterite = 1;
    public int Manipulation = 1;
    public int Astuce = 1;
    public int Vigueur = 1;
    public int SangFroid = 1;
    public int Resolution = 1;

    /*Touche -�-tout : une comp�tence � 3, huit comp�tences � 2 et dix comp�tences � 1.
    �quilibr� : trois comp�tences � 3, cinq comp�tences � 2 et sept comp�tences � 1.
    Sp�cialiste : une comp�tence � 4, trois comp�tences � 3, trois comp�tences � 2 et trois comp�tences � 1.
    */
    [Header("Specialite")]
    Dictionary<string, int> specialites = new Dictionary<string, int> {
           { nameof(Specialite.Athletisme), 1 },
           { nameof(Specialite.Bagarre), 1 },
           { nameof(Specialite.Artisanat), 1 },
           { nameof(Specialite.Conduite), 1 },
           { nameof(Specialite.ArmesAfeu), 1 },
           { nameof(Specialite.Melee), 1 },
           { nameof(Specialite.Larcin), 1 },
           { nameof(Specialite.Furtivite), 1 },
           { nameof(Specialite.Survie), 1 },
           { nameof(Specialite.Animaux), 1 },
           { nameof(Specialite.Etiquette), 1 },
           { nameof(Specialite.Empathie), 1 },
           { nameof(Specialite.Intimidation), 1 },
           { nameof(Specialite.Commandement), 1 },
           { nameof(Specialite.Representation), 1 },
           { nameof(Specialite.Persuasion), 1 },
           { nameof(Specialite.ExperienceDeLaRue), 1 },
           { nameof(Specialite.Subterfuge), 1 },
           { nameof(Specialite.Erudition), 1 },
           { nameof(Specialite.Vigilance), 1 },
           { nameof(Specialite.Finances), 1 },
           { nameof(Specialite.Investigation), 1 },
           { nameof(Specialite.Medecine), 1 },
           { nameof(Specialite.Occultisme), 1 },
           { nameof(Specialite.Politique), 1 },
           { nameof(Specialite.Science), 1 },
           { nameof(Specialite.Technologie), 1 }
    };

    Dictionary<string, int> attributs = new Dictionary<string, int> {
           { nameof(Attributs.Force), 1 },
           { nameof(Attributs.Charisme), 1 },
           { nameof(Attributs.Intelligence), 1 },
           { nameof(Attributs.Dexterite), 1 },
           { nameof(Attributs.Manipulation), 1 },
           { nameof(Attributs.Astuce), 1 },
           { nameof(Attributs.Vigueur), 1 },
           { nameof(Attributs.SangFroid), 1 },
           { nameof(Attributs.Resolution), 1 }
    };

    public List<Power> availablePowers = new List<Power>();
    public List<Power> actualPowers = new List<Power>();
    public Dictionary<string, int> actualDisciplinesAndLevel = new Dictionary<string, int> {
           { nameof(Disciplines.Celerite), 0 },
           { nameof(Disciplines.Abyssale), 0 },
           { nameof(Disciplines.Animalisme), 0 },
           { nameof(Disciplines.Auspex), 0 },
           { nameof(Disciplines.Domination), 0 },
           { nameof(Disciplines.ForceDame), 0 },
           { nameof(Disciplines.Occultation), 0 },
           { nameof(Disciplines.Presence), 0 },
           { nameof(Disciplines.Proteism), 0 },
           { nameof(Disciplines.Puissance), 0 },
           { nameof(Disciplines.SorcellerieDuSang), 0 }
    };


    public void updateAttributsDictionnary()
    {
        attributs[nameof(Attributs.Force)] = Force;
        attributs[nameof(Attributs.Charisme)] = Charisme;
        attributs[nameof(Attributs.Intelligence)] = Intelligence;
        attributs[nameof(Attributs.Dexterite)] = Dexterite;
        attributs[nameof(Attributs.Manipulation)] = Manipulation;
        attributs[nameof(Attributs.Astuce)] = Astuce;
        attributs[nameof(Attributs.Vigueur)] = Vigueur;
        attributs[nameof(Attributs.SangFroid)] = SangFroid;
        attributs[nameof(Attributs.Resolution)] = Resolution;
    }

    public void updateSpecialiteDictionnary()
    {
        specialites[nameof(Specialite.Athletisme)] = Athletisme;
        specialites[nameof(Specialite.Bagarre)] = Bagarre;
        specialites[nameof(Specialite.Artisanat)] = Artisanat;
        specialites[nameof(Specialite.Conduite)] = Conduite;
        specialites[nameof(Specialite.ArmesAfeu)] = ArmesAfeu;
        specialites[nameof(Specialite.Melee)] = Melee;
        specialites[nameof(Specialite.Larcin)] = Larcin;
        specialites[nameof(Specialite.Furtivite)] = Furtivite;
        specialites[nameof(Specialite.Survie)] = Survie;
        specialites[nameof(Specialite.Animaux)] = Animaux;
        specialites[nameof(Specialite.Etiquette)] = Etiquette;
        specialites[nameof(Specialite.Empathie)] = Empathie;
        specialites[nameof(Specialite.Intimidation)] = Intimidation;
        specialites[nameof(Specialite.Commandement)] = Commandement;
        specialites[nameof(Specialite.Representation)] = Representation;
        specialites[nameof(Specialite.Persuasion)] = Persuasion;
        specialites[nameof(Specialite.ExperienceDeLaRue)] = ExperienceDeLaRue; 
        specialites[nameof(Specialite.Subterfuge)] = Subterfuge;
        specialites[nameof(Specialite.Erudition)] = Erudition;
        specialites[nameof(Specialite.Vigilance)] = Vigilance;
        specialites[nameof(Specialite.Finances)] = Finances;
        specialites[nameof(Specialite.Investigation)] = Investigation;
        specialites[nameof(Specialite.Medecine)] = Medecine;
        specialites[nameof(Specialite.Occultisme)] = Occultisme;
        specialites[nameof(Specialite.Politique)] = Politique;
        specialites[nameof(Specialite.Science)] = Science;
        specialites[nameof(Specialite.Technologie)] = Technologie;
    }

    public int Athletisme = 1;
    public int Bagarre = 1;
    public int Artisanat = 1;
    public int Conduite = 1;
    public int ArmesAfeu = 1;
    public int Melee = 1;
    public int Larcin = 1;
    public int Furtivite = 1;
    public int Survie = 1;
    public int Animaux = 1;
    public int Etiquette = 1;
    public int Empathie = 1;
    public int Intimidation = 1;
    public int Commandement = 1;
    public int Representation = 1;
    public int Persuasion = 1;
    public int ExperienceDeLaRue = 1;
    public int Subterfuge = 1;
    public int Erudition = 1;
    public int Vigilance = 1;
    public int Finances = 1;
    public int Investigation = 1;
    public int Medecine = 1;
    public int Occultisme = 1;
    public int Politique = 1;
    public int Science = 1;
    public int Technologie = 1;

    [Header("Clan")]
    public bool isBrujah = false;
    public bool isGangrel = false;
    public bool isMalkavian = false;
    public bool isNosferatu = false;
    public bool isToreador = false;
    public bool isTremere = false;
    public bool isVentrue = false;
    public bool isLassombra = false;
    public bool isTzimisce = false;

    [Header("Internal, do not touch")]
    public List<Disciplines> BrujahDisciplines = new List<Disciplines> { Disciplines.Presence, Disciplines.Celerite, Disciplines.Puissance };
    public List<Disciplines> GangrelDisciplines = new List<Disciplines> { Disciplines.Proteism, Disciplines.Animalisme, Disciplines.ForceDame};
    public List<Disciplines> MalkavianDisciplines = new List<Disciplines> { Disciplines.Auspex, Disciplines.Domination, Disciplines.Occultation};
    public List<Disciplines> NosferatuDisciplines = new List<Disciplines> { Disciplines.Occultation, Disciplines.Puissance, Disciplines.Animalisme};
    public List<Disciplines> ToreadorDisciplines = new List<Disciplines> { Disciplines.Presence, Disciplines.Celerite, Disciplines.Auspex};
    public List<Disciplines> TremereDisciplines = new List<Disciplines> { Disciplines.SorcellerieDuSang, Disciplines.Domination, Disciplines.Auspex};
    public List<Disciplines> VentrueDisciplines = new List<Disciplines> { Disciplines.Domination, Disciplines.ForceDame, Disciplines.Presence};
    public List<Disciplines> LassombraDisciplines = new List<Disciplines> { Disciplines.Abyssale, Disciplines.Domination, Disciplines.Puissance};
    public List<Disciplines> TzimisceDisciplines = new List<Disciplines> { Disciplines.Animalisme, Disciplines.Domination, Disciplines.Proteism };
    public List<Disciplines> SalubriDisciplines = new List<Disciplines> { Disciplines.Auspex, Disciplines.Domination, Disciplines.ForceDame };

    private void Awake()
    {
        current = this;
    }

    void Start()
    {
        setNombreSpecialite();
    }

    public void setNombreSpecialite()
    {
        if (isEquilibre)
        {
            setEquilibre();
        }
        else if (isToucheTout)
        {
            setToucheATout();
        }
        else if (isSpecialist)
        {
            setSpecialist();
        }
    }
    public void resetAll()
    {
        cleanAllUi();
        ActionsDuJoueur = new List<int>();
        ActionPrecedente = 0;
        resetClan();
        resetDiscipline();
        resetSpecialite();
        resetAttributs();
    }

    public void resetAttributs()
    {
        attributs = new Dictionary<string, int> {
           { nameof(Attributs.Force), 1 },
           { nameof(Attributs.Charisme), 1 },
           { nameof(Attributs.Intelligence), 1 },
           { nameof(Attributs.Dexterite), 1 },
           { nameof(Attributs.Manipulation), 1 },
           { nameof(Attributs.Astuce), 1 },
           { nameof(Attributs.Vigueur), 1 },
           { nameof(Attributs.SangFroid), 1 },
           { nameof(Attributs.Resolution), 1 }
          };

            Force = 1;
            Charisme = 1;
            Intelligence = 1;
            Dexterite = 1;
            Manipulation = 1;
            Astuce = 1;
            Vigueur = 1;
            SangFroid = 1;
            Resolution = 1;
    }

    public void resetSpecialite()
    {

             Athletisme = 1;
     Bagarre = 1;
     Artisanat = 1;
     Conduite = 1;
     ArmesAfeu = 1;
     Melee = 1;
     Larcin = 1;
     Furtivite = 1;
     Survie = 1;
     Animaux = 1;
     Etiquette = 1;
     Empathie = 1;
     Intimidation = 1;
     Commandement = 1;
     Representation = 1;
     Persuasion = 1;
     ExperienceDeLaRue = 1;
     Subterfuge = 1;
     Erudition = 1;
     Vigilance = 1;
     Finances = 1;
     Investigation = 1;
     Medecine = 1;
     Occultisme = 1;
     Politique = 1;
     Science = 1;
     Technologie = 1;
        specialites = new Dictionary<string, int> {
           { nameof(Specialite.Athletisme), 1 },
           { nameof(Specialite.Bagarre), 1 },
           { nameof(Specialite.Artisanat), 1 },
           { nameof(Specialite.Conduite), 1 },
           { nameof(Specialite.ArmesAfeu), 1 },
           { nameof(Specialite.Melee), 1 },
           { nameof(Specialite.Larcin), 1 },
           { nameof(Specialite.Furtivite), 1 },
           { nameof(Specialite.Survie), 1 },
           { nameof(Specialite.Animaux), 1 },
           { nameof(Specialite.Etiquette), 1 },
           { nameof(Specialite.Empathie), 1 },
           { nameof(Specialite.Intimidation), 1 },
           { nameof(Specialite.Commandement), 1 },
           { nameof(Specialite.Representation), 1 },
           { nameof(Specialite.Persuasion), 1 },
           { nameof(Specialite.ExperienceDeLaRue), 1 },
           { nameof(Specialite.Subterfuge), 1 },
           { nameof(Specialite.Erudition), 1 },
           { nameof(Specialite.Vigilance), 1 },
           { nameof(Specialite.Finances), 1 },
           { nameof(Specialite.Investigation), 1 },
           { nameof(Specialite.Medecine), 1 },
           { nameof(Specialite.Occultisme), 1 },
           { nameof(Specialite.Politique), 1 },
           { nameof(Specialite.Science), 1 },
           { nameof(Specialite.Technologie), 1 }
    };
    }

    public void resetClan()
    {
        isBrujah = false;
        isGangrel = false;
        isMalkavian = false;
        isNosferatu = false;
        isToreador = false;
        isTremere = false;
        isVentrue = false;
        isLassombra = false;
        isTzimisce = false;
        computeClan();
    }

    public void resetDiscipline()
    {
        actualPowers = new List<Power>();
        actualDisciplinesAndLevel = new Dictionary<string, int> {
           { nameof(Disciplines.Celerite), 0 },
           { nameof(Disciplines.Abyssale), 0 },
           { nameof(Disciplines.Animalisme), 0 },
           { nameof(Disciplines.Auspex), 0 },
           { nameof(Disciplines.Domination), 0 },
           { nameof(Disciplines.ForceDame), 0 },
           { nameof(Disciplines.Occultation), 0 },
           { nameof(Disciplines.Presence), 0 },
           { nameof(Disciplines.Proteism), 0 },
           { nameof(Disciplines.Puissance), 0 },
           { nameof(Disciplines.SorcellerieDuSang), 0 } 
        };
    }

    public void setEquilibre()
    {
        isEquilibre = true;
        isSpecialist = false;
        isToucheTout = false;
        NombreSpecialiteMax4 = 0;
        NombreSpecialiteMax3 = 3;
        NombreSpecialiteMax2 = 5;
        NombreSpecialiteMax1 = 7;
    }

    public void setSpecialist()
    {
        isEquilibre = false;
        isSpecialist = true;
        isToucheTout = false;
        NombreSpecialiteMax4 = 1;
        NombreSpecialiteMax3 = 3;
        NombreSpecialiteMax2 = 3;
        NombreSpecialiteMax1 = 3;
    }

    public void setToucheATout()
    {
        isEquilibre = false;
        isSpecialist = false;
        isToucheTout = true;
        NombreSpecialiteMax4 = 0;
        NombreSpecialiteMax3 = 1;
        NombreSpecialiteMax2 = 8;
        NombreSpecialiteMax1 = 10;
    }

    public bool hasPlayerActionNumber(int actionNumber)
    {
        return ActionsDuJoueur.Contains(actionNumber);
    }

    public void AddAnswerNumber(Answer answer)
    {
        ActionsDuJoueur.Add(answer.answerNumber);
    }

    public void AddAnswerPlayerPoints(Answer answer)
    {
        VampireUIManager.current.NotificationManager.title = "Points Gagn�s";
        VampireUIManager.current.NotificationManager.description = "";
        VampireUIManager.current.NotificationManager.timer = 4f;
        string description = "";
        description += AddAttributePoint(answer.attributsBoost);
        if (description != "")
            description += "\n";
       description+= AddSpecialitePoint(answer.specialiteBoost);
        if (description == "\n")
            description = "";

        VampireUIManager.current.NotificationManager.description = description;
        VampireUIManager.current.NotificationManager.UpdateUI();
        if (description != "")
            VampireUIManager.current.NotificationManager.OpenNotification();
    }

    public string AddAttributePoint(List<string> attributs)
    {
        string txt = "";
        foreach (string attribut in attributs)
        {
            switch (attribut)
            {
                case nameof(Attributs.Astuce):
                    Astuce += 1;
                    txt += nameof(Attributs.Astuce) + "\n";
                    break;
                case nameof(Attributs.Charisme):
                    Charisme += 1;
                    txt += nameof(Attributs.Charisme) + "\n";
                    break;
                case nameof(Attributs.Dexterite):
                    Dexterite += 1;
                    txt += nameof(Attributs.Dexterite) + "\n";
                    break;
                case nameof(Attributs.Force):
                    Force += 1;
                    txt += nameof(Attributs.Force) + "\n";
                    break;
                case nameof(Attributs.Intelligence):
                    Intelligence += 1;
                    txt += nameof(Attributs.Intelligence) + "\n";
                    break;
                case nameof(Attributs.Manipulation):
                    Manipulation += 1;
                    txt += nameof(Attributs.Manipulation) + "\n";
                    break;
                case nameof(Attributs.Resolution):
                    Resolution += 1;
                    txt += nameof(Attributs.Resolution) + "\n";
                    break;
                case nameof(Attributs.SangFroid):
                    SangFroid += 1;
                    txt += nameof(Attributs.SangFroid) + "\n";
                    break;
                case nameof(Attributs.Vigueur):
                    Vigueur += 1;
                    txt += nameof(Attributs.Vigueur) + "\n";
                    break;
                default:
                    break;
            }
        }
        updateAttributsDictionnary();
        return txt;
    }

    public string AddSpecialitePoint(List<string> specialites)
    {
        string txt = "";
        foreach (string specialite in specialites)
        {
            switch (specialite)
            {
                case nameof(Specialite.Animaux):
                    Animaux += 1;
                    txt += nameof(Specialite.Animaux) + "\n";
                    break;
                case nameof(Specialite.ArmesAfeu):
                    ArmesAfeu += 1;
                    txt += nameof(Specialite.ArmesAfeu) + "\n";
                    break;
                case nameof(Specialite.Artisanat):
                    Artisanat += 1;
                    txt += nameof(Specialite.Artisanat) + "\n";
                    break;
                case nameof(Specialite.Athletisme):
                    Athletisme += 1;
                    txt += nameof(Specialite.Athletisme) + "\n";
                    break;
                case nameof(Specialite.Bagarre):
                    Bagarre += 1;
                    txt += nameof(Specialite.Bagarre) + "\n";
                    break;
                case nameof(Specialite.Commandement):
                    Commandement += 1;
                    txt += nameof(Specialite.Commandement) + "\n";
                    break;
                case nameof(Specialite.Conduite):
                    Conduite += 1;
                    txt += nameof(Specialite.Conduite) + "\n";
                    break;
                case nameof(Specialite.Empathie):
                    Empathie += 1;
                    txt += nameof(Specialite.Empathie) + "\n";
                    break;
                case nameof(Specialite.Erudition):
                    Erudition += 1;
                    txt += nameof(Specialite.Erudition) + "\n";
                    break;
                case nameof(Specialite.Etiquette):
                    Etiquette += 1;
                    txt += nameof(Specialite.Etiquette) + "\n";
                    break;
                case nameof(Specialite.ExperienceDeLaRue):
                    ExperienceDeLaRue += 1;
                    txt += nameof(Specialite.ExperienceDeLaRue) + "\n";
                    break;
                case nameof(Specialite.Finances):
                    Finances += 1;
                    txt += nameof(Specialite.Finances) + "\n";
                    break;
                case nameof(Specialite.Furtivite):
                    Furtivite += 1;
                    txt += nameof(Specialite.Furtivite) + "\n";
                    break;
                case nameof(Specialite.Intimidation):
                    Intimidation += 1;
                    txt += nameof(Specialite.Intimidation) + "\n";
                    break;
                case nameof(Specialite.Investigation):
                    Investigation += 1;
                    txt += nameof(Specialite.Investigation) + "\n";
                    break;
                case nameof(Specialite.Larcin):
                    Larcin += 1;
                    txt += nameof(Specialite.Larcin) + "\n";
                    break;
                case nameof(Specialite.Medecine):
                    Medecine += 1;
                    txt += nameof(Specialite.Medecine) + "\n";
                    break;
                case nameof(Specialite.Melee):
                    Melee += 1;
                    txt += nameof(Specialite.Melee) + "\n";
                    break;
                case nameof(Specialite.Occultisme):
                    Occultisme += 1;
                    txt += nameof(Specialite.Occultisme) + "\n";
                    break;
                case nameof(Specialite.Persuasion):
                    Persuasion += 1;
                    txt += nameof(Specialite.Persuasion) + "\n";
                    break;
                case nameof(Specialite.Politique):
                    Politique += 1;
                    txt += nameof(Specialite.Politique) + "\n";
                    break;
                case nameof(Specialite.Representation):
                    Representation += 1;
                    txt += nameof(Specialite.Representation) + "\n";
                    break;
                case nameof(Specialite.Science):
                    Science += 1;
                    txt += nameof(Specialite.Science) + "\n";
                    break;
                case nameof(Specialite.Subterfuge):
                    Subterfuge += 1;
                    txt += nameof(Specialite.Subterfuge) + "\n";
                    break;
                case nameof(Specialite.Survie):
                    Survie += 1;
                    txt += nameof(Specialite.Survie) + "\n";
                    break;
                case nameof(Specialite.Technologie):
                    Technologie += 1;
                    txt += nameof(Specialite.Technologie) + "\n";
                    break;
                case nameof(Specialite.Vigilance):
                    Vigilance += 1;
                    txt += nameof(Specialite.Vigilance) + "\n";
                    break;
                default:
                    break;
            }
        }
        updateSpecialiteDictionnary();
        return txt;
    }

    public void AddAnswerDescriptionsNumbers(List<AnswerDescription> answerDescriptions)
    {
        foreach(AnswerDescription answerDescription in answerDescriptions)
        {
            ActionsDuJoueur.Add(answerDescription.answerDescriptionNumber);
        }
    }
    public void AddStoryDescriptionsNumbers(List<StoryDescription> storyDescriptions)
    {
        foreach (StoryDescription storyDescription in storyDescriptions)
        {
            ActionsDuJoueur.Add(storyDescription.storyDescriptionNumber);
        }
    }

    public void computeClan()
    {
        if (isBrujah)
        {
            VampireUIManager.current.clanText.text = "Brujah";
        } else if(isGangrel)
        {
            VampireUIManager.current.clanText.text = "Gangrel";
        }
        else if(isLassombra)
        {
            VampireUIManager.current.clanText.text = "Lassombra";
        }
        else if(isMalkavian)
        {
            VampireUIManager.current.clanText.text = "Malkavian";
        }
        else if(isNosferatu)
        {
            VampireUIManager.current.clanText.text = "Nosferatu";
        }
        else if(isToreador)
        {
            VampireUIManager.current.clanText.text = "Toreador";
        }
        else if(isTremere)
        {
            VampireUIManager.current.clanText.text = "Tremere";
        } else if(isTzimisce)
        {
            VampireUIManager.current.clanText.text = "Tzimisce";
        }
        else if(isVentrue)
        {
            VampireUIManager.current.clanText.text = "Ventrue";
        } else
        {
            VampireUIManager.current.clanText.text = "";
        }
        VampireUIManager.current.clanText.GetComponent<CustomInputField>().UpdateState();
    }

    public void computeAllStats()
    {
        cleanAllUi();
        computeAttributes();
        computeSpecialite();
        computeDisciplines();
        computeClan();
    }

    public void cleanAllUi()
    {
        VampireUIManager.current.attributeUIManager.cleanAll();
        VampireUIManager.current.specialiteUIManager.cleanAll();
        VampireUIManager.current.disciplineUIManager.cleanAll();
    }

    public void computeDisciplines()
    {
        VampireUIManager.current.disciplineUIManager.cleanAll();

        foreach(Power power in actualPowers)
        {
            VampireUIManager.current.disciplineUIManager.addDiscipline(power.powerDiscipline.ToString(), (int) power.powerRank, power.powerName);
        }
    }

    public void computeAttributes()
    {
        updateAttributsDictionnary();

        //Order the dictionnary
        var ordered = attributs.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
        int i = 0;

        // trois attributs � 3
        for (i = 0; i < NombreAttributMax4; i++)
        {
            //Order the dictionnary
            ordered = attributs.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
            VampireUIManager.current.attributeUIManager.setAttributeValue(ordered.First().Key, 4);
            //Set the value of the dictionnary key entered to character sheet to 0
            attributs[ordered.First().Key] = 0;
        }

        // trois attributs � 3
        for (i = 0; i < NombreAttributMax3; i++)
        {
            //Order the dictionnary
            ordered = attributs.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
            VampireUIManager.current.attributeUIManager.setAttributeValue(ordered.First().Key, 3);
            //Set the value of the dictionnary key entered to character sheet to 0
            attributs[ordered.First().Key] = 0;
        }

        //quatre attributs � 2

        for (i = 0; i < NombreAttributMax2; i++)
        {
            //Order the dictionnary
            ordered = attributs.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
            VampireUIManager.current.attributeUIManager.setAttributeValue(ordered.First().Key, 2);
            //Set the value of the dictionnary key entered to character sheet to 0
            attributs[ordered.First().Key] = 0;
        }

        //Un attribut � 1
        for (i = 0; i < NombreAttributMax1; i++)
        {
            //Order the dictionnary
            ordered = attributs.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
            VampireUIManager.current.attributeUIManager.setAttributeValue(ordered.First().Key, 1);
            //Set the value of the dictionnary key entered to character sheet to 0
            attributs[ordered.First().Key] = 0;
        }

        //It's over, reset the dictionnary

        // BUG If the player never used a stat, it won't be used and will bug. It'll act like there is only 8 stats. And none will be 4. The stat at 0 will remain at 0.


        updateAttributsDictionnary();
    }

    public void computeSpecialite()
    {
        updateSpecialiteDictionnary();


        //Specialites a 4
        for (int i = 0; i < NombreSpecialiteMax4; i++)
        {
            //Order the dictionnary
            var ordered = specialites.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
            VampireUIManager.current.specialiteUIManager.setSpecialiteValue(ordered.First().Key, 4);
            //Set the value of the dictionnary key entered to character sheet to 0
            specialites[ordered.First().Key] = 0;
        }

        //Specialites a 3
        for (int i = 0; i < NombreSpecialiteMax3; i++)
        {
            //Order the dictionnary
            var ordered = specialites.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
            VampireUIManager.current.specialiteUIManager.setSpecialiteValue(ordered.First().Key, 3);
            //Set the value of the dictionnary key entered to character sheet to 0
            specialites[ordered.First().Key] = 0;
        }

        //Specialites a 2
        for (int i = 0; i < NombreSpecialiteMax2; i++)
        {
            //Order the dictionnary
            var ordered = specialites.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
            VampireUIManager.current.specialiteUIManager.setSpecialiteValue(ordered.First().Key, 2);
            //Set the value of the dictionnary key entered to character sheet to 0
            specialites[ordered.First().Key] = 0;
        }

        //Specialites a 1
        for (int i = 0; i < NombreSpecialiteMax1; i++)
        {
            //Order the dictionnary
            var ordered = specialites.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
            VampireUIManager.current.specialiteUIManager.setSpecialiteValue(ordered.First().Key, 1);
            //Set the value of the dictionnary key entered to character sheet to 0
            specialites[ordered.First().Key] = 0;
        }

        //It's over, reset the dictionnary
        updateSpecialiteDictionnary();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.G))
        {
            computeAllStats();
        }
    }
}
