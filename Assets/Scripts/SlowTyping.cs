using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;
using Michsky.UI.ModernUIPack;

public class SlowTyping : MonoBehaviour 
{

	TMP_Text txt;
	ButtonManager buttonManager;
	IEnumerator coroutine;
	void Awake()
	{
		if(GetComponent<TMP_Text>())
		{
			txt = GetComponent<TMP_Text>();
		}else if(GetComponent<ButtonManager>())
		{
			buttonManager = GetComponent<ButtonManager>();
		}
		clearText();
		coroutine = PlayText("");
	}

	public IEnumerator playText(string text)
	{
		StopCoroutine(coroutine);
		clearText();
		coroutine = PlayText(text);
		yield return(StartCoroutine(coroutine));
	}

	public void clearText()
	{
		if (txt)
		{
			txt.text = "";
		}
		else if (buttonManager)
		{
			buttonManager.buttonText = "";
			buttonManager.UpdateUI();
		}
	}

	IEnumerator PlayText(string text)
	{
		foreach (char c in text)
		{
			if(txt)
			{
				txt.text += c;
			} else if(buttonManager)
			{
				buttonManager.buttonText += c;
				buttonManager.UpdateUI();
			}
			yield return new WaitForSeconds(VampireUIManager.current.textSpeed);
		}
	}

}