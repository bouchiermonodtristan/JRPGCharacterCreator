using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialiteUIManager : MonoBehaviour
{
    public List<SpecialiteUI> specialitesUI;

    private void Start()
    {
        foreach (SpecialiteUI specialiteUI in GameObject.FindObjectsOfType<SpecialiteUI>())
        {
            specialitesUI.Add(specialiteUI);
        }
    }
    public void setSpecialiteValue(string specialite, int value)
    {
        SpecialiteUI specialiteUI = getSpecialiteUIByName(specialite);
        if(specialiteUI != null)
        {
            specialiteUI.setSpecialiteValue(value);
        }
    }

    public void cleanAll()
    {
        foreach (SpecialiteUI specialite in specialitesUI)
        {
            specialite.clean();
        }
    }

    public SpecialiteUI getSpecialiteUIByName(string specialite)
    {
        foreach(SpecialiteUI specialiteUI in specialitesUI)
        {
            if(specialiteUI.specialite.ToString() == specialite)
            {
                return specialiteUI;
            }
        }

        return null;
    }

}
