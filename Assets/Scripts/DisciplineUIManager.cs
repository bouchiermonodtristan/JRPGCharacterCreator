using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisciplineUIManager : MonoBehaviour
{
    public List<DisciplineUI> disciplinesUI;

    private void Start()
    {
        foreach (DisciplineUI disciplineUI in GameObject.FindObjectsOfType<DisciplineUI>())
        {
            disciplinesUI.Add(disciplineUI);
        }
    }

    public void cleanAll()
    {
        foreach(DisciplineUI discipline in disciplinesUI)
        {
            discipline.clean();
        }
    }

    public void addDiscipline(string localDiscipline, int value, string power)
    {
        DisciplineUI existingDiscipline = getDisciplineUIByName(localDiscipline);

        if(!existingDiscipline)
        {
            existingDiscipline = getFirstAvailableDisciplineUI();
        }

        existingDiscipline.setDiscipline(localDiscipline);
        existingDiscipline.setDisciplineValue(value);
        existingDiscipline.addNewPower(power);
    }

    public DisciplineUI getDisciplineUIByName(string name)
    {
        DisciplineUI existingDiscipline = null;

        foreach (DisciplineUI disciplineUI in disciplinesUI)
        {
            if (disciplineUI.discipline == name)
            {
                existingDiscipline = disciplineUI;
            }
        }

        return existingDiscipline;
    }

    public DisciplineUI getFirstAvailableDisciplineUI()
    {
        DisciplineUI existingDiscipline = null;

        foreach (DisciplineUI disciplineUI in disciplinesUI)
        {
            if (disciplineUI.discipline == "")
            {
                existingDiscipline = disciplineUI;
            }
        }

        return existingDiscipline;
    }

}
