using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttributeUI : MonoBehaviour
{
    public DotsUI dotsUI;
    public Attributs attribut;

    public void setAttributeValue(int value)
    {
        dotsUI.activateToggles(value);
    }

    public void clean()
    {
        dotsUI.deactivateAllToggles();
    }

}
