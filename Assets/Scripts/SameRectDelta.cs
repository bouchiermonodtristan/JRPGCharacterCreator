using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SameRectDelta : MonoBehaviour
{

    public RectTransform myrect;
    public RectTransform targetRect;
    public RectTransform parentTargetRect;

    // Update is called once per frame
    void Update()
    {
        myrect.sizeDelta = new Vector2(0, targetRect.sizeDelta.y + parentTargetRect.sizeDelta.y);
    }
}
