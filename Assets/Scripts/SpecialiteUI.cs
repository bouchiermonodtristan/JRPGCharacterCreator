using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialiteUI : MonoBehaviour
{
    public DotsUI dotsUI;
    public Specialite specialite;

    public void setSpecialiteValue(int value)
    {
        dotsUI.activateToggles(value);
    }

    public void clean()
    {
        dotsUI.deactivateAllToggles();
    }
}
