using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenBackStory : MonoBehaviour
{
    public void openBackStory()
    {
        if(VampireUIManager.current.isWindow1)
        {
            VampireUIManager.current.ButtonWindow1Button.onClick.Invoke();
        } else
        {
            VampireUIManager.current.ButtonWindow2Button.onClick.Invoke();
        }
    }
}
