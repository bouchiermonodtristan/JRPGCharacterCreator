using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttributeUIManager : MonoBehaviour
{
    public List<AttributeUI> attributesUI;

    private void Start()
    {
        foreach(AttributeUI attributeUI in GameObject.FindObjectsOfType<AttributeUI>())
        {
            attributesUI.Add(attributeUI);
        }
    }
    public void cleanAll()
    {
        foreach (AttributeUI attribute in attributesUI)
        {
            attribute.clean();
        }
    }

    public void setAttributeValue(string attribut, int value)
    {
        AttributeUI attributUI = getAttributeUIByName(attribut);
        if(attributUI != null)
        {
            attributUI.setAttributeValue(value);
        }
    }

    public AttributeUI getAttributeUIByName(string attribut)
    {
        foreach(AttributeUI attributeUI in attributesUI)
        {
            if(attributeUI.attribut.ToString() == attribut)
            {
                return attributeUI;
            }
        }

        return null;
    }

}
