using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Michsky.UI.ModernUIPack;

public class VampireUIManager : MonoBehaviour
{
    public static VampireUIManager current;
    public VampireVariables vampireVariables;

    [Header("Json Variable")]
    public NotificationManager NotificationManager;
    public ModalWindowManager ClanGainedModale;
    public TMP_InputField clanText;
    public InputField JsonContainer;
    public Stories stories;
    public Story previousStory = null;
    public Story actualStory = null;
    public List<AnswerDescription> avaiblableAnswerDescriptions = new List<AnswerDescription>();
    public List<StoryDescription> avaiblableStoryDescriptions = new List<StoryDescription>();
    public bool isWindow1 = true;
    public float textSpeed = 0.125f;
    IEnumerator fillWindow;

    public AttributeUIManager attributeUIManager;
    public SpecialiteUIManager specialiteUIManager;
    public DisciplineUIManager disciplineUIManager;

    public GameObject ButtonWindow1;
    public GameObject ButtonWindow1MainText;
    public GameObject ButtonWindow1Option1;
    public GameObject ButtonWindow1Option2;
    public GameObject ButtonWindow1Option3;
    public GameObject ButtonWindow1Option4;
    public GameObject ButtonWindow1Option5;
    public GameObject ButtonWindow1Option6;

    public GameObject ButtonWindow2;
    public GameObject ButtonWindow2MainText;
    public GameObject ButtonWindow2Option1;
    public GameObject ButtonWindow2Option2;
    public GameObject ButtonWindow2Option3;
    public GameObject ButtonWindow2Option4;
    public GameObject ButtonWindow2Option5;
    public GameObject ButtonWindow2Option6;

    public Button ButtonWindow1Button;
    SlowTyping SlowTypingWindow1MainText;
    SlowTyping SlowTypingWindow1Option1;
    SlowTyping SlowTypingWindow1Option2;
    SlowTyping SlowTypingWindow1Option3;
    SlowTyping SlowTypingWindow1Option4;
    SlowTyping SlowTypingWindow1Option5;
    SlowTyping SlowTypingWindow1Option6;
    public Button ButtonWindow2Button;
    SlowTyping SlowTypingWindow2MainText;
    SlowTyping SlowTypingWindow2Option1;
    SlowTyping SlowTypingWindow2Option2;
    SlowTyping SlowTypingWindow2Option3;
    SlowTyping SlowTypingWindow2Option4;
    SlowTyping SlowTypingWindow2Option5;
    SlowTyping SlowTypingWindow2Option6;

    private void Awake()
    {
        current = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        ButtonWindow1Button = ButtonWindow1.GetComponent<Button>();
        SlowTypingWindow1MainText = ButtonWindow1MainText.GetComponent<SlowTyping>();
         SlowTypingWindow1Option1 = ButtonWindow1Option1.GetComponent<SlowTyping>();
        SlowTypingWindow1Option2 = ButtonWindow1Option2.GetComponent<SlowTyping>();
        SlowTypingWindow1Option3 = ButtonWindow1Option3.GetComponent<SlowTyping>();
        SlowTypingWindow1Option4 = ButtonWindow1Option4.GetComponent<SlowTyping>();
        SlowTypingWindow1Option5 = ButtonWindow1Option5.GetComponent<SlowTyping>();
        SlowTypingWindow1Option6 = ButtonWindow1Option6.GetComponent<SlowTyping>();
        ButtonWindow2Button = ButtonWindow2.GetComponent<Button>();
        SlowTypingWindow2MainText = ButtonWindow2MainText.GetComponent<SlowTyping>();
        SlowTypingWindow2Option1 = ButtonWindow2Option1.GetComponent<SlowTyping>();
        SlowTypingWindow2Option2 = ButtonWindow2Option2.GetComponent<SlowTyping>();
        SlowTypingWindow2Option3 = ButtonWindow2Option3.GetComponent<SlowTyping>();
        SlowTypingWindow2Option4 = ButtonWindow2Option4.GetComponent<SlowTyping>();
        SlowTypingWindow2Option5 = ButtonWindow2Option5.GetComponent<SlowTyping>();
        SlowTypingWindow2Option6 = ButtonWindow2Option6.GetComponent<SlowTyping>();
        vampireVariables = VampireVariables.current;
    }

    public void StartStories()
    {
        VampireVariables.current.resetAll();
        isWindow1 = true;
        stories = JsonUtility.FromJson<Stories>(JsonContainer.text);
        actualStory = stories.listStory[0];
        if (actualStory == null)
        {
            EndStories();
        }
        else
        {
            //Reset the list of the story description used (The one the player can read cause of his past choices)
            avaiblableStoryDescriptions = getAvailableStoryDescriptions(actualStory.storyDescriptions);
            deactivateAllAnswersSlots();
            ButtonWindow1Button.onClick.Invoke();
            fillWindow = FillWindow(
            SlowTypingWindow1MainText,
            SlowTypingWindow1Option1,
            SlowTypingWindow1Option2,
            SlowTypingWindow1Option3,
            SlowTypingWindow1Option4,
            SlowTypingWindow1Option5,
            SlowTypingWindow1Option6);
            StartCoroutine(fillWindow);
        }
    }

    public Story getNextStory(int StoryNumber)
    {
        Story nextStory = null;
        foreach (Story story in stories.listStory)
        {
            if (story.storyNumber == StoryNumber)
            {
                return nextStory = story;
            }
        }

        return null;
    }

    public void setClanModale(string clan)
    {
        ClanGainedModale.titleText = clan;
        ClanGainedModale.descriptionText = "F�licitation vous venez de gagner un CLAN. Cliquez sur Character pour voir vos statistiques. Vous pouvez �galement poursuivre l'histoire pour r�cup�rer vos disciplines vampiriques.";
        ClanGainedModale.UpdateUI();
        ClanGainedModale.OpenWindow();
    }

    public Power getPowerByName(string name)
    {
        foreach(Power power in VampireVariables.current.availablePowers)
        {
            if(power.powerName == name)
            {
                return power;
            }
        }
        return null;
    }

    public void addNewDiscipline(string power)
    {
        string description = "";
        Power localPower = null;
        switch (power)
        {
            case "Grace feline":
                if (VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Celerite)] < 1)
                {
                    VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Celerite)] = 1;
                }
                localPower = getPowerByName("Gr�ce f�line");
                VampireVariables.current.actualPowers.Add(localPower);
                description = localPower.powerName;
                description += " \n";
                description += localPower.powerDescription;
                break;
            case "Reflexe eclaire":
                if (VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Celerite)] < 1)
                {
                    VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Celerite)] = 1;
                }
                localPower = getPowerByName("R�flexe �clair");
                VampireVariables.current.actualPowers.Add(localPower);
                description = localPower.powerName;
                description += " \n";
                description += localPower.powerDescription;
                break;
            case "Bond surhumain":
                if (VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Puissance)] < 1)
                {
                    VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Puissance)] = 1;
                }
                localPower = getPowerByName("Bond Surhumain");
                VampireVariables.current.actualPowers.Add(localPower);
                description = localPower.powerName;
                description += " \n";
                description += localPower.powerDescription;
                break;
            case "Corps lethal":
                if (VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Puissance)] < 1)
                {
                    VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Puissance)] = 1;
                }
                localPower = getPowerByName("Corps L�tal");
                VampireVariables.current.actualPowers.Add(localPower);
                description = localPower.powerName;
                description += " \n";
                description += localPower.powerDescription;
                break;
            case "Intimider":
                if (VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Presence)] < 1)
                {
                    VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Presence)] = 1;
                }
                localPower = getPowerByName("Intimider");
                VampireVariables.current.actualPowers.Add(localPower);
                description = localPower.powerName;
                description += " \n";
                description += localPower.powerDescription;
                break;
            case "Reverence":
                if (VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Presence)] < 1)
                {
                    VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Presence)] = 1;
                }
                localPower = getPowerByName("R�v�rence");
                VampireVariables.current.actualPowers.Add(localPower);
                description = localPower.powerName;
                description += " \n";
                description += localPower.powerDescription;
                break;
            case "Regard Abyssal":
                if (VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Abyssale)] < 1)
                {
                    VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Abyssale)] = 1;
                }
                localPower = getPowerByName("Regard Abyssal");
                VampireVariables.current.actualPowers.Add(localPower);
                description = localPower.powerName;
                description += " \n";
                description += localPower.powerDescription;
                break;
            case "Manteau Tenebreux":
                if (VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Abyssale)] < 1)
                {
                    VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Abyssale)] = 1;
                }
                localPower = getPowerByName("Manteau T�n�breux");
                VampireVariables.current.actualPowers.Add(localPower);
                description = localPower.powerName;
                description += " \n";
                description += localPower.powerDescription;
                break;
            case "Lier le famulus":
                if (VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Animalisme)] < 1)
                {
                    VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Animalisme)] = 1;
                }
                localPower = getPowerByName("Lier le famulus");
                VampireVariables.current.actualPowers.Add(localPower);
                description = localPower.powerName;
                description += " \n";
                description += localPower.powerDescription;
                break;
            case "Sens accrus":
                if (VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Auspex)] < 1)
                {
                    VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Auspex)] = 1;
                }
                localPower = getPowerByName("Sens accrus");
                VampireVariables.current.actualPowers.Add(localPower);
                description = localPower.powerName;
                description += " \n";
                description += localPower.powerDescription;
                break;
            case "Sentir linvisible":
                if (VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Auspex)] < 1)
                {
                    VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Auspex)] = 1;
                }
                localPower = getPowerByName("Sentir l'invisible");
                VampireVariables.current.actualPowers.Add(localPower);
                description = localPower.powerName;
                description += " \n";
                description += localPower.powerDescription;
                break;
            case "Brouillage memoriel":
                if (VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Domination)] < 1)
                {
                    VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Domination)] = 1;
                }
                localPower = getPowerByName("Brouillage m�moriel");
                VampireVariables.current.actualPowers.Add(localPower);
                description = localPower.powerName;
                description += " \n";
                description += localPower.powerDescription;
                break;
            case "Contrainte":
                if (VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Domination)] < 1)
                {
                    VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Domination)] = 1;
                }
                localPower = getPowerByName("Contrainte");
                VampireVariables.current.actualPowers.Add(localPower);
                description = localPower.powerName;
                description += " \n";
                description += localPower.powerDescription;
                break;
            case "Esprit resolu":
                if (VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.ForceDame)] < 1)
                {
                    VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.ForceDame)] = 1;
                }
                localPower = getPowerByName("Esprit R�solu");
                VampireVariables.current.actualPowers.Add(localPower);
                description = localPower.powerName;
                description += " \n";
                description += localPower.powerDescription;
                break;
            case "Resilience":
                if (VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.ForceDame)] < 1)
                {
                    VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.ForceDame)] = 1;
                }
                localPower = getPowerByName("R�silience");
                VampireVariables.current.actualPowers.Add(localPower);
                description = localPower.powerName;
                description += " \n";
                description += localPower.powerDescription;
                break;
            case "Manteau dombre":
                if (VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Occultation)] < 1)
                {
                    VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Occultation)] = 1;
                }
                localPower = getPowerByName("Manteau d�Ombre");
                VampireVariables.current.actualPowers.Add(localPower);
                description = localPower.powerName;
                description += " \n";
                description += localPower.powerDescription;
                break;
            case "Silence de la mort":
                if (VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Occultation)] < 1)
                {
                    VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Occultation)] = 1;
                }
                localPower = getPowerByName("Silence de la mort");
                VampireVariables.current.actualPowers.Add(localPower);
                description = localPower.powerName;
                description += " \n";
                description += localPower.powerDescription;
                break;
            case "Poid plume":
                if (VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Proteism)] < 1)
                {
                    VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Proteism)] = 1;
                }
                localPower = getPowerByName("Poids plume");
                VampireVariables.current.actualPowers.Add(localPower);
                description = localPower.powerName;
                description += " \n";
                description += localPower.powerDescription;
                break;
            case "Yeux de la bete":
                if (VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Proteism)] < 1)
                {
                    VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Proteism)] = 1;
                }
                localPower = getPowerByName("Yeux de la b�te");
                VampireVariables.current.actualPowers.Add(localPower);
                description = localPower.powerName;
                description += " \n";
                description += localPower.powerDescription;
                break;
            case "Gout du sang":
                if (VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.SorcellerieDuSang)] < 1)
                {
                    VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.SorcellerieDuSang)] = 1;
                }
                localPower = getPowerByName("Go�t du sang");
                VampireVariables.current.actualPowers.Add(localPower);
                description = localPower.powerName;
                description += " \n";
                description += localPower.powerDescription;
                break;
            case "Vitae Corrosive":
                if (VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.SorcellerieDuSang)] < 1)
                {
                    VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.SorcellerieDuSang)] = 1;
                }
                localPower = getPowerByName("Vitae corrosive");
                VampireVariables.current.actualPowers.Add(localPower);
                description = localPower.powerName;
                description += " \n";
                description += localPower.powerDescription;
                break;
            case "Rapidite":
                if (VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Celerite)] < 2)
                {
                    VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Celerite)] = 2;
                }
                localPower = getPowerByName("Rapidit�");
                VampireVariables.current.actualPowers.Add(localPower);
                description = localPower.powerName;
                description += " \n";
                description += localPower.powerDescription;
                break;
            case "Prouesse":
                if (VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Puissance)] < 2)
                {
                    VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Puissance)] = 2;
                }
                localPower = getPowerByName("Prouesse");
                VampireVariables.current.actualPowers.Add(localPower);
                description = localPower.powerName;
                description += " \n";
                description += localPower.powerDescription;
                break;
            case "Baiser persistant":
                if (VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Presence)] < 2)
                {
                    VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Presence)] = 2;
                }
                localPower = getPowerByName("Baiser Persistant");
                VampireVariables.current.actualPowers.Add(localPower);
                description = localPower.powerName;
                description += " \n";
                description += localPower.powerDescription;
                break;
            case "Manipulation dombre":
                if (VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Abyssale)] < 2)
                {
                    VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Abyssale)] = 2;
                }
                localPower = getPowerByName("Invocation d'ombre");
                VampireVariables.current.actualPowers.Add(localPower);
                description = localPower.powerName;
                description += " \n";
                description += localPower.powerDescription;
                break;
            case "Bras darhiman":
                if (VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Abyssale)] < 2)
                {
                    VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Abyssale)] = 2;
                }
                localPower = getPowerByName("Bras d'Ahriman");
                VampireVariables.current.actualPowers.Add(localPower);
                description = localPower.powerName;
                description += " \n";
                description += localPower.powerDescription;
                break;
            case "Murmure sauvage":
                if (VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Animalisme)] < 2)
                {
                    VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Animalisme)] = 2;
                }
                localPower = getPowerByName("Murmures Sauvages");
                VampireVariables.current.actualPowers.Add(localPower);
                description = localPower.powerName;
                description += " \n";
                description += localPower.powerDescription;
                break;
            case "Premonition":
                if (VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Auspex)] < 2)
                {
                    VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Auspex)] = 2;
                }
                localPower = getPowerByName("Premonition");
                VampireVariables.current.actualPowers.Add(localPower);
                description = localPower.powerName;
                description += " \n";
                description += localPower.powerDescription;
                break;
            case "Hypnose":
                if (VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Domination)] < 2)
                {
                    VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Domination)] = 2;
                }
                localPower = getPowerByName("Hypnose");
                VampireVariables.current.actualPowers.Add(localPower);
                description = localPower.powerName;
                description += " \n";
                description += localPower.powerDescription;
                break;
            case "Resistance":
                if (VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.ForceDame)] < 2)
                {
                    VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.ForceDame)] = 2;
                }
                localPower = getPowerByName("R�sistance");
                VampireVariables.current.actualPowers.Add(localPower);
                description = localPower.powerName;
                description += " \n";
                description += localPower.powerDescription;
                break;
            case "Bete endurante":
                if (VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.ForceDame)] < 2)
                {
                    VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.ForceDame)] = 2;
                }
                localPower = getPowerByName("B�tes endurante");
                VampireVariables.current.actualPowers.Add(localPower);
                description = localPower.powerName;
                description += " \n";
                description += localPower.powerDescription;
                break;
            case "Passage invisible":
                if (VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Occultation)] < 2)
                {
                    VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Occultation)] = 2;
                }
                localPower = getPowerByName("Passage Invisible");
                VampireVariables.current.actualPowers.Add(localPower);
                description = localPower.powerName;
                description += " \n";
                description += localPower.powerDescription;
                break;
            case "Chimie":
                if (VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Occultation)] < 2)
                {
                    VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Occultation)] = 2;
                }
                localPower = getPowerByName("Chimie");
                VampireVariables.current.actualPowers.Add(localPower);
                description = localPower.powerName;
                description += " \n";
                description += localPower.powerDescription;
                break;
            case "Armes bestiales":
                if (VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Proteism)] < 2)
                {
                    VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Proteism)] = 2;
                }
                localPower = getPowerByName("Armes bestiales");
                VampireVariables.current.actualPowers.Add(localPower);
                description = localPower.powerName;
                description += " \n";
                description += localPower.powerDescription;
                break;
            case "Vicissitudes":
                if (VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Proteism)] < 2)
                {
                    VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.Proteism)] = 2;
                }
                localPower = getPowerByName("Vicissitude");
                VampireVariables.current.actualPowers.Add(localPower);
                description = localPower.powerName;
                description += " \n";
                description += localPower.powerDescription;
                break;
            case "Eteindre la vitae":
                if (VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.SorcellerieDuSang)] < 2)
                {
                    VampireVariables.current.actualDisciplinesAndLevel[nameof(Disciplines.SorcellerieDuSang)] = 2;
                }
                localPower = getPowerByName("Eteindre la vitae");
                VampireVariables.current.actualPowers.Add(localPower);
                description = localPower.powerName;
                description += " \n";
                description += localPower.powerDescription;
                break;
            default:
                break;
        }
        NotificationManager.title = "Pouvoir Vampirique";
        NotificationManager.description = description;
        NotificationManager.UpdateUI();
        if (description == "")
        {
            NotificationManager.description = "An error occured ginging you your new power, the json is wrong";
            NotificationManager.UpdateUI();
        }
        NotificationManager.OpenNotification();
    }

    public void addNewClan(string clan)
    {
        VampireVariables.current.resetClan();
        VampireVariables.current.resetDiscipline();
        switch (clan)
        {
            case nameof(Clans.Brujah):
                VampireVariables.current.isBrujah = true;
                break;
            case nameof(Clans.Gangrel):
                VampireVariables.current.isGangrel = true;
                break;
            case nameof(Clans.Lassombra):
                VampireVariables.current.isLassombra = true;
                break;
            case nameof(Clans.Malkavian):
                VampireVariables.current.isMalkavian = true;
                break;
            case nameof(Clans.Nosferatu):
                VampireVariables.current.isNosferatu = true;
                break;
            case nameof(Clans.Toreador):
                VampireVariables.current.isToreador = true;
                break;
            case nameof(Clans.Tremere):
                VampireVariables.current.isTremere = true;
                break;
            case nameof(Clans.Tzimisce):
                VampireVariables.current.isTzimisce = true;
                break;
            case nameof(Clans.Ventrue):
                VampireVariables.current.isVentrue = true;
                break;
            default:
                VampireVariables.current.isVentrue = true;
                break;
        }
        setClanModale(clan);
    }

    public void EndStories()
    {
        //TODO call the end menu and algorithm to print the character sheet.
       // ButtonEndButton.onClick.Invoke();

        // ComputeSheet();
    }

    //On click on an answer
    public void NextStory(GameObject go)
    {
        Answer answer = go.GetComponent<AnswerButton>().answer;
        //Reset the list of the answer description used (The one the player can read cause of his past choices)
        avaiblableAnswerDescriptions = getAvailableAnswerDescription(answer.answerDescriptions);

        vampireVariables.AddAnswerNumber(answer);
        vampireVariables.AddAnswerPlayerPoints(answer);

        vampireVariables.AddAnswerDescriptionsNumbers(avaiblableAnswerDescriptions);
        if(answer.clan != "")
        {
            addNewClan(answer.clan);
        }
        if (answer.discipline != "")
        {
            addNewDiscipline(answer.discipline);
        }
        previousStory = actualStory;
        actualStory = getNextStory(answer.nextStory);
        if(actualStory == null)
        {
            EndStories();
        } else
        {
            //Reset the list of the story description used (The one the player can read cause of his past choices)
            avaiblableStoryDescriptions = getAvailableStoryDescriptions(actualStory.storyDescriptions);
            vampireVariables.AddStoryDescriptionsNumbers(avaiblableStoryDescriptions);
            switchWindow();
        }
    }

    public List<Answer> getAvailableAnswer(List<Answer> allAnswers)
    {
        List<Answer> localStoryDescriptions = new List<Answer>();

        foreach (Answer storyDescription in allAnswers)
        {
            if (storyDescription.isPreRequiseListAndAndNotOr) // &&
            {
                if (storyDescription.isPreRequiseNotIn) // !5 && !3 TODO
                {
                    bool hasPlayerAllPrerequis = false;
                    foreach (int preRequis in storyDescription.answerNumberPreRequise)
                    {
                        if (!vampireVariables.ActionsDuJoueur.Contains(preRequis))
                        {
                            hasPlayerAllPrerequis = true;
                        }
                        else
                        {
                            hasPlayerAllPrerequis = false;
                        }
                    }
                    if (storyDescription.answerNumberPreRequise.Count < 1)
                    {
                        hasPlayerAllPrerequis = true;
                    }
                    if (hasPlayerAllPrerequis)
                    {
                        localStoryDescriptions.Add(storyDescription);
                    }
                }
                else // 5 && 3
                {
                    bool hasPlayerAllPrerequis = true;
                    foreach (int preRequis in storyDescription.answerNumberPreRequise)
                    {
                        if (!vampireVariables.ActionsDuJoueur.Contains(preRequis))
                        {
                            hasPlayerAllPrerequis = false;
                        }
                    }
                    if (storyDescription.answerNumberPreRequise.Count < 1)
                    {
                        hasPlayerAllPrerequis = true;
                    }
                    if (hasPlayerAllPrerequis)
                    {
                        localStoryDescriptions.Add(storyDescription);
                    }
                }
            }
            else // ||
            {
                if (storyDescription.isPreRequiseNotIn)  // !5 || !3
                {
                    bool hasPlayerAllPrerequis = false;
                    foreach (int preRequis in storyDescription.answerNumberPreRequise)
                    {
                        if (!vampireVariables.ActionsDuJoueur.Contains(preRequis))
                        {
                            hasPlayerAllPrerequis = true;
                        }
                    }
                    if (storyDescription.answerNumberPreRequise.Count < 1)
                    {
                        hasPlayerAllPrerequis = true;
                    }
                    if (hasPlayerAllPrerequis)
                    {
                        localStoryDescriptions.Add(storyDescription);
                    }
                }
                else // 5 || 3
                {
                    bool hasPlayerAllPrerequis = false;
                    foreach (int preRequis in storyDescription.answerNumberPreRequise)
                    {
                        if (vampireVariables.ActionsDuJoueur.Contains(preRequis))
                        {
                            hasPlayerAllPrerequis = true;
                        }
                    }
                    if (storyDescription.answerNumberPreRequise.Count < 1)
                    {
                        hasPlayerAllPrerequis = true;
                    }
                    if (hasPlayerAllPrerequis)
                    {
                        localStoryDescriptions.Add(storyDescription);
                    }
                }
            }
        }

        return localStoryDescriptions;
    }

    public List<StoryDescription> getAvailableStoryDescriptions(List<StoryDescription> allStoryDescriptions)
    {
        List<StoryDescription> localStoryDescriptions = new List<StoryDescription>();

        foreach(StoryDescription storyDescription in allStoryDescriptions)
        {
            if (storyDescription.isPreRequiseListAndAndNotOr) // &&
            {
                if(storyDescription.isPreRequiseNotIn) // !5 && !3 TODO
                {
                    bool hasPlayerAllPrerequis = false;
                    foreach (int preRequis in storyDescription.answerNumberPreRequise)
                    {
                        if (!vampireVariables.ActionsDuJoueur.Contains(preRequis))
                        {
                            hasPlayerAllPrerequis = true;
                        } else
                        {
                            hasPlayerAllPrerequis = false;
                        }
                    }
                    if (storyDescription.answerNumberPreRequise.Count < 1)
                    {
                        hasPlayerAllPrerequis = true;
                    }
                    if (hasPlayerAllPrerequis)
                    {
                        localStoryDescriptions.Add(storyDescription);
                    }
                }
                else // 5 && 3
                {
                    bool hasPlayerAllPrerequis = true;
                    foreach (int preRequis in storyDescription.answerNumberPreRequise)
                    {
                        if (!vampireVariables.ActionsDuJoueur.Contains(preRequis))
                        {
                            hasPlayerAllPrerequis = false;
                        }
                    }
                    if (storyDescription.answerNumberPreRequise.Count < 1)
                    {
                        hasPlayerAllPrerequis = true;
                    }
                    if (hasPlayerAllPrerequis)
                    {
                        localStoryDescriptions.Add(storyDescription);
                    }
                }
            }
            else // ||
            {
                if (storyDescription.isPreRequiseNotIn)  // !5 || !3
                {
                    bool hasPlayerAllPrerequis = false;
                    foreach (int preRequis in storyDescription.answerNumberPreRequise)
                    {
                        if (!vampireVariables.ActionsDuJoueur.Contains(preRequis))
                        {
                            hasPlayerAllPrerequis = true;
                        }
                    }
                    if (storyDescription.answerNumberPreRequise.Count < 1)
                    {
                        hasPlayerAllPrerequis = true;
                    }
                    if (hasPlayerAllPrerequis)
                    {
                        localStoryDescriptions.Add(storyDescription);
                    }
                }
                else // 5 || 3
                {
                    bool hasPlayerAllPrerequis = false;
                    foreach (int preRequis in storyDescription.answerNumberPreRequise)
                    {
                        if (vampireVariables.ActionsDuJoueur.Contains(preRequis))
                        {
                            hasPlayerAllPrerequis = true;
                        }
                    }
                    if (storyDescription.answerNumberPreRequise.Count < 1)
                    {
                        hasPlayerAllPrerequis = true;
                    }
                    if (hasPlayerAllPrerequis)
                    {
                        localStoryDescriptions.Add(storyDescription);
                    }
                }
            }
        }

        return localStoryDescriptions;
    }

    public List<AnswerDescription> getAvailableAnswerDescription(List<AnswerDescription> allAnswerDescriptions)
    {
        List<AnswerDescription> localStoryDescriptions = new List<AnswerDescription>();

        foreach (AnswerDescription storyDescription in allAnswerDescriptions)
        {
            if (storyDescription.isPreRequiseListAndAndNotOr) // &&
            {
                if (storyDescription.isPreRequiseNotIn) // !5 && !3 TODO
                {
                    bool hasPlayerAllPrerequis = false;
                    foreach (int preRequis in storyDescription.answerNumberPreRequise)
                    {
                        if (!vampireVariables.ActionsDuJoueur.Contains(preRequis))
                        {
                            hasPlayerAllPrerequis = true;
                        }
                        else
                        {
                            hasPlayerAllPrerequis = false;
                        }
                    }
                    if (storyDescription.answerNumberPreRequise.Count < 1)
                    {
                        hasPlayerAllPrerequis = true;
                    }
                    if (hasPlayerAllPrerequis)
                    {
                        localStoryDescriptions.Add(storyDescription);
                    }
                }
                else // 5 && 3
                {
                    bool hasPlayerAllPrerequis = true;
                    foreach (int preRequis in storyDescription.answerNumberPreRequise)
                    {
                        if (!vampireVariables.ActionsDuJoueur.Contains(preRequis))
                        {
                            hasPlayerAllPrerequis = false;
                        }
                    }
                    if (storyDescription.answerNumberPreRequise.Count < 1)
                    {
                        hasPlayerAllPrerequis = true;
                    }
                    if (hasPlayerAllPrerequis)
                    {
                        localStoryDescriptions.Add(storyDescription);
                    }
                }
            }
            else // ||
            {
                if (storyDescription.isPreRequiseNotIn)  // !5 || !3
                {
                    bool hasPlayerAllPrerequis = false;
                    foreach (int preRequis in storyDescription.answerNumberPreRequise)
                    {
                        if (!vampireVariables.ActionsDuJoueur.Contains(preRequis))
                        {
                            hasPlayerAllPrerequis = true;
                        }
                    }
                    if (storyDescription.answerNumberPreRequise.Count < 1)
                    {
                        hasPlayerAllPrerequis = true;
                    }
                    if (hasPlayerAllPrerequis)
                    {   
                        localStoryDescriptions.Add(storyDescription);
                    }
                }
                else // 5 || 3
                {
                    bool hasPlayerAllPrerequis = false;
                    foreach (int preRequis in storyDescription.answerNumberPreRequise)
                    {
                        if (vampireVariables.ActionsDuJoueur.Contains(preRequis))
                        {
                            hasPlayerAllPrerequis = true;
                        }
                    }
                    if (storyDescription.answerNumberPreRequise.Count < 1)
                    {
                        hasPlayerAllPrerequis = true;
                    }
                    if (hasPlayerAllPrerequis)
                    {
                        localStoryDescriptions.Add(storyDescription);
                    }
                }
            }
        }

        return localStoryDescriptions;
    }

    public void switchWindow(bool isStart = false)
    {
        if (fillWindow != null)
        {
            StopCoroutine(fillWindow);
        }
        if (isWindow1)
        {
            ButtonWindow2Button.onClick.Invoke();
            fillWindow = FillWindow(
            SlowTypingWindow2MainText,
            SlowTypingWindow2Option1,
            SlowTypingWindow2Option2,
            SlowTypingWindow2Option3,
            SlowTypingWindow2Option4,
            SlowTypingWindow2Option5,
            SlowTypingWindow2Option6);
            StartCoroutine(fillWindow);
        }
        else
        {
            ButtonWindow1Button.onClick.Invoke();
            fillWindow = FillWindow(
                SlowTypingWindow1MainText,
                SlowTypingWindow1Option1,
                SlowTypingWindow1Option2,
                SlowTypingWindow1Option3,
                SlowTypingWindow1Option4,
                SlowTypingWindow1Option5,
                SlowTypingWindow1Option6);
            StartCoroutine(fillWindow);
        }
        isWindow1 = !isWindow1;
    }

    public string getMainText()
    {
        string text = "";
        foreach (AnswerDescription answerDescription in avaiblableAnswerDescriptions)
        {
            text = text + answerDescription.description + " ";
        }

        if(text != "")
            text = text +  "\n";

        foreach (StoryDescription storyDescription in avaiblableStoryDescriptions)
        {
            text = text + storyDescription.description + " ";
        }

        return text;
    }
    public void deactivateAllAnswersSlots()
    {
            SlowTypingWindow1MainText.gameObject.SetActive(false);
            SlowTypingWindow1Option1.transform.parent.gameObject.SetActive(false);
            SlowTypingWindow1Option2.transform.parent.gameObject.SetActive(false);
            SlowTypingWindow1Option3.transform.parent.gameObject.SetActive(false);
            SlowTypingWindow1Option4.transform.parent.gameObject.SetActive(false);
            SlowTypingWindow1Option5.transform.parent.gameObject.SetActive(false);
            SlowTypingWindow1Option6.transform.parent.gameObject.SetActive(false);
            SlowTypingWindow2MainText.gameObject.SetActive(false);
            SlowTypingWindow2Option1.transform.parent.gameObject.SetActive(false);
            SlowTypingWindow2Option2.transform.parent.gameObject.SetActive(false);
            SlowTypingWindow2Option3.transform.parent.gameObject.SetActive(false);
            SlowTypingWindow2Option4.transform.parent.gameObject.SetActive(false);
            SlowTypingWindow2Option5.transform.parent.gameObject.SetActive(false);
            SlowTypingWindow2Option6.transform.parent.gameObject.SetActive(false);
    }
    public void deactivateCurrentAnswersSlots()
    {
        if (!isWindow1)
        {
            SlowTypingWindow1MainText.gameObject.SetActive(false);
            SlowTypingWindow1Option1.transform.parent.gameObject.SetActive(false);
            SlowTypingWindow1Option2.transform.parent.gameObject.SetActive(false);
            SlowTypingWindow1Option3.transform.parent.gameObject.SetActive(false);
            SlowTypingWindow1Option4.transform.parent.gameObject.SetActive(false);
            SlowTypingWindow1Option5.transform.parent.gameObject.SetActive(false);
            SlowTypingWindow1Option6.transform.parent.gameObject.SetActive(false);
        } else
        {
            SlowTypingWindow2MainText.gameObject.SetActive(false);
            SlowTypingWindow2Option1.transform.parent.gameObject.SetActive(false);
            SlowTypingWindow2Option2.transform.parent.gameObject.SetActive(false);
            SlowTypingWindow2Option3.transform.parent.gameObject.SetActive(false);
            SlowTypingWindow2Option4.transform.parent.gameObject.SetActive(false);
            SlowTypingWindow2Option5.transform.parent.gameObject.SetActive(false);
            SlowTypingWindow2Option6.transform.parent.gameObject.SetActive(false);
        }
    }

    public IEnumerator FillWindow(SlowTyping main, SlowTyping one, SlowTyping two, SlowTyping three, SlowTyping four, SlowTyping five, SlowTyping six)
    {
        deactivateCurrentAnswersSlots();

        yield return new WaitForSeconds(.5f);

        main.gameObject.SetActive(true);
        yield return(StartCoroutine(main.playText(getMainText())));

        int i = 0;
        foreach(Answer answer in getAvailableAnswer(actualStory.answers))
        {
            switch(i)
            {
                case 0:
                   one.transform.parent.gameObject.SetActive(true);
                    one.GetComponent<AnswerButton>().answer = answer;
                    yield return StartCoroutine(one.playText(answer.description));
                    break;
                case 1:
                    two.transform.parent.gameObject.SetActive(true);
                    two.GetComponent<AnswerButton>().answer = answer;
                    yield return StartCoroutine(two.playText(answer.description));
                    break;
                case 2:
                    three.transform.parent.gameObject.SetActive(true);
                    three.GetComponent<AnswerButton>().answer = answer;
                    yield return StartCoroutine(three.playText(answer.description));
                    break;
                case 3:
                    four.transform.parent.gameObject.SetActive(true);
                    four.GetComponent<AnswerButton>().answer = answer;
                    yield return StartCoroutine(four.playText(answer.description));
                    break;
                case 4:
                    five.transform.parent.gameObject.SetActive(true);
                    five.GetComponent<AnswerButton>().answer = answer;
                    yield return StartCoroutine(five.playText(answer.description));
                    break;
                case 5:
                    six.transform.parent.gameObject.SetActive(true);
                    six.GetComponent<AnswerButton>().answer = answer;
                    yield return StartCoroutine(six.playText(answer.description));
                    break;
                default:
                    break;
            }
            i++;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
