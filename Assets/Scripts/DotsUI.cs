using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DotsUI : MonoBehaviour
{
    public List<Toggle> toggles;

    private void Start()
    {
        foreach(Toggle toggle in gameObject.GetComponentsInChildren<Toggle>())
        {
            toggles.Add(toggle);
        }
    }

    public void activateToggles(int value)
    {
        deactivateAllToggles();
        Debug.Log("Activate toggle with value " + value);
        foreach (Toggle toggle in toggles)
        {
            if(value >= 1)
            {
                toggle.isOn = true;
            }
            value -= 1;
        }
    }

    public void deactivateAllToggles()
    {
        foreach(Toggle toggle in toggles)
        {
            toggle.isOn = false;
        }
    }
}
