using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[CreateAssetMenu(fileName = "DataPower", menuName = "ScriptableObjects/VampirePower", order = 1)]
public class Power : ScriptableObject
{
    [Header("The discipline of the power")]
    public Disciplines powerDiscipline;
    [Header("The power rank going from 1 to 5")]
    public PowerRank powerRank;
    [Header("The name of the power")]
    public string powerName;
    [Header("The description of the power")]
    public string powerDescription;
    [Header("Is exhaltation roll required on use")]
    public bool isExhaltation = false;
    public bool isDoubleExhaltation = false;

    [Header("If another discipline is needed at some point to use it")]
    public bool isComposedPower = false;
    public Disciplines requiredDiscipline;
    public PowerRank requiredDisciplineRank;
}