using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetStatsFromStory : MonoBehaviour
{
    public List<string> usedWordsThisStory = new List<string>(); //Can only get one same stat per story so we do not want to comptabilise when there is lets say 3 time manipulation in answers of the same story

    [Header("Attributs")]
    public int Force = 0;
    public int Charisme = 0;
    public int Intelligence = 0;
    public int Dexterite = 0;
    public int Manipulation = 0;
    public int Astuce = 0;
    public int Vigueur = 0;
    public int SangFroid = 0;
    public int Resolution = 0;
    [Header("Specialite")]
    public int Athletisme = 0;
    public int Bagarre = 0;
    public int Artisanat = 0;
    public int Conduite = 0;
    public int ArmesAfeu = 0;
    public int Melee = 0;
    public int Larcin = 0;
    public int Furtivite = 0;
    public int Survie = 0;
    public int Animaux = 0;
    public int Etiquette = 0;
    public int Empathie = 0;
    public int Intimidation = 0;
    public int Commandement = 0;
    public int Representation = 0;
    public int Persuasion = 0;
    public int ExperienceDeLaRue = 0;
    public int Subterfuge = 0;
    public int Erudition = 0;
    public int Vigilance = 0;
    public int Finances = 0;
    public int Investigation = 0;
    public int Medecine = 0;
    public int Occultisme = 0;
    public int Politique = 0;
    public int Science = 0;
    public int Technologie = 0;

    // Start is called before the first frame update
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.H))
        {
            resetStats();
            getStats(VampireUIManager.current.stories);
        }
    }

    public void resetStats()
    {

         Force = 0;
     Charisme = 0;
     Intelligence = 0;
     Dexterite = 0;
     Manipulation = 0;
     Astuce = 0;
     Vigueur = 0;
     SangFroid = 0;
     Resolution = 0;
     Athletisme = 0;
     Bagarre = 0;
     Artisanat = 0;
     Conduite = 0;
     ArmesAfeu = 0;
     Melee = 0;
     Larcin = 0;
     Furtivite = 0;
     Survie = 0;
     Animaux = 0;
     Etiquette = 0;
     Empathie = 0;
     Intimidation = 0;
     Commandement = 0;
     Representation = 0;
     Persuasion = 0;
     ExperienceDeLaRue = 0;
     Subterfuge = 0;
     Erudition = 0;
     Vigilance = 0;
     Finances = 0;
     Investigation = 0;
     Medecine = 0;
     Occultisme = 0;
     Politique = 0;
     Science = 0;
     Technologie = 0;
}

    public void getStats(Stories stories)
    {
        
        foreach(Story story in stories.listStory)
        {
            usedWordsThisStory = new List<string>();
            foreach (Answer answer in story.answers)
            {
                if(answer.attributsBoost != null && answer.attributsBoost.Count > 0)
                {
                    AddAttributePoint(answer.attributsBoost);
                }
                if (answer.specialiteBoost != null && answer.specialiteBoost.Count > 0)
                {
                    AddSpecialitePoint(answer.specialiteBoost);
                }
            }
        }
    }


    public void AddAttributePoint(List<string> attributs)
    {
        foreach (string attribut in attributs)
        {
            if (!usedWordsThisStory.Contains(attribut))
            {
                usedWordsThisStory.Add(attribut);
                switch (attribut)
                {
                    case nameof(Attributs.Astuce):
                        Astuce += 1;
                        break;
                    case nameof(Attributs.Charisme):
                        Charisme += 1;
                        break;
                    case nameof(Attributs.Dexterite):
                        Dexterite += 1;
                        break;
                    case nameof(Attributs.Force):
                        Force += 1;
                        break;
                    case nameof(Attributs.Intelligence):
                        Intelligence += 1;
                        break;
                    case nameof(Attributs.Manipulation):
                        Manipulation += 1;
                        break;
                    case nameof(Attributs.Resolution):
                        Resolution += 1;
                        break;
                    case nameof(Attributs.SangFroid):
                        SangFroid += 1;
                        break;
                    case nameof(Attributs.Vigueur):
                        Vigueur += 1;
                        break;
                    default:
                        break;
                }
            }
        }
    }

    public void AddSpecialitePoint(List<string> specialites)
    {
        foreach (string specialite in specialites)
        {
            if (!usedWordsThisStory.Contains(specialite))
            {
                usedWordsThisStory.Add(specialite);
                switch (specialite)
                {
                    case nameof(Specialite.Animaux):
                        Animaux += 1;
                        break;
                    case nameof(Specialite.ArmesAfeu):
                        ArmesAfeu += 1;
                        break;
                    case nameof(Specialite.Artisanat):
                        Artisanat += 1;
                        break;
                    case nameof(Specialite.Athletisme):
                        Athletisme += 1;
                        break;
                    case nameof(Specialite.Bagarre):
                        Bagarre += 1;
                        break;
                    case nameof(Specialite.Commandement):
                        Commandement += 1;
                        break;
                    case nameof(Specialite.Conduite):
                        Conduite += 1;
                        break;
                    case nameof(Specialite.Empathie):
                        Empathie += 1;
                        break;
                    case nameof(Specialite.Erudition):
                        Erudition += 1;
                        break;
                    case nameof(Specialite.Etiquette):
                        Etiquette += 1;
                        break;
                    case nameof(Specialite.ExperienceDeLaRue):
                        ExperienceDeLaRue += 1;
                        break;
                    case nameof(Specialite.Finances):
                        Finances += 1;
                        break;
                    case nameof(Specialite.Furtivite):
                        Furtivite += 1;
                        break;
                    case nameof(Specialite.Intimidation):
                        Intimidation += 1;
                        break;
                    case nameof(Specialite.Investigation):
                        Investigation += 1;
                        break;
                    case nameof(Specialite.Larcin):
                        Larcin += 1;
                        break;
                    case nameof(Specialite.Medecine):
                        Medecine += 1;
                        break;
                    case nameof(Specialite.Melee):
                        Melee += 1;
                        break;
                    case nameof(Specialite.Occultisme):
                        Occultisme += 1;
                        break;
                    case nameof(Specialite.Persuasion):
                        Persuasion += 1;
                        break;
                    case nameof(Specialite.Politique):
                        Politique += 1;
                        break;
                    case nameof(Specialite.Representation):
                        Representation += 1;
                        break;
                    case nameof(Specialite.Science):
                        Science += 1;
                        break;
                    case nameof(Specialite.Subterfuge):
                        Subterfuge += 1;
                        break;
                    case nameof(Specialite.Survie):
                        Survie += 1;
                        break;
                    case nameof(Specialite.Technologie):
                        Technologie += 1;
                        break;
                    case nameof(Specialite.Vigilance):
                        Vigilance += 1;
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
