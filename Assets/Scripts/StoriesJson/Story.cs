using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Story 
{
    public int storyNumber = 0;
    public List<Answer> answers = new List<Answer>();
    public List<StoryDescription> storyDescriptions = new List<StoryDescription>();
}
