using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Serializationtest : MonoBehaviour
{
    void Update()
    {
        if(Input.GetKeyDown("space"))
        {
            SerializeTest();
        }
    }

    public void SerializeTest()
    {
        ////////////////////////////////////////// Story 1
        //On creer les stories
        Stories stories = new Stories();

        Story story1 = new Story();
        story1.storyNumber = 1;

        //On ajoute la description (Le main text qui viendra apr�s la partie answer description choisie au dernier round)
        StoryDescription story1Description1 = new StoryDescription();
        story1Description1.answerNumberPreRequise = new List<int>();
        story1Description1.description = "Vous vous r�veillez apr�s une bonne nuit de sommeil � l'h�tel de la gare. Vous �tes en voyage d�affaire � Montpellier et le soleil brille � l'ext�rieur. Vous allez vous pr�parer dans la salle de bain et vous vous plantez devant un miroir. Comme � votre habitude";
        story1.storyDescriptions.Add(story1Description1);


        Answer story1answer1 = new Answer();
        Answer story1answer2 = new Answer();
        Answer story1answer3 = new Answer();
        story1answer1.attributsBoost = new List<string> ();
        story1answer2.attributsBoost = new List<string> { nameof(Attributs.Charisme) };
        story1answer1.specialiteBoost = new List<string>();
        story1answer2.specialiteBoost = new List<string>();
        story1answer3.specialiteBoost = new List<string> { nameof(Specialite.Technologie) };
        story1answer1.nextStory = 2;
        story1answer2.nextStory = 2;
        story1answer3.nextStory = 29;
        story1answer1.description = "Vous �vitez de vous regarder.";
        story1answer2.description = "Vous prenez le temps de vous inspecter sous chaque couture.";
        story1answer3.description = "DEBUG -> Je me tampone la nouille d'avoir mes stats de personnage, je veux juste mon clan et mes disciplines";

        AnswerDescription story1answer1Description1 = new AnswerDescription();
        AnswerDescription story1answer2Description1 = new AnswerDescription();
        AnswerDescription story1answer3Description1 = new AnswerDescription();

        story1answer1Description1.answerDescriptionNumber = 1;
        story1answer2Description1.answerDescriptionNumber = 2;

        story1answer1Description1.answerNumberPreRequise = new List<int>();
        story1answer2Description1.answerNumberPreRequise = new List<int>();

        story1answer1Description1.description = "De toute fa�on vous savez bien � quoi vous ressemblez, vous �tes tr�s commun.";
        story1answer2Description1.description = "Et vous vous sentez bien log� et savez prendre soin de vous m�me.";
        story1answer3Description1.description = "";

        story1answer1.answerDescriptions.Add(story1answer1Description1);
        story1answer2.answerDescriptions.Add(story1answer2Description1);
        story1answer3.answerDescriptions.Add(story1answer3Description1);

        story1.answers.Add(story1answer1);
        story1.answers.Add(story1answer2);
        story1.answers.Add(story1answer3);

        stories.listStory.Add(story1);

        ////////////////////////////////////////// Story 2
        Story story2 = new Story();
        story2.storyNumber = 2;

        StoryDescription story2Description1 = new StoryDescription();
        story2Description1.answerNumberPreRequise = new List<int>();
        story2Description1.description = "Rapidement on toque � la porte, la femme de chambre vous interromp dans votre rituel matinal pour vous annoncer qu�il faut rendre la chambre. Mais vous n��tes pas tout � fait pr�t";
        story2.storyDescriptions.Add(story2Description1);

        Answer story2answer1 = new Answer();
        Answer story2answer2 = new Answer();
        Answer story2answer3 = new Answer();
        story2answer1.attributsBoost = new List<string> ();
        story2answer2.attributsBoost = new List<string> ();
        story2answer3.attributsBoost = new List<string> ();
        story2answer1.specialiteBoost = new List<string> { nameof(Specialite.Furtivite) };
        story2answer2.specialiteBoost = new List<string> { nameof(Specialite.Empathie) };
        story2answer3.specialiteBoost = new List<string> { nameof(Specialite.Commandement) };
        story2answer1.nextStory = 3;
        story2answer2.nextStory = 3;
        story2answer3.nextStory = 3;
        story2answer1.description = "Vous n�ouvrez pas la porte ! Vous restez discret sans faire de bruit jusqu�� ce qu�elle parte.";
        story2answer2.description = "Vous ouvrez directement la porte pour inspecter la femme de chambre et savoir comment g�rer la situation au mieux.";
        story2answer3.description = "Vous lui ordonnez directement d�entrer de mani�re imp�rieuse.";

        AnswerDescription story2answer1Description1 = new AnswerDescription();
        AnswerDescription story2answer2Description1 = new AnswerDescription();
        AnswerDescription story2answer3Description1 = new AnswerDescription();

        story2answer1Description1.answerDescriptionNumber = 3;
        story2answer2Description1.answerDescriptionNumber = 4;
        story2answer3Description1.answerDescriptionNumber = 5;

        story2answer1Description1.answerNumberPreRequise = new List<int>();
        story2answer2Description1.answerNumberPreRequise = new List<int>();
        story2answer3Description1.answerNumberPreRequise = new List<int>();

        story2answer1Description1.description = "Tel un piquet, vous restez dans un coin de la pi�ce, en attendant qu�elle s�en aille. Mais certaine qu�il n�y a personne dans la pi�ce, elle ouvre la porte doucement. Cela lui prend bien dix secondes avant de vous voir, et choqu�e de vous trouver ici, elle tente de s�excuser.";
        story2answer2Description1.description = "Vous la gaugez calmement. Vous notez qu�elle est mari�e avec une bague au doigt et qu�elle a des chaussettes Batman, preuve qu�elle a des enfants. Un tic nerveux sur son visage montre qu'elle n'est pas � l�aise et � d�but� r�cemment. Vous allez pouvoir utiliser ces informations contre elle.";
        story2answer3Description1.description = "Vous n'alliez pas vous abaisser � ouvrir la porte vous-m�me. La porte s�ouvre sur la femme de chambre qui, avoir crois� votre regard, baisse les yeux en un signe de soumission.";

        story2answer1.answerDescriptions.Add(story2answer1Description1);
        story2answer2.answerDescriptions.Add(story2answer2Description1);
        story2answer3.answerDescriptions.Add(story2answer3Description1);

        story2.answers.Add(story2answer1);
        story2.answers.Add(story2answer2);
        story2.answers.Add(story2answer3);

        stories.listStory.Add(story2);

        ////////////////////////////////////////// Story 3
        Story story3 = new Story();
        story3.storyNumber = 3;

        StoryDescription story3Description1 = new StoryDescription();
        story3Description1.answerNumberPreRequise = new List<int>();
        story3Description1.description = "Rapidement on toque � la porte, la femme de chambre vous interromp dans votre rituel matinal pour vous annoncer qu�il faut rendre la chambre. Mais vous n��tes pas tout � fait pr�t";
        story3.storyDescriptions.Add(story3Description1);

        Answer story3answer1 = new Answer();
        Answer story3answer2 = new Answer();
        Answer story3answer3 = new Answer();
        story3answer1.attributsBoost = new List<string> { nameof(Attributs.Manipulation) };
        story3answer2.attributsBoost = new List<string> { nameof(Attributs.Manipulation) };
        story3answer3.attributsBoost = new List<string> { nameof(Attributs.Manipulation) };
        story3answer1.specialiteBoost = new List<string> { nameof(Specialite.Intimidation) };
        story3answer2.specialiteBoost = new List<string> { nameof(Specialite.Persuasion) };
        story3answer3.specialiteBoost = new List<string> { nameof(Specialite.Subterfuge) };
        story3answer1.nextStory = 4;
        story3answer2.nextStory = 4;
        story3answer3.nextStory = 4;
        story3answer1.description = "Vous l�insultez s�chement pour vous avoir interrompue et lui portez un regard mauvais.";
        story3answer2.description = "Vous faites appel � ses bons sentiments et lui demandez gentiment de vous laisser une demie heure de plus pour vous pr�parer.";
        story3answer3.description = "Vous lui faites croire que vous travaillez pour Michelin, et qu�elle tient entre ses mains sa future promotion. Si elle vous laisse tranquille jusqu�� ce que vous preniez votre petit d�jeuner.";

        AnswerDescription story3answer1Description1 = new AnswerDescription();
        AnswerDescription story3answer2Description1 = new AnswerDescription();
        AnswerDescription story3answer3Description1 = new AnswerDescription();

        story3answer1Description1.answerDescriptionNumber = 6;
        story3answer2Description1.answerDescriptionNumber = 7;
        story3answer3Description1.answerDescriptionNumber = 8;

        story3answer1Description1.answerNumberPreRequise = new List<int>();
        story3answer2Description1.answerNumberPreRequise = new List<int>();
        story3answer3Description1.answerNumberPreRequise = new List<int>();

        story3answer1Description1.description = "Elle se sent absolument minable, et tente de b�gayer une r�ponse� Vous imitez son b�gaiement avant de lui intimer de se casser et fissa si elle ne veut pas une plainte au cul, vous la voyez s�enfuir au d�tour du couloir.";
        story3answer2Description1.description = "Vous faites appel � ses bons sentiments et lui demandez gentiment de vous laisser une demie heure de plus pour vous pr�parer.";
        story3answer3Description1.description = "Vous lui faites croire que vous travaillez pour Michelin, et qu�elle tient entre ses mains sa future promotion. Si elle vous laisse tranquille jusqu�� ce que vous preniez votre petit d�jeuner.";

        story3answer1.answerDescriptions.Add(story3answer1Description1);
        story3answer2.answerDescriptions.Add(story3answer2Description1);
        story3answer3.answerDescriptions.Add(story3answer3Description1);

        story3.answers.Add(story3answer1);
        story3.answers.Add(story3answer2);
        story3.answers.Add(story3answer3);

        stories.listStory.Add(story3);

        ///// Story 4 
        Story story4 = new Story();
        story4.storyNumber = 4;

        StoryDescription story4Description1 = new StoryDescription();
        story4Description1.answerNumberPreRequise = new List<int>();
        story4Description1.description = "Fier de votre tour de force, vous avez r�ussi � glaner 30 minutes suppl�mentaires dans votre chambre et d�cidez alors de terminer votre routine du matin par";
        story4.storyDescriptions.Add(story4Description1);

        Answer story4answer1 = new Answer();
        Answer story4answer2 = new Answer();
        Answer story4answer3 = new Answer();
        Answer story4answer4 = new Answer();
        story4answer1.attributsBoost = new List<string> { nameof(Attributs.Force), nameof(Attributs.Vigueur) };
        story4answer2.attributsBoost = new List<string> { nameof(Attributs.Vigueur), nameof(Attributs.Dexterite) };
        story4answer3.attributsBoost = new List<string> { nameof(Attributs.Intelligence) };
        story4answer4.attributsBoost = new List<string> { nameof(Attributs.Intelligence) };
        story4answer1.specialiteBoost = new List<string> ();
        story4answer2.specialiteBoost = new List<string> { nameof(Specialite.Athletisme) };
        story4answer3.specialiteBoost = new List<string> { nameof(Specialite.Finances), nameof(Specialite.Technologie) };
        story4answer4.specialiteBoost = new List<string> ();
        story4answer1.nextStory = 5;
        story4answer2.nextStory = 7;
        story4answer3.nextStory = 7;
        story4answer4.nextStory = 6;
        story4answer1.description = "Votre entra�nement de crossfit matinal.";
        story4answer2.description = "Une s�ance de fitness bien soutenue.";
        story4answer3.description = "La gestion de vos placements boursiers sur votre Iphone 54 X Pro Gold +.";
        story4answer4.description = "Un chapitre de votre dernier livre en date.";

        AnswerDescription story4answer1Description1 = new AnswerDescription();
        AnswerDescription story4answer2Description1 = new AnswerDescription();
        AnswerDescription story4answer3Description1 = new AnswerDescription();
        AnswerDescription story4answer4Description1 = new AnswerDescription();

        story4answer1Description1.answerDescriptionNumber = 9;
        story4answer2Description1.answerDescriptionNumber = 10;
        story4answer3Description1.answerDescriptionNumber = 11;
        story4answer4Description1.answerDescriptionNumber = 12;

        story4answer1Description1.answerNumberPreRequise = new List<int>();
        story4answer2Description1.answerNumberPreRequise = new List<int>();
        story4answer3Description1.answerNumberPreRequise = new List<int>();
        story4answer4Description1.answerNumberPreRequise = new List<int>();

        story4answer1Description1.description = "Vous commencez par 5 s�ries de 20 squattes, puis encha�nez sur des pompes. Mais rapidement, vous avez envie de cogner.";
        story4answer2Description1.description = "Vous commencez par des �tirements. Puis, vous improvisez un saut � la corde � l�aide des draps de votre lit. Enfin vous tentez une acrobatie avant de vous effondrer sur le lit, satisfait.";
        story4answer3Description1.description = "Vous sortez votre t�l�phone et vous connectez � ElToro. Rapidement, vous achetez 6 actions de chez haribo avant d'�teindre, satisfait par votre nouvelle acquisition.";
        story4answer4Description1.description = "Vous sortez votre livre et soupirez.";

        story4answer1.answerDescriptions.Add(story4answer1Description1);
        story4answer2.answerDescriptions.Add(story4answer2Description1);
        story4answer3.answerDescriptions.Add(story4answer3Description1);
        story4answer4.answerDescriptions.Add(story4answer4Description1);

        story4.answers.Add(story4answer1);
        story4.answers.Add(story4answer2);
        story4.answers.Add(story4answer3);
        story4.answers.Add(story4answer4);

        stories.listStory.Add(story4);


        ///// Story 5 
        Story story5 = new Story();
        story5.storyNumber = 5;

        StoryDescription story5Description1 = new StoryDescription();
        story5Description1.answerNumberPreRequise = new List<int>();
        story5Description1.description = "Vous accrochez donc un oreiller sur le mur et commencez � le bourriner";
        story5.storyDescriptions.Add(story5Description1);

        Answer story5answer1 = new Answer();
        Answer story5answer2 = new Answer();
        Answer story5answer3 = new Answer();
        story5answer1.attributsBoost = new List<string>();
        story5answer2.attributsBoost = new List<string>();
        story5answer3.attributsBoost = new List<string> { nameof(Attributs.Dexterite) };
        story5answer1.specialiteBoost = new List<string> { nameof(Specialite.Bagarre) };
        story5answer2.specialiteBoost = new List<string> { nameof(Specialite.Melee) };
        story5answer2.specialiteBoost = new List<string> { nameof(Specialite.ArmesAfeu) };
        story5answer1.nextStory = 7;
        story5answer2.nextStory = 7;
        story5answer3.nextStory = 7;
        story5answer1.description = "Violemment � main nue.";
        story5answer2.description = "A l�aide de la lampe sur pied qui tr�ne � c�t� du lit.";
        story5answer3.description = "Avec la crosse de votre revolver.";

        AnswerDescription story5answer1Description1 = new AnswerDescription();
        AnswerDescription story5answer2Description1 = new AnswerDescription();
        AnswerDescription story5answer3Description1 = new AnswerDescription();

        story5answer1Description1.answerDescriptionNumber = 16;
        story5answer2Description1.answerDescriptionNumber = 17;
        story5answer3Description1.answerDescriptionNumber = 17;

        story5answer1Description1.answerNumberPreRequise = new List<int>();
        story5answer2Description1.answerNumberPreRequise = new List<int>();
        story5answer3Description1.answerNumberPreRequise = new List<int>();

        story5answer1Description1.description = "Vous fracassez le coussin avec vos poings, et sentez la douleur vous remonter dans les membres. Cela vous fait du bien et vous calme.";
        story5answer2Description1.description = "L�ecume au bord des l�vres vous explosez la lampe contre le coussin et la cassez en deux. L�entendre se briser sur le mur vous fait du bien.";
        story5answer3Description1.description = "Vous tireriez bien dedans mais vous voulez pas reveiller tout l�hotel. Alors vous explosez la lampe contre le coussin et la cassez en deux.";

        story5answer1.answerDescriptions.Add(story5answer1Description1);
        story5answer2.answerDescriptions.Add(story5answer2Description1);
        story5answer3.answerDescriptions.Add(story5answer3Description1);

        story5.answers.Add(story5answer1);
        story5.answers.Add(story5answer2);
        story5.answers.Add(story5answer3);

        stories.listStory.Add(story5);

        /// Story 6
        Story story6 = new Story();
        story6.storyNumber = 6;

        StoryDescription story6Description1 = new StoryDescription();
        story6Description1.answerNumberPreRequise = new List<int>();
        story6Description1.description = "Ce passage est compliqu� � comprendre, mais apr�s tout c�est normal pour";
        story6.storyDescriptions.Add(story6Description1);

        Answer story6answer1 = new Answer();
        Answer story6answer2 = new Answer();
        Answer story6answer3 = new Answer();
        story6answer1.attributsBoost = new List<string>();
        story6answer2.attributsBoost = new List<string> { nameof(Attributs.Manipulation) };
        story6answer3.attributsBoost = new List<string>();
        story6answer1.specialiteBoost = new List<string> { nameof(Specialite.Occultisme), nameof(Specialite.Erudition) };
        story6answer2.specialiteBoost = new List<string> { nameof(Specialite.Politique), nameof(Specialite.Erudition) };
        story6answer3.specialiteBoost = new List<string> { nameof(Specialite.Artisanat), nameof(Specialite.Science), nameof(Specialite.Erudition) };
        story6answer1.nextStory = 7;
        story6answer2.nextStory = 7;
        story6answer3.nextStory = 7;
        story6answer1.description = "L�int�grale recompil� sur le vaudou ancien de la Tribue des Mata� d�Afrique profonde.";
        story6answer2.description = "Un petit trait� de manipulation � l�usage des honn�tes gens.";
        story6answer3.description = "Une copie de Fight Club. Le passage sur la fabrication de la nitroglyc�rine vous int�resse tout particuli�rement.";

        AnswerDescription story6answer1Description1 = new AnswerDescription();
        AnswerDescription story6answer2Description1 = new AnswerDescription();
        AnswerDescription story6answer3Description1 = new AnswerDescription();

        story6answer1Description1.answerDescriptionNumber = 13;
        story6answer2Description1.answerDescriptionNumber = 14;
        story6answer3Description1.answerDescriptionNumber = 15;

        story6answer1Description1.answerNumberPreRequise = new List<int>();
        story6answer2Description1.answerNumberPreRequise = new List<int>();
        story6answer3Description1.answerNumberPreRequise = new List<int>();

        story6answer1Description1.description = "Ce livre est passionnant. Saviez vous qu��tant une tradition �tant n�e de l�esclavagisme et les esclaves ne sachant pas �crire, le vaudou se transmettait seulement oralement ? Une aubaine d�avoir trouv� ce livre totalement legit ! ";
        story6answer2Description1.description = "Un passage extraordinaire vous apprend le <<pied dans la porte>>. Demander un premier service, comme demander l�heure � quelqu�un le rendra bien plus susceptible de vous donner une pi�ce de monnaie si vous lui en demandez une par la suite car il se sentira engag� envers vous. Une technique remarquable !";
        story6answer3Description1.description = "Le passage sur la fabrication de la nitroglyc�rine vous int�resse tout particuli�rement. Vous sentez que vous pouvez faire des merveilles d�artisanat en m�langeant tout plein de choses entre elles ! C'est fou la Chimie !";

        story6answer1.answerDescriptions.Add(story6answer1Description1);
        story6answer2.answerDescriptions.Add(story6answer2Description1);
        story6answer3.answerDescriptions.Add(story6answer3Description1);

        story6.answers.Add(story6answer1);
        story6.answers.Add(story6answer2);
        story6.answers.Add(story6answer3);

        stories.listStory.Add(story6);

        //// Story 7 ///////////////////
        Story story7 = new Story();
        story7.storyNumber = 7;

        StoryDescription story7Description1 = new StoryDescription();
        story7Description1.answerNumberPreRequise = new List<int>();
        story7Description1.description = "Vous voil� bien r�veill� et vous �tes fier de vous. Mais vous vous sentez soudain affam� et vous rappelez qu�il ne vous reste pas longtemps avant que le restaurant de l'h�tel cesse de servir le petit d�jeuner. Vous descendez donc dans la salle � manger et";
        story7.storyDescriptions.Add(story7Description1);

        Answer story7answer1 = new Answer();
        Answer story7answer2 = new Answer();
        story7answer1.attributsBoost = new List<string> { nameof(Attributs.SangFroid)  };
        story7answer2.attributsBoost = new List<string> { nameof(Attributs.Astuce) };
        story7answer1.specialiteBoost = new List<string> { nameof(Specialite.Vigilance) };
        story7answer2.specialiteBoost = new List<string> { nameof(Specialite.Etiquette) };
        story7answer1.nextStory = 8;
        story7answer2.nextStory = 8;
        story7answer1.description = "Vous avisez les lieux et prenez conscience de votre environnement avant de choisir o� vous asseoir.";
        story7answer2.description = "Votre intuition vous pousse et vous allez directement vous asseoir � la premi�re table en vue.";

        AnswerDescription story7answer1Description1 = new AnswerDescription();
        AnswerDescription story7answer2Description1 = new AnswerDescription();

        story7answer1Description1.answerDescriptionNumber = 18;
        story7answer2Description1.answerDescriptionNumber = 19;

        story7answer1Description1.answerNumberPreRequise = new List<int>();
        story7answer2Description1.answerNumberPreRequise = new List<int>();

        story7answer1Description1.description = "Votre intuition vous a parfois jou� des tours, alors vous observez votre environnement et notez qu�une des tables se trouve � c�t� des toilettes. Quelle inf�mie, vous ne voulez pas de cette table. Vous �tes maintenant install� � une table correcte.";
        story7answer2Description1.description = "Apr�s tout vous savez qu�il est mal vu de stalker et de montrer ouvertement sa d�fiance. Vous vous installez sur une table, mais vous vous rendez compte qu�elle est proche des toilettes et il y a du passage, ce qui vous frustre.";

        story7answer1.answerDescriptions.Add(story7answer1Description1);
        story7answer2.answerDescriptions.Add(story7answer2Description1);

        story7.answers.Add(story7answer1);
        story7.answers.Add(story7answer2);

        stories.listStory.Add(story7);

        //// Story 8 ///////////////////
        Story story8 = new Story();
        story8.storyNumber = 8;

        StoryDescription story8Description1 = new StoryDescription();
        story8Description1.answerNumberPreRequise = new List<int> { 6 };
        story8Description1.description = "Les responsable de l�hotel ne sont vraiment bon � rien et ne semblent pas capable de vous servir correctement, vous avez bien fait de remettre � sa place cette femme de chambre tout � l�heure et allez peut �tre r�it�rer l�exploit sur le serveur.";
        story8.storyDescriptions.Add(story8Description1);
        StoryDescription story8Description2 = new StoryDescription();
        story8Description2.answerNumberPreRequise = new List<int> { 7 };
        story8Description2.description = "Vous repensez � comment vous vous �tes comport� avec la femme de chambre. Vous l�avez bien trait� et lui avez parl� gentiment. Vous souriez. Vous vous demandez si vous allez avoir envie de traiter le serveur de la m�me fa�on.";
        story8.storyDescriptions.Add(story8Description2);
        StoryDescription story8Description3 = new StoryDescription();
        story8Description3.answerNumberPreRequise = new List<int> { 8 };
        story8Description3.description = "Le placement des tables est vraiment m�diocre. Vous vous souvenez vous �tre fait passer pour un responsable michelin, et allez peut �tre rejouer le jeu pour avoir une meilleur qualit� de service aupr�s du serveur.";
        story8.storyDescriptions.Add(story8Description3);
        StoryDescription story8Description4 = new StoryDescription();
        story8Description4.answerNumberPreRequise = new List<int> ();
        story8Description4.description = "Celui-ci vous approche, avec un plateau � la main. Il vous informe aimablement que l�heure du petit d�jeuner offert est termin�e et que le service � tourn� en cuisine. A pr�sent, il vous faut commander � la carte et payer pour votre repas. C�est un scandale :";
        story8.storyDescriptions.Add(story8Description4);

        Answer story8answer1 = new Answer();
        Answer story8answer2 = new Answer();
        Answer story8answer3 = new Answer();
        Answer story8answer4 = new Answer();
        story8answer1.attributsBoost = new List<string> { nameof(Attributs.Manipulation), nameof(Attributs.Force) };
        story8answer2.attributsBoost = new List<string> { nameof(Attributs.Manipulation), nameof(Attributs.Resolution) };
        story8answer3.attributsBoost = new List<string> { nameof(Attributs.Manipulation), nameof(Attributs.Astuce) };
        story8answer4.attributsBoost = new List<string>();
        story8answer1.specialiteBoost = new List<string> { nameof(Specialite.ExperienceDeLaRue), nameof(Specialite.Intimidation) };
        story8answer2.specialiteBoost = new List<string> { nameof(Specialite.Empathie), nameof(Specialite.Persuasion) };
        story8answer3.specialiteBoost = new List<string> { nameof(Specialite.Representation), nameof(Specialite.Intimidation) };
        story8answer4.specialiteBoost = new List<string> { nameof(Specialite.ExperienceDeLaRue), nameof(Specialite.Intimidation), nameof(Specialite.Larcin), nameof(Specialite.ArmesAfeu) };
        story8answer1.nextStory = 9;
        story8answer2.nextStory = 9;
        story8answer3.nextStory = 9;
        story8answer4.nextStory = 9;
        story8answer1.answerNumber = 23;
        story8answer2.answerNumber = 48;
        story8answer3.answerNumber = 49;
        story8answer4.answerNumber = 104;
        story8answer1.description = "Vous vous levez subitement et l�attrapez par le col pour l�intimider.";
        story8answer2.description = "Vos regards se croisent et vous l�inspectez de la t�te au pied pour savoir comment le convaincre de vous amuser.";
        story8answer3.description = "Vous d�cidez de vous faire passer pour un employ� de chez Michelin, pr�sent pour noter la qualit� de l'h�tel.";
        story8answer4.description = "Vous d�gainez votre revolver et le menacez du bout du canon";

        AnswerDescription story8answer1Description0 = new AnswerDescription();
        AnswerDescription story8answer1Description1 = new AnswerDescription();
        AnswerDescription story8answer1Description2 = new AnswerDescription();
        AnswerDescription story8answer1Description3 = new AnswerDescription();
        AnswerDescription story8answer1Description4 = new AnswerDescription();
        AnswerDescription story8answer1Description5 = new AnswerDescription();
        AnswerDescription story8answer1Description6 = new AnswerDescription();
        AnswerDescription story8answer1Description7 = new AnswerDescription();

        AnswerDescription story8answer2Description0 = new AnswerDescription();
        AnswerDescription story8answer2Description1 = new AnswerDescription();
        AnswerDescription story8answer2Description2 = new AnswerDescription();
        AnswerDescription story8answer2Description3 = new AnswerDescription();

        AnswerDescription story8answer3Description1 = new AnswerDescription();
        AnswerDescription story8answer3Description2 = new AnswerDescription();
        AnswerDescription story8answer3Description3 = new AnswerDescription();

        AnswerDescription story8answer4Description1 = new AnswerDescription();

        story8answer1Description0.answerDescriptionNumber = 23;
        story8answer1Description1.answerDescriptionNumber = 24;
        story8answer1Description2.answerDescriptionNumber = 25;
        story8answer1Description3.answerDescriptionNumber = 26;
        story8answer1Description4.answerDescriptionNumber = 27;
        story8answer1Description5.answerDescriptionNumber = 28;
        story8answer1Description6.answerDescriptionNumber = 29;
        story8answer1Description7.answerDescriptionNumber = 30;

        story8answer2Description0.answerDescriptionNumber = 48;
        story8answer2Description1.answerDescriptionNumber = 40;
        story8answer2Description2.answerDescriptionNumber = 41;
        story8answer2Description3.answerDescriptionNumber = 42;

        story8answer3Description1.answerDescriptionNumber = 43;
        story8answer3Description2.answerDescriptionNumber = 44;
        story8answer3Description3.answerDescriptionNumber = 45;

        story8answer4Description1.answerDescriptionNumber = 0;

        story8answer1Description0.answerNumberPreRequise = new List<int> ();
        story8answer1Description1.answerNumberPreRequise = new List<int> { 16 };
        story8answer1Description2.answerNumberPreRequise = new List<int> { 17 };
        story8answer1Description3.answerNumberPreRequise = new List<int> { 10 };
        story8answer1Description4.answerNumberPreRequise = new List<int> { 11 };
        story8answer1Description5.answerNumberPreRequise = new List<int> { 13 };
        story8answer1Description6.answerNumberPreRequise = new List<int> { 14 };
        story8answer1Description7.answerNumberPreRequise = new List<int> { 15 };

        story8answer2Description0.answerNumberPreRequise = new List<int> ();
        story8answer2Description1.answerNumberPreRequise = new List<int> { 4 };
        story8answer2Description2.answerNumberPreRequise = new List<int> { 5 };
        story8answer2Description3.answerNumberPreRequise = new List<int> { 6 };

        story8answer3Description1.answerNumberPreRequise = new List<int> { 8 };
        story8answer3Description2.answerNumberPreRequise = new List<int> { 7 };
        story8answer3Description3.answerNumberPreRequise = new List<int> { 6 };

        story8answer4Description1.answerNumberPreRequise = new List<int> ();

        story8answer1Description0.description = "Ce moins que rien va comprendre.";
        story8answer1Description1.description = "Apr�s avoir d�boit� le mur � main nu, vous allez exp�rimenter la solidit� de sa t�te. Le serveur est terroris�.";
        story8answer1Description2.description = "Vous avisez la chaise � c�t� de vous et pensez � comment vous avez fracass� la lampe sur le mur tout � l�heure. Vous r�itererez peut �tre l�exploit. Le serveur �carquille les yeux d'effroi.";
        story8answer1Description3.description = "Vous �tes bien en forme apr�s votre s�ance de fitness et allez l��taler devant toute la salle s�il ne vous traite pas correctement. Le serveur est terroris�.";
        story8answer1Description4.description = "Vous avez de quoi acheter la moiti� de cet �tablissement rien qu�avec vos actions Unity Engine, et ce smicard n�aurait pas d� supposer que vous n�avez pas de quoi payer votre propre repas. Le serveur est terroris�.";
        story8answer1Description5.description = "Vous repensez � votre livre et consid�rez la situation avec recul et intelligence. Vous vous souvenez que Boudilana, une sorci�re Vaudou, maudissait ses ennemis � l�aide d�une phrase occulte et vous la prononcez � voix haute pour intimider ce pauvre serveur. Le serveur est terroris�.";
        story8answer1Description6.description = "Le passage du petit trait� de manipulation parlait de la technique de la porte dans le nez. Vous allez demander quelque chose d�improbable et d�irr�alisable au serveur, avant de lui demander quelque chose de plus simple pour qu�il accepte. Vous vous �nervez donc contre lui en lui ordonnant de vous offrir la nuit car son comportement est inacceptable. Il bafouille, alors vous le tenez et lui commandez de vous offrir le repas. Le serveur soupire de soulagement quand il entend votre demande plus raisonnable.";
        story8answer1Description7.description = "Votre livre de fight club vous a inspir�. Vous ouvrez sous les yeux du serveur le poivre et le sel et versez l�int�gralit� du contenu des pots dans votre main. Vous menacez le serveur de lui balancer cette bombe au poivre improvis�e. Il vous regarde effray� par votre comportement.";

        story8answer2Description0.description = "Il a des cernes sous les yeux, une barbe mal ras�e et les cheveux gras de quelqu�un qui n�a pas eu le temps de se laver r�cemment. Ces �l�ments montrent qu�il fait des heures sup et que c�est lui qui fait tourner la boutique. De plus, ses chaussures sont de mauvaise qualit�, il est donc n�gligent ou trop peu pay� pour les heures qu�il fait. Vous faites donc jouer la possibilit� de laisser un pourboir cons�quent s�il vous sert avec talent et brio.";
        story8answer2Description1.description = "Votre �tude de la femme de chambre ce matin a port� ses fruits, insp�cter les gens pour les gauger est devenu une habitude chez vous. Le serveur est convaincu que vous allez bien le r�compenser.";
        story8answer2Description2.description = "Au souvenir de la fa�on dont vous avez ordonn� � la femme de chambre d�entrer ce matin, vous avez trait� le serveur avec autant d�assurance. Le serveur aussi a baiss� les yeux et est persuad� que vous allez bien le r�compenser.";
        story8answer2Description3.description = "Cette fois ci, vous avez fait le premier pas vers l�action. Tout � l�heure vous �tiez rest� cach� en esp�rant que la femme de chambre parte. Mais l� l�heure �tait clairement � l�action. Le serveur est convaincu d��tre bien r�compens�.";

        story8answer3Description1.description = "Apr�s tout vous l�avez d�j� fait pour la femme de m�nage et �a a fonctionn�. Vous pestif�rez et annoncez que vous allez retirer une �toile � cet h�tel. Vous vous levez et partez en une tirade convaincante. Le serveur devient livide devant votre num�ro  et vous annonce qu�il va vous offrir le petit d�jeuner.";
        story8answer3Description2.description = "Une fois n�est pas coutume. C�est pas ce que vous avez bien trait� la femme de chambre tout � l�heure que vous ne pouvez pas manipuler ce d�bile de serveur. Il devient livide devant votre num�ro et vous annonce qu�il va vous offrir le petit d�jeuner.";
        story8answer3Description3.description = "Une fois n�est pas coutume. C�est pas par ce que vous avez effray� la femme de chambre tout � l�heure que vous ne pouvez pas manipuler ce serveur en vous faisant passer pour quelqu�un d�autre. Celui-ci devient livide devant votre num�ro et vous annonce qu�il va vous offrir le petit d�jeuner.";

        story8answer4Description1.description = "Le serveur est terroris� alors que vous menacez de le tuer sur place. On ne d�conne pas avec vous et vous ajoutez que s�il raconte ce qu�il se passe vous le retrouverez o� qu�il se trouve.";


        story8answer1.answerDescriptions.Add(story8answer1Description0);
        story8answer1.answerDescriptions.Add(story8answer1Description1);
        story8answer1.answerDescriptions.Add(story8answer1Description2);
        story8answer1.answerDescriptions.Add(story8answer1Description3);
        story8answer1.answerDescriptions.Add(story8answer1Description4);
        story8answer1.answerDescriptions.Add(story8answer1Description5);
        story8answer1.answerDescriptions.Add(story8answer1Description6);
        story8answer1.answerDescriptions.Add(story8answer1Description7);

        story8answer2.answerDescriptions.Add(story8answer2Description0);
        story8answer2.answerDescriptions.Add(story8answer2Description1);
        story8answer2.answerDescriptions.Add(story8answer2Description2);
        story8answer2.answerDescriptions.Add(story8answer2Description3);

        story8answer3.answerDescriptions.Add(story8answer3Description1);
        story8answer3.answerDescriptions.Add(story8answer3Description2);
        story8answer3.answerDescriptions.Add(story8answer3Description3);
        story8answer4.answerDescriptions.Add(story8answer4Description1);
        story8.answers.Add(story8answer1);
        story8.answers.Add(story8answer2);
        story8.answers.Add(story8answer3);
        story8.answers.Add(story8answer4);

        stories.listStory.Add(story8);

        //////////// STORY 9
        Story story9 = new Story();
        story9.storyNumber = 9;

        StoryDescription story9Description1 = new StoryDescription();
        story9Description1.answerNumberPreRequise = new List<int> { 19 };
        story9Description1.description = "Le serveur se plie donc � votre volont� et apr�s votre commande part aussit�t en cuisine de mani�re tr�s professionnelle pour satisfaire vos envies culinaires. Une nouvelle sc�ne s�offre � vous quand une vieille femme passe � c�t� de vous avec un petit chien en laisse. Elle se rend aux toilettes. Votre table est dans le passage � cause de votre mauvais choix de positionnement. Le chien vous renifle les jambes et se frotte � vous sous la table vous trouvez �a ";
        story9.storyDescriptions.Add(story9Description1);
        StoryDescription story9Description2 = new StoryDescription();
        story9Description2.answerNumberPreRequise = new List<int> { 18 };
        story9Description2.description = "Le serveur se plie donc � votre volont� et apr�s votre commande part aussit�t en cuisine de mani�re tr�s professionnelle pour satisfaire vos envies culinaires. Une nouvelle sc�ne s�offre � vous quand une vieille femme passe � c�t� de vous avec un petit chien en laisse. Vous avez pourtant bien choisi votre table pour �viter d��tre dans le passage et ce genre de situation. Le chien vous renifle les jambes et se frotte � vous sous la table vous trouvez �a ";
        story9.storyDescriptions.Add(story9Description2);

        Answer story9answer1 = new Answer();
        Answer story9answer2 = new Answer();
        Answer story9answer3 = new Answer();
        story9answer1.attributsBoost = new List<string>();
        story9answer2.attributsBoost = new List<string>();
        story9answer3.attributsBoost = new List<string>();
        story9answer1.specialiteBoost = new List<string> { nameof(Specialite.Bagarre) };
        story9answer2.specialiteBoost = new List<string> { nameof(Specialite.Animaux) };
        story9answer3.specialiteBoost = new List<string> { nameof(Specialite.Melee) };
        story9answer1.nextStory = 10;
        story9answer2.nextStory = 10;
        story9answer3.nextStory = 10;
        story9answer1.description = "Tr�s mal �lev� et vous donnez un petit coup de pied dedans.";
        story9answer2.description = "Super mignon. Vous adorez les animaux.";
        story9answer3.description = "Proprement scandaleux, vous avisez un cure-dent dans un pot sur la table�";

        AnswerDescription story9answer1Description1 = new AnswerDescription();
        AnswerDescription story9answer2Description1 = new AnswerDescription();
        AnswerDescription story9answer3Description1 = new AnswerDescription();
        AnswerDescription story9answer3Description2 = new AnswerDescription();
        AnswerDescription story9answer3Description3 = new AnswerDescription();
        AnswerDescription story9answer3Description4 = new AnswerDescription();

        story9answer1Description1.answerDescriptionNumber = 50;
        story9answer2Description1.answerDescriptionNumber = 51;
        story9answer3Description1.answerDescriptionNumber = 52;
        story9answer3Description2.answerDescriptionNumber = 52;
        story9answer3Description3.answerDescriptionNumber = 52;
        story9answer3Description4.answerDescriptionNumber = 52;

        story9answer1Description1.answerNumberPreRequise = new List<int>();
        story9answer2Description1.answerNumberPreRequise = new List<int>();
        story9answer3Description1.answerNumberPreRequise = new List<int> { 23 };
        story9answer3Description2.answerNumberPreRequise = new List<int> { 49 };
        story9answer3Description3.answerNumberPreRequise = new List<int> { 50 };
        story9answer3Description4.answerNumberPreRequise = new List<int> ();

        story9answer1Description1.description = "Le petit chien couine et la vieille femme vous regarde m�dus�e. Vous lui faites peur et elle tire sur la laisse en s��loignant de vous. ";
        story9answer2Description1.description = "Vous vous baissez pour le caresser. Cette brave b�te vous redonne le sourire et la vieille femme se permet d�echanger quelques mots avant de s�en aller.";
        story9answer3Description1.description = "Vous voyez le serveur du coin de l'�il, vous l�avez intimid� tout � l�heure et il accoure pour demander � la femme de faire sortir le chien.";
        story9answer3Description2.description = "Vous voyez le serveur du coin de l�oeil, persuad� que vous allez laisser un pourboire d�mesur� il accoure pour demander � la femme de faire sortir le chien.";
        story9answer3Description3.description = "Vous voyez le serveur du coin de l'�il. Persuad� que vous venez de Michelin il accoure pour demander � la femme de faire sortir le chien.";
        story9answer3Description4.description = "Vous en attrapez un et le plantez � l�aveugle dans la fourrure de la b�te qui se met � aboyer violemment. La situation escalade alors que la vieille femme vous insulte � grand renfort de juron.";

        story9answer1.answerDescriptions.Add(story9answer1Description1);
        story9answer2.answerDescriptions.Add(story9answer2Description1);
        story9answer3.answerDescriptions.Add(story9answer3Description4);
        story9answer3.answerDescriptions.Add(story9answer3Description1);
        story9answer3.answerDescriptions.Add(story9answer3Description2);
        story9answer3.answerDescriptions.Add(story9answer3Description3);

        story9.answers.Add(story9answer1);
        story9.answers.Add(story9answer2);
        story9.answers.Add(story9answer3);

        stories.listStory.Add(story9);

        ///////////////STORY 10
        Story story10 = new Story();
        story10.storyNumber = 10;

        StoryDescription story10Description1 = new StoryDescription();
        story10Description1.answerNumberPreRequise = new List<int>();
        story10Description1.description = "Vous �tes satisfait de la tournure des �v�nements, et bient�t on vous apporte le fameux petit d�jeuner qu�on vous avait refus� plus t�t. Un immense plateau garni de croissant, de confiture, de beurre et une cafeti�re. Vous n�arrivez cependant pas � terminer tout le plat et d�cidez";
        story10.storyDescriptions.Add(story10Description1);

        Answer story10answer1 = new Answer();
        Answer story10answer2 = new Answer();
        Answer story10answer3 = new Answer();
        story10answer1.attributsBoost = new List<string> { nameof(Attributs.SangFroid) };
        story10answer2.attributsBoost = new List<string> { nameof(Attributs.Astuce) };
        story10answer3.attributsBoost = new List<string>();
        story10answer1.specialiteBoost = new List<string> { nameof(Specialite.Larcin) };
        story10answer2.specialiteBoost = new List<string> { nameof(Specialite.Commandement) };
        story10answer3.specialiteBoost = new List<string> { nameof(Specialite.Etiquette), nameof(Specialite.Finances) };
        story10answer1.nextStory = 11;
        story10answer2.nextStory = 11;
        story10answer3.nextStory = 11;
        story10answer1.description = "De subtiliser quelques croissants en toute discr�tion.";
        story10answer2.description = "De commander au serveur de vous r�aliser un doggy bag.";
        story10answer3.description = "De partir directement. Ce n'est pas l�argent qui vous manque.";

        AnswerDescription story10answer1Description1 = new AnswerDescription();
        AnswerDescription story10answer2Description1 = new AnswerDescription();
        AnswerDescription story10answer2Description2 = new AnswerDescription();
        AnswerDescription story10answer2Description3 = new AnswerDescription();
        AnswerDescription story10answer2Description4 = new AnswerDescription();
        AnswerDescription story10answer3Description1 = new AnswerDescription();
        AnswerDescription story10answer3Description2 = new AnswerDescription();

        story10answer1Description1.answerDescriptionNumber = 53;
        story10answer2Description1.answerDescriptionNumber = 54;
        story10answer2Description2.answerDescriptionNumber = 54;
        story10answer2Description3.answerDescriptionNumber = 54;
        story10answer2Description4.answerDescriptionNumber = 54;
        story10answer3Description1.answerDescriptionNumber = 55;
        story10answer3Description2.answerDescriptionNumber = 55;

        story10answer1Description1.answerNumberPreRequise = new List<int>();
        story10answer2Description1.answerNumberPreRequise = new List<int> { 48 };
        story10answer2Description2.answerNumberPreRequise = new List<int> { 23  };
        story10answer2Description3.answerNumberPreRequise = new List<int> { 49 };
        story10answer2Description4.answerNumberPreRequise = new List<int> { 104 };
        story10answer3Description1.answerNumberPreRequise = new List<int>();
        story10answer3Description2.answerNumberPreRequise = new List<int> { 11 };

        story10answer1Description1.description = "Vous mettez discr�tement quelques croissants dans votre sac et finissez votre caf�. ";
        story10answer2Description1.description = "Il est tr�s surpris, persuad� qu�il �tait que vous alliez bien le r�compenser. Vous semblez compter vos sous au point de vouloir partir avec du rab de nourriture. Il s'ex�cute n�anmoins par professionnalisme.";
        story10answer2Description2.description = "Vous l�avez mit mal en l�attrapant par le col tout � l�heure et il s'ex�cute sans broncher sans vous quitter des yeux.";
        story10answer2Description3.description = "Persuad� que vous �tes un repr�sentant de chez michelin, le serveur s��tale presque au sol en s�inclinant. Il vous pr�pare un doggy bag magnifique avec un ruban.";
        story10answer2Description4.description = "Persuad� que vous �tes un barron de la mafia et que vous allez le descendre s�il ne s'ex�cute pas le serveur vous pr�pare le plus beau doggy bag de votre vie.";
        story10answer3Description1.description = "Vous pouvez vous permettre de gaspiller et vous savez qu�il est mal vu de prendre ce qu�il ne vous appartient pas.";
        story10answer3Description2.description = "Vous laissez �galement un pourboire g�n�reux, parce que pourquoi pas, avec toutes vos actions vous n'avez pas de probl�me d�argent.";

        story10answer1.answerDescriptions.Add(story10answer1Description1);
        story10answer2.answerDescriptions.Add(story10answer2Description1);
        story10answer2.answerDescriptions.Add(story10answer2Description2);
        story10answer2.answerDescriptions.Add(story10answer2Description3);
        story10answer2.answerDescriptions.Add(story10answer2Description4);
        story10answer3.answerDescriptions.Add(story10answer3Description1);
        story10answer3.answerDescriptions.Add(story10answer3Description2);

        story10.answers.Add(story10answer1);
        story10.answers.Add(story10answer2);
        story10.answers.Add(story10answer3);

        stories.listStory.Add(story10);


        /////////// STORY 11
        Story story11 = new Story();
        story11.storyNumber = 11;

        StoryDescription story11Description1 = new StoryDescription();
        story11Description1.answerNumberPreRequise = new List<int>();
        story11Description1.description = "Vous vous levez donc et vous dirigez vers la sortie. En sortant vous avisez au tout dernier moment ";
        story11.storyDescriptions.Add(story11Description1);
        StoryDescription story11Description2 = new StoryDescription();
        story11Description2.answerNumberPreRequise = new List<int> { 50 };
        story11Description2.description = "la vieille femme et le chien que vous avez cogn� du pied tout � l�heure. En vous reconnaissant il couine.";
        story11.storyDescriptions.Add(story11Description2);
        StoryDescription story11Description3 = new StoryDescription();
        story11Description3.answerNumberPreRequise = new List<int> { 51 };
        story11Description3.description = "la vieille femme et le brave chien que vous avez caress� tout � l�heure. Il vous attendrit et revient se frotter � vous.";
        story11.storyDescriptions.Add(story11Description3);
        StoryDescription story11Description4 = new StoryDescription();
        story11Description4.answerNumberPreRequise = new List<int> { 52 };
        story11Description4.description = "la vieille femme et son foutu clebs, que le serveur avait fait sortir tout � l�heure. Vous notez avec satisfaction qu�un peu de sang s��coule de son pelage l� o� le cure dent s�est plant�. Il aboie en vous voyant et montre les dents.";
        story11.storyDescriptions.Add(story11Description4);
        StoryDescription story11Description5 = new StoryDescription();
        story11Description5.answerNumberPreRequise = new List<int>();
        story11Description5.description = "Vous avez � peine le temps de consid�rer la situation, et manquez de tr�bucher sur le chien.";
        story11.storyDescriptions.Add(story11Description5);

        Answer story11answer1 = new Answer();
        Answer story11answer2 = new Answer();
        Answer story11answer3 = new Answer();
        Answer story11answer4 = new Answer();
        story11answer1.attributsBoost = new List<string> { nameof(Attributs.Dexterite) };
        story11answer2.attributsBoost = new List<string> { nameof(Attributs.Intelligence) };
        story11answer3.attributsBoost = new List<string> { nameof(Attributs.Force) };
        story11answer4.attributsBoost = new List<string> { nameof(Attributs.Dexterite) };
        story11answer1.specialiteBoost = new List<string> { nameof(Specialite.Athletisme), nameof(Specialite.Animaux) };
        story11answer2.specialiteBoost = new List<string> { nameof(Specialite.Vigilance), nameof(Specialite.Animaux) };
        story11answer3.specialiteBoost = new List<string> { nameof(Specialite.Bagarre) };
        story11answer4.specialiteBoost = new List<string> { nameof(Specialite.ArmesAfeu) };
        story11answer1.nextStory = 12;
        story11answer2.nextStory = 12;
        story11answer3.nextStory = 12;
        story11answer4.nextStory = 12;
        story11answer1.description = "Vous utilisez votre dext�rit� pour l�esquiver agilement.";
        story11answer2.description = "Vous l�avez vu en avance et vous vous cramponnez intelligemment � la porte pour �viter de tomber.";
        story11answer3.description = "Mais casse toi le chien !";
        story11answer4.description = "Vous manquez de tomber par terre et de rage tuez le chien avec votre revolver !";

        AnswerDescription story11answer1Description1 = new AnswerDescription();
        AnswerDescription story11answer2Description1 = new AnswerDescription();
        AnswerDescription story11answer3Description1 = new AnswerDescription();
        AnswerDescription story11answer4Description1 = new AnswerDescription();

        story11answer1Description1.answerDescriptionNumber = 56;
        story11answer2Description1.answerDescriptionNumber = 57;
        story11answer3Description1.answerDescriptionNumber = 58;
        story11answer4Description1.answerDescriptionNumber = 105;

        story11answer1Description1.answerNumberPreRequise = new List<int>();
        story11answer2Description1.answerNumberPreRequise = new List<int>();
        story11answer3Description1.answerNumberPreRequise = new List<int>();
        story11answer4Description1.answerNumberPreRequise = new List<int>();

        story11answer1Description1.description = "Vous n�allez pas cogner sur l�animal, et vous enjambez la b�te agilement pendant qu�elle se faufile entre vos jambes. Vous voyez du soulagement dans le regard de la vieille femme.";
        story11answer2Description1.description = "Vous n�allez pas cogner sur l�animal, et vous vous cramponnez � la porte de sortie de l'h�tel pendant sur le chien passe � c�t� de vous. Vous voyez du soulagement dans le regard de la vieille femme.";
        story11answer3Description1.description = "Le chien vole sur un m�tre apr�s une solide collision avec votre chaussure. Il s�effondre par terre dans un couinement aigu et la vieille femme se pr�cipite vers lui.";
        story11answer4Description1.description = "Vous d�gainez et cela ne prend qu�une seconde pour que 3 balles se fichent dans le dos de l�animal. Du sang gicle, et la vieille femme se met � hurler. Mais vous l�ignorez et �tes satisfait.";

        story11answer1.answerDescriptions.Add(story11answer1Description1);
        story11answer2.answerDescriptions.Add(story11answer2Description1);
        story11answer3.answerDescriptions.Add(story11answer3Description1);
        story11answer4.answerDescriptions.Add(story11answer4Description1);

        story11.answers.Add(story11answer1);
        story11.answers.Add(story11answer2);
        story11.answers.Add(story11answer3);
        story11.answers.Add(story11answer4);

        stories.listStory.Add(story11);

        /////////STORY 12
         Story story12 = new Story();
        story12.storyNumber = 12;

        StoryDescription story12Description1 = new StoryDescription();
        story12Description1.answerNumberPreRequise = new List<int>();
        story12Description1.description = "Vous laissez ces deux protagonistes et vous enfoncez dans la rue. Vous devez vous rendre � l�autre bout de la ville pour assister � une r�union et tout naturellement vous";
        story12.storyDescriptions.Add(story12Description1);

        Answer story12answer1 = new Answer();
        Answer story12answer2 = new Answer();
        Answer story12answer3 = new Answer();
        story12answer1.attributsBoost = new List<string>();
        story12answer2.attributsBoost = new List<string> { nameof(Attributs.Vigueur), nameof(Attributs.Resolution) };
        story12answer3.attributsBoost = new List<string>();
        story12answer1.specialiteBoost = new List<string> { nameof(Specialite.Finances), nameof(Specialite.Technologie) };
        story12answer2.specialiteBoost = new List<string> { nameof(Specialite.ExperienceDeLaRue) };
        story12answer3.specialiteBoost = new List<string> { nameof(Specialite.Finances), nameof(Specialite.Conduite) };
        //TODO Fin de la Scene 1 => Next story 0 (Mettre 13 plus tard)
        story12answer1.nextStory = 13;
        story12answer2.nextStory = 13;
        story12answer3.nextStory = 13;
        story12answer1.description = "Appelez un taxi.";
        story12answer2.description = "Y allez � pied.";
        story12answer3.description = "Montez dans votre voiture.";

        AnswerDescription story12answer1Description1 = new AnswerDescription();
        AnswerDescription story12answer2Description1 = new AnswerDescription();
        AnswerDescription story12answer3Description1 = new AnswerDescription();

        story12answer1Description1.answerDescriptionNumber = 59;
        story12answer2Description1.answerDescriptionNumber = 60;
        story12answer3Description1.answerDescriptionNumber = 61;

        story12answer1Description1.answerNumberPreRequise = new List<int>();
        story12answer2Description1.answerNumberPreRequise = new List<int>();
        story12answer3Description1.answerNumberPreRequise = new List<int>();

        story12answer1Description1.description = "";
        story12answer2Description1.description = "";
        story12answer3Description1.description = "";

        story12answer1.answerDescriptions.Add(story12answer1Description1);
        story12answer2.answerDescriptions.Add(story12answer2Description1);
        story12answer3.answerDescriptions.Add(story12answer3Description1);

        story12.answers.Add(story12answer1);
        story12.answers.Add(story12answer2);
        story12.answers.Add(story12answer3);

        stories.listStory.Add(story12);


        //////////////////////////PHASE 2

        //// STORY 13

        Story story13 = new Story();
        story13.storyNumber = 13;

        StoryDescription story13Description1 = new StoryDescription();
        story13Description1.answerNumberPreRequise = new List<int>();
        story13Description1.description = "La journ�e est pass�e, votre rendez vous s�est d�roul� convenablement, et le soir est tomb�. Vous sortez d�un afterwork et vos coll�gues vous l�chent. Vous d�cidez de poursuivre votre soir�e et �tes entr� dans un bar tr�s vivant du centre ville. Vous avez soif et avisez la barmaid !";
        story13.storyDescriptions.Add(story13Description1);

        Answer story13answer1 = new Answer();
        Answer story13answer2 = new Answer();
        Answer story13answer3 = new Answer();
        Answer story13answer4 = new Answer();
        Answer story13answer5 = new Answer();
        story13answer1.attributsBoost = new List<string>();
        story13answer2.attributsBoost = new List<string> { nameof(Attributs.Charisme) };
        story13answer3.attributsBoost = new List<string> { nameof(Attributs.Manipulation) };
        story13answer4.attributsBoost = new List<string> ();
        story13answer5.attributsBoost = new List<string> { nameof(Attributs.Intelligence) };
        story13answer1.specialiteBoost = new List<string> { nameof(Specialite.Commandement), nameof(Specialite.Larcin) };
        story13answer2.specialiteBoost = new List<string> { nameof(Specialite.Empathie) };
        story13answer3.specialiteBoost = new List<string> { nameof(Specialite.Intimidation)};
        story13answer4.specialiteBoost = new List<string> { nameof(Specialite.Furtivite), nameof(Specialite.Vigilance) };
        story13answer5.specialiteBoost = new List<string> { nameof(Specialite.Etiquette) };
        story13answer1.nextStory = 14;
        story13answer2.nextStory = 14;
        story13answer3.nextStory = 14;
        story13answer4.nextStory = 14;
        story13answer5.nextStory = 14;
        story13answer1.description = "Vous commandez de mani�re autoritaire un verre de votre liqueur favorite.";
        story13answer2.description = "Vous tentez de s�duire la barmaid en misant sur votre charisme naturel.";
        story13answer3.description = "Vous intimidez la barmaid pour qu�elle vous serve plus vite.";
        story13answer4.description = "Vous vous faites discret pour �viter qu�on ne vous remarque.";
        story13answer5.description = "Vous demandez poliment un verre.";

        AnswerDescription story13answer1Description1 = new AnswerDescription();
        AnswerDescription story13answer1Description2 = new AnswerDescription();
        AnswerDescription story13answer2Description1 = new AnswerDescription();
        AnswerDescription story13answer2Description2 = new AnswerDescription();
        AnswerDescription story13answer3Description1 = new AnswerDescription();
        AnswerDescription story13answer3Description2 = new AnswerDescription();
        AnswerDescription story13answer3Description3 = new AnswerDescription();
        AnswerDescription story13answer3Description4 = new AnswerDescription();
        AnswerDescription story13answer4Description1 = new AnswerDescription();
        AnswerDescription story13answer4Description2 = new AnswerDescription();
        AnswerDescription story13answer4Description3 = new AnswerDescription();
        AnswerDescription story13answer5Description1 = new AnswerDescription();

        story13answer1Description1.answerDescriptionNumber = 62;
        story13answer1Description2.answerDescriptionNumber = 62;
        story13answer2Description1.answerDescriptionNumber = 63;
        story13answer2Description2.answerDescriptionNumber = 63;
        story13answer3Description1.answerDescriptionNumber = 64;
        story13answer3Description2.answerDescriptionNumber = 64;
        story13answer3Description3.answerDescriptionNumber = 64;
        story13answer3Description4.answerDescriptionNumber = 64;
        story13answer4Description1.answerDescriptionNumber = 65;
        story13answer4Description2.answerDescriptionNumber = 65;
        story13answer4Description3.answerDescriptionNumber = 65;
        story13answer5Description1.answerDescriptionNumber = 66;

        story13answer1Description1.answerNumberPreRequise = new List<int> { 5 };
        story13answer1Description2.isPreRequiseNotIn = true;
        story13answer1Description2.answerNumberPreRequise = new List<int> { 5 };
        story13answer2Description1.answerNumberPreRequise = new List<int> { 1 };
        story13answer2Description2.answerNumberPreRequise = new List<int> { 2 };
        story13answer3Description1.answerNumberPreRequise = new List<int> { 6 };
        story13answer3Description2.answerNumberPreRequise = new List<int> { 23 };
        story13answer3Description3.answerNumberPreRequise = new List<int>();
        story13answer3Description4.isPreRequiseListAndAndNotOr = false;
        story13answer3Description4.answerNumberPreRequise = new List<int> { 6, 23 };
        story13answer4Description1.answerNumberPreRequise = new List<int>();
        story13answer4Description2.answerNumberPreRequise = new List<int> { 3 };
        story13answer4Description3.isPreRequiseNotIn = true;
        story13answer4Description3.answerNumberPreRequise = new List<int> { 3 };
        story13answer5Description1.answerNumberPreRequise = new List<int> ();

        story13answer1Description1.description = "Vous aviez d�j� fait �a ce matin pour ordonner � la femme de chambre d�ouvrir la porte elle m�me. La barmaid s'ex�cute tr�s rapidement et vous �tes servi plus vite qu�au naturel. Vous payez et elle s��loigne pour servir un autre client. ";
        story13answer1Description2.description = "Vous n��tes pas tr�s habitu� � ordonner imp�rieusement mais il faut bien commencer quelque part. La barmaid est surprise et h�site mais elle s�incline et vous sert votre verre. Vous payez et elle s'�loigne pour servir un autre client. ";
        story13answer2Description1.description = "Celle-ci plisse les yeux et vous soupirez. Vous aviez bien vu ce matin dans le miroir que vous �tiez commun. Mais vous vous �tes bien habill� pour renforcer votre charisme tout de m�me et y avez cru. La barmaid vous pose votre commande sous le nez un poil d�go�t� et vous payez. Vous allez retravailler votre sourire et peut-�tre que la prochaine tentative de s�duction fonctionnera mieux.";
        story13answer2Description2.description = "Celle-ci est sous le charme. Vos dents bien align�es la font fondre. Ce matin en vous regardant dans le miroir vous avez bien vu que vous �tiez beau gosse, et vous savez vous en servir. Elle vous offre un verre en vous faisant les yeux doux.";
        story13answer3Description1.description = "Vous avez intimid� la femme de chambre en lui mena�ant de d�poser une plainte contre elle.";
        story13answer3Description2.description = "Vous avez attrap� le serveur par le col tout � l�heure pour l�impressionner.";
        story13answer3Description3.description = "Vous sentez un peu de peur dans les yeux de la barmaid, et elle vous sert rapidement avant de se retourner vers un autre client.";
        story13answer3Description4.description = "Vous notez qu�elle ne vous a pas tout de suite encaiss�, par crainte. Ce que vous avez fait ce matin a port� ses fruits.";
        story13answer4Description1.description = "Vous vous faites tout petit au bar et �vitez tout eye contact tout en scrutant la pi�ce timidement.";
        story13answer4Description2.description = "Vous aviez d�j� pratiqu� �a ce matin quand vous vous �tes fait discret pour que la femme de menage s�en aille. Cette fois ci �a marche plut�t bien et personne ne vient vous ennuyer, pas m�me la barmaid.";
        story13answer4Description3.description = "C�est la premi�re fois que vous tentez de vous faire aussi discret. Ce matin vous avez pr�f�r� aller au contact avec la femme de chambre, mais il faut un d�but � tout. Vous sentez quelques regards sur vous, la barmaid vous regarde et pense que vous attendez quelqu�un, elle vous laisse tranquille.";
        story13answer5Description1.description = "La barmaid s'ex�cute normalement, elle vous sert votre commande, encaisse, et se dirige de l�autre c�t� du bar pour servir un autre client";

        story13answer1.answerDescriptions.Add(story13answer1Description1);
        story13answer1.answerDescriptions.Add(story13answer1Description2);
        story13answer2.answerDescriptions.Add(story13answer2Description1);
        story13answer2.answerDescriptions.Add(story13answer2Description2);
        story13answer3.answerDescriptions.Add(story13answer3Description1);
        story13answer3.answerDescriptions.Add(story13answer3Description2);
        story13answer3.answerDescriptions.Add(story13answer3Description3);
        story13answer3.answerDescriptions.Add(story13answer3Description4);
        story13answer4.answerDescriptions.Add(story13answer4Description1);
        story13answer4.answerDescriptions.Add(story13answer4Description2);
        story13answer4.answerDescriptions.Add(story13answer4Description3);
        story13answer5.answerDescriptions.Add(story13answer5Description1);

        story13.answers.Add(story13answer1);
        story13.answers.Add(story13answer2);
        story13.answers.Add(story13answer3);
        story13.answers.Add(story13answer4);
        story13.answers.Add(story13answer5);

        stories.listStory.Add(story13);

        //STORY 14

        Story story14 = new Story();
        story14.storyNumber = 14;

        StoryDescription story14Description1 = new StoryDescription();
        story14Description1.answerNumberPreRequise = new List<int>();
        story14Description1.description = "Soudainement, une femme magnifique s�allonge presque sur le comptoir juste � c�t� de vous. Elle sort de la piste de danse et est visiblement �m�ch�e. En plus de �a elle est carr�ment votre style. Son charisme est rayonnant et presque surnaturel. Vous voulez attirer son attention et d�cidez de :";
        story14.storyDescriptions.Add(story14Description1);

        Answer story14answer1 = new Answer();
        Answer story14answer2 = new Answer();
        Answer story14answer3 = new Answer();
        Answer story14answer4 = new Answer();
        story14answer1.attributsBoost = new List<string> { nameof(Attributs.Astuce) };
        story14answer2.attributsBoost = new List<string> { nameof(Attributs.Manipulation) };
        story14answer3.attributsBoost = new List<string>();
        story14answer4.attributsBoost = new List<string> { nameof(Attributs.Charisme) };
        story14answer1.specialiteBoost = new List<string> { nameof(Specialite.Larcin) };
        story14answer2.specialiteBoost = new List<string> { nameof(Specialite.Subterfuge) };
        story14answer3.specialiteBoost = new List<string> { nameof(Specialite.Technologie), nameof(Specialite.Finances) };
        story14answer4.specialiteBoost = new List<string> { nameof(Specialite.Representation) };
        story14answer1.nextStory = 15;
        story14answer2.nextStory = 15;
        story14answer3.nextStory = 15;
        story14answer4.nextStory = 15;
        story14answer1.description = "Voler son portefeuille pour vous faire passer pour son sauveur.";
        story14answer2.description = "Lancer la conversation en mentant sur votre position sociale.";
        story14answer3.description = "Vous vous rapprochez d�elle avec votre t�l�phone ouvert bien en vue sur votre compte bancaire.";
        story14answer4.description = "Vous v�rifiez que vous n�avez rien entre les dents et bombez le torse avant de lui parler d�une voix suave.";
        
        AnswerDescription story14answer1Description1 = new AnswerDescription();
        AnswerDescription story14answer1Description2 = new AnswerDescription();
        AnswerDescription story14answer1Description3 = new AnswerDescription();
        AnswerDescription story14answer1Description4 = new AnswerDescription();
        AnswerDescription story14answer2Description1 = new AnswerDescription();
        AnswerDescription story14answer2Description2 = new AnswerDescription();
        AnswerDescription story14answer2Description3 = new AnswerDescription();
        AnswerDescription story14answer3Description1 = new AnswerDescription();
        AnswerDescription story14answer3Description2 = new AnswerDescription();
        AnswerDescription story14answer3Description3 = new AnswerDescription();
        AnswerDescription story14answer4Description1 = new AnswerDescription();
        AnswerDescription story14answer4Description2 = new AnswerDescription();
        AnswerDescription story14answer4Description3 = new AnswerDescription();
        AnswerDescription story14answer4Description4 = new AnswerDescription();

        story14answer1Description1.answerDescriptionNumber = 67;
        story14answer1Description2.answerDescriptionNumber = 67;
        story14answer1Description3.answerDescriptionNumber = 67;
        story14answer1Description4.answerDescriptionNumber = 67;
        story14answer2Description1.answerDescriptionNumber = 68;
        story14answer2Description2.answerDescriptionNumber = 68;
        story14answer2Description3.answerDescriptionNumber = 68;
        story14answer3Description1.answerDescriptionNumber = 69;
        story14answer3Description2.answerDescriptionNumber = 69;
        story14answer3Description3.answerDescriptionNumber = 69;
        story14answer4Description1.answerDescriptionNumber = 77;
        story14answer4Description2.answerDescriptionNumber = 76;
        story14answer4Description3.answerDescriptionNumber = 78;
        story14answer4Description4.answerDescriptionNumber = 70;

        story14answer1Description1.answerNumberPreRequise = new List<int> { 53 };
        story14answer1Description2.isPreRequiseNotIn = true;
        story14answer1Description2.answerNumberPreRequise = new List<int> { 53 };
        story14answer1Description3.answerNumberPreRequise = new List<int>();
        story14answer1Description4.answerNumberPreRequise = new List<int> { 53 };

        story14answer2Description1.answerNumberPreRequise = new List<int>();
        story14answer2Description2.isPreRequiseListAndAndNotOr = false;
        story14answer2Description2.answerNumberPreRequise = new List<int> { 8, 49 };
        story14answer2Description3.answerNumberPreRequise = new List<int>();

        story14answer3Description1.answerNumberPreRequise = new List<int> { 11 };
        story14answer3Description2.answerNumberPreRequise = new List<int> { 55 };
        story14answer3Description3.answerNumberPreRequise = new List<int>();

        story14answer4Description1.isPreRequiseListAndAndNotOr = false;
        story14answer4Description1.answerNumberPreRequise = new List<int> { 1, 53 };
        story14answer4Description2.answerNumberPreRequise = new List<int> { 1, 53 };
        story14answer4Description3.isPreRequiseNotIn = true;
        story14answer4Description3.answerNumberPreRequise = new List<int> { 1, 53 };
        story14answer4Description4.answerNumberPreRequise = new List<int>();

        story14answer1Description1.description = "Vous avez d�j� vol� des croissants ce matin et �tes habitu� � ce genre de man�uvres.";
        story14answer1Description2.description = "Vous avez h�sit� � voler les croissants ce matin, mais il y avait trop de regards sur vous. Ce soir dans le chaos du bar, vous sentez que c�est votre moment.";
        story14answer1Description3.description = "Vous subtilisez sans probl�me le portefeuille de votre cible et lui agitez sous le nez. Elle plonge ses grands yeux dans les v�tres, choqu�e. Vous parvenez � la convaincre que vous �tes un homme bon, lui ayant rendu son pr�cieux bien.";
        story14answer1Description4.description = "Au passage vous arrivez m�me � lire son nom sur la carte d�identit�. Elle s'appelle Am�lia.";
        story14answer2Description1.description = "Vous lui affirmez que vous �tes le propri�taire du bar. Elle est surprise que vous lui adressiez la parole.";
        story14answer2Description2.description = "Vous vous �tes fait passer pour un repr�sentant de Michelin ce matin alors c�est facile pour vous.";
        story14answer2Description3.description = "Qu�elle vous croit ou non, elle se laisse prendre au jeu, alors que vous lui expliquez une histoire farfelue sur la mani�re dont vous avez chass� vous-m�me le cerf dont la t�te est accroch�e au mur du bar.";
        story14answer3Description1.description = "Vous avez plein d'actions de chez Haribo fra�chement r�cup�r�es ce matin, c�est s�r que �a va l�impressionner.";
        story14answer3Description2.description = "Vous ne manquez pas de thune. Preuve, vous avez laiss� vos croissants sur place � l'h�tel ce matin.";
        story14answer3Description3.description = "Elle regarde votre t�l�phone du coin de l�oeil et semble se figer � la vue du chiffre";
        story14answer4Description1.description = "Vous �tes d�finitivement charismatique et vous sentez que vous pouvez l�impressionner.";
        story14answer4Description2.description = "Apr�s tout, vous �tes une b�te de sex. Ce matin vous �tiez au top devant le miroir et m�me la barmaid n�a pas r�sist� � vos charmes.";
        story14answer4Description3.description = "Vous �tes pas tr�s � l�aise avec votre corps mais il faut un d�but � tout.";
        story14answer4Description4.description = "Vous souriez de vos plus belles dents avant de roucouler et elle se laisse prendre au jeu, rapidement vous vous mettez � discuter d�after shave et de marque de parfum.";

        story14answer1.answerDescriptions.Add(story14answer1Description1);
        story14answer1.answerDescriptions.Add(story14answer1Description2);
        story14answer1.answerDescriptions.Add(story14answer1Description3);
        story14answer1.answerDescriptions.Add(story14answer1Description4);
        story14answer2.answerDescriptions.Add(story14answer2Description1);
        story14answer2.answerDescriptions.Add(story14answer2Description2);
        story14answer2.answerDescriptions.Add(story14answer2Description3);
        story14answer3.answerDescriptions.Add(story14answer3Description1);
        story14answer3.answerDescriptions.Add(story14answer3Description2);
        story14answer3.answerDescriptions.Add(story14answer3Description3);
        story14answer4.answerDescriptions.Add(story14answer4Description1);
        story14answer4.answerDescriptions.Add(story14answer4Description2);
        story14answer4.answerDescriptions.Add(story14answer4Description3);
        story14answer4.answerDescriptions.Add(story14answer4Description4);

        story14.answers.Add(story14answer1);
        story14.answers.Add(story14answer2);
        story14.answers.Add(story14answer3);
        story14.answers.Add(story14answer4);

        stories.listStory.Add(story14);

        //////////////STORY 15 
        ///

        Story story15 = new Story();
        story15.storyNumber = 15;

        StoryDescription story15Description1 = new StoryDescription();
        story15Description1.answerNumberPreRequise = new List<int>();
        story15Description1.description = "Alors que la conversation est lanc�e et que vous sentez qu�elle est int�ress�e, un gentleman s�approche de votre crush du soir. S�excuse aupr�s de vous de vous d�ranger et propose � la femme de venir danser avec lui sur la piste. Il vous lance un regard avec un petit rictus mauvais sur son visage. Vous voulez l�impressioner pour montrer que vous �tes meilleur que lui.";
        story15.storyDescriptions.Add(story15Description1);

        Answer story15answer1 = new Answer();
        Answer story15answer2 = new Answer();
        Answer story15answer3 = new Answer();
        Answer story15answer4 = new Answer();
        story15answer1.attributsBoost = new List<string> { nameof(Attributs.Dexterite) };
        story15answer2.attributsBoost = new List<string> { nameof(Attributs.Force) };
        story15answer3.attributsBoost = new List<string> { nameof(Attributs.Astuce) };
        story15answer4.attributsBoost = new List<string> { nameof(Attributs.Charisme) };
        story15answer1.specialiteBoost = new List<string> { nameof(Specialite.Athletisme) };
        story15answer2.specialiteBoost = new List<string> { nameof(Specialite.Representation) };
        story15answer3.specialiteBoost = new List<string> { nameof(Specialite.Survie) };
        story15answer4.specialiteBoost = new List<string> { nameof(Specialite.Representation) };
        story15answer1.nextStory = 16;
        story15answer2.nextStory = 16;
        story15answer3.nextStory = 16;
        story15answer4.nextStory = 16;
        story15answer1.description = "Alors vous faites tomber votre verre, pour le r�cup�rer in-extremis de mani�re acrobatique avant qu�il ne s��crase au sol.";
        story15answer2.description = "Vous fracassez alors votre verre sur le bar et roulez des pectoraux pour l�impressionner par votre virilit�.";
        story15answer3.description = "Mais vous savez que vous �tes pas de taille face � sa masse musculaire, vous d�cidez de le baratiner.";
        story15answer4.description = "Peut �tre qu�il pourrait craquer pour vous, vous tentez de le s�duire.";

        AnswerDescription story15answer1Description1 = new AnswerDescription();
        AnswerDescription story15answer1Description2 = new AnswerDescription();
        AnswerDescription story15answer1Description3 = new AnswerDescription();
        AnswerDescription story15answer1Description4 = new AnswerDescription();
        AnswerDescription story15answer2Description1 = new AnswerDescription();
        AnswerDescription story15answer2Description2 = new AnswerDescription();
        AnswerDescription story15answer2Description3 = new AnswerDescription();
        AnswerDescription story15answer2Description4 = new AnswerDescription();
        AnswerDescription story15answer2Description5 = new AnswerDescription();
        AnswerDescription story15answer3Description1 = new AnswerDescription();
        AnswerDescription story15answer3Description2 = new AnswerDescription();
        AnswerDescription story15answer3Description3 = new AnswerDescription();
        AnswerDescription story15answer3Description4 = new AnswerDescription();
        AnswerDescription story15answer3Description5 = new AnswerDescription();
        AnswerDescription story15answer4Description1 = new AnswerDescription();
        AnswerDescription story15answer4Description2 = new AnswerDescription();
        AnswerDescription story15answer4Description3 = new AnswerDescription();
        AnswerDescription story15answer4Description4 = new AnswerDescription();

        story15answer1Description1.answerDescriptionNumber = 71;
        story15answer1Description2.answerDescriptionNumber = 71;
        story15answer1Description3.answerDescriptionNumber = 71;
        story15answer1Description4.answerDescriptionNumber = 71;
        story15answer2Description1.answerDescriptionNumber = 72;
        story15answer2Description2.answerDescriptionNumber = 72;
        story15answer2Description3.answerDescriptionNumber = 72;
        story15answer2Description4.answerDescriptionNumber = 72;
        story15answer2Description5.answerDescriptionNumber = 75;
        story15answer3Description1.answerDescriptionNumber = 73;
        story15answer3Description2.answerDescriptionNumber = 73;
        story15answer3Description3.answerDescriptionNumber = 73;
        story15answer3Description4.answerDescriptionNumber = 73;
        story15answer3Description5.answerDescriptionNumber = 73;
        story15answer4Description1.answerDescriptionNumber = 74;
        story15answer4Description2.answerDescriptionNumber = 80;
        story15answer4Description3.answerDescriptionNumber = 79;
        story15answer4Description4.answerDescriptionNumber = 81;

        story15answer1Description1.answerNumberPreRequise = new List<int> { 10 };
        story15answer1Description2.answerNumberPreRequise = new List<int> { 56 };
        story15answer1Description3.isPreRequiseListAndAndNotOr = false;
        story15answer1Description3.answerNumberPreRequise = new List<int> { 10, 56 };
        story15answer1Description4.isPreRequiseNotIn = true;
        story15answer1Description4.answerNumberPreRequise = new List<int> { 10, 56 };

        story15answer2Description1.answerNumberPreRequise = new List<int>();
        story15answer2Description2.answerNumberPreRequise = new List<int> { 9 };
        story15answer2Description3.answerNumberPreRequise = new List<int> { 23 };
        story15answer2Description4.isPreRequiseListAndAndNotOr = false;
        story15answer2Description4.answerNumberPreRequise = new List<int> { 9, 23 };
        story15answer2Description5.isPreRequiseNotIn = true;
        story15answer2Description5.answerNumberPreRequise = new List<int> { 9, 23 };

        story15answer3Description1.answerNumberPreRequise = new List<int>();
        story15answer3Description2.answerNumberPreRequise = new List<int> { 67 };
        story15answer3Description3.answerNumberPreRequise = new List<int> { 68 };
        story15answer3Description4.answerNumberPreRequise = new List<int> { 69 };
        story15answer3Description5.answerNumberPreRequise = new List<int> { 70 };

        story15answer4Description1.answerNumberPreRequise = new List<int>();
        story15answer4Description2.isPreRequiseListAndAndNotOr = false;
        story15answer4Description2.answerNumberPreRequise = new List<int> { 1, 53, 70 };
        story15answer4Description3.answerNumberPreRequise = new List<int> { 1, 53, 70 };
        story15answer4Description4.isPreRequiseNotIn = true;
        story15answer4Description4.answerNumberPreRequise = new List<int> { 1, 53, 70 };

        story15answer1Description1.description = "Vous r�alisez tous les jours vos s�ances de fitness.";
        story15answer1Description2.description = "Vous savez esquiver agilement au dernier des obstacles comme le petit chien tout � l�heure.";
        story15answer1Description3.description = "Ceci n�est rien pour vous, vous jetez votre verre en l�air entre vous et lui. Puis vous tombez de votre chaise, effectuez une roulade bien calibr�e, vous relevez face � face avec lui et r�cup�rez � l�aveugle le verre qui tombe dans votre dos. Le gars est t�tanis�, il n�a jamais vu �a.";
        story15answer1Description4.description = "La Dext�rit� c�est pas votre fort mais vous allez tenter le tout pour le tout. Vous balancez votre verre en l�air. Mais votre mouvement vous d�s�quilibre et vous tombez de votre chaise en vous fracassant au sol. �tal� par terre, vous voyez le verre qui tombe, droit sur la t�te du gars et s'�clate en mille morceaux sur son cr�ne. Vous avez � peine le temps de vous lever pendant que l�autre se tient la t�te, d�finitivement choqu�.";
        story15answer2Description1.description = "Vous �tes bien �m�ch� et vous n�allez pas vous laisser dominer par un autre type.";
        story15answer2Description2.description = "Vous r�alisez tous les jours un entra�nement musculaire intense.";
        story15answer2Description3.description = "Vous avez m�me soulev� du sol le serveur tout � l�heure.";
        story15answer2Description4.description = "Vous explosez sans probl�me le verre sur le comptoir qui part en mille �clats et pointez la base de celui-ci tel un tesson vers votre nouvel ennemi. Celui-ci est t�tanis�.";
        story15answer2Description5.description = "Vous n�avez pas vraiment l�habitude de faire �talage de votre force mais fracasser un verre c�est � la port�e de tous. Vous l�explosez sur le comptoir mais grincez des dents aussit�t. Un tesson de verre s�est coinc� dans la paume de votre main et saigne abondamment. L�autre vous regarde, m�dus�.";
        story15answer3Description1.description = "Vous devez survivre � la situation alors quoi de mieux que du baratin. Vous analysez rapidement la situation et trouvez la r�ponse �vidente. Vous faites intervenir votre crush dans la conversation pour qu�elle s�interpose.";
        story15answer3Description2.description = "Elle vante la mani�re dont vous avez retrouv� son portefeuille et annonce au type qu�elle n�est pas int�ress�e.";
        story15answer3Description3.description = "Elle clame que vous �tes le propri�taire du bar et que vous pouvez sans soucis l�obliger � sortir d�ici si vous appelez les videurs.";
        story15answer3Description4.description = "Elle annonce au type qu�elle ne parle pas aux pauvres et se retourne vers vous les yeux pleins d��motion.";
        story15answer3Description5.description = "Elle s�accroche � votre bras en respirant votre parfum et dit au type qu�il n�est pas � son go�t.";
        story15answer4Description1.description = "Votre crush vous regarde d�un dr�le d�air.";
        story15answer4Description2.description = "Vous savez comment faire avec les gens, mais cette personne n�est pas int�ress�e par les hommes. N�anmoins vous sentez � son regard troubl� que votre action lui a fait quelque chose. Il refoule ses �motions et semble s��nerver.";
        story15answer4Description3.description = "Toutefois personne ne pourrait r�sister � votre charme divin. Vous �tes un dieu, et quand vous posez votre main sur le visage du gentleman, il fond pour vous incapable de tout mouvement.";
        story15answer4Description4.description = "Vous n'avez jamais vraiment tent� de s�duire, mais vous vous dites pourquoi pas. Vous approchez l�homme et posez votre main sur la joue. Il �carquille les yeux de d�go�t et se pr�pare � vous frapper.";

        story15answer1.answerDescriptions.Add(story15answer1Description1);
        story15answer1.answerDescriptions.Add(story15answer1Description2);
        story15answer1.answerDescriptions.Add(story15answer1Description3);
        story15answer1.answerDescriptions.Add(story15answer1Description4);
        story15answer2.answerDescriptions.Add(story15answer2Description1);
        story15answer2.answerDescriptions.Add(story15answer2Description2);
        story15answer2.answerDescriptions.Add(story15answer2Description3);
        story15answer2.answerDescriptions.Add(story15answer2Description4);
        story15answer2.answerDescriptions.Add(story15answer2Description5);
        story15answer3.answerDescriptions.Add(story15answer3Description1);
        story15answer3.answerDescriptions.Add(story15answer3Description2);
        story15answer3.answerDescriptions.Add(story15answer3Description3);
        story15answer3.answerDescriptions.Add(story15answer3Description4);
        story15answer3.answerDescriptions.Add(story15answer3Description5);
        story15answer4.answerDescriptions.Add(story15answer4Description1);
        story15answer4.answerDescriptions.Add(story15answer4Description2);
        story15answer4.answerDescriptions.Add(story15answer4Description3);
        story15answer4.answerDescriptions.Add(story15answer4Description4);

        story15.answers.Add(story15answer1);
        story15.answers.Add(story15answer2);
        story15.answers.Add(story15answer3);
        story15.answers.Add(story15answer4);

        stories.listStory.Add(story15);


        //////////////STORY 16 
        ///


        Story story16 = new Story();
        story16.storyNumber = 16;

        StoryDescription story16Description1 = new StoryDescription();
        story16Description1.answerNumberPreRequise = new List<int>();
        story16Description1.description = "Vous d�cidez donc de prendre l�initiative et de finir cette situation grotesque. Apr�s tout vous voulez garder votre crush du soir pour vous alors :";
        story16.storyDescriptions.Add(story16Description1);

        Answer story16answer1 = new Answer();
        Answer story16answer2 = new Answer();
        Answer story16answer3 = new Answer();
        story16answer1.attributsBoost = new List<string> { nameof(Attributs.Force) };
        story16answer2.attributsBoost = new List<string> { nameof(Attributs.Dexterite) };
        story16answer3.attributsBoost = new List<string> { nameof(Attributs.Intelligence) };
        story16answer1.specialiteBoost = new List<string>();
        story16answer2.specialiteBoost = new List<string>();
        story16answer3.specialiteBoost = new List<string> { nameof(Specialite.Survie) };
        story16answer1.nextStory = 17;
        story16answer2.nextStory = 18;
        story16answer3.nextStory = 19;
        story16answer1.description = "Vous rentrez dans le lard du type.";
        story16answer2.description = "Vous le poussez en arri�re et vous pr�parez � esquiver pour lui placer une contre attaque.";
        story16answer3.description = "Vous saisissez votre crush par le bras et d�cidez de partir en plantant le gars.";

        AnswerDescription story16answer1Description1 = new AnswerDescription();
        AnswerDescription story16answer2Description1 = new AnswerDescription();
        AnswerDescription story16answer3Description1 = new AnswerDescription();

        story16answer1Description1.answerDescriptionNumber = 82;
        story16answer2Description1.answerDescriptionNumber = 83;
        story16answer3Description1.answerDescriptionNumber = 84;

        story16answer1Description1.answerNumberPreRequise = new List<int>();
        story16answer2Description1.answerNumberPreRequise = new List<int>();
        story16answer3Description1.answerNumberPreRequise = new List<int>();

        story16answer1Description1.description = "";
        story16answer2Description1.description = "";
        story16answer3Description1.description = "Le type ne comprend pas ce qu�il se passe. Il vous regarde partir dans un coin du bar avec votre rencontre du soir. Il dispara�t de votre vue.";

        story16answer1.answerDescriptions.Add(story16answer1Description1);
        story16answer2.answerDescriptions.Add(story16answer2Description1);
        story16answer3.answerDescriptions.Add(story16answer3Description1);

        story16.answers.Add(story16answer1);
        story16.answers.Add(story16answer2);
        story16.answers.Add(story16answer3);

        stories.listStory.Add(story16);

        //////////////STORY 17
        ///

        Story story17 = new Story();
        story17.storyNumber = 17;

        StoryDescription story17Description1 = new StoryDescription();
        story17Description1.answerNumberPreRequise = new List<int>();
        story17Description1.description = "D�un bond vous �tes sur lui et vous le d�marrez.";
        story17.storyDescriptions.Add(story17Description1);

        Answer story17answer1 = new Answer();
        Answer story17answer2 = new Answer();
        story17answer1.attributsBoost = new List<string>();
        story17answer2.attributsBoost = new List<string>();
        story17answer1.specialiteBoost = new List<string> { nameof(Specialite.Melee) };
        story17answer2.specialiteBoost = new List<string> { nameof(Specialite.Bagarre) };
        story17answer1.nextStory = 19;
        story17answer2.nextStory = 19;
        story17answer1.description = "Vous avisez le tabouret sur lequel vous �tes assis et le saisissez pour le fracasser sur le type.";
        story17answer2.description = "Vous lui rentrez dedans � main nue.";

        AnswerDescription story17answer1Description1 = new AnswerDescription();
        AnswerDescription story17answer2Description1 = new AnswerDescription();

        story17answer1Description1.answerDescriptionNumber = 85;
        story17answer2Description1.answerDescriptionNumber = 86;

        story17answer1Description1.answerNumberPreRequise = new List<int>();
        story17answer2Description1.answerNumberPreRequise = new List<int>();

        story17answer1Description1.description = "Vous saisissez le tabouret et vous vous en servez comme d�une masse d'armes en lui explosant litt�ralement sur la face. Celui-ci se brise en m�me temps que le type s'effondre dans un bruit de craquement sourd. Vous prenez votre crush par le bras et vous en allez avec elle en laissant votre victime au sol.";
        story17answer2Description1.description = "Il n�a pas le temps d�esquiver, votre poing s��crase sur sa m�choire et vous l�entendez craquer. Votre victime est soulev�e du sol sur quelques centim�tres et s��crase sur les fesses, du sang sortant de la bouche. Vous le laissez l�, prenant votre crush par le bras pour vous en aller avec elle.";

        story17answer1.answerDescriptions.Add(story17answer1Description1);
        story17answer2.answerDescriptions.Add(story17answer2Description1);

        story17.answers.Add(story17answer1);
        story17.answers.Add(story17answer2);

        stories.listStory.Add(story17);

        //////////////STORY 18
        ///

        Story story18 = new Story();
        story18.storyNumber = 18;

        StoryDescription story18Description1 = new StoryDescription();
        story18Description1.answerNumberPreRequise = new List<int>();
        story18Description1.description = "Il s'�nerve apr�s vous et vous rentre dedans, mais vous esquivez d�un pas sur le c�t� avant de contre attaquer.";
        story18.storyDescriptions.Add(story18Description1);

        Answer story18answer1 = new Answer();
        Answer story18answer2 = new Answer();
        story18answer1.attributsBoost = new List<string>();
        story18answer2.attributsBoost = new List<string>();
        story18answer1.specialiteBoost = new List<string> { nameof(Specialite.Melee) };
        story18answer2.specialiteBoost = new List<string> { nameof(Specialite.Bagarre) };
        story18answer1.nextStory = 19;
        story18answer2.nextStory = 19;
        story18answer1.description = "Vous avisez le tabouret sur lequel vous �tes assis et le saisissez pour le fracasser sur le type.";
        story18answer2.description = "Vous lui rentrez dedans � main nue.";

        AnswerDescription story18answer1Description1 = new AnswerDescription();
        AnswerDescription story18answer2Description1 = new AnswerDescription();

        story18answer1Description1.answerDescriptionNumber = 85;
        story18answer2Description1.answerDescriptionNumber = 86;

        story18answer1Description1.answerNumberPreRequise = new List<int>();
        story18answer2Description1.answerNumberPreRequise = new List<int>();

        story18answer1Description1.description = "Vous saisissez le tabouret et vous vous en servez comme d�une masse d'armes en lui explosant litt�ralement sur la face. Celui-ci se brise en m�me temps que le type s'effondre dans un bruit de craquement sourd. Vous prenez votre crush par le bras et vous en allez avec elle en laissant votre victime au sol.";
        story18answer2Description1.description = "Il n�a pas le temps d�esquiver, votre poing s��crase sur sa m�choire et vous l�entendez craquer. Votre victime est soulev�e du sol sur quelques centim�tres et s��crase sur les fesses, du sang sortant de la bouche. Vous le laissez l�, prenant votre crush par le bras pour vous en aller avec elle.";

        story18answer1.answerDescriptions.Add(story18answer1Description1);
        story18answer2.answerDescriptions.Add(story18answer2Description1);

        story18.answers.Add(story18answer1);
        story18.answers.Add(story18answer2);

        stories.listStory.Add(story18);

        //////////////STORY 19
        ///

        Story story19 = new Story();
        story19.storyNumber = 19;

        StoryDescription story19Description1 = new StoryDescription();
        story19Description1.answerNumberPreRequise = new List<int>();
        story19Description1.description = "La musique r�sonne fort et les stroboscopes flashent autour de vous. Vous conduisez votre crush dans un coin isol� du bar. Elle vous regarde dans les yeux";
        story19.storyDescriptions.Add(story19Description1);
        StoryDescription story19Description2 = new StoryDescription();
        story19Description2.answerNumberPreRequise = new List<int> { 82 };
        story19Description2.description = "et vous complimente sur votre agressivit�. Elle trouve votre impulsivit� terriblement virile.";
        story19.storyDescriptions.Add(story19Description2);
        StoryDescription story19Description3 = new StoryDescription();
        story19Description3.answerNumberPreRequise = new List<int> { 83 };
        story19Description3.description = "et vous complimente sur votre agilit�. La mani�re dont vous avez esquiv� le coup pour en redonner un derri�re l�a �pat�.";
        story19.storyDescriptions.Add(story19Description3);
        StoryDescription story19Description4 = new StoryDescription();
        story19Description4.answerNumberPreRequise = new List<int> { 84 };
        story19Description4.description = "et vous complimente sur votre magnanimit�. Elle trouve �galement que combattre dans un bar au milieu de tout le monde n�est pas digne de vous et que vous avez bien fait de vous en aller.";
        story19.storyDescriptions.Add(story19Description4);
        StoryDescription story19Description5 = new StoryDescription();
        story19Description5.answerNumberPreRequise = new List<int>();
        story19Description5.description = "Mais alors que vous vous asseyez tous deux sur des fauteuils � l�arri�re de la piste de danse et qu�elle vous prend les mains dans les siennes vous avez une sensation �trange";
        story19.storyDescriptions.Add(story19Description5);


        Answer story19answer1 = new Answer();
        Answer story19answer2 = new Answer();
        Answer story19answer3 = new Answer();
        story19answer1.attributsBoost = new List<string> { nameof(Attributs.Resolution) };
        story19answer2.attributsBoost = new List<string> { nameof(Attributs.SangFroid) };
        story19answer3.attributsBoost = new List<string> { nameof(Attributs.Vigueur) };
        story19answer1.specialiteBoost = new List<string> { nameof(Specialite.Investigation) };
        story19answer2.specialiteBoost = new List<string> { nameof(Specialite.Etiquette) };
        story19answer3.specialiteBoost = new List<string> { nameof(Specialite.Vigilance) };
        story19answer1.nextStory = 20;
        story19answer2.nextStory = 21;
        story19answer3.nextStory = 21;
        story19answer1.description = "Vous tentez de creuser cette sensation et savoir d�o� elle provient quitte � �tre moins pr�sent dans la conversation.";
        story19answer2.description = "Vous gardez votre sang froid et portez toute votre attention sur les mots de la jeune femme pour ne pas la decevoir.";
        story19answer3.description = "Vous serez vigoureusement les mains de votre crush dans les v�tres en restant � l��coute de cette �motion �trange.";

        AnswerDescription story19answer1Description1 = new AnswerDescription();
        AnswerDescription story19answer2Description1 = new AnswerDescription();
        AnswerDescription story19answer3Description1 = new AnswerDescription();

        story19answer1Description1.answerDescriptionNumber = 87;
        story19answer2Description1.answerDescriptionNumber = 88;
        story19answer3Description1.answerDescriptionNumber = 89;

        story19answer1Description1.answerNumberPreRequise = new List<int>();
        story19answer2Description1.answerNumberPreRequise = new List<int>();
        story19answer3Description1.answerNumberPreRequise = new List<int>();

        story19answer1Description1.description = "";
        story19answer2Description1.description = "";
        story19answer3Description1.description = "";

        story19answer1.answerDescriptions.Add(story19answer1Description1);
        story19answer2.answerDescriptions.Add(story19answer2Description1);
        story19answer3.answerDescriptions.Add(story19answer3Description1);

        story19.answers.Add(story19answer1);
        story19.answers.Add(story19answer2);
        story19.answers.Add(story19answer3);

        stories.listStory.Add(story19);

        //  STORY 20

        Story story20 = new Story();
        story20.storyNumber = 20;

        StoryDescription story20Description1 = new StoryDescription();
        story20Description1.answerNumberPreRequise = new List<int>();
        story20Description1.description = "Vous avez froid aux mains, et tentez de comprendre ce qu�il se passe, heureusement vous avez de solides connaissances :";
        story20.storyDescriptions.Add(story20Description1);

        Answer story20answer1 = new Answer();
        Answer story20answer2 = new Answer();
        Answer story20answer3 = new Answer();
        Answer story20answer4 = new Answer();
        story20answer1.attributsBoost = new List<string>();
        story20answer2.attributsBoost = new List<string>();
        story20answer3.attributsBoost = new List<string>();
        story20answer4.attributsBoost = new List<string>();
        story20answer1.specialiteBoost = new List<string> { nameof(Specialite.Medecine) };
        story20answer2.specialiteBoost = new List<string> { nameof(Specialite.Science) };
        story20answer3.specialiteBoost = new List<string> { nameof(Specialite.Occultisme) };
        story20answer4.specialiteBoost = new List<string> { nameof(Specialite.ExperienceDeLaRue) };
        story20answer1.nextStory = 21;
        story20answer2.nextStory = 21;
        story20answer3.nextStory = 21;
        story20answer4.nextStory = 21;
        story20answer1.description = "Dans le corps humain, et avez touch� un peu � la m�decine.";
        story20answer2.description = "En thermodynamique, la science �a vous conna�t.";
        story20answer3.description = "En occultisme en sciences �sot�riques.";
        story20answer4.description = "Avec les femmes, vous ne comptez plus les prostitu�es r�cup�r� � m�me la rue, ou vos proies chass�es dans les bars comme ce soir.";

        AnswerDescription story20answer1Description1 = new AnswerDescription();
        AnswerDescription story20answer2Description1 = new AnswerDescription();
        AnswerDescription story20answer3Description1 = new AnswerDescription();
        AnswerDescription story20answer4Description1 = new AnswerDescription();

        story20answer1Description1.answerDescriptionNumber = 90;
        story20answer2Description1.answerDescriptionNumber = 91;
        story20answer3Description1.answerDescriptionNumber = 92;
        story20answer4Description1.answerDescriptionNumber = 93;

        story20answer1Description1.answerNumberPreRequise = new List<int>();
        story20answer2Description1.answerNumberPreRequise = new List<int>();
        story20answer3Description1.answerNumberPreRequise = new List<int>();
        story20answer4Description1.answerNumberPreRequise = new List<int>();

        story20answer1Description1.description = "Vous constatez ainsi avec surprise que ses mains sont rigides, presque cadav�riques et �a, c�est pas normal du tout.";
        story20answer2Description1.description = "La loi de l�entropie veut que la chaleur du corps humain se diffuse aussi dans les mains. Hors ses mains sont gel�es, et �a c�est pas normal du tout.";
        story20answer3Description1.description = "Vous avez lu des saga enti�res sur les morts vivants. Vous ne savez pas trop pourquoi mais dans l�instant, ses mains glaciales et son regard vorace vous fait penser � celui d�un zombi et �a vous fait flipper.";
        story20answer4Description1.description = "";

        story20answer1.answerDescriptions.Add(story20answer1Description1);
        story20answer2.answerDescriptions.Add(story20answer2Description1);
        story20answer3.answerDescriptions.Add(story20answer3Description1);
        story20answer4.answerDescriptions.Add(story20answer4Description1);

        story20.answers.Add(story20answer1);
        story20.answers.Add(story20answer2);
        story20.answers.Add(story20answer3);
        story20.answers.Add(story20answer4);

        stories.listStory.Add(story20);

        //////STORY 21
        ///
        Story story21 = new Story();
        story21.storyNumber = 21;

        StoryDescription story21Description1 = new StoryDescription();
        story21Description1.answerNumberPreRequise = new List<int>();
        story21Description1.description = "A mesure que la conversation avance, vous sentez que votre capacit� � penser et raisonner dispara�t peu � peu. Vous ne savez qu�une chose, ses grands yeux voraces vous d�vorent et vous br�lez de d�sir pour elle. Vous la voulez rien qu�� vous et vous mettez �a sous le coup de l�alcool. Vous tentez alors dans un dernier �lan de lucidit� :";
        story21.storyDescriptions.Add(story21Description1);

        Answer story21answer1 = new Answer();
        Answer story21answer2 = new Answer();
        Answer story21answer3 = new Answer();
        Answer story21answer4 = new Answer();
        story21answer1.attributsBoost = new List<string> { nameof(Attributs.Charisme) };
        story21answer2.attributsBoost = new List<string> { nameof(Attributs.Vigueur) };
        story21answer3.attributsBoost = new List<string> { nameof(Attributs.Resolution) };
        story21answer4.attributsBoost = new List<string> { nameof(Attributs.SangFroid) };
        story21answer1.specialiteBoost = new List<string> { nameof(Specialite.Representation) };
        story21answer2.specialiteBoost = new List<string> { nameof(Specialite.Survie) };
        story21answer3.specialiteBoost = new List<string> { nameof(Specialite.Investigation) };
        story21answer4.specialiteBoost = new List<string> { nameof(Specialite.Politique) };
        story21answer1.nextStory = 22;
        story21answer2.nextStory = 22;
        story21answer3.nextStory = 22;
        story21answer4.nextStory = 22;
        story21answer1.description = "De la s�duire de votre plus beau sourire.";
        story21answer2.description = "De vous enfuir de force cette situation �trange.";
        story21answer3.description = "De r�fl�chir � ce que vous faites l�.";
        story21answer4.description = "De lui annoncer tel un diplomate que vous vous sentez mal et que vous devriez songer � rentrer.";

        AnswerDescription story21answer1Description1 = new AnswerDescription();
        AnswerDescription story21answer2Description1 = new AnswerDescription();
        AnswerDescription story21answer3Description1 = new AnswerDescription();
        AnswerDescription story21answer4Description1 = new AnswerDescription();

        story21answer1Description1.answerDescriptionNumber = 94;
        story21answer2Description1.answerDescriptionNumber = 95;
        story21answer3Description1.answerDescriptionNumber = 96;
        story21answer4Description1.answerDescriptionNumber = 97;

        story21answer1Description1.answerNumberPreRequise = new List<int>();
        story21answer2Description1.answerNumberPreRequise = new List<int>();
        story21answer3Description1.answerNumberPreRequise = new List<int>();
        story21answer4Description1.answerNumberPreRequise = new List<int>();

        story21answer1Description1.description = "Et cela fonctionne � la perfection.";
        story21answer2Description1.description = "Vous voulez vous lever mais vos mains restent bloqu�es dans les siennes et alors que vous tentez de bander vos muscles cela reste sans effet. Vous ne parvenez pas � vous d�gager de sa poigne surnaturelle.";
        story21answer3Description1.description = "Vous vous rem�morez les derniers �v�nements en date, comment �tes vous d�j� arriv� dans ce bar d�j� ? Vos souvenirs sont si confus.";
        story21answer4Description1.description = "Votre crush en rigole doucement et son rire �clatant vous transcende. Vous sentez toute votre combativit� partir � mesure que ses l�vres se rapprochent des v�tres.";

        story21answer1.answerDescriptions.Add(story21answer1Description1);
        story21answer2.answerDescriptions.Add(story21answer2Description1);
        story21answer3.answerDescriptions.Add(story21answer3Description1);
        story21answer4.answerDescriptions.Add(story21answer4Description1);

        story21.answers.Add(story21answer1);
        story21.answers.Add(story21answer2);
        story21.answers.Add(story21answer3);
        story21.answers.Add(story21answer4);

        stories.listStory.Add(story21);

        /////STORY 22
        ///
        Story story22 = new Story();
        story22.storyNumber = 22;

        StoryDescription story22Description1 = new StoryDescription();
        story22Description1.answerNumberPreRequise = new List<int>();
        story22Description1.description = "La jeune femme se penche vers vous et commence � vous embrasser goul�ment. Son parfum vous enivre, et bient�t ses l�vres quittent les votre pour s�attaquer � votre joue. Elle vous mordille doucement l�oreille avant de descendre. Sa bouche se pose sur votre cou, et vous sentez sa langue vous caresser la peau.Vos mains s'agrippent � ses cheveux mais bient�t vous �tes transperc� d�une sensation extraordinaire avant d��tre emport� par ce tourbillon de plaisir, une derni�re pens�e vous traverse l�esprit.";
        story22.storyDescriptions.Add(story22Description1);

        Answer story22answer1 = new Answer();
        Answer story22answer2 = new Answer();
        Answer story22answer3 = new Answer();
        Answer story22answer4 = new Answer();
        Answer story22answer5 = new Answer();
        Answer story22answer6 = new Answer();
        story22answer1.attributsBoost = new List<string>();
        story22answer2.attributsBoost = new List<string>();
        story22answer3.attributsBoost = new List<string>();
        story22answer4.attributsBoost = new List<string>();
        story22answer5.attributsBoost = new List<string> ();
        story22answer6.attributsBoost = new List<string> { nameof(Attributs.Intelligence) };
        story22answer1.specialiteBoost = new List<string> { nameof(Specialite.ExperienceDeLaRue), nameof(Specialite.Medecine) };
        story22answer2.specialiteBoost = new List<string> { nameof(Specialite.Conduite), nameof(Specialite.Athletisme) };
        story22answer3.specialiteBoost = new List<string> { nameof(Specialite.ArmesAfeu), nameof(Specialite.Technologie) };
        story22answer4.specialiteBoost = new List<string> { nameof(Specialite.Occultisme), nameof(Specialite.Erudition) };
        story22answer5.specialiteBoost = new List<string> { nameof(Specialite.Science), nameof(Specialite.Artisanat) };
        story22answer6.specialiteBoost = new List<string> { nameof(Specialite.Furtivite) };
        story22answer1.nextStory = 23;
        story22answer2.nextStory = 23;
        story22answer3.nextStory = 23;
        story22answer4.nextStory = 23;
        story22answer5.nextStory = 23;
        story22answer6.nextStory = 23;
        story22answer1.description = "C�est encore mieux que l�h�ro�ne que vous aviez achet� une fois � un dealer dans la rue et que vous vous �tiez inject�e.";
        story22answer2.description = "Cette sensation vous fait penser � la fois o� vous avez conduit une porsche sur un circuit de course, en beaucoup plus violent.";
        story22answer3.description = "Ce sentiment fulgurant est celui que vous ressentez lorsque vous partez chasser le gibier au fusil et sentez la vie s'�chapper de votre proie.";
        story22answer4.description = "Vous �tes transit, cette situation est la m�me que celle d�crite dans un de vos livres occultes sur les vampires.";
        story22answer5.description = "Dans votre jeunesse vous avez fabriqu� votre propre m�thamph�tamine et avez ador� �a. Mais le plaisir actuel est incomparable.";
        story22answer6.description = "Vous �tes terroris� des cons�quences de ce qui vous arrive et aimeriez dispara�tre";

        AnswerDescription story22answer1Description1 = new AnswerDescription();
        AnswerDescription story22answer2Description1 = new AnswerDescription();
        AnswerDescription story22answer3Description1 = new AnswerDescription();
        AnswerDescription story22answer4Description1 = new AnswerDescription();
        AnswerDescription story22answer5Description1 = new AnswerDescription();
        AnswerDescription story22answer6Description1 = new AnswerDescription();

        story22answer1Description1.answerDescriptionNumber = 98;
        story22answer2Description1.answerDescriptionNumber = 99;
        story22answer3Description1.answerDescriptionNumber = 100;
        story22answer4Description1.answerDescriptionNumber = 101;
        story22answer5Description1.answerDescriptionNumber = 102;
        story22answer6Description1.answerDescriptionNumber = 103;

        story22answer1Description1.answerNumberPreRequise = new List<int>();
        story22answer2Description1.answerNumberPreRequise = new List<int>();
        story22answer3Description1.answerNumberPreRequise = new List<int>();
        story22answer4Description1.answerNumberPreRequise = new List<int>();
        story22answer5Description1.answerNumberPreRequise = new List<int>();
        story22answer6Description1.answerNumberPreRequise = new List<int>();

        story22answer1Description1.description = "";
        story22answer2Description1.description = "";
        story22answer3Description1.description = "";
        story22answer4Description1.description = "";
        story22answer5Description1.description = "";
        story22answer6Description1.description = "";


        story22answer1.answerDescriptions.Add(story22answer1Description1);
        story22answer2.answerDescriptions.Add(story22answer2Description1);
        story22answer3.answerDescriptions.Add(story22answer3Description1);
        story22answer4.answerDescriptions.Add(story22answer4Description1);
        story22answer5.answerDescriptions.Add(story22answer5Description1);
        story22answer6.answerDescriptions.Add(story22answer6Description1);

        story22.answers.Add(story22answer1);
        story22.answers.Add(story22answer2);
        story22.answers.Add(story22answer3);
        story22.answers.Add(story22answer4);
        story22.answers.Add(story22answer5);
        story22.answers.Add(story22answer6);

        stories.listStory.Add(story22);

        ///STORY 23
        ///
        Story story23 = new Story();
        story23.storyNumber = 23;

        StoryDescription story23Description1 = new StoryDescription();
        story23Description1.answerNumberPreRequise = new List<int>();
        story23Description1.description = "Vous vous r�veillez subitement, allong�s dans un lit que vous ne connaissez pas, dans une vaste chambre. La premi�re chose qui vous vient � l�esprit est :";
        story23.storyDescriptions.Add(story23Description1);

        Answer story23answer1 = new Answer();
        Answer story23answer2 = new Answer();
        Answer story23answer3 = new Answer();
        Answer story23answer4 = new Answer();
        story23answer1.attributsBoost = new List<string>();
        story23answer2.attributsBoost = new List<string>();
        story23answer3.attributsBoost = new List<string>();
        story23answer4.attributsBoost = new List<string>();
        story23answer1.specialiteBoost = new List<string> { nameof(Specialite.Animaux) };
        story23answer2.specialiteBoost = new List<string> { nameof(Specialite.Survie) };
        story23answer3.specialiteBoost = new List<string> { nameof(Specialite.Investigation), nameof(Specialite.Vigilance) };
        story23answer4.specialiteBoost = new List<string> { nameof(Specialite.Investigation), nameof(Specialite.Erudition) };
        story23answer1.nextStory = 25;
        story23answer2.nextStory = 25;
        story23answer3.nextStory = 25;
        story23answer4.nextStory = 24;
        story23answer1.description = "Que ce chat en face de vous est vraiment beau";
        story23answer2.description = "De songer � comment vous barrer d�ici.";
        story23answer3.description = "De glaner plus d'informations sur l�endroit o� vous vous trouvez en regardant autour de vous";
        story23answer4.description = "De vous concentrer sur vos souvenirs et connaissances pour tenter d�y voir plus clair.";

        AnswerDescription story23answer1Description1 = new AnswerDescription();
        AnswerDescription story23answer2Description1 = new AnswerDescription();
        AnswerDescription story23answer3Description1 = new AnswerDescription();
        AnswerDescription story23answer4Description1 = new AnswerDescription();

        story23answer1Description1.answerDescriptionNumber = 106;
        story23answer2Description1.answerDescriptionNumber = 107;
        story23answer3Description1.answerDescriptionNumber = 108;
        story23answer4Description1.answerDescriptionNumber = 109;

        story23answer1Description1.answerNumberPreRequise = new List<int>();
        story23answer2Description1.answerNumberPreRequise = new List<int>();
        story23answer3Description1.answerNumberPreRequise = new List<int>();
        story23answer4Description1.answerNumberPreRequise = new List<int>();

        story23answer1Description1.description = "Pos� sur un canap�, un grand chat blanc vous regarde de ses yeux per�ants et il vous semble que vous pourriez presque le comprendre.";
        story23answer2Description1.description = "Et cela s�av�re compliqu�. Vous regardez autour de vous et ne voyez aucune fen�tre.";
        story23answer3Description1.description = "Un chat tr�ne sur un canap� en face de vous. Un feu cr�pite dans une chemin�e, l�ambiance victorienne de la grande pi�ce bien meuble vous indique que son propri�taire est ais� financi�rement.";
        story23answer4Description1.description = "";

        story23answer1.answerDescriptions.Add(story23answer1Description1);
        story23answer2.answerDescriptions.Add(story23answer2Description1);
        story23answer3.answerDescriptions.Add(story23answer3Description1);
        story23answer4.answerDescriptions.Add(story23answer4Description1);

        story23.answers.Add(story23answer1);
        story23.answers.Add(story23answer2);
        story23.answers.Add(story23answer3);
        story23.answers.Add(story23answer4);

        stories.listStory.Add(story23);

        ///STORY 24
        ///
        Story story24 = new Story();
        story24.storyNumber = 24;

        StoryDescription story24Description1 = new StoryDescription();
        story24Description1.answerNumberPreRequise = new List<int>();
        story24Description1.description = "Vous froncez les sourcils et faites appel � vos connaissances en :";
        story24.storyDescriptions.Add(story24Description1);

        Answer story24answer1 = new Answer();
        Answer story24answer2 = new Answer();
        Answer story24answer3 = new Answer();
        story24answer1.attributsBoost = new List<string>();
        story24answer2.attributsBoost = new List<string>();
        story24answer3.attributsBoost = new List<string>();
        story24answer1.specialiteBoost = new List<string> { nameof(Specialite.Science) };
        story24answer2.specialiteBoost = new List<string> { nameof(Specialite.Occultisme) };
        story24answer3.specialiteBoost = new List<string> { nameof(Specialite.Medecine) };
        story24answer1.nextStory = 25;
        story24answer2.nextStory = 25;
        story24answer3.nextStory = 25;
        story24answer1.description = "Science.";
        story24answer2.description = "Occultisme.";
        story24answer3.description = "Medecine.";

        AnswerDescription story24answer1Description1 = new AnswerDescription();
        AnswerDescription story24answer2Description1 = new AnswerDescription();
        AnswerDescription story24answer3Description1 = new AnswerDescription();

        story24answer1Description1.answerDescriptionNumber = 110;
        story24answer2Description1.answerDescriptionNumber = 111;
        story24answer3Description1.answerDescriptionNumber = 112;

        story24answer1Description1.answerNumberPreRequise = new List<int>();
        story24answer2Description1.answerNumberPreRequise = new List<int>();
        story24answer3Description1.answerNumberPreRequise = new List<int>();

        story24answer1Description1.description = "Et rien ne vous vient � l�esprit. Pourtant vous en connaissez des substances de vos cours de chimie, mais rien qui ne puisse vous plonger dans l��tat actuel.";
        story24answer2Description1.description = "C�est bien �a. C��tait dans un document intitul� le Livre de Nod. Vous avez appris l�existence de cr�atures surnaturelles et ce qui vous est arriv� hier soir vous aide � faire un lien.";
        story24answer3Description1.description = "Aucun m�dicament de votre connaissance ne procure un tel plaisir soudain sans avoir de contrecoup. Vous passez votre main sur notre nuque mais n�y sentez rien.";

        story24answer1.answerDescriptions.Add(story24answer1Description1);
        story24answer2.answerDescriptions.Add(story24answer2Description1);
        story24answer3.answerDescriptions.Add(story24answer3Description1);

        story24.answers.Add(story24answer1);
        story24.answers.Add(story24answer2);
        story24.answers.Add(story24answer3);

        stories.listStory.Add(story24);
        ///STORY 25
        ///
        Story story25 = new Story();
        story25.storyNumber = 25;

        StoryDescription story25Description1 = new StoryDescription();
        story25Description1.answerNumberPreRequise = new List<int>();
        story25Description1.description = "Brusquement la porte claque et la jeune femme de la veille vous apparait dans le plus simple des appareils. Elle s�approche de vous, alors que vous �tes encore allong� dans un grand lit et approche de vos l�vres un calice rempli d�un liquide rouge�tre. L�odeur vous prend au nez et instinctivement vous y buvez � grandes gorg�es. Votre regard se pose alors sur votre h�te assise au bord du lit et vous:";
        story25.storyDescriptions.Add(story25Description1);

        Answer story25answer1 = new Answer();
        Answer story25answer2 = new Answer();
        Answer story25answer3 = new Answer();
        Answer story25answer4 = new Answer();
        story25answer1.attributsBoost = new List<string> { nameof(Attributs.Manipulation) };
        story25answer2.attributsBoost = new List<string> { nameof(Attributs.Charisme) };
        story25answer3.attributsBoost = new List<string>();
        story25answer4.attributsBoost = new List<string> { nameof(Attributs.SangFroid), nameof(Attributs.Resolution)};
        story25answer1.specialiteBoost = new List<string>();
        story25answer2.specialiteBoost = new List<string> { nameof(Specialite.Representation) };
        story25answer3.specialiteBoost = new List<string>();
        story25answer4.specialiteBoost = new List<string>();
        story25answer1.nextStory = 26;
        story25answer2.nextStory = 28;
        story25answer3.nextStory = 27;
        story25answer4.nextStory = 28;
        story25answer1.description = "allez user d�une technique pour qu�elle vous explique ce qu�il se passe";
        story25answer2.description = "d�cidez de reprendre le jeu du s�ducteur pour glaner des infos";
        story25answer3.description = "lui sautez dessus pour la ma�triser et vous mettre en position de force.";
        story25answer4.description = "faire appel � votre volont� pour communiquer avec elle en conservant votre sang froid comme toute personne sens�e.";

        AnswerDescription story25answer1Description1 = new AnswerDescription();
        AnswerDescription story25answer2Description1 = new AnswerDescription();
        AnswerDescription story25answer3Description1 = new AnswerDescription();
        AnswerDescription story25answer4Description1 = new AnswerDescription();

        story25answer1Description1.answerDescriptionNumber = 113;
        story25answer2Description1.answerDescriptionNumber = 114;
        story25answer3Description1.answerDescriptionNumber = 115;
        story25answer4Description1.answerDescriptionNumber = 116;

        story25answer1Description1.answerNumberPreRequise = new List<int>();
        story25answer2Description1.answerNumberPreRequise = new List<int>();
        story25answer3Description1.answerNumberPreRequise = new List<int>();
        story25answer4Description1.answerNumberPreRequise = new List<int>();

        story25answer1Description1.description = "";
        story25answer2Description1.description = "";
        story25answer3Description1.description = "";
        story25answer4Description1.description = "";

        story25answer1.answerDescriptions.Add(story25answer1Description1);
        story25answer2.answerDescriptions.Add(story25answer2Description1);
        story25answer3.answerDescriptions.Add(story25answer3Description1);
        story25answer4.answerDescriptions.Add(story25answer4Description1);

        story25.answers.Add(story25answer1);
        story25.answers.Add(story25answer2);
        story25.answers.Add(story25answer3);
        story25.answers.Add(story25answer4);

        stories.listStory.Add(story25);

        ////STORY 26
        ///
        Story story26 = new Story();
        story26.storyNumber = 26;

        StoryDescription story26Description1 = new StoryDescription();
        story26Description1.answerNumberPreRequise = new List<int>();
        story26Description1.description = "Vous plongez votre regard dans le sien et tentez de manipuler la jeune femme.";
        story26.storyDescriptions.Add(story26Description1);

        Answer story26answer1 = new Answer();
        Answer story26answer2 = new Answer();
        Answer story26answer3 = new Answer();
        Answer story26answer4 = new Answer();
        story26answer1.attributsBoost = new List<string>();
        story26answer2.attributsBoost = new List<string>();
        story26answer3.attributsBoost = new List<string>();
        story26answer4.attributsBoost = new List<string>();
        story26answer1.specialiteBoost = new List<string> { nameof(Specialite.Commandement) };
        story26answer2.specialiteBoost = new List<string> { nameof(Specialite.Persuasion) };
        story26answer3.specialiteBoost = new List<string> { nameof(Specialite.Intimidation) };
        story26answer4.specialiteBoost = new List<string> { nameof(Specialite.Subterfuge) };
        story26answer1.nextStory = 28;
        story26answer2.nextStory = 28;
        story26answer3.nextStory = 28;
        story26answer4.nextStory = 28;
        story26answer1.description = "Vous lui ordonnez s�chement de tout vous expliquer.";
        story26answer2.description = "Vous lui expliquez qu�il est dans son int�r�t qu�elle vous raconte tout.";
        story26answer3.description = "Vous l�intimidez en lui annon�ant qu�elle le regrettera si elle ne vous dit pas tout.";
        story26answer4.description = "Vous vous faites passer pour un baron de la drogue et lui expliquez qu�elle devrait vraiment vous raconter ce qu�il s�est pass� hier soir.";

        AnswerDescription story26answer1Description1 = new AnswerDescription();
        AnswerDescription story26answer2Description1 = new AnswerDescription();
        AnswerDescription story26answer3Description1 = new AnswerDescription();
        AnswerDescription story26answer4Description1 = new AnswerDescription();

        story26answer1Description1.answerDescriptionNumber = 120;
        story26answer2Description1.answerDescriptionNumber = 121;
        story26answer3Description1.answerDescriptionNumber = 122;
        story26answer4Description1.answerDescriptionNumber = 123;

        story26answer1Description1.answerNumberPreRequise = new List<int>();
        story26answer2Description1.answerNumberPreRequise = new List<int>();
        story26answer3Description1.answerNumberPreRequise = new List<int>();
        story26answer4Description1.answerNumberPreRequise = new List<int>();

        story26answer1Description1.description = "Elle bat des paupi�res et �clate de rire, vous annon�ant que la personne qui pourra lui donner un ordre n�est pas encore n�e.";
        story26answer2Description1.description = "Elle bat des paupi�res et �clate de rire. Elle annonce qu�elle sait tr�s bien o� son int�r�t se trouve dans cette histoire.";
        story26answer3Description1.description = "Elle bat des paupi�res et �clate de rire, elle n�a pas l�air du tout intimid�e.";
        story26answer4Description1.description = "Elle bat des paupi�res et �clate de rire, elle sait que vous racontez de la merde.";

        story26answer1.answerDescriptions.Add(story26answer1Description1);
        story26answer2.answerDescriptions.Add(story26answer2Description1);
        story26answer3.answerDescriptions.Add(story26answer3Description1);
        story26answer4.answerDescriptions.Add(story26answer4Description1);

        story26.answers.Add(story26answer1);
        story26.answers.Add(story26answer2);
        story26.answers.Add(story26answer3);
        story26.answers.Add(story26answer4);

        stories.listStory.Add(story26);
        //STORY 27

        Story story27 = new Story();
        story27.storyNumber = 27;

        StoryDescription story27Description1 = new StoryDescription();
        story27Description1.answerNumberPreRequise = new List<int>();
        story27Description1.description = "Vous bondissez hors du lit. Votre action la laisse sans voix et vous fait gagner quelques pr�cieuses secondes. Vous d�cidez de :";
        story27.storyDescriptions.Add(story27Description1);

        Answer story27answer1 = new Answer();
        Answer story27answer2 = new Answer();
        Answer story27answer3 = new Answer();
        story27answer1.attributsBoost = new List<string> { nameof(Attributs.Dexterite) };
        story27answer2.attributsBoost = new List<string> { nameof(Attributs.Force) };
        story27answer3.attributsBoost = new List<string> { nameof(Attributs.Force) };
        story27answer1.specialiteBoost = new List<string> { nameof(Specialite.ArmesAfeu) };
        story27answer2.specialiteBoost = new List<string> { nameof(Specialite.Bagarre) };
        story27answer3.specialiteBoost = new List<string> { nameof(Specialite.Melee) };
        story27answer1.nextStory = 28;
        story27answer2.nextStory = 28;
        story27answer3.nextStory = 28;
        story27answer1.description = "D�gainer le revolver � votre ceinture pour la prendre en joue.";
        story27answer2.description = "Lui faire une cl� de bras, comme vous l�avez appris en lutte gr�co-romaine.";
        story27answer3.description = "De l'assommer avec la coupe dans laquelle vous venez de boire.";

        AnswerDescription story27answer1Description1 = new AnswerDescription();
        AnswerDescription story27answer2Description1 = new AnswerDescription();
        AnswerDescription story27answer3Description1 = new AnswerDescription();

        story27answer1Description1.answerDescriptionNumber = 124;
        story27answer2Description1.answerDescriptionNumber = 125;
        story27answer3Description1.answerDescriptionNumber = 126;

        story27answer1Description1.answerNumberPreRequise = new List<int>();
        story27answer2Description1.answerNumberPreRequise = new List<int>();
        story27answer3Description1.answerNumberPreRequise = new List<int>();

        story27answer1Description1.description = "Tout se passe tr�s vite, vous parvenez � la pointer avec votre arme. Elle a l�air de s�en moquer et avance vers vous, d�termin�e. Alors vous allez tirer. Mais en une seconde votre visage est contre le plancher et vous ne pouvez rien faire.";
        story27answer2Description1.description = "Tout se passe tr�s vite, vous parvenez � la maitriser d�une cl� de bras. Mais tout � coup, elle d�boite son �paule et c�est vous qui vous vous retrouvez en position de faiblesse. En une seconde votre visage est contre le plancher et vous ne pouvez rien faire.";
        story27answer3Description1.description = "Vous lui fracassez la t�te avec, mais son regard ne vous quitte pas. La coupe rebondit sur sa t�te comme sur du m�tal. Elle vous agrippe alors. En une seconde votre visage est contre le plancher et vous ne pouvez rien faire.";

        story27answer1.answerDescriptions.Add(story27answer1Description1);
        story27answer2.answerDescriptions.Add(story27answer2Description1);
        story27answer3.answerDescriptions.Add(story27answer3Description1);

        story27.answers.Add(story27answer1);
        story27.answers.Add(story27answer2);
        story27.answers.Add(story27answer3);

        stories.listStory.Add(story27);

        //STORY 28


        Story story28 = new Story();
        story28.storyNumber = 28;

        //On ajoute la description (Le main text qui viendra apr�s la partie answer description choisie au dernier round)
        StoryDescription story28Description1 = new StoryDescription();
        story28Description1.answerNumberPreRequise = new List<int>();
        story28Description1.description = "La jeune femme vous parle alors d�une voix douce et sulfureuse, et vous annonce que plus rien ne sera jamais comme avant. Et � mesure qu�elle parle, et qu�elle vous raconte comment elle vous a nourris de son propre sang, vos yeux s��carquillent et vous sentez une puissance inconnue surgir du plus profond de votre corps. Elle vous demande de vous asseoir tranquillement pour tout vous expliquer.";
        story28.storyDescriptions.Add(story28Description1);


        Answer story28answer1 = new Answer();
        story28answer1.attributsBoost = new List<string>();
        story28answer1.specialiteBoost = new List<string>();
        story28answer1.nextStory = 29;
        story28answer1.description = "Vous plissez les yeux, conscient que les prochains choix que vous allez faire auront une grande importance sur votre vie et qu�il va vous falloir choisir avec attention.";

        AnswerDescription story28answer1Description1 = new AnswerDescription();

        story28answer1Description1.answerDescriptionNumber = 127;

        story28answer1Description1.answerNumberPreRequise = new List<int>();

        story28answer1Description1.description = "";

        story28answer1.answerDescriptions.Add(story28answer1Description1);

        story28.answers.Add(story28answer1);

        stories.listStory.Add(story28);

        //STORY 29

        Story story29 = new Story();
        story29.storyNumber = 29;

        StoryDescription story29Description1 = new StoryDescription();
        story29Description1.answerNumberPreRequise = new List<int>();
        story29Description1.description = "A mesure du discours vos sens vous reviennent, vous vous sentez plus lucide et plus vivant que jamais. C�est comme si une �nergie nouvelle coulait dans vos veines. La jeune femme vous explique que vous �tes maintenant une cr�ature de la nuit, et que vous poss�dez une partie de ses pouvoirs, et m�me si c�est difficile � avaler vous savez qu�elle dit vrai, que vous �tes exceptionnel.";
        story29.storyDescriptions.Add(story29Description1);

        Answer story29answer1 = new Answer();
        Answer story29answer2 = new Answer();
        Answer story29answer3 = new Answer();
        Answer story29answer4 = new Answer();
        Answer story29answer5 = new Answer();
        Answer story29answer6 = new Answer();
        story29answer1.attributsBoost = new List<string> { nameof(Attributs.Manipulation) };
        story29answer2.attributsBoost = new List<string> { nameof(Attributs.Intelligence) };
        story29answer3.attributsBoost = new List<string> { nameof(Attributs.Dexterite) };
        story29answer4.attributsBoost = new List<string>();
        story29answer5.attributsBoost = new List<string> { nameof(Attributs.Force) };
        story29answer6.attributsBoost = new List<string>();
        story29answer1.specialiteBoost = new List<string>();
        story29answer2.specialiteBoost = new List<string>();
        story29answer3.specialiteBoost = new List<string>();
        story29answer4.specialiteBoost = new List<string>();
        story29answer5.specialiteBoost = new List<string>();
        story29answer6.specialiteBoost = new List<string> { nameof(Specialite.Animaux) };
        story29answer1.nextStory = 30;
        story29answer2.nextStory = 31;
        story29answer3.nextStory = 32;
        story29answer4.nextStory = 33;
        story29answer5.nextStory = 34;
        story29answer6.nextStory = 35;
        story29answer1.description = "Vous avez soif de pouvoir, vous voulez le monde � vos pieds et sentez que d'un regard, d'une parole vous pourriez ordonner � n'importe qui d'exaucer le moindre de vos d�sirs.";
        story29answer2.description = "Votre esprit semble vous avoir r�v�l� un pan entier et nouveau de la r�alit�. En vous concentrant, vous pouvez sentir votre �me s'�tendre autours de vous dans le pass�, le pr�sent mais aussi l'avenir.";
        story29answer3.description = "Le temps semble s'�tre ralentis autour de vous. Chacun de vos mouvements vous semble plus pr�cis, terriblement plus rapide. Vos yeux alertes percent le moment pr�sent et vous sentez que vous pourriez m�me esquiver des balles.";
        story29answer4.description = "Votre corps vous semble instable et mall�able. Vous sentez qu'avec un peu d'entrainement vous pourrez changer de forme.";
        story29answer5.description = "Vous vous sentez l�ger et incroyablement fort. Vos muscles semblent br�ler de bondir hors de votre corps, vous voulez cogner et vous savez que �a va faire mal. ";
        story29answer6.description = "Votre regard se pose sur le chat blanc au milieu de la salle. Vous adorez les animaux et sentez qu'au fond de vous se trouve maintenant une b�te terriblement puissante que vous pourriez contr�ler.";

        AnswerDescription story29answer1Description1 = new AnswerDescription();
        AnswerDescription story29answer2Description1 = new AnswerDescription();
        AnswerDescription story29answer3Description1 = new AnswerDescription();
        AnswerDescription story29answer4Description1 = new AnswerDescription();
        AnswerDescription story29answer5Description1 = new AnswerDescription();
        AnswerDescription story29answer6Description1 = new AnswerDescription();

        story29answer1Description1.answerDescriptionNumber = 128;
        story29answer2Description1.answerDescriptionNumber = 129;
        story29answer3Description1.answerDescriptionNumber = 130;
        story29answer4Description1.answerDescriptionNumber = 131;
        story29answer5Description1.answerDescriptionNumber = 132;
        story29answer6Description1.answerDescriptionNumber = 133;

        story29answer1Description1.answerNumberPreRequise = new List<int>();
        story29answer2Description1.answerNumberPreRequise = new List<int>();
        story29answer3Description1.answerNumberPreRequise = new List<int>();
        story29answer4Description1.answerNumberPreRequise = new List<int>();
        story29answer5Description1.answerNumberPreRequise = new List<int>();
        story29answer6Description1.answerNumberPreRequise = new List<int>();

        story29answer1Description1.description = "";
        story29answer2Description1.description = "";
        story29answer3Description1.description = "";
        story29answer4Description1.description = "";
        story29answer5Description1.description = "";
        story29answer6Description1.description = "";


        story29answer1.answerDescriptions.Add(story29answer1Description1);
        story29answer2.answerDescriptions.Add(story29answer2Description1);
        story29answer3.answerDescriptions.Add(story29answer3Description1);
        story29answer4.answerDescriptions.Add(story29answer4Description1);
        story29answer5.answerDescriptions.Add(story29answer5Description1);
        story29answer6.answerDescriptions.Add(story29answer6Description1);

        story29.answers.Add(story29answer1);
        story29.answers.Add(story29answer2);
        story29.answers.Add(story29answer3);
        story29.answers.Add(story29answer4);
        story29.answers.Add(story29answer5);
        story29.answers.Add(story29answer6);

        stories.listStory.Add(story29);

        ///STORY 30
        ///
        Story story30 = new Story();
        story30.storyNumber = 30;

        StoryDescription story30Description1 = new StoryDescription();
        story30Description1.answerNumberPreRequise = new List<int>();
        story30Description1.description = "Mais ce n�est pas tout, des tr�fonds de votre �tre �mane un pouvoir monumental et ancestral :";
        story30.storyDescriptions.Add(story30Description1);

        Answer story30answer1 = new Answer();
        Answer story30answer2 = new Answer();
        Answer story30answer3 = new Answer();
        Answer story30answer4 = new Answer();
        Answer story30answer5 = new Answer();
        story30answer1.attributsBoost = new List<string>();
        story30answer2.attributsBoost = new List<string>();
        story30answer3.attributsBoost = new List<string> { nameof(Attributs.Astuce) };
        story30answer4.attributsBoost = new List<string>();
        story30answer5.attributsBoost = new List<string>();
        story30answer1.specialiteBoost = new List<string> { nameof(Specialite.Furtivite) };
        story30answer2.specialiteBoost = new List<string> { nameof(Specialite.Occultisme) };
        story30answer3.specialiteBoost = new List<string>();
        story30answer4.specialiteBoost = new List<string> { nameof(Specialite.Survie) };
        story30answer5.specialiteBoost = new List<string>();
        story30answer1.nextStory = 3000;
        story30answer2.nextStory = 6000;
        story30answer3.nextStory = 8000;
        story30answer4.nextStory = 7000;
        story30answer5.nextStory = 9000;
        story30answer1.clan = "Malkavian";
        story30answer2.clan = "Tremere";
        story30answer3.clan = "Lassombra";
        story30answer4.clan = "Ventrue";
        story30answer5.clan = "Tzimisce";
        story30answer1.description = "Votre essence est impalpable. Vous savez que si vous le d�sirez, vous pourriez dispara�tre aux yeux du monde et que personne ne saurait vous retrouver. peut �tre pourriez vous m�me vous faire passer pour une personne en particulier, utiliser cette invisibilit� sur d'autres, ou leur provoquer des hallucinations.";
        story30answer2.description = "Votre sang bouillonne dans vos veines, vos yeux brillent d'un �clat intense. Vous savez que vous n'�tes plus le m�me. Et qu'une force spirituelle coule en vous. Vous voulez du sang, et vous voulez exp�rimenter ce que votre sang peut accomplir";
        story30answer3.description = "Le monde vous para�t soudainement plus sombre et plus froid. Vous voyez la barri�re entre le monde des morts et celui des vivants. Vous sentez les ombres bouger autour de vous, vous r�pondre et vous ob�ir. Peut-�tre pourriez vous devenir une ombre vous aussi ?";
        story30answer4.description = "Votre corps vous para�t dur comme l'acier. Vous vous sentez immortel et intouchable. Rien ne peut plus vous atteindre vous en �tes certain.";
        story30answer5.description = "Votre corps vous semble instable et mall�able. Vous sentez qu'avec un peu d'entrainement vous pourriez changer de forme.";

        AnswerDescription story30answer1Description1 = new AnswerDescription();
        AnswerDescription story30answer2Description1 = new AnswerDescription();
        AnswerDescription story30answer3Description1 = new AnswerDescription();
        AnswerDescription story30answer4Description1 = new AnswerDescription();
        AnswerDescription story30answer5Description1 = new AnswerDescription();

        story30answer1Description1.answerDescriptionNumber = 128;
        story30answer2Description1.answerDescriptionNumber = 130;
        story30answer3Description1.answerDescriptionNumber = 130;
        story30answer4Description1.answerDescriptionNumber = 131;
        story30answer5Description1.answerDescriptionNumber = 132;

        story30answer1Description1.answerNumberPreRequise = new List<int>();
        story30answer2Description1.answerNumberPreRequise = new List<int>();
        story30answer3Description1.answerNumberPreRequise = new List<int>();
        story30answer4Description1.answerNumberPreRequise = new List<int>();
        story30answer5Description1.answerNumberPreRequise = new List<int>();

        story30answer1Description1.description = "La jeune femme vous regarde, compr�hensive. D�une voix pos�e, elle vous annonce que vous faites d�sormais partie du clan Malkavien.";
        story30answer2Description1.description = "La jeune femme vous regarde, compr�hensive. D�une voix pos�e, elle vous annonce que vous faites d�sormais partie du clan Tremere.";
        story30answer3Description1.description = "La jeune femme vous regarde, compr�hensive. D�une voix pos�e, elle vous annonce que vous faites d�sormais partie du clan Lassombra.";
        story30answer4Description1.description = "La jeune femme vous regarde, compr�hensive. D�une voix pos�e, elle vous annonce que vous faites d�sormais partie du clan Ventrue.";
        story30answer5Description1.description = "La jeune femme vous regarde, compr�hensive. D�une voix pos�e, elle vous annonce que vous faites d�sormais partie du clan Tzimisce.";

        story30answer1.answerDescriptions.Add(story30answer1Description1);
        story30answer2.answerDescriptions.Add(story30answer2Description1);
        story30answer3.answerDescriptions.Add(story30answer3Description1);
        story30answer4.answerDescriptions.Add(story30answer4Description1);
        story30answer5.answerDescriptions.Add(story30answer5Description1);

        story30.answers.Add(story30answer1);
        story30.answers.Add(story30answer2);
        story30.answers.Add(story30answer3);
        story30.answers.Add(story30answer4);
        story30.answers.Add(story30answer5);

        stories.listStory.Add(story30);

        ////STORY 31
        ///       

        Story story31 = new Story();
        story31.storyNumber = 31;

        StoryDescription story31Description1 = new StoryDescription();
        story31Description1.answerNumberPreRequise = new List<int>();
        story31Description1.description = "Mais ce n�est pas tout, des tr�fonds de votre �tre �mane un pouvoir monumental et ancestral :";
        story31.storyDescriptions.Add(story31Description1);

        Answer story31answer1 = new Answer();
        Answer story31answer2 = new Answer();
        Answer story31answer3 = new Answer();
        story31answer1.attributsBoost = new List<string>();
        story31answer2.attributsBoost = new List<string> { nameof(Attributs.Charisme) };
        story31answer3.attributsBoost = new List<string>();
        story31answer1.specialiteBoost = new List<string> { nameof(Specialite.Furtivite) };
        story31answer2.specialiteBoost = new List<string>();
        story31answer3.specialiteBoost = new List<string> { nameof(Specialite.Occultisme) };
        story31answer1.nextStory = 3000;
        story31answer2.nextStory = 5000;
        story31answer3.nextStory = 6000;
        story31answer1.clan = "Malkavian";
        story31answer2.clan = "Toreador";
        story31answer3.clan = "Tremere";
        story31answer1.description = "Votre essence est impalpable. Vous savez que si vous le d�sirez, vous pourriez dispara�tre aux yeux du monde et que personne ne saurait vous retrouver. peut �tre pourriez vous m�me vous faire passer pour une personne en particulier, utiliser cette invisibilit� sur d'autres, ou leur provoquer des hallucinations.";
        story31answer2.description = "Vous �tes devenu quelqu'un de nouveau, quelqu'un au-dessus du reste du monde. Votre regard rayonne d'une puissance divine. Vous savez que votre simple charisme pourrait faire ployer des foules � vos pieds et vous conduire aux plus hautes positions de ce monde.";
        story31answer3.description = "Votre sang bouillonne dans vos veines, vos yeux brillent d'un �clat intense. Vous savez que vous n'�tes plus le m�me. Et qu'une force spirituelle coule en vous. Vous voulez du sang, et vous voulez exp�rimenter ce que votre sang peut accomplir.";

        AnswerDescription story31answer1Description1 = new AnswerDescription();
        AnswerDescription story31answer2Description1 = new AnswerDescription();
        AnswerDescription story31answer3Description1 = new AnswerDescription();

        story31answer1Description1.answerDescriptionNumber = 139;
        story31answer2Description1.answerDescriptionNumber = 140;
        story31answer3Description1.answerDescriptionNumber = 141;

        story31answer1Description1.answerNumberPreRequise = new List<int>();
        story31answer2Description1.answerNumberPreRequise = new List<int>();
        story31answer3Description1.answerNumberPreRequise = new List<int>();

        story31answer1Description1.description = "La jeune femme vous regarde, compr�hensive. D�une voix pos�e, elle vous annonce que vous faites d�sormais partie du clan Malkavien.";
        story31answer2Description1.description = "La jeune femme vous regarde, compr�hensive. D�une voix pos�e, elle vous annonce que vous faites d�sormais partie du clan Toreador.";
        story31answer3Description1.description = "La jeune femme vous regarde, compr�hensive. D�une voix pos�e, elle vous annonce que vous faites d�sormais partie du clan Tremere.";

        story31answer1.answerDescriptions.Add(story31answer1Description1);
        story31answer2.answerDescriptions.Add(story31answer2Description1);
        story31answer3.answerDescriptions.Add(story31answer3Description1);

        story31.answers.Add(story31answer1);
        story31.answers.Add(story31answer2);
        story31.answers.Add(story31answer3);

        stories.listStory.Add(story31);
        ///Story 32

        Story story32 = new Story();
        story32.storyNumber = 32;

        StoryDescription story32Description1 = new StoryDescription();
        story32Description1.answerNumberPreRequise = new List<int>();
        story32Description1.description = "Mais ce n�est pas tout, des tr�fonds de votre �tre �mane un pouvoir monumental et ancestral :";
        story32.storyDescriptions.Add(story32Description1);

        Answer story32answer1 = new Answer();
        Answer story32answer2 = new Answer();
        story32answer1.attributsBoost = new List<string> { nameof(Attributs.Force) };
        story32answer2.attributsBoost = new List<string> { nameof(Attributs.Intelligence) };
        story32answer1.specialiteBoost = new List<string>();
        story32answer2.specialiteBoost = new List<string>();
        story32answer1.nextStory = 1000;
        story32answer2.nextStory = 5000;
        story32answer1.clan = "Brujah";
        story32answer2.clan = "Toreador";
        story32answer1.description = "Vous vous sentez l�ger et incroyablement fort. Vos muscles semblent br�ler de bondir hors de votre corps, vous voulez cogner et vous savez que �a va faire mal. ";
        story32answer2.description = "Votre esprit semble vous avoir r�v�l� un pan entier et nouveau de la r�alit�. En vous concentrant, vous pouvez sentir votre �me s'�tendre autours de vous dans le pass�, le pr�sent mais aussi l'avenir";

        AnswerDescription story32answer1Description1 = new AnswerDescription();
        AnswerDescription story32answer2Description1 = new AnswerDescription();

        story32answer1Description1.answerDescriptionNumber = 142;
        story32answer2Description1.answerDescriptionNumber = 143;

        story32answer1Description1.answerNumberPreRequise = new List<int>();
        story32answer2Description1.answerNumberPreRequise = new List<int>();

        story32answer1Description1.description = "La jeune femme vous regarde, compr�hensive. D�une voix pos�e, elle vous annonce que vous faites d�sormais partie du clan Brujah.";
        story32answer2Description1.description = "La jeune femme vous regarde, compr�hensive. D�une voix pos�e, elle vous annonce que vous faites d�sormais partie du clan Toreador.";

        story32answer1.answerDescriptions.Add(story32answer1Description1);
        story32answer2.answerDescriptions.Add(story32answer2Description1);

        story32.answers.Add(story32answer1);
        story32.answers.Add(story32answer2);

        stories.listStory.Add(story32);

        //STORY 33


        Story story33 = new Story();
        story33.storyNumber = 33;

        StoryDescription story33Description1 = new StoryDescription();
        story33Description1.answerNumberPreRequise = new List<int>();
        story33Description1.description = "Mais ce n�est pas tout, des tr�fonds de votre �tre �mane un pouvoir monumental et ancestral :";
        story33.storyDescriptions.Add(story33Description1);

        Answer story33answer1 = new Answer();
        Answer story33answer2 = new Answer();
        story33answer1.attributsBoost = new List<string> { nameof(Attributs.Manipulation) };
        story33answer2.attributsBoost = new List<string>();
        story33answer1.specialiteBoost = new List<string>();
        story33answer2.specialiteBoost = new List<string> { nameof(Specialite.Survie) };
        story33answer1.nextStory = 9000;
        story33answer2.nextStory = 2000;
        story33answer1.clan = "Tzimisce";
        story33answer2.clan = "Gangrel";
        story33answer1.description = "Vous avez soif de pouvoir, vous voulez le monde � vos pieds et sentez que d'un regard, d'une parole vous pourriez ordonner � n'importe qui d'exaucer le moindre de vos d�sirs.";
        story33answer2.description = "Votre corps vous para�t dur comme l'acier. Vous vous sentez immortel et intouchable. Rien ne peut plus vous atteindre vous en �tes certain.";

        AnswerDescription story33answer1Description1 = new AnswerDescription();
        AnswerDescription story33answer2Description1 = new AnswerDescription();

        story33answer1Description1.answerDescriptionNumber = 144;
        story33answer2Description1.answerDescriptionNumber = 145;

        story33answer1Description1.answerNumberPreRequise = new List<int>();
        story33answer2Description1.answerNumberPreRequise = new List<int>();

        story33answer1Description1.description = "La jeune femme vous regarde, compr�hensive. D�une voix pos�e, elle vous annonce que vous faites d�sormais partie du clan Tzimisce.";
        story33answer2Description1.description = "La jeune femme vous regarde, compr�hensive. D�une voix pos�e, elle vous annonce que vous faites d�sormais partie du clan Gangrel.";

        story33answer1.answerDescriptions.Add(story33answer1Description1);
        story33answer2.answerDescriptions.Add(story33answer2Description1);

        story33.answers.Add(story33answer1);
        story33.answers.Add(story33answer2);

        stories.listStory.Add(story33);

        //STORY 34

        Story story34 = new Story();
        story34.storyNumber = 34;

        StoryDescription story34Description1 = new StoryDescription();
        story34Description1.answerNumberPreRequise = new List<int>();
        story34Description1.description = "Mais ce n�est pas tout, des tr�fonds de votre �tre �mane un pouvoir monumental et ancestral :";
        story34.storyDescriptions.Add(story34Description1);

        Answer story34answer1 = new Answer();
        Answer story34answer2 = new Answer();
        Answer story34answer3 = new Answer();
        story34answer1.attributsBoost = new List<string> { nameof(Attributs.Charisme) };
        story34answer2.attributsBoost = new List<string>();
        story34answer3.attributsBoost = new List<string> { nameof(Attributs.Astuce) };
        story34answer1.specialiteBoost = new List<string> { nameof(Specialite.Furtivite) };
        story34answer2.specialiteBoost = new List<string>();
        story34answer3.specialiteBoost = new List<string>();
        story34answer1.nextStory = 1000;
        story34answer2.nextStory = 4000;
        story34answer3.nextStory = 8000;
        story34answer1.clan = "Brujah";
        story34answer2.clan = "Nosferatu";
        story34answer3.clan = "Lassombra";
        story34answer1.description = "Vous �tes devenu quelqu'un de nouveau, quelqu'un au-dessus du reste du monde. Votre regard rayonne d'une puissance divine. Vous savez que votre simple charisme pourrait faire ployer des foules � vos pieds et vous conduire aux plus hautes positions de ce monde.";
        story34answer2.description = "Vous sentez que vous �tes devenu immonde, mais votre essence est impalpable. Vous savez que si vous le d�sirez, vous pourriez dispara�tre aux yeux du monde et que personne ne saurait vous retrouver. peut �tre pourriez vous m�me vous faire passer pour une personne en particulier, utiliser cette invisibilit� sur d'autres, ou leur provoquer des hallucinations.";
        story34answer3.description = "Le monde vous para�t soudainement plus sombre et plus froid. Vous voyez la barri�re entre le monde des morts et celui des vivants. Vous sentez les ombres bouger autour de vous, vous r�pondre et vous ob�ir. Peut-�tre pourriez vous devenir une ombre vous aussi ? ";

        AnswerDescription story34answer1Description1 = new AnswerDescription();
        AnswerDescription story34answer2Description1 = new AnswerDescription();
        AnswerDescription story34answer3Description1 = new AnswerDescription();

        story34answer1Description1.answerDescriptionNumber = 146;
        story34answer2Description1.answerDescriptionNumber = 147;
        story34answer3Description1.answerDescriptionNumber = 148;

        story34answer1Description1.answerNumberPreRequise = new List<int>();
        story34answer2Description1.answerNumberPreRequise = new List<int>();
        story34answer3Description1.answerNumberPreRequise = new List<int>();

        story34answer1Description1.description = "La jeune femme vous regarde, compr�hensive. D�une voix pos�e, elle vous annonce que vous faites d�sormais partie du clan Brujah.";
        story34answer2Description1.description = "La jeune femme vous regarde, compr�hensive. D�une voix pos�e, elle vous annonce que vous faites d�sormais partie du clan Nosferatu.";
        story34answer3Description1.description = "La jeune femme vous regarde, compr�hensive. D�une voix pos�e, elle vous annonce que vous faites d�sormais partie du clan Lassombra.";

        story34answer1.answerDescriptions.Add(story34answer1Description1);
        story34answer2.answerDescriptions.Add(story34answer2Description1);
        story34answer3.answerDescriptions.Add(story34answer3Description1);

        story34.answers.Add(story34answer1);
        story34.answers.Add(story34answer2);
        story34.answers.Add(story34answer3);

        stories.listStory.Add(story34);

        ///STORY 35
        ///

        Story story35 = new Story();
        story35.storyNumber = 35;

        StoryDescription story35Description1 = new StoryDescription();
        story35Description1.answerNumberPreRequise = new List<int>();
        story35Description1.description = "Mais ce n�est pas tout, des tr�fonds de votre �tre �mane un pouvoir monumental et ancestral :";
        story35.storyDescriptions.Add(story35Description1);

        Answer story35answer1 = new Answer();
        Answer story35answer2 = new Answer();
        Answer story35answer3 = new Answer();
        story35answer1.attributsBoost = new List<string>();
        story35answer2.attributsBoost = new List<string>();
        story35answer3.attributsBoost = new List<string> { nameof(Attributs.Manipulation) };
        story35answer1.specialiteBoost = new List<string> { nameof(Specialite.Survie) };
        story35answer2.specialiteBoost = new List<string> { nameof(Specialite.Furtivite) };
        story35answer3.specialiteBoost = new List<string>();
        story35answer1.nextStory = 2000;
        story35answer2.nextStory = 4000;
        story35answer3.nextStory = 9000;
        story35answer1.clan = "Gangrel";
        story35answer2.clan = "Nosferatu";
        story35answer3.clan = "Tzimisce";
        story35answer1.description = "Votre corps vous para�t dur comme l'acier. Vous vous sentez immortel et intouchable. Rien ne peut plus vous atteindre vous en �tes certain.";
        story35answer2.description = "Vous sentez que vous �tes devenu immonde, mais votre essence est impalpable. Vous savez que si vous le d�sirez, vous pourriez dispara�tre aux yeux du monde et que personne ne saurait vous retrouver. peut �tre pourriez vous m�me vous faire passer pour une personne en particulier, utiliser cette invisibilit� sur d'autres, ou leur provoquer des hallucinations.";
        story35answer3.description = "Vous avez soif de pouvoir, vous voulez le monde � vos pieds et sentez que d'un regard, d'une parole vous pourriez ordonner � n'importe qui d'exaucer le moindre de vos d�sirs.";

        AnswerDescription story35answer1Description1 = new AnswerDescription();
        AnswerDescription story35answer2Description1 = new AnswerDescription();
        AnswerDescription story35answer3Description1 = new AnswerDescription();

        story35answer1Description1.answerDescriptionNumber = 146;
        story35answer2Description1.answerDescriptionNumber = 147;
        story35answer3Description1.answerDescriptionNumber = 148;

        story35answer1Description1.answerNumberPreRequise = new List<int>();
        story35answer2Description1.answerNumberPreRequise = new List<int>();
        story35answer3Description1.answerNumberPreRequise = new List<int>();

        story35answer1Description1.description = "La jeune femme vous regarde, compr�hensive. D�une voix pos�e, elle vous annonce que vous faites d�sormais partie du clan Gangrel.";
        story35answer2Description1.description = "La jeune femme vous regarde, compr�hensive. D�une voix pos�e, elle vous annonce que vous faites d�sormais partie du clan Nosferatu.";
        story35answer3Description1.description = "La jeune femme vous regarde, compr�hensive. D�une voix pos�e, elle vous annonce que vous faites d�sormais partie du clan Tzimisce.";

        story35answer1.answerDescriptions.Add(story35answer1Description1);
        story35answer2.answerDescriptions.Add(story35answer2Description1);
        story35answer3.answerDescriptions.Add(story35answer3Description1);

        story35.answers.Add(story35answer1);
        story35.answers.Add(story35answer2);
        story35.answers.Add(story35answer3);

        stories.listStory.Add(story35);

        ///STORY 1000
        ///TODO
        ///

        Story story1000 = new Story();
        story1000.storyNumber = 1000;

        StoryDescription story1000Description1 = new StoryDescription();
        story1000Description1.answerNumberPreRequise = new List<int>();
        story1000Description1.description = "Vous sentez la chaleur dans ses paroles et savez que vous venez de trouver une nouvelle famille en ce monde, m�me si vous ne savez pas encore ce qui vous attend. Tout � coup, la porte de la salle explose, une grenade roule au sol et se met � cracher de la fum�e partout. La jeune femme se precipite dessus, mais aussitot des lasers percent la fum�e et des coups de feu retentissent dans la salle, assourdissants.";
        story1000.storyDescriptions.Add(story1000Description1);

        Answer story1000answer1 = new Answer();
        Answer story1000answer2 = new Answer();
        Answer story1000answer3 = new Answer();
        Answer story1000answer4 = new Answer();
        Answer story1000answer5 = new Answer();
        Answer story1000answer6 = new Answer();


        story1000answer1.isPreRequiseNotIn = true;
        story1000answer1.answerNumberPreRequise = new List<int>();
        story1000answer2.isPreRequiseNotIn = true;
        story1000answer2.answerNumberPreRequise = new List<int>();
        story1000answer3.isPreRequiseNotIn = true;
        story1000answer3.answerNumberPreRequise = new List<int>();
        story1000answer4.isPreRequiseNotIn = true;
        story1000answer4.answerNumberPreRequise = new List<int>();
        story1000answer5.isPreRequiseNotIn = true;
        story1000answer5.answerNumberPreRequise = new List<int>();
        story1000answer6.isPreRequiseNotIn = true;
        story1000answer6.answerNumberPreRequise = new List<int>();

        story1000answer1.nextStory = 1010;
        story1000answer2.nextStory = 1010;
        story1000answer3.nextStory = 1010;
        story1000answer4.nextStory = 1010;
        story1000answer5.nextStory = 1010;
        story1000answer6.nextStory = 1010;
        story1000answer1.discipline = "Grace feline";
        story1000answer2.discipline = "Reflexe eclaire";
        story1000answer3.discipline = "Bond surhumain";
        story1000answer4.discipline = "Corps lethal";
        story1000answer5.discipline = "Intimider";
        story1000answer6.discipline = "Reverence";
        story1000answer1.description = "Vous bondissez en l�air et retombez gracieusement d�bout sur l��paule d�un soldat. Vous le poussez violemment du pied avant de sauter sur un autre.";
        story1000answer2.description = "Les soldats tirent sur vous et vous ralentissez le temps, esquivant chacune des balles.";
        story1000answer3.description = "Vous vous propulsez de toute la puissance de vos jambes � travers la masse de soldats. La violence de l�impact d�boite des �paules et les fait chuter au sol.";
        story1000answer4.description = "Vous foncez sur le premier soldat et envoyez un coup de poing fulgurant pour d�truire son gilet par balle.";
        story1000answer5.description = "Vous vous redressez de toute votre taille et regardez les soldats d�un air mauvais, semant un vent de panique surnaturelle.";
        story1000answer6.description = "Vous rayonnez de majest�, d�gageant une aura quasi divine, for�ant vos ennemis � mettre un genoux � terre.";

        AnswerDescription story1000answer1Description1 = new AnswerDescription();
        AnswerDescription story1000answer2Description1 = new AnswerDescription();
        AnswerDescription story1000answer3Description1 = new AnswerDescription();
        AnswerDescription story1000answer4Description1 = new AnswerDescription();
        AnswerDescription story1000answer5Description1 = new AnswerDescription();
        AnswerDescription story1000answer6Description1 = new AnswerDescription();

        story1000answer1Description1.answerDescriptionNumber = 1001;
        story1000answer2Description1.answerDescriptionNumber = 1002;
        story1000answer3Description1.answerDescriptionNumber = 1003;
        story1000answer4Description1.answerDescriptionNumber = 1004;
        story1000answer5Description1.answerDescriptionNumber = 1005;
        story1000answer6Description1.answerDescriptionNumber = 1006;

        story1000answer1Description1.description = "Vous bondissez d�ennemis en ennemis tel un f�lin, posant vos pieds tant�t sur leurs t�tes, tant�t sur leurs �paules et rien ne vient perturber votre �quilibre, vous semez le chaos autours de vous.";
        story1000answer2Description1.description = "Vos ennemis sont sous le choc, et malgr� la fum�e ils ont bien vu qu�aucune balle n�a eu d'impact sur vous, vous entendez leurs jurons retentir alors qu�ils rechargent leurs armes.";
        story1000answer3Description1.description = "Des jurons explosent dans tous les sens au milieu des cris de douleur. Vous avez fonc� � travers vos ennemis tel un boulet de canon, et ceux-ci mirent quelques secondes � se relever.";
        story1000answer4Description1.description = "Le gilet est transperc� dans une gerbe de sang. Vous sentez votre poing s�enfoncer dans les tripes humides, chaudes et gluantes de votre ennemi. Vous retirez brusquement votre main et celui-ci tombe � terre, mort sur le coup.";
        story1000answer5Description1.description = "A travers les fumig�nes votre aura p�se d�une lourdeur extr�me sur les �paules des soldats, vous-en entendez quelques-un crier et partir en courant. Les autres sont terroris�s et n�osent plus s�approcher.";
        story1000answer6Description1.description = "Les soldats sont �cras�s par la force mystique qui rayonne de votre position. Les plus proches l�chent les armes et tombent � genoux, des larmes pleins les yeux devant votre splendeur. Les autres sont bloqu�s sur place quelques secondes t�tanis�s par la sc�ne.";

        story1000answer1.answerDescriptions.Add(story1000answer1Description1);
        story1000answer2.answerDescriptions.Add(story1000answer2Description1);
        story1000answer3.answerDescriptions.Add(story1000answer3Description1);
        story1000answer4.answerDescriptions.Add(story1000answer4Description1);
        story1000answer5.answerDescriptions.Add(story1000answer5Description1);
        story1000answer6.answerDescriptions.Add(story1000answer6Description1);

        story1000.answers.Add(story1000answer1);
        story1000.answers.Add(story1000answer2);
        story1000.answers.Add(story1000answer3);
        story1000.answers.Add(story1000answer4);
        story1000.answers.Add(story1000answer5);
        story1000.answers.Add(story1000answer6);

        stories.listStory.Add(story1000);

        //STORY 1010

        Story story1010 = new Story();
        story1010.storyNumber = 1010;

        StoryDescription story1010Description1 = new StoryDescription();
        story1010Description1.answerNumberPreRequise = new List<int>();
        story1010Description1.description = "Certains des hommes arm�s r�sistent et se mettent alors � tirer dans tous les sens, et vous sentez que vous pouvez les contrer : ";
        story1010.storyDescriptions.Add(story1010Description1);

        Answer story1010answer1 = new Answer();
        Answer story1010answer2 = new Answer();
        Answer story1010answer3 = new Answer();
        Answer story1010answer4 = new Answer();
        Answer story1010answer5 = new Answer();
        Answer story1010answer6 = new Answer();


        story1010answer1.isPreRequiseNotIn = true;
        story1010answer1.answerNumberPreRequise = new List<int>{ 1001 };
        story1010answer2.isPreRequiseNotIn = true;
        story1010answer2.answerNumberPreRequise = new List<int>{ 1002 };
        story1010answer3.isPreRequiseNotIn = true;
        story1010answer3.answerNumberPreRequise = new List<int>{ 1003 };
        story1010answer4.isPreRequiseNotIn = true;
        story1010answer4.answerNumberPreRequise = new List<int>{ 1004 };
        story1010answer5.isPreRequiseNotIn = true;
        story1010answer5.answerNumberPreRequise = new List<int>{ 1005 };
        story1010answer6.isPreRequiseNotIn = true;
        story1010answer6.answerNumberPreRequise = new List<int>{ 1006 };

        story1010answer1.nextStory = 1020;
        story1010answer2.nextStory = 1020;
        story1010answer3.nextStory = 1020;
        story1010answer4.nextStory = 1020;
        story1010answer5.nextStory = 1020;
        story1010answer6.nextStory = 1020;
        story1010answer1.discipline = "Grace feline";
        story1010answer2.discipline = "Reflexe eclaire";
        story1010answer3.discipline = "Bond surhumain";
        story1010answer4.discipline = "Corps lethal";
        story1010answer5.discipline = "Intimider";
        story1010answer6.discipline = "Reverence";
        story1010answer1.description = "Vous bondissez en l�air et retombez gracieusement d�bout sur l��paule d�un soldat. Vous le poussez violemment du pied avant de sauter sur un autre.";
        story1010answer2.description = "Les soldats tirent sur vous et vous ralentissez le temps, esquivant chacune des balles.";
        story1010answer3.description = "Vous vous propulsez de toute la puissance de vos jambes � travers la masse de soldats. La violence de l�impact d�boite des �paules et les fait chuter au sol.";
        story1010answer4.description = "Vous foncez sur le premier soldat et envoyez un coup de poing fulgurant pour d�truire son gilet par balle.";
        story1010answer5.description = "Vous vous redressez de toute votre taille et regardez les soldats d�un air mauvais, semant un vent de panique surnaturelle.";
        story1010answer6.description = "Vous rayonnez de majest�, d�gageant une aura quasi divine, for�ant vos ennemis � mettre un genoux � terre.";

        AnswerDescription story1010answer1Description1 = new AnswerDescription();
        AnswerDescription story1010answer2Description1 = new AnswerDescription();
        AnswerDescription story1010answer3Description1 = new AnswerDescription();
        AnswerDescription story1010answer4Description1 = new AnswerDescription();
        AnswerDescription story1010answer5Description1 = new AnswerDescription();
        AnswerDescription story1010answer6Description1 = new AnswerDescription();

        story1010answer1Description1.answerDescriptionNumber = 1001;
        story1010answer2Description1.answerDescriptionNumber = 1002;
        story1010answer3Description1.answerDescriptionNumber = 1003;
        story1010answer4Description1.answerDescriptionNumber = 1004;
        story1010answer5Description1.answerDescriptionNumber = 1005;
        story1010answer6Description1.answerDescriptionNumber = 1006;

        story1010answer1Description1.description = "Vous bondissez d�ennemis en ennemis tel un f�lin, posant vos pieds tant�t sur leurs t�tes, tant�t sur leurs �paules et rien ne vient perturber votre �quilibre, vous semez le chaos autours de vous.";
        story1010answer2Description1.description = "Vos ennemis sont sous le choc, et malgr� la fum�e ils ont bien vu qu�aucune balle n�a eu d'impact sur vous, vous entendez leurs jurons retentir alors qu�ils rechargent leurs armes.";
        story1010answer3Description1.description = "Des jurons explosent dans tous les sens au milieu des cris de douleur. Vous avez fonc� � travers vos ennemis tel un boulet de canon, et ceux-ci mirent quelques secondes � se relever.";
        story1010answer4Description1.description = "Le gilet est transperc� dans une gerbe de sang. Vous sentez votre poing s�enfoncer dans les tripes humides, chaudes et gluantes de votre ennemi. Vous retirez brusquement votre main et celui-ci tombe � terre, mort sur le coup.";
        story1010answer5Description1.description = "A travers les fumig�nes votre aura p�se d�une lourdeur extr�me sur les �paules des soldats, vous-en entendez quelques-un crier et partir en courant. Les autres sont terroris�s et n�osent plus s�approcher.";
        story1010answer6Description1.description = "Les soldats sont �cras�s par la force mystique qui rayonne de votre position. Les plus proches l�chent les armes et tombent � genoux, des larmes pleins les yeux devant votre splendeur. Les autres sont bloqu�s sur place quelques secondes t�tanis�s par la sc�ne.";

        story1010answer1.answerDescriptions.Add(story1010answer1Description1);
        story1010answer2.answerDescriptions.Add(story1010answer2Description1);
        story1010answer3.answerDescriptions.Add(story1010answer3Description1);
        story1010answer4.answerDescriptions.Add(story1010answer4Description1);
        story1010answer5.answerDescriptions.Add(story1010answer5Description1);
        story1010answer6.answerDescriptions.Add(story1010answer6Description1);

        story1010.answers.Add(story1010answer1);
        story1010.answers.Add(story1010answer2);
        story1010.answers.Add(story1010answer3);
        story1010.answers.Add(story1010answer4);
        story1010.answers.Add(story1010answer5);
        story1010.answers.Add(story1010answer6);

        stories.listStory.Add(story1010);

        ////STORY 1020

        Story story1020 = new Story();
        story1020.storyNumber = 1020;

        StoryDescription story1020Description1 = new StoryDescription();
        story1020Description1.answerNumberPreRequise = new List<int>();
        story1020Description1.description = "Dans une derni�re tentative pour r�sister vous voyez un petit groupe de soldat se former en un ultime bastion pr�s de la porte : ";
        story1020.storyDescriptions.Add(story1020Description1);

        Answer story1020answer1 = new Answer();
        Answer story1020answer2 = new Answer();
        Answer story1020answer3 = new Answer();
        Answer story1020answer4 = new Answer();
        Answer story1020answer5 = new Answer();
        Answer story1020answer6 = new Answer();
        Answer story1020answer7 = new Answer();
        Answer story1020answer8 = new Answer();
        Answer story1020answer9 = new Answer();

        story1020answer1.isPreRequiseNotIn = true;
        story1020answer1.answerNumberPreRequise = new List<int>{ 1001 };
        story1020answer2.isPreRequiseNotIn = true;
        story1020answer2.answerNumberPreRequise = new List<int>{ 1002 };
        story1020answer3.isPreRequiseNotIn = true;
        story1020answer3.answerNumberPreRequise = new List<int>{ 1003 };
        story1020answer4.isPreRequiseNotIn = true;
        story1020answer4.answerNumberPreRequise = new List<int>{ 1004 };
        story1020answer5.isPreRequiseNotIn = true;
        story1020answer5.answerNumberPreRequise = new List<int>{ 1005 };
        story1020answer6.isPreRequiseNotIn = true;
        story1020answer6.answerNumberPreRequise = new List<int>{ 1006 };
        story1020answer7.isPreRequiseListAndAndNotOr = false;
        story1020answer7.answerNumberPreRequise = new List<int>{ 1001, 1002 };
        story1020answer8.isPreRequiseListAndAndNotOr = false;
        story1020answer8.answerNumberPreRequise = new List<int>{ 1003, 1004 };
        story1020answer9.isPreRequiseListAndAndNotOr = false;
        story1020answer9.answerNumberPreRequise = new List<int>{ 1005, 1006 };

        story1020answer1.nextStory = 10000;
        story1020answer2.nextStory = 10000;
        story1020answer3.nextStory = 10000;
        story1020answer4.nextStory = 10000;
        story1020answer5.nextStory = 10000;
        story1020answer6.nextStory = 10000;
        story1020answer7.nextStory = 10000;
        story1020answer8.nextStory = 10000;
        story1020answer9.nextStory = 10000;
        story1020answer1.discipline = "Grace feline";
        story1020answer2.discipline = "Reflexe eclaire";
        story1020answer3.discipline = "Bond surhumain";
        story1020answer4.discipline = "Corps lethal";
        story1020answer5.discipline = "Intimider";
        story1020answer6.discipline = "Reverence";
        story1020answer7.discipline = "Rapidite";
        story1020answer8.discipline = "Prouesse";
        story1020answer9.discipline = "Baiser persistant";
        story1020answer1.description = "Vous bondissez en l�air et retombez gracieusement d�bout sur l��paule d�un soldat. Vous le poussez violemment du pied avant de sauter sur un autre.";
        story1020answer2.description = "Les soldats tirent sur vous et vous ralentissez le temps, esquivant chacune des balles.";
        story1020answer3.description = "Vous vous propulsez de toute la puissance de vos jambes � travers la masse de soldats. La violence de l�impact d�boite des �paules et les fait chuter au sol.";
        story1020answer4.description = "Vous foncez sur le premier soldat et envoyez un coup de poing fulgurant pour d�truire son gilet par balle.";
        story1020answer5.description = "Vous vous redressez de toute votre taille et regardez les soldats d�un air mauvais, semant un vent de panique surnaturelle.";
        story1020answer6.description = "Vous rayonnez de majest�, d�gageant une aura quasi divine, for�ant vos ennemis � mettre un genoux � terre.";
        story1020answer7.description = "Vous sentez le temps se solidifier autour de vous. Toutes vos actions deviennent terriblement plus rapides alors que les soldats se meuvent au ralenti.";
        story1020answer8.description = "Vous sentez vos muscles se gorger de puissance. Toutes vos actions deviennent terriblement plus destructives.";
        story1020answer9.description = "Vous plongez vos crocs dans la nuque d�un soldat. Lorsque vous le l�chez, celui-ci est envout� et se retourne contre ses alli�s pour vous prot�ger.";

        AnswerDescription story1020answer1Description1 = new AnswerDescription();
        AnswerDescription story1020answer2Description1 = new AnswerDescription();
        AnswerDescription story1020answer3Description1 = new AnswerDescription();
        AnswerDescription story1020answer4Description1 = new AnswerDescription();
        AnswerDescription story1020answer5Description1 = new AnswerDescription();
        AnswerDescription story1020answer6Description1 = new AnswerDescription();
        AnswerDescription story1020answer7Description1 = new AnswerDescription();
        AnswerDescription story1020answer8Description1 = new AnswerDescription();
        AnswerDescription story1020answer9Description1 = new AnswerDescription();

        story1020answer1Description1.answerDescriptionNumber = 1001;
        story1020answer2Description1.answerDescriptionNumber = 1002;
        story1020answer3Description1.answerDescriptionNumber = 1003;
        story1020answer4Description1.answerDescriptionNumber = 1004;
        story1020answer5Description1.answerDescriptionNumber = 1005;
        story1020answer6Description1.answerDescriptionNumber = 1006;
        story1020answer7Description1.answerDescriptionNumber = 1021;
        story1020answer8Description1.answerDescriptionNumber = 1022;
        story1020answer9Description1.answerDescriptionNumber = 1023;

        story1020answer1Description1.description = "Vous bondissez d�ennemis en ennemis tel un f�lin, posant vos pieds tant�t sur leurs t�tes, tant�t sur leurs �paules et rien ne vient perturber votre �quilibre, vous semez le chaos autours de vous.";
        story1020answer2Description1.description = "Vos ennemis sont sous le choc, et malgr� la fum�e ils ont bien vu qu�aucune balle n�a eu d'impact sur vous, vous entendez leurs jurons retentir alors qu�ils rechargent leurs armes.";
        story1020answer3Description1.description = "Des jurons explosent dans tous les sens au milieu des cris de douleur. Vous avez fonc� � travers vos ennemis tel un boulet de canon, et ceux-ci mirent quelques secondes � se relever.";
        story1020answer4Description1.description = "Le gilet est transperc� dans une gerbe de sang. Vous sentez votre poing s�enfoncer dans les tripes humides, chaudes et gluantes de votre ennemi. Vous retirez brusquement votre main et celui-ci tombe � terre, mort sur le coup.";
        story1020answer5Description1.description = "A travers les fumig�nes votre aura p�se d�une lourdeur extr�me sur les �paules des soldats, vous-en entendez quelques-un crier et partir en courant. Les autres sont terroris�s et n�osent plus s�approcher.";
        story1020answer6Description1.description = "Les soldats sont �cras�s par la force mystique qui rayonne de votre position. Les plus proches l�chent les armes et tombent � genoux, des larmes pleins les yeux devant votre splendeur. Les autres sont bloqu�s sur place quelques secondes t�tanis�s par la sc�ne.";
        story1020answer7Description1.description = "Les balles des fusils mitrailleurs filent autour de vous tandis que dans les flashs de lumi�res des canons vous frappez, d�truisant des m�choires et fracassant des nez dans des craquements sourds et cris d�horreurs.";
        story1020answer8Description1.description = "Vous agrippez le soldat le plus proche par la figure, et votre seule force de pr�hension suffit � faire exploser son visage sous vos doigts. Vous balancez son corps sur ses alli�s terroris�s.";
        story1020answer9Description1.description = "C�est un forcen� que vous venez de lib�rer, le cou encore en sang il beugle que vous �tes un dieu ancien et qu�il vous prot�gera � jamais au p�ril de sa vie. Il d�goupille une grenade et la jette au milieu des soldats qui explosent dans une gicl�e de membres arrach�s";

        story1020answer1.answerDescriptions.Add(story1020answer1Description1);
        story1020answer2.answerDescriptions.Add(story1020answer2Description1);
        story1020answer3.answerDescriptions.Add(story1020answer3Description1);
        story1020answer4.answerDescriptions.Add(story1020answer4Description1);
        story1020answer5.answerDescriptions.Add(story1020answer5Description1);
        story1020answer6.answerDescriptions.Add(story1020answer6Description1);
        story1020answer7.answerDescriptions.Add(story1020answer7Description1);
        story1020answer8.answerDescriptions.Add(story1020answer8Description1);
        story1020answer9.answerDescriptions.Add(story1020answer9Description1);

        story1020.answers.Add(story1020answer7); //Put the lvl 2 firsts
        story1020.answers.Add(story1020answer8);
        story1020.answers.Add(story1020answer9);
        story1020.answers.Add(story1020answer1);
        story1020.answers.Add(story1020answer2);
        story1020.answers.Add(story1020answer3);
        story1020.answers.Add(story1020answer4);
        story1020.answers.Add(story1020answer5);
        story1020.answers.Add(story1020answer6);

        stories.listStory.Add(story1020);

        ////STORY 2000

        Story story2000 = new Story();
        story2000.storyNumber = 2000;

        StoryDescription story2000Description1 = new StoryDescription();
        story2000Description1.answerNumberPreRequise = new List<int>();
        story2000Description1.description = "Vous sentez la chaleur dans ses paroles et savez que vous venez de trouver une nouvelle famille en ce monde, m�me si vous ne savez pas encore ce qui vous attend. Tout � coup, la porte de la salle explose, une grenade roule au sol et se met � cracher de la fum�e partout. La jeune femme se precipite dessus, mais aussitot des lasers percent la fum�e et des coups de feu retentissent dans la salle, assourdissants.";
        story2000.storyDescriptions.Add(story2000Description1);

        Answer story2000answer1 = new Answer();
        Answer story2000answer2 = new Answer();
        Answer story2000answer3 = new Answer();
        Answer story2000answer4 = new Answer();
        Answer story2000answer5 = new Answer();

        story2000answer1.isPreRequiseNotIn = true;
        story2000answer1.answerNumberPreRequise = new List<int>();
        story2000answer2.isPreRequiseNotIn = true;
        story2000answer2.answerNumberPreRequise = new List<int>();
        story2000answer3.isPreRequiseNotIn = true;
        story2000answer3.answerNumberPreRequise = new List<int>();
        story2000answer4.isPreRequiseNotIn = true;
        story2000answer4.answerNumberPreRequise = new List<int>();
        story2000answer5.isPreRequiseNotIn = true;
        story2000answer5.answerNumberPreRequise = new List<int>();

        story2000answer1.nextStory = 2010;
        story2000answer2.nextStory = 2010;
        story2000answer3.nextStory = 2010;
        story2000answer4.nextStory = 2010;
        story2000answer5.nextStory = 2010;

        story2000answer1.discipline = "Lier le famulus";
        story2000answer2.discipline = "Esprit resolu";
        story2000answer3.discipline = "Resilience";
        story2000answer4.discipline = "Poid plume";
        story2000answer5.discipline = "Yeux de la bete";
        story2000answer1.description = "Vous vous concentrez sur le gros chat et vous liez � lui pour qu�il attaque f�rocement les soldats.";
        story2000answer2.description = "Vous augmentez votre r�sistance mentale et avancez maintenant sans aucune crainte face aux soldats. La situation ne vous effraie plus du tout.";
        story2000answer3.description = "Vous augmentez votre r�sistance physique au moment o� les soldats vous tirent dessus.";
        story2000answer4.description = "Vous sautez vers les soldats et votre poids diminue jusqu�� ce que vous ne pesiez plus rien. Vous vous accrochez au plafond pour les surprendre, la gravit� n�a plus d�effet sur vous.";
        story2000answer5.description = "Votre regard devient rouge �carlate et vous permet d�identifier vos cibles � travers les fumig�nes. Les soldats les voient briller comme deux brasiers et sont terrifi�s.";

        AnswerDescription story2000answer1Description1 = new AnswerDescription();
        AnswerDescription story2000answer2Description1 = new AnswerDescription();
        AnswerDescription story2000answer3Description1 = new AnswerDescription();
        AnswerDescription story2000answer4Description1 = new AnswerDescription();
        AnswerDescription story2000answer5Description1 = new AnswerDescription();

        story2000answer1Description1.answerDescriptionNumber = 2001;
        story2000answer2Description1.answerDescriptionNumber = 2002;
        story2000answer3Description1.answerDescriptionNumber = 2003;
        story2000answer4Description1.answerDescriptionNumber = 2004;
        story2000answer5Description1.answerDescriptionNumber = 2005;

        story2000answer1Description1.description = "Les pupilles de l�animal se dilatent comme s�il comprenait votre volont�. Vous ne le commandez pas, mais il vous rend cela comme un service. Il bondit, toutes griffes dehors sur un soldat proche de vous et lui cr�ve les yeux d�un coup de pattes dans un feulement terrifiant. Vous le voyez alors bondir sur le prochain ennemi alors que sa victime se roule par terre en hurlant, aveugle � jamais.";
        story2000answer2Description1.description = "Et vous sentez que les soldats eux se mettent � h�siter longuement face � votre confiance en vous.";
        story2000answer3Description1.description = "Les balles s�enfoncent dans votre chaire, mais l�impact vous fait � peine souffrir, c�est comme si votre peau �tait devenue aussi solide et �paisse que du cuir.";
        story2000answer4Description1.description = "Et vous vous tenez l�, la t�te � l�envers dans un coin du plafond pendant que les soldats finissent d�entrer dans la pi�ce, confus de n�y trouver personne.";
        story2000answer5Description1.description = "Vous entendez quelques-un des soldats crier et partir en courant. Les autres mettent un moment avant d�oser avancer dans votre direction.";

        story2000answer1.answerDescriptions.Add(story2000answer1Description1);
        story2000answer2.answerDescriptions.Add(story2000answer2Description1);
        story2000answer3.answerDescriptions.Add(story2000answer3Description1);
        story2000answer4.answerDescriptions.Add(story2000answer4Description1);
        story2000answer5.answerDescriptions.Add(story2000answer5Description1);

        story2000.answers.Add(story2000answer1);
        story2000.answers.Add(story2000answer2);
        story2000.answers.Add(story2000answer3);
        story2000.answers.Add(story2000answer4);
        story2000.answers.Add(story2000answer5);

        stories.listStory.Add(story2000);

        //STORY 2010

        Story story2010 = new Story();
        story2010.storyNumber = 2010;

        StoryDescription story2010Description1 = new StoryDescription();
        story2010Description1.answerNumberPreRequise = new List<int>();
        story2010Description1.description = "Certains des hommes arm�s r�sistent et se mettent alors � tirer dans tous les sens, et vous sentez que vous pouvez les contrer : ";
        story2010.storyDescriptions.Add(story2010Description1);

        Answer story2010answer1 = new Answer();
        Answer story2010answer2 = new Answer();
        Answer story2010answer3 = new Answer();
        Answer story2010answer4 = new Answer();
        Answer story2010answer5 = new Answer();

        story2010answer1.isPreRequiseNotIn = true;
        story2010answer1.answerNumberPreRequise = new List<int>{ 2001 };
        story2010answer2.isPreRequiseNotIn = true;
        story2010answer2.answerNumberPreRequise = new List<int>{ 2002 };
        story2010answer3.isPreRequiseNotIn = true;
        story2010answer3.answerNumberPreRequise = new List<int>{ 2003 };
        story2010answer4.isPreRequiseNotIn = true;
        story2010answer4.answerNumberPreRequise = new List<int>{ 2004 };
        story2010answer5.isPreRequiseNotIn = true;
        story2010answer5.answerNumberPreRequise = new List<int>{ 2005 };

        story2010answer1.nextStory = 2020;
        story2010answer2.nextStory = 2020;
        story2010answer3.nextStory = 2020;
        story2010answer4.nextStory = 2020;
        story2010answer5.nextStory = 2020;

        story2010answer1.discipline = "Lier le famulus";
        story2010answer2.discipline = "Esprit resolu";
        story2010answer3.discipline = "Resilience";
        story2010answer4.discipline = "Poid plume";
        story2010answer5.discipline = "Yeux de la bete";
        story2010answer1.description = "Vous vous concentrez sur le gros chat et vous liez � lui pour qu�il attaque f�rocement les soldats.";
        story2010answer2.description = "Vous augmentez votre r�sistance mentale et avancez maintenant sans aucune crainte face aux soldats. La situation ne vous effraie plus du tout.";
        story2010answer3.description = "Vous augmentez votre r�sistance physique au moment o� les soldats vous tirent dessus.";
        story2010answer4.description = "Vous sautez vers les soldats et votre poids diminue jusqu�� ce que vous ne pesiez plus rien. Vous vous accrochez au plafond pour les surprendre, la gravit� n�a plus d�effet sur vous.";
        story2010answer5.description = "Votre regard devient rouge �carlate et vous permet d�identifier vos cibles � travers les fumig�nes. Les soldats les voient briller comme deux brasiers et sont terrifi�s.";

        AnswerDescription story2010answer1Description1 = new AnswerDescription();
        AnswerDescription story2010answer2Description1 = new AnswerDescription();
        AnswerDescription story2010answer3Description1 = new AnswerDescription();
        AnswerDescription story2010answer4Description1 = new AnswerDescription();
        AnswerDescription story2010answer5Description1 = new AnswerDescription();

        story2010answer1Description1.answerDescriptionNumber = 2001;
        story2010answer2Description1.answerDescriptionNumber = 2002;
        story2010answer3Description1.answerDescriptionNumber = 2003;
        story2010answer4Description1.answerDescriptionNumber = 2004;
        story2010answer5Description1.answerDescriptionNumber = 2005;

        story2010answer1Description1.description = "Les pupilles de l�animal se dilatent comme s�il comprenait votre volont�. Vous ne le commandez pas, mais il vous rend cela comme un service. Il bondit, toutes griffes dehors sur un soldat proche de vous et lui cr�ve les yeux d�un coup de pattes dans un feulement terrifiant. Vous le voyez alors bondir sur le prochain ennemi alors que sa victime se roule par terre en hurlant, aveugle � jamais.";
        story2010answer2Description1.description = "Et vous sentez que les soldats eux se mettent � h�siter longuement face � votre confiance en vous.";
        story2010answer3Description1.description = "Les balles s�enfoncent dans votre chaire, mais l�impact vous fait � peine souffrir, c�est comme si votre peau �tait devenue aussi solide et �paisse que du cuir.";
        story2010answer4Description1.description = "Et vous vous tenez l�, la t�te � l�envers dans un coin du plafond pendant que les soldats finissent d�entrer dans la pi�ce, confus de n�y trouver personne.";
        story2010answer5Description1.description = "Vous entendez quelques-un des soldats crier et partir en courant. Les autres mettent un moment avant d�oser avancer dans votre direction.";

        story2010answer1.answerDescriptions.Add(story2010answer1Description1);
        story2010answer2.answerDescriptions.Add(story2010answer2Description1);
        story2010answer3.answerDescriptions.Add(story2010answer3Description1);
        story2010answer4.answerDescriptions.Add(story2010answer4Description1);
        story2010answer5.answerDescriptions.Add(story2010answer5Description1);

        story2010.answers.Add(story2010answer1);
        story2010.answers.Add(story2010answer2);
        story2010.answers.Add(story2010answer3);
        story2010.answers.Add(story2010answer4);
        story2010.answers.Add(story2010answer5);

        stories.listStory.Add(story2010);

        ////STORY 2020

        Story story2020 = new Story();
        story2020.storyNumber = 2020;

        StoryDescription story2020Description1 = new StoryDescription();
        story2020Description1.answerNumberPreRequise = new List<int>();
        story2020Description1.description = "Dans une derni�re tentative pour r�sister vous voyez un petit groupe de soldat se former en un ultime bastion pr�s de la porte : ";
        story2020.storyDescriptions.Add(story2020Description1);

        Answer story2020answer1 = new Answer();
        Answer story2020answer2 = new Answer();
        Answer story2020answer3 = new Answer();
        Answer story2020answer4 = new Answer();
        Answer story2020answer5 = new Answer();
        Answer story2020answer6 = new Answer();
        Answer story2020answer7 = new Answer();
        Answer story2020answer8 = new Answer();
        Answer story2020answer9 = new Answer();
        Answer story2020answer10 = new Answer();

        story2020answer1.isPreRequiseNotIn = true;
        story2020answer1.answerNumberPreRequise = new List<int>{ 2001 };
        story2020answer2.isPreRequiseNotIn = true;
        story2020answer2.answerNumberPreRequise = new List<int>{ 2002 };
        story2020answer3.isPreRequiseNotIn = true;
        story2020answer3.answerNumberPreRequise = new List<int>{ 2003 };
        story2020answer4.isPreRequiseNotIn = true;
        story2020answer4.answerNumberPreRequise = new List<int>{ 2004 };
        story2020answer5.isPreRequiseNotIn = true;
        story2020answer5.answerNumberPreRequise = new List<int>{ 2005 };

        story2020answer6.answerNumberPreRequise = new List<int>{ 2001 };
        story2020answer7.isPreRequiseListAndAndNotOr = false;
        story2020answer7.answerNumberPreRequise = new List<int>{ 2002, 2003 };
        story2020answer8.answerNumberPreRequise = new List<int>{ 2001, 2003 };
        story2020answer9.answerNumberPreRequise = new List<int>{ 2001, 2004 };
        story2020answer10.isPreRequiseListAndAndNotOr = false;
        story2020answer10.answerNumberPreRequise = new List<int>{ 2004, 2005 };

        story2020answer1.nextStory = 10000;
        story2020answer2.nextStory = 10000;
        story2020answer3.nextStory = 10000;
        story2020answer4.nextStory = 10000;
        story2020answer5.nextStory = 10000;
        story2020answer6.nextStory = 10000;
        story2020answer7.nextStory = 10000;
        story2020answer8.nextStory = 10000;
        story2020answer9.nextStory = 10000;
        story2020answer10.nextStory = 10000;
        story2020answer1.discipline = "Lier le famulus";
        story2020answer2.discipline = "Esprit resolu";
        story2020answer3.discipline = "Resilience";
        story2020answer4.discipline = "Poid plume";
        story2020answer5.discipline = "Yeux de la bete";
        story2020answer6.discipline = "Murmure sauvage";
        story2020answer7.discipline = "Resistance";
        story2020answer8.discipline = "Bete endurante";
        story2020answer9.discipline = "Bete endurante";
        story2020answer10.discipline = "Armes bestiales";
        story2010answer1.description = "Vous vous concentrez sur le gros chat et vous liez � lui pour qu�il attaque f�rocement les soldats.";
        story2010answer2.description = "Vous augmentez votre r�sistance mentale et avancez maintenant sans aucune crainte face aux soldats. La situation ne vous effraie plus du tout.";
        story2010answer3.description = "Vous augmentez votre r�sistance physique au moment o� les soldats vous tirent dessus.";
        story2010answer4.description = "Vous sautez vers les soldats et votre poids diminue jusqu�� ce que vous ne pesiez plus rien. Vous vous accrochez au plafond pour les surprendre, la gravit� n�a plus d�effet sur vous.";
        story2010answer5.description = "Votre regard devient rouge �carlate et vous permet d�identifier vos cibles � travers les fumig�nes. Les soldats les voient briller comme deux brasiers et sont terrifi�s.";
        story2020answer6.description = "Vous avez maintenant un total contr�le sur toutes les cr�atures animales. Le gros chat blanc est desormais couvert de sang et vous lui ordonnez de redoubler de violence.";
        story2020answer7.description = "Votre peau s��paissit et les balles se fichent dans votre �chine sans p�n�trer sa surface.";
        story2020answer8.description = "Vous vous concentrez sur le chat qui continue � massacrer des soldats et lui conf�rez toute votre r�sistance.";
        story2020answer9.description = "Vous vous concentrez sur le chat qui continue � massacrer des soldats et lui conf�rez toute votre r�sistance.";
        story2020answer10.description = "Vos ongles s�allongent et prennent la forme de griffes tranchantes. Elles d�coupent indistinctement la chaire et l�acier.";

        AnswerDescription story2020answer1Description1 = new AnswerDescription();
        AnswerDescription story2020answer2Description1 = new AnswerDescription();
        AnswerDescription story2020answer3Description1 = new AnswerDescription();
        AnswerDescription story2020answer4Description1 = new AnswerDescription();
        AnswerDescription story2020answer5Description1 = new AnswerDescription();
        AnswerDescription story2020answer6Description1 = new AnswerDescription();
        AnswerDescription story2020answer7Description1 = new AnswerDescription();
        AnswerDescription story2020answer8Description1 = new AnswerDescription();
        AnswerDescription story2020answer9Description1 = new AnswerDescription();
        AnswerDescription story2020answer10Description1 = new AnswerDescription();

        story2020answer1Description1.answerDescriptionNumber = 2001;
        story2020answer2Description1.answerDescriptionNumber = 2002;
        story2020answer3Description1.answerDescriptionNumber = 2003;
        story2020answer4Description1.answerDescriptionNumber = 2004;
        story2020answer5Description1.answerDescriptionNumber = 2005;
        story2020answer6Description1.answerDescriptionNumber = 2021;
        story2020answer7Description1.answerDescriptionNumber = 2022;
        story2020answer8Description1.answerDescriptionNumber = 2023;
        story2020answer9Description1.answerDescriptionNumber = 2024;
        story2020answer10Description1.answerDescriptionNumber = 2025;

        story2010answer1Description1.description = "Les pupilles de l�animal se dilatent comme s�il comprenait votre volont�. Vous ne le commandez pas, mais il vous rend cela comme un service. Il bondit, toutes griffes dehors sur un soldat proche de vous et lui cr�ve les yeux d�un coup de pattes dans un feulement terrifiant. Vous le voyez alors bondir sur le prochain ennemi alors que sa victime se roule par terre en hurlant, aveugle � jamais.";
        story2010answer2Description1.description = "Et vous sentez que les soldats eux se mettent � h�siter longuement face � votre confiance en vous.";
        story2010answer3Description1.description = "Les balles s�enfoncent dans votre chaire, mais l�impact vous fait � peine souffrir, c�est comme si votre peau �tait devenue aussi solide et �paisse que du cuir.";
        story2010answer4Description1.description = "Et vous vous tenez l�, la t�te � l�envers dans un coin du plafond pendant que les soldats finissent d�entrer dans la pi�ce, confus de n�y trouver personne.";
        story2010answer5Description1.description = "Vous entendez quelques-un des soldats crier et partir en courant. Les autres mettent un moment avant d�oser avancer dans votre direction.";
        story2020answer6Description1.description = "Celui ci ne peut pas resister � votre appel mental et est totalement sous votre controle. Les millions d�ann�e d'�volution de son esp�ce remonte au grand jour et il d�chiqu�te avec all�gresse tous les soldats � sa port�e.";
        story2020answer7Description1.description = "Les balles qui �taient entr�es en vous en ressortent et tombent � terre dans un tintement sonore. Les soldats sont subjugu�s, vous r�sistez aux balles de front et avancez vers eux sans crainte. Ils reculent.";
        story2020answer8Description1.description = "Certains des soldats le matraque avec la crosse de leur fusils, mais c�est inutile, le chat est comme fou et les ignore comme s�ils ne lui faisaient rien et continue son carnage sanguinaire.";
        story2020answer9Description1.description = "Certains des soldats le matraque avec la crosse de leur fusils, mais c�est inutile, le chat est comme fou et les ignore comme s�ils ne lui faisaient rien et continue son carnage sanguinaire.";
        story2020answer10Description1.description = "Vous vous propulsez ainsi dans la m�l�e et d�coupez un canon en train de tirer. Vous griffez ensuite le visage d�un soldat qui tente vainement de se d�fendre en levant des mains devant son visage. Et ses mains tombent au sol, d�coup�es dans une gerbe de sang.";

        story2020answer1.answerDescriptions.Add(story2020answer1Description1);
        story2020answer2.answerDescriptions.Add(story2020answer2Description1);
        story2020answer3.answerDescriptions.Add(story2020answer3Description1);
        story2020answer4.answerDescriptions.Add(story2020answer4Description1);
        story2020answer5.answerDescriptions.Add(story2020answer5Description1);
        story2020answer6.answerDescriptions.Add(story2020answer6Description1);
        story2020answer7.answerDescriptions.Add(story2020answer7Description1);
        story2020answer8.answerDescriptions.Add(story2020answer8Description1);
        story2020answer9.answerDescriptions.Add(story2020answer9Description1);
        story2020answer10.answerDescriptions.Add(story2020answer10Description1);

        story2020.answers.Add(story2020answer6);
        story2020.answers.Add(story2020answer7); //Put the lvl 2 firsts
        story2020.answers.Add(story2020answer8);
        story2020.answers.Add(story2020answer9);
        story2020.answers.Add(story2020answer10);
        story2020.answers.Add(story2020answer1);
        story2020.answers.Add(story2020answer2);
        story2020.answers.Add(story2020answer3);
        story2020.answers.Add(story2020answer4);
        story2020.answers.Add(story2020answer5);

        stories.listStory.Add(story2020);

        ////STORY 3000

        Story story3000 = new Story();
        story3000.storyNumber = 3000;

        StoryDescription story3000Description1 = new StoryDescription();
        story3000Description1.answerNumberPreRequise = new List<int>();
        story3000Description1.description = "Vous sentez la chaleur dans ses paroles et savez que vous venez de trouver une nouvelle famille en ce monde, m�me si vous ne savez pas encore ce qui vous attend. Tout � coup, la porte de la salle explose, une grenade roule au sol et se met � cracher de la fum�e partout. La jeune femme se precipite dessus, mais aussitot des lasers percent la fum�e et des coups de feu retentissent dans la salle, assourdissants.";
        story3000.storyDescriptions.Add(story3000Description1);

        Answer story3000answer1 = new Answer();
        Answer story3000answer2 = new Answer();
        Answer story3000answer3 = new Answer();
        Answer story3000answer4 = new Answer();
        Answer story3000answer5 = new Answer();
        Answer story3000answer6 = new Answer();


        story3000answer1.isPreRequiseNotIn = true;
        story3000answer1.answerNumberPreRequise = new List<int>();
        story3000answer2.isPreRequiseNotIn = true;
        story3000answer2.answerNumberPreRequise = new List<int>();
        story3000answer3.isPreRequiseNotIn = true;
        story3000answer3.answerNumberPreRequise = new List<int>();
        story3000answer4.isPreRequiseNotIn = true;
        story3000answer4.answerNumberPreRequise = new List<int>();
        story3000answer5.isPreRequiseNotIn = true;
        story3000answer5.answerNumberPreRequise = new List<int>();
        story3000answer6.isPreRequiseNotIn = true;
        story3000answer6.answerNumberPreRequise = new List<int>();

        story3000answer1.nextStory = 3010;
        story3000answer2.nextStory = 3010;
        story3000answer3.nextStory = 3010;
        story3000answer4.nextStory = 3010;
        story3000answer5.nextStory = 3010;
        story3000answer6.nextStory = 3010;
        story3000answer1.discipline = "Sens accrus";
        story3000answer2.discipline = "Sentir linvisible";
        story3000answer3.discipline = "Brouillage memoriel";
        story3000answer4.discipline = "Contrainte";
        story3000answer5.discipline = "Manteau dombre";
        story3000answer6.discipline = "Silence de la mort";
        story3000answer1.description = "Vous vous concentrez et parvenez � voir � travers les fumig�nes, vous arrivez m�me � entendre les conversations dans les oreillettes des soldats.";
        story3000answer2.description = "Vous vous concentrez et parvenez � voir quelque chose qui �tait invisible jusqu�ici.";
        story3000answer3.description = "Vous tendez votre main en avant et supprimez brutalement les derniers souvenirs des soldats pour semer la confusion.";
        story3000answer4.description = "Vous avisez le premier soldat et lui ordonnez de se retourner en tirant sur ses alli�s.";
        story3000answer5.description = "Vous vous figez sur place, vous ne bougez pas et vous devenez totalement invisible.";
        story3000answer6.description = "Plus aucun son n'�mane de vous, c�est comme si vous les absorbiez � votre contact. Vous en profitez pour vous glisser au milieu des soldats � la faveur des fumig�nes.";

        AnswerDescription story3000answer1Description1 = new AnswerDescription();
        AnswerDescription story3000answer2Description1 = new AnswerDescription();
        AnswerDescription story3000answer3Description1 = new AnswerDescription();
        AnswerDescription story3000answer4Description1 = new AnswerDescription();
        AnswerDescription story3000answer5Description1 = new AnswerDescription();
        AnswerDescription story3000answer6Description1 = new AnswerDescription();

        story3000answer1Description1.answerDescriptionNumber = 3001;
        story3000answer2Description1.answerDescriptionNumber = 3002;
        story3000answer3Description1.answerDescriptionNumber = 3003;
        story3000answer4Description1.answerDescriptionNumber = 3004;
        story3000answer5Description1.answerDescriptionNumber = 3005;
        story3000answer6Description1.answerDescriptionNumber = 3006;

        story3000answer1Description1.description = "Vous entendez les consignes de leur chef d�escouade, vous savez ainsi que vous affrontez la team sigma de l�inquisition de Montpellier. Il y a 8 ennemis en face de vous.";
        story3000answer2Description1.description = "Vous voyez pleinement la jeune femme qui vous a transform� cette nuit. Elle est sto�que, immobile et impassible dans un coin de la pi�ce, les soldats semblent incapable de la voir et elle vous regarde dans les yeux. Vous sentez qu�elle est en train de vous juger pour exp�rimenter vos comp�tences face � cette situation.";
        story3000answer3Description1.description = "Et �a fonctionne � merveille. Vous entendez les soldats hurler confus, se demandant o� est pass� le camion et comment ils sont arriv�s dans cette pi�ce. Ils regardent autour d�eux sans comprendre et perdent de pr�cieuses secondes � prendre connaissance de leur environnement.";
        story3000answer4Description1.description = "Les yeux de ce dernier deviennent vitreux. Il se retourne brusquement en appuyant sur la g�chette et c�est un carnage, il tire dans son groupe pendant plusieurs secondes et des hurlements de douleur retentissent dans les rangs de vos ennemis.";
        story3000answer5Description1.description = "Une puissance mystique s�empare de vous et d�tourne la lumi�re de votre corps. Vous sentez que les soldats vous �vitent du regard, mais ils semblent confus, et clament qu�il n�y a personne dans la pi�ce alors m�me qu�ils balayent l�endroit o� vous vous trouvez.";
        story3000answer6Description1.description = "Vous �tes comme une ombre silencieuse, vous vous faites l�effet d�un assassin vous glissant derri�re les lignes ennemies.";

        story3000answer1.answerDescriptions.Add(story3000answer1Description1);
        story3000answer2.answerDescriptions.Add(story3000answer2Description1);
        story3000answer3.answerDescriptions.Add(story3000answer3Description1);
        story3000answer4.answerDescriptions.Add(story3000answer4Description1);
        story3000answer5.answerDescriptions.Add(story3000answer5Description1);
        story3000answer6.answerDescriptions.Add(story3000answer6Description1);

        story3000.answers.Add(story3000answer1);
        story3000.answers.Add(story3000answer2);
        story3000.answers.Add(story3000answer3);
        story3000.answers.Add(story3000answer4);
        story3000.answers.Add(story3000answer5);
        story3000.answers.Add(story3000answer6);

        stories.listStory.Add(story3000);

        //STORY 3010

        Story story3010 = new Story();
        story3010.storyNumber = 3010;

        StoryDescription story3010Description1 = new StoryDescription();
        story3010Description1.answerNumberPreRequise = new List<int>();
        story3010Description1.description = "Certains des hommes arm�s r�sistent et se mettent alors � tirer dans tous les sens, et vous sentez que vous pouvez les contrer : ";
        story3010.storyDescriptions.Add(story3010Description1);

        Answer story3010answer1 = new Answer();
        Answer story3010answer2 = new Answer();
        Answer story3010answer3 = new Answer();
        Answer story3010answer4 = new Answer();
        Answer story3010answer5 = new Answer();
        Answer story3010answer6 = new Answer();


        story3010answer1.isPreRequiseNotIn = true;
        story3010answer1.answerNumberPreRequise = new List<int>{ 3001 };
        story3010answer2.isPreRequiseNotIn = true;
        story3010answer2.answerNumberPreRequise = new List<int>{ 3002 };
        story3010answer3.isPreRequiseNotIn = true;
        story3010answer3.answerNumberPreRequise = new List<int>{ 3003 };
        story3010answer4.isPreRequiseNotIn = true;
        story3010answer4.answerNumberPreRequise = new List<int>{ 3004 };
        story3010answer5.isPreRequiseNotIn = true;
        story3010answer5.answerNumberPreRequise = new List<int>{ 3005 };
        story3010answer6.isPreRequiseNotIn = true;
        story3010answer6.answerNumberPreRequise = new List<int>{ 3006 };

        story3010answer1.nextStory = 3020;
        story3010answer2.nextStory = 3020;
        story3010answer3.nextStory = 3020;
        story3010answer4.nextStory = 3020;
        story3010answer5.nextStory = 3020;
        story3010answer6.nextStory = 3020;
        story3010answer1.discipline = "Sens accrus";
        story3010answer2.discipline = "Sentir linvisible";
        story3010answer3.discipline = "Brouillage memoriel";
        story3010answer4.discipline = "Contrainte";
        story3010answer5.discipline = "Manteau dombre";
        story3010answer6.discipline = "Silence de la mort";
        story3010answer1.description = "Vous vous concentrez et parvenez � voir � travers les fumig�nes, vous arrivez m�me � entendre les conversations dans les oreillettes des soldats.";
        story3010answer2.description = "Vous vous concentrez et parvenez � voir quelque chose qui �tait invisible jusqu�ici.";
        story3010answer3.description = "Vous tendez votre main en avant et supprimez brutalement les derniers souvenirs des soldats pour semer la confusion.";
        story3010answer4.description = "Vous avisez le premier soldat et lui ordonnez de se retourner en tirant sur ses alli�s.";
        story3010answer5.description = "Vous vous figez sur place, vous ne bougez pas et vous devenez totalement invisible.";
        story3010answer6.description = "Plus aucun son n'�mane de vous, c�est comme si vous les absorbiez � votre contact. Vous en profitez pour vous glisser au milieu des soldats � la faveur des fumig�nes.";

        AnswerDescription story3010answer1Description1 = new AnswerDescription();
        AnswerDescription story3010answer2Description1 = new AnswerDescription();
        AnswerDescription story3010answer3Description1 = new AnswerDescription();
        AnswerDescription story3010answer4Description1 = new AnswerDescription();
        AnswerDescription story3010answer5Description1 = new AnswerDescription();
        AnswerDescription story3010answer6Description1 = new AnswerDescription();

        story3010answer1Description1.answerDescriptionNumber = 3001;
        story3010answer2Description1.answerDescriptionNumber = 3002;
        story3010answer3Description1.answerDescriptionNumber = 3003;
        story3010answer4Description1.answerDescriptionNumber = 3004;
        story3010answer5Description1.answerDescriptionNumber = 3005;
        story3010answer6Description1.answerDescriptionNumber = 3006;

        story3010answer1Description1.description = "Vous entendez les consignes de leur chef d�escouade, vous savez ainsi que vous affrontez la team sigma de l�inquisition de Montpellier. Il y a 8 ennemis en face de vous.";
        story3010answer2Description1.description = "Vous voyez pleinement la jeune femme qui vous a transform� cette nuit. Elle est sto�que, immobile et impassible dans un coin de la pi�ce, les soldats semblent incapable de la voir et elle vous regarde dans les yeux. Vous sentez qu�elle est en train de vous juger pour exp�rimenter vos comp�tences face � cette situation.";
        story3010answer3Description1.description = "Et �a fonctionne � merveille. Vous entendez les soldats hurler confus, se demandant o� est pass� le camion et comment ils sont arriv�s dans cette pi�ce. Ils regardent autour d�eux sans comprendre et perdent de pr�cieuses secondes � prendre connaissance de leur environnement.";
        story3010answer4Description1.description = "Les yeux de ce dernier deviennent vitreux. Il se retourne brusquement en appuyant sur la g�chette et c�est un carnage, il tire dans son groupe pendant plusieurs secondes et des hurlements de douleur retentissent dans les rangs de vos ennemis.";
        story3010answer5Description1.description = "Une puissance mystique s�empare de vous et d�tourne la lumi�re de votre corps. Vous sentez que les soldats vous �vitent du regard, mais ils semblent confus, et clament qu�il n�y a personne dans la pi�ce alors m�me qu�ils balayent l�endroit o� vous vous trouvez.";
        story3010answer6Description1.description = "Vous �tes comme une ombre silencieuse, vous vous faites l�effet d�un assassin vous glissant derri�re les lignes ennemies.";

        story3010answer1.answerDescriptions.Add(story3010answer1Description1);
        story3010answer2.answerDescriptions.Add(story3010answer2Description1);
        story3010answer3.answerDescriptions.Add(story3010answer3Description1);
        story3010answer4.answerDescriptions.Add(story3010answer4Description1);
        story3010answer5.answerDescriptions.Add(story3010answer5Description1);
        story3010answer6.answerDescriptions.Add(story3010answer6Description1);

        story3010.answers.Add(story3010answer1);
        story3010.answers.Add(story3010answer2);
        story3010.answers.Add(story3010answer3);
        story3010.answers.Add(story3010answer4);
        story3010.answers.Add(story3010answer5);
        story3010.answers.Add(story3010answer6);

        stories.listStory.Add(story3010);

        ////STORY 3020

        Story story3020 = new Story();
        story3020.storyNumber = 3020;

        StoryDescription story3020Description1 = new StoryDescription();
        story3020Description1.answerNumberPreRequise = new List<int>();
        story3020Description1.description = "Dans une derni�re tentative pour r�sister vous voyez un petit groupe de soldat se former en un ultime bastion pr�s de la porte : ";
        story3020.storyDescriptions.Add(story3020Description1);

        Answer story3020answer1 = new Answer();
        Answer story3020answer2 = new Answer();
        Answer story3020answer3 = new Answer();
        Answer story3020answer4 = new Answer();
        Answer story3020answer5 = new Answer();
        Answer story3020answer6 = new Answer();
        Answer story3020answer7 = new Answer();
        Answer story3020answer8 = new Answer();
        Answer story3020answer9 = new Answer();

        story3020answer1.isPreRequiseNotIn = true;
        story3020answer1.answerNumberPreRequise = new List<int>{ 3001 };
        story3020answer2.isPreRequiseNotIn = true;
        story3020answer2.answerNumberPreRequise = new List<int>{ 3002 };
        story3020answer3.isPreRequiseNotIn = true;
        story3020answer3.answerNumberPreRequise = new List<int>{ 3003 };
        story3020answer4.isPreRequiseNotIn = true;
        story3020answer4.answerNumberPreRequise = new List<int>{ 3004 };
        story3020answer5.isPreRequiseNotIn = true;
        story3020answer5.answerNumberPreRequise = new List<int>{ 3005 };
        story3020answer6.isPreRequiseNotIn = true;
        story3020answer6.answerNumberPreRequise = new List<int>{ 3006 };
        story3020answer7.isPreRequiseListAndAndNotOr = false;
        story3020answer7.answerNumberPreRequise = new List<int>{ 3001, 3002 };
        story3020answer8.isPreRequiseListAndAndNotOr = false;
        story3020answer8.answerNumberPreRequise = new List<int>{ 3003, 3004 };
        story3020answer9.answerNumberPreRequise = new List<int>{ 3005 };

        story3020answer1.nextStory = 10000;
        story3020answer2.nextStory = 10000;
        story3020answer3.nextStory = 10000;
        story3020answer4.nextStory = 10000;
        story3020answer5.nextStory = 10000;
        story3020answer6.nextStory = 10000;
        story3020answer7.nextStory = 10000;
        story3020answer8.nextStory = 10000;
        story3020answer9.nextStory = 10000;
        story3020answer1.discipline = "Sens accrus";
        story3020answer2.discipline = "Sentir linvisible";
        story3020answer3.discipline = "Brouillage memoriel";
        story3020answer4.discipline = "Contrainte";
        story3020answer5.discipline = "Manteau dombre";
        story3020answer6.discipline = "Silence de la mort";
        story3020answer7.discipline = "Premonition";
        story3020answer8.discipline = "Hypnose";
        story3020answer9.discipline = "Passage invisible";
        story3020answer1.description = "Vous vous concentrez et parvenez � voir � travers les fumig�nes, vous arrivez m�me � entendre les conversations dans les oreillettes des soldats.";
        story3020answer2.description = "Vous vous concentrez et parvenez � voir quelque chose qui �tait invisible jusqu�ici.";
        story3020answer3.description = "Vous tendez votre main en avant et supprimez brutalement les derniers souvenirs des soldats pour semer la confusion.";
        story3020answer4.description = "Vous avisez le premier soldat et lui ordonnez de se retourner en tirant sur ses alli�s.";
        story3020answer5.description = "Vous vous figez sur place, vous ne bougez pas et vous devenez totalement invisible.";
        story3020answer6.description = "Plus aucun son n'�mane de vous, c�est comme si vous les absorbiez � votre contact. Vous en profitez pour vous glisser au milieu des soldats � la faveur des fumig�nes.";
        story3020answer7.description = "Vos yeux se retournent sous vos paupi�res. Vous avez une pr�monition et brusquement vous prenez conscience d�une v�rit� jusque-l� cach�e.";
        story3020answer8.description = "Vous profitez du chaos pour poser votre main sur les yeux d�un soldat et en une phrase vous l�hypnotisez compl�tement.";
        story3020answer9.description = "Vous vous mouvez librement � travers les soldats tout en restant invisible � l�oeil nue.";

        AnswerDescription story3020answer1Description1 = new AnswerDescription();
        AnswerDescription story3020answer2Description1 = new AnswerDescription();
        AnswerDescription story3020answer3Description1 = new AnswerDescription();
        AnswerDescription story3020answer4Description1 = new AnswerDescription();
        AnswerDescription story3020answer5Description1 = new AnswerDescription();
        AnswerDescription story3020answer6Description1 = new AnswerDescription();
        AnswerDescription story3020answer7Description1 = new AnswerDescription();
        AnswerDescription story3020answer8Description1 = new AnswerDescription();
        AnswerDescription story3020answer9Description1 = new AnswerDescription();

        story3020answer1Description1.answerDescriptionNumber = 3001;
        story3020answer2Description1.answerDescriptionNumber = 3002;
        story3020answer3Description1.answerDescriptionNumber = 3003;
        story3020answer4Description1.answerDescriptionNumber = 3004;
        story3020answer5Description1.answerDescriptionNumber = 3005;
        story3020answer6Description1.answerDescriptionNumber = 3006;
        story3020answer7Description1.answerDescriptionNumber = 3021;
        story3020answer8Description1.answerDescriptionNumber = 3022;
        story3020answer9Description1.answerDescriptionNumber = 3023;

        story3020answer1Description1.description = "Vous entendez les consignes de leur chef d�escouade, vous savez ainsi que vous affrontez la team sigma de l�inquisition de Montpellier. Il y a 8 ennemis en face de vous.";
        story3020answer2Description1.description = "Vous voyez pleinement la jeune femme qui vous a transform� cette nuit. Elle est sto�que, immobile et impassible dans un coin de la pi�ce, les soldats semblent incapable de la voir et elle vous regarde dans les yeux. Vous sentez qu�elle est en train de vous juger pour exp�rimenter vos comp�tences face � cette situation.";
        story3020answer3Description1.description = "Et �a fonctionne � merveille. Vous entendez les soldats hurler confus, se demandant o� est pass� le camion et comment ils sont arriv�s dans cette pi�ce. Ils regardent autour d�eux sans comprendre et perdent de pr�cieuses secondes � prendre connaissance de leur environnement.";
        story3020answer4Description1.description = "Les yeux de ce dernier deviennent vitreux. Il se retourne brusquement en appuyant sur la g�chette et c�est un carnage, il tire dans son groupe pendant plusieurs secondes et des hurlements de douleur retentissent dans les rangs de vos ennemis.";
        story3020answer5Description1.description = "Une puissance mystique s�empare de vous et d�tourne la lumi�re de votre corps. Vous sentez que les soldats vous �vitent du regard, mais ils semblent confus, et clament qu�il n�y a personne dans la pi�ce alors m�me qu�ils balayent l�endroit o� vous vous trouvez.";
        story3020answer6Description1.description = "Vous �tes comme une ombre silencieuse, vous vous faites l�effet d�un assassin vous glissant derri�re les lignes ennemies.";
        story3020answer7Description1.description = "Une grenade d�goupill�e � roul�e jusqu�� vous, cette v�rit� vous frappe. Vous regardez � vos pied et savez qu�il vous reste 2 secondes pour la renvoyer avant qu�elle ne vous explose dessus. Vous la ramassez et la projetez de toutes vos forces sur vos ennemis. A peine a- t-elle quitt� vos doigts et parcourus 3 m�tres qu�elle d�tonne dans un souffle explosif projetant des membres dans une gerbe de sang.";
        story3020answer8Description1.description = "Il est persuad� d��tre votre petit fr�re, et compte utiliser tout son arsenal d'armes pour vous prot�ger. Vous le voyez sortir une grenade, il vous fait un clin d'�il rassurant et la balance sur vos ennemis. A peine a- t-elle quitt� ses doigts et parcourus 3 m�tres qu�elle d�tonne dans un souffle explosif projetant des membres dans une gerbe de sang.";
        story3020answer9Description1.description = "Vous en profitez pour d�goupiller en toute discr�tion une des grenades � leur ceinture, ils ne vous voient pas circuler dans leur rang et ils ne virent pas non plus la grenade d�tonner encore accroch�e sur un pantalon. Vous �tes d�j� loin quand la grenade d�tonne dans un souffle explosif projetant des membres dans une gerbe de sang.";

        story3020answer1.answerDescriptions.Add(story3020answer1Description1);
        story3020answer2.answerDescriptions.Add(story3020answer2Description1);
        story3020answer3.answerDescriptions.Add(story3020answer3Description1);
        story3020answer4.answerDescriptions.Add(story3020answer4Description1);
        story3020answer5.answerDescriptions.Add(story3020answer5Description1);
        story3020answer6.answerDescriptions.Add(story3020answer6Description1);
        story3020answer7.answerDescriptions.Add(story3020answer7Description1);
        story3020answer8.answerDescriptions.Add(story3020answer8Description1);
        story3020answer9.answerDescriptions.Add(story3020answer9Description1);

        story3020.answers.Add(story3020answer7); //Put the lvl 2 firsts
        story3020.answers.Add(story3020answer8);
        story3020.answers.Add(story3020answer9);
        story3020.answers.Add(story3020answer1);
        story3020.answers.Add(story3020answer2);
        story3020.answers.Add(story3020answer3);
        story3020.answers.Add(story3020answer4);
        story3020.answers.Add(story3020answer5);
        story3020.answers.Add(story3020answer6);

        stories.listStory.Add(story3020);




        //////////STORY 4000

        Story story4000 = new Story();
        story4000.storyNumber = 4000;

        StoryDescription story4000Description1 = new StoryDescription();
        story4000Description1.answerNumberPreRequise = new List<int>();
        story4000Description1.description = "Vous sentez la chaleur dans ses paroles et savez que vous venez de trouver une nouvelle famille en ce monde, m�me si vous ne savez pas encore ce qui vous attend. Tout � coup, la porte de la salle explose, une grenade roule au sol et se met � cracher de la fum�e partout. La jeune femme se precipite dessus, mais aussitot des lasers percent la fum�e et des coups de feu retentissent dans la salle, assourdissants.";
        story4000.storyDescriptions.Add(story4000Description1);

        Answer story4000answer1 = new Answer();
        Answer story4000answer2 = new Answer();
        Answer story4000answer3 = new Answer();
        Answer story4000answer4 = new Answer();
        Answer story4000answer5 = new Answer();

        story4000answer1.isPreRequiseNotIn = true;
        story4000answer1.answerNumberPreRequise = new List<int>();
        story4000answer2.isPreRequiseNotIn = true;
        story4000answer2.answerNumberPreRequise = new List<int>();
        story4000answer3.isPreRequiseNotIn = true;
        story4000answer3.answerNumberPreRequise = new List<int>();
        story4000answer4.isPreRequiseNotIn = true;
        story4000answer4.answerNumberPreRequise = new List<int>();
        story4000answer5.isPreRequiseNotIn = true;
        story4000answer5.answerNumberPreRequise = new List<int>();

        story4000answer1.nextStory = 4010;
        story4000answer2.nextStory = 4010;
        story4000answer3.nextStory = 4010;
        story4000answer4.nextStory = 4010;
        story4000answer5.nextStory = 4010;
        story4000answer1.discipline = "Bond surhumain";
        story4000answer2.discipline = "Corps lethal";
        story4000answer3.discipline = "Lier le famulus";
        story4000answer4.discipline = "Manteau dombre";
        story4000answer5.discipline = "Silence de la mort";
        story4000answer1.description = "Vous vous propulsez de toute la puissance de vos jambes � travers la masse de soldats. La violence de l�impact d�boite des �paules et les fait chuter au sol.";
        story4000answer2.description = "Vous foncez sur le premier soldat et envoyez un coup de poing fulgurant pour d�truire son gilet par balle.";
        story4000answer3.description = "Vous vous concentrez sur le gros chat et vous liez � lui pour qu�il attaque f�rocement les soldats.";
        story4000answer4.description = "Vous vous figez sur place, vous ne bougez pas et vous devenez totalement invisible.";
        story4000answer5.description = "Plus aucun son n'�mane de vous, c�est comme si vous les absorbiez � votre contact. Vous en profitez pour vous glisser au milieu des soldats � la faveur des fumig�nes.";

        AnswerDescription story4000answer1Description1 = new AnswerDescription();
        AnswerDescription story4000answer2Description1 = new AnswerDescription();
        AnswerDescription story4000answer3Description1 = new AnswerDescription();
        AnswerDescription story4000answer4Description1 = new AnswerDescription();
        AnswerDescription story4000answer5Description1 = new AnswerDescription();

        story4000answer1Description1.answerDescriptionNumber = 4001;
        story4000answer2Description1.answerDescriptionNumber = 4002;
        story4000answer3Description1.answerDescriptionNumber = 4003;
        story4000answer4Description1.answerDescriptionNumber = 4004;
        story4000answer5Description1.answerDescriptionNumber = 4005;

        story4000answer1Description1.description = "Des jurons explosent dans tous les sens au milieu des cris de douleur. Vous avez fonc� � travers vos ennemis tel un boulet de canon, et ceux-ci mirent quelques secondes � se relever.";
        story4000answer2Description1.description = "Le gilet est transperc� dans une gerbe de sang. Vous sentez votre poing s�enfoncer dans les tripes humides, chaudes et gluantes de votre ennemi. Vous retirez brusquement votre main et celui-ci tombe � terre, mort sur le coup. ";
        story4000answer3Description1.description = "Les pupilles de l�animal se dilatent comme s�il comprenait votre volont�. Vous ne le commandez pas, mais il vous rend cela comme un service. Il bondit, toutes griffes dehors sur un soldat proche de vous et lui cr�ve les yeux d�un coup de pattes dans un feulement terrifiant. Vous le voyez alors bondir sur le prochain ennemi alors que sa victime se roule par terre en hurlant, aveugle � jamais.";
        story4000answer4Description1.description = "Une puissance mystique s�empare de vous et d�tourne la lumi�re de votre corps. Vous sentez que les soldats vous �vitent du regard, mais ils semblent confus, et clament qu�il n�y a personne dans la pi�ce alors m�me qu�ils balayent l�endroit o� vous vous trouvez.";
        story4000answer5Description1.description = "Vous �tes comme une ombre silencieuse, vous vous faites l�effet d�un assassin vous glissant derri�re les lignes ennemies.";

        story4000answer1.answerDescriptions.Add(story4000answer1Description1);
        story4000answer2.answerDescriptions.Add(story4000answer2Description1);
        story4000answer3.answerDescriptions.Add(story4000answer3Description1);
        story4000answer4.answerDescriptions.Add(story4000answer4Description1);
        story4000answer5.answerDescriptions.Add(story4000answer5Description1);

        story4000.answers.Add(story4000answer1);
        story4000.answers.Add(story4000answer2);
        story4000.answers.Add(story4000answer3);
        story4000.answers.Add(story4000answer4);
        story4000.answers.Add(story4000answer5);

        stories.listStory.Add(story4000);

        //STORY 4010

        Story story4010 = new Story();
        story4010.storyNumber = 4010;

        StoryDescription story4010Description1 = new StoryDescription();
        story4010Description1.answerNumberPreRequise = new List<int>();
        story4010Description1.description = "Certains des hommes arm�s r�sistent et se mettent alors � tirer dans tous les sens, et vous sentez que vous pouvez les contrer : ";
        story4010.storyDescriptions.Add(story4010Description1);

        Answer story4010answer1 = new Answer();
        Answer story4010answer2 = new Answer();
        Answer story4010answer3 = new Answer();
        Answer story4010answer4 = new Answer();
        Answer story4010answer5 = new Answer();

        story4010answer1.isPreRequiseNotIn = true;
        story4010answer1.answerNumberPreRequise = new List<int>{ 4001 };
        story4010answer2.isPreRequiseNotIn = true;
        story4010answer2.answerNumberPreRequise = new List<int>{ 4002 };
        story4010answer3.isPreRequiseNotIn = true;
        story4010answer3.answerNumberPreRequise = new List<int>{ 4003 };
        story4010answer4.isPreRequiseNotIn = true;
        story4010answer4.answerNumberPreRequise = new List<int>{ 4004 };
        story4010answer5.isPreRequiseNotIn = true;
        story4010answer5.answerNumberPreRequise = new List<int>{ 4005 };

        story4010answer1.nextStory = 4020;
        story4010answer2.nextStory = 4020;
        story4010answer3.nextStory = 4020;
        story4010answer4.nextStory = 4020;
        story4010answer5.nextStory = 4020;
        story4010answer1.discipline = "Bond surhumain";
        story4010answer2.discipline = "Corps lethal";
        story4010answer3.discipline = "Lier le famulus";
        story4010answer4.discipline = "Manteau dombre";
        story4010answer5.discipline = "Silence de la mort";
        story4010answer1.description = "Vous vous propulsez de toute la puissance de vos jambes � travers la masse de soldats. La violence de l�impact d�boite des �paules et les fait chuter au sol.";
        story4010answer2.description = "Vous foncez sur le premier soldat et envoyez un coup de poing fulgurant pour d�truire son gilet par balle.";
        story4010answer3.description = "Vous vous concentrez sur le gros chat et vous liez � lui pour qu�il attaque f�rocement les soldats.";
        story4010answer4.description = "Vous vous figez sur place, vous ne bougez pas et vous devenez totalement invisible.";
        story4010answer5.description = "Plus aucun son n'�mane de vous, c�est comme si vous les absorbiez � votre contact. Vous en profitez pour vous glisser au milieu des soldats � la faveur des fumig�nes.";

        AnswerDescription story4010answer1Description1 = new AnswerDescription();
        AnswerDescription story4010answer2Description1 = new AnswerDescription();
        AnswerDescription story4010answer3Description1 = new AnswerDescription();
        AnswerDescription story4010answer4Description1 = new AnswerDescription();
        AnswerDescription story4010answer5Description1 = new AnswerDescription();

        story4010answer1Description1.answerDescriptionNumber = 4001;
        story4010answer2Description1.answerDescriptionNumber = 4002;
        story4010answer3Description1.answerDescriptionNumber = 4003;
        story4010answer4Description1.answerDescriptionNumber = 4004;
        story4010answer5Description1.answerDescriptionNumber = 4005;

        story4010answer1Description1.description = "Des jurons explosent dans tous les sens au milieu des cris de douleur. Vous avez fonc� � travers vos ennemis tel un boulet de canon, et ceux-ci mirent quelques secondes � se relever.";
        story4010answer2Description1.description = "Le gilet est transperc� dans une gerbe de sang. Vous sentez votre poing s�enfoncer dans les tripes humides, chaudes et gluantes de votre ennemi. Vous retirez brusquement votre main et celui-ci tombe � terre, mort sur le coup. ";
        story4010answer3Description1.description = "Les pupilles de l�animal se dilatent comme s�il comprenait votre volont�. Vous ne le commandez pas, mais il vous rend cela comme un service. Il bondit, toutes griffes dehors sur un soldat proche de vous et lui cr�ve les yeux d�un coup de pattes dans un feulement terrifiant. Vous le voyez alors bondir sur le prochain ennemi alors que sa victime se roule par terre en hurlant, aveugle � jamais.";
        story4010answer4Description1.description = "Une puissance mystique s�empare de vous et d�tourne la lumi�re de votre corps. Vous sentez que les soldats vous �vitent du regard, mais ils semblent confus, et clament qu�il n�y a personne dans la pi�ce alors m�me qu�ils balayent l�endroit o� vous vous trouvez.";
        story4010answer5Description1.description = "Vous �tes comme une ombre silencieuse, vous vous faites l�effet d�un assassin vous glissant derri�re les lignes ennemies.";

        story4010answer1.answerDescriptions.Add(story4010answer1Description1);
        story4010answer2.answerDescriptions.Add(story4010answer2Description1);
        story4010answer3.answerDescriptions.Add(story4010answer3Description1);
        story4010answer4.answerDescriptions.Add(story4010answer4Description1);
        story4010answer5.answerDescriptions.Add(story4010answer5Description1);

        story4010.answers.Add(story4010answer1);
        story4010.answers.Add(story4010answer2);
        story4010.answers.Add(story4010answer3);
        story4010.answers.Add(story4010answer4);
        story4010.answers.Add(story4010answer5);

        stories.listStory.Add(story4010);

        ////STORY 4020

        Story story4020 = new Story();
        story4020.storyNumber = 4020;

        StoryDescription story4020Description1 = new StoryDescription();
        story4020Description1.answerNumberPreRequise = new List<int>();
        story4020Description1.description = "Dans une derni�re tentative pour r�sister vous voyez un petit groupe de soldat se former en un ultime bastion pr�s de la porte : ";
        story4020.storyDescriptions.Add(story4020Description1);

        Answer story4020answer1 = new Answer();
        Answer story4020answer2 = new Answer();
        Answer story4020answer3 = new Answer();
        Answer story4020answer4 = new Answer();
        Answer story4020answer5 = new Answer();
        Answer story4020answer6 = new Answer();
        Answer story4020answer7 = new Answer();
        Answer story4020answer8 = new Answer();

        story4020answer1.isPreRequiseNotIn = true;
        story4020answer1.answerNumberPreRequise = new List<int>{ 4001 };
        story4020answer2.isPreRequiseNotIn = true;
        story4020answer2.answerNumberPreRequise = new List<int>{ 4002 };
        story4020answer3.isPreRequiseNotIn = true;
        story4020answer3.answerNumberPreRequise = new List<int>{ 4003 };
        story4020answer4.isPreRequiseNotIn = true;
        story4020answer4.answerNumberPreRequise = new List<int>{ 4004 };
        story4020answer5.isPreRequiseNotIn = true;
        story4020answer5.answerNumberPreRequise = new List<int>{ 4005 };
        story4020answer6.isPreRequiseListAndAndNotOr = false;
        story4020answer6.answerNumberPreRequise = new List<int>{ 4001, 4002 };
        story4020answer7.answerNumberPreRequise = new List<int>{ 4003 };
        story4020answer8.answerNumberPreRequise = new List<int>{ 4004 };

        story4020answer1.nextStory = 10000;
        story4020answer2.nextStory = 10000;
        story4020answer3.nextStory = 10000;
        story4020answer4.nextStory = 10000;
        story4020answer5.nextStory = 10000;
        story4020answer6.nextStory = 10000;
        story4020answer7.nextStory = 10000;
        story4020answer8.nextStory = 10000;
        story4020answer1.discipline = "Bond surhumain";
        story4020answer2.discipline = "Corps lethal";
        story4020answer3.discipline = "Lier le famulus";
        story4020answer4.discipline = "Manteau dombre";
        story4020answer5.discipline = "Silence de la mort";
        story4020answer6.discipline = "Prouesse";
        story4020answer7.discipline = "Murmure sauvage";
        story4020answer8.discipline = "Passage invisible";
        story4020answer1.description = "Vous vous propulsez de toute la puissance de vos jambes � travers la masse de soldats. La violence de l�impact d�boite des �paules et les fait chuter au sol.";
        story4020answer2.description = "Vous foncez sur le premier soldat et envoyez un coup de poing fulgurant pour d�truire son gilet par balle.";
        story4020answer3.description = "Vous vous concentrez sur le gros chat et vous liez � lui pour qu�il attaque f�rocement les soldats.";
        story4020answer4.description = "Vous vous figez sur place, vous ne bougez pas et vous devenez totalement invisible.";
        story4020answer5.description = "Plus aucun son n'�mane de vous, c�est comme si vous les absorbiez � votre contact. Vous en profitez pour vous glisser au milieu des soldats � la faveur des fumig�nes.";
        story4020answer6.description = "Vous sentez vos muscles se gorger de puissance. Toutes vos actions deviennent terriblement plus destructives.";
        story4020answer7.description = "Vous avez maintenant un total contr�le sur toutes les cr�atures animales. Le gros chat blanc est desormais couvert de sang et vous lui ordonnez de redoubler de violence.";
        story4020answer8.description = "Vous vous mouvez librement � travers les soldats tout en restant invisible � l�oeil nue.";

        AnswerDescription story4020answer1Description1 = new AnswerDescription();
        AnswerDescription story4020answer2Description1 = new AnswerDescription();
        AnswerDescription story4020answer3Description1 = new AnswerDescription();
        AnswerDescription story4020answer4Description1 = new AnswerDescription();
        AnswerDescription story4020answer5Description1 = new AnswerDescription();
        AnswerDescription story4020answer6Description1 = new AnswerDescription();
        AnswerDescription story4020answer7Description1 = new AnswerDescription();
        AnswerDescription story4020answer8Description1 = new AnswerDescription();

        story4020answer1Description1.answerDescriptionNumber = 4001;
        story4020answer2Description1.answerDescriptionNumber = 4002;
        story4020answer3Description1.answerDescriptionNumber = 4003;
        story4020answer4Description1.answerDescriptionNumber = 4004;
        story4020answer5Description1.answerDescriptionNumber = 4005;
        story4020answer6Description1.answerDescriptionNumber = 4021;
        story4020answer7Description1.answerDescriptionNumber = 4022;
        story4020answer8Description1.answerDescriptionNumber = 4023;

        story4020answer1Description1.description = "Des jurons explosent dans tous les sens au milieu des cris de douleur. Vous avez fonc� � travers vos ennemis tel un boulet de canon, et ceux-ci mirent quelques secondes � se relever.";
        story4020answer2Description1.description = "Le gilet est transperc� dans une gerbe de sang. Vous sentez votre poing s�enfoncer dans les tripes humides, chaudes et gluantes de votre ennemi. Vous retirez brusquement votre main et celui-ci tombe � terre, mort sur le coup. ";
        story4020answer3Description1.description = "Les pupilles de l�animal se dilatent comme s�il comprenait votre volont�. Vous ne le commandez pas, mais il vous rend cela comme un service. Il bondit, toutes griffes dehors sur un soldat proche de vous et lui cr�ve les yeux d�un coup de pattes dans un feulement terrifiant. Vous le voyez alors bondir sur le prochain ennemi alors que sa victime se roule par terre en hurlant, aveugle � jamais.";
        story4020answer4Description1.description = "Une puissance mystique s�empare de vous et d�tourne la lumi�re de votre corps. Vous sentez que les soldats vous �vitent du regard, mais ils semblent confus, et clament qu�il n�y a personne dans la pi�ce alors m�me qu�ils balayent l�endroit o� vous vous trouvez.";
        story4020answer5Description1.description = "Vous �tes comme une ombre silencieuse, vous vous faites l�effet d�un assassin vous glissant derri�re les lignes ennemies.";
        story4020answer6Description1.description = "Vous agrippez le soldat le plus proche par la figure, et votre seule force de pr�hension suffit � faire exploser son visage sous vos doigts. Vous balancez son corps sur ses alli�s terroris�s.";
        story4020answer7Description1.description = "Celui ci ne peut pas resister � votre appel mental et est totalement sous votre controle. Les millions d�ann�e d'�volution de son esp�ce remonte au grand jour et il d�chiqu�te avec all�gresse tous les soldats � sa port�e.";
        story4020answer8Description1.description = "Vous en profitez pour d�goupiller en toute discr�tion une des grenades � leur ceinture, ils ne vous voient pas circuler dans leur rang et ils ne virent pas non plus la grenade d�tonner encore accroch�e sur un pantalon. Vous �tes d�j� loin quand la grenade d�tonne dans un souffle explosif projetant des membres dans une gerbe de sang.";

        story4020answer1.answerDescriptions.Add(story4020answer1Description1);
        story4020answer2.answerDescriptions.Add(story4020answer2Description1);
        story4020answer3.answerDescriptions.Add(story4020answer3Description1);
        story4020answer4.answerDescriptions.Add(story4020answer4Description1);
        story4020answer5.answerDescriptions.Add(story4020answer5Description1);
        story4020answer6.answerDescriptions.Add(story4020answer6Description1);
        story4020answer7.answerDescriptions.Add(story4020answer7Description1);
        story4020answer8.answerDescriptions.Add(story4020answer8Description1);

        story4020.answers.Add(story4020answer6);
        story4020.answers.Add(story4020answer7); //Put the lvl 2 firsts
        story4020.answers.Add(story4020answer8);
        story4020.answers.Add(story4020answer1);
        story4020.answers.Add(story4020answer2);
        story4020.answers.Add(story4020answer3);
        story4020.answers.Add(story4020answer4);
        story4020.answers.Add(story4020answer5);

        stories.listStory.Add(story4020);

        /////STORY 5000



        Story story5000 = new Story();
        story5000.storyNumber = 5000;

        StoryDescription story5000Description1 = new StoryDescription();
        story5000Description1.answerNumberPreRequise = new List<int>();
        story5000Description1.description = "Vous sentez la chaleur dans ses paroles et savez que vous venez de trouver une nouvelle famille en ce monde, m�me si vous ne savez pas encore ce qui vous attend. Tout � coup, la porte de la salle explose, une grenade roule au sol et se met � cracher de la fum�e partout. La jeune femme se precipite dessus, mais aussitot des lasers percent la fum�e et des coups de feu retentissent dans la salle, assourdissants.";
        story5000.storyDescriptions.Add(story5000Description1);

        Answer story5000answer1 = new Answer();
        Answer story5000answer2 = new Answer();
        Answer story5000answer3 = new Answer();
        Answer story5000answer4 = new Answer();
        Answer story5000answer5 = new Answer();
        Answer story5000answer6 = new Answer();


        story5000answer1.isPreRequiseNotIn = true;
        story5000answer1.answerNumberPreRequise = new List<int>();
        story5000answer2.isPreRequiseNotIn = true;
        story5000answer2.answerNumberPreRequise = new List<int>();
        story5000answer3.isPreRequiseNotIn = true;
        story5000answer3.answerNumberPreRequise = new List<int>();
        story5000answer4.isPreRequiseNotIn = true;
        story5000answer4.answerNumberPreRequise = new List<int>();
        story5000answer5.isPreRequiseNotIn = true;
        story5000answer5.answerNumberPreRequise = new List<int>();
        story5000answer6.isPreRequiseNotIn = true;
        story5000answer6.answerNumberPreRequise = new List<int>();

        story5000answer1.nextStory = 5010;
        story5000answer2.nextStory = 5010;
        story5000answer3.nextStory = 5010;
        story5000answer4.nextStory = 5010;
        story5000answer5.nextStory = 5010;
        story5000answer6.nextStory = 5010;
        story5000answer1.discipline = "Grace feline";
        story5000answer2.discipline = "Reflexe eclaire";
        story5000answer3.discipline = "Intimider";
        story5000answer4.discipline = "Reverence";
        story5000answer5.discipline = "Sens accrus";
        story5000answer6.discipline = "Sentir linvisible";
        story5000answer1.description = "Vous bondissez en l�air et retombez gracieusement d�bout sur l��paule d�un soldat. Vous le poussez violemment du pied avant de sauter sur un autre.";
        story5000answer2.description = "Les soldats tirent sur vous et vous ralentissez le temps, esquivant chacune des balles.";
        story5000answer3.description = "Vous vous redressez de toute votre taille et regardez les soldats d�un air mauvais, semant un vent de panique surnaturelle.";
        story5000answer4.description = "Vous rayonnez de majest�, d�gageant une aura quasi divine, for�ant vos ennemis � mettre un genoux � terre.";
        story5000answer5.description = "Vous vous concentrez et parvenez � voir � travers les fumig�nes, vous arrivez m�me � entendre les conversations dans les oreillettes des soldats.";
        story5000answer6.description = "Vous vous concentrez et parvenez � voir quelque chose qui �tait invisible jusqu�ici.";

        AnswerDescription story5000answer1Description1 = new AnswerDescription();
        AnswerDescription story5000answer2Description1 = new AnswerDescription();
        AnswerDescription story5000answer3Description1 = new AnswerDescription();
        AnswerDescription story5000answer4Description1 = new AnswerDescription();
        AnswerDescription story5000answer5Description1 = new AnswerDescription();
        AnswerDescription story5000answer6Description1 = new AnswerDescription();

        story5000answer1Description1.answerDescriptionNumber = 5001;
        story5000answer2Description1.answerDescriptionNumber = 5002;
        story5000answer3Description1.answerDescriptionNumber = 5003;
        story5000answer4Description1.answerDescriptionNumber = 5004;
        story5000answer5Description1.answerDescriptionNumber = 5005;
        story5000answer6Description1.answerDescriptionNumber = 5006;

        story5000answer1Description1.description = "Vous bondissez d�ennemis en ennemis tel un f�lin, posant vos pieds tant�t sur leurs t�tes, tant�t sur leurs �paules et rien ne vient perturber votre �quilibre, vous semez le chaos autours de vous.";
        story5000answer2Description1.description = "Vos ennemis sont sous le choc, et malgr� la fum�e ils ont bien vu qu�aucune balle n�a eu d'impact sur vous, vous entendez leurs jurons retentir alors qu�ils rechargent leurs armes.";
        story5000answer3Description1.description = "A travers les fumig�nes votre aura p�se d�une lourdeur extr�me sur les �paules des soldats, vous-en entendez quelques-un crier et partir en courant. Les autres sont terroris�s et n�osent plus s�approcher.";
        story5000answer4Description1.description = "Les soldats sont �cras�s par la force mystique qui rayonne de votre position. Les plus proches l�chent les armes et tombent � genoux, des larmes pleins les yeux devant votre splendeur. Les autres sont bloqu�s sur place quelques secondes t�tanis�s par la sc�ne.";
        story5000answer5Description1.description = "Vous entendez les consignes de leur chef d�escouade, vous savez ainsi que vous affrontez la team sigma de l�inquisition de Montpellier. Il y a 8 ennemis en face de vous.";
        story5000answer6Description1.description = "Vous voyez pleinement la jeune femme qui vous a transform� cette nuit. Elle est sto�que, immobile et impassible dans un coin de la pi�ce, les soldats semblent incapable de la voir et elle vous regarde dans les yeux. Vous sentez qu�elle est en train de vous juger pour exp�rimenter vos comp�tences face � cette situation.";

        story5000answer1.answerDescriptions.Add(story5000answer1Description1);
        story5000answer2.answerDescriptions.Add(story5000answer2Description1);
        story5000answer3.answerDescriptions.Add(story5000answer3Description1);
        story5000answer4.answerDescriptions.Add(story5000answer4Description1);
        story5000answer5.answerDescriptions.Add(story5000answer5Description1);
        story5000answer6.answerDescriptions.Add(story5000answer6Description1);

        story5000.answers.Add(story5000answer1);
        story5000.answers.Add(story5000answer2);
        story5000.answers.Add(story5000answer3);
        story5000.answers.Add(story5000answer4);
        story5000.answers.Add(story5000answer5);
        story5000.answers.Add(story5000answer6);

        stories.listStory.Add(story5000);

        //STORY 5010

        Story story5010 = new Story();
        story5010.storyNumber = 5010;

        StoryDescription story5010Description1 = new StoryDescription();
        story5010Description1.answerNumberPreRequise = new List<int>();
        story5010Description1.description = "Certains des hommes arm�s r�sistent et se mettent alors � tirer dans tous les sens, et vous sentez que vous pouvez les contrer : ";
        story5010.storyDescriptions.Add(story5010Description1);

        Answer story5010answer1 = new Answer();
        Answer story5010answer2 = new Answer();
        Answer story5010answer3 = new Answer();
        Answer story5010answer4 = new Answer();
        Answer story5010answer5 = new Answer();
        Answer story5010answer6 = new Answer();


        story5010answer1.isPreRequiseNotIn = true;
        story5010answer1.answerNumberPreRequise = new List<int>{ 5001 };
        story5010answer2.isPreRequiseNotIn = true;
        story5010answer2.answerNumberPreRequise = new List<int>{ 5002 };
        story5010answer3.isPreRequiseNotIn = true;
        story5010answer3.answerNumberPreRequise = new List<int>{ 5003 };
        story5010answer4.isPreRequiseNotIn = true;
        story5010answer4.answerNumberPreRequise = new List<int>{ 5004 };
        story5010answer5.isPreRequiseNotIn = true;
        story5010answer5.answerNumberPreRequise = new List<int>{ 5005 };
        story5010answer6.isPreRequiseNotIn = true;
        story5010answer6.answerNumberPreRequise = new List<int>{ 5006 };

        story5010answer1.nextStory = 5020;
        story5010answer2.nextStory = 5020;
        story5010answer3.nextStory = 5020;
        story5010answer4.nextStory = 5020;
        story5010answer5.nextStory = 5020;
        story5010answer6.nextStory = 5020;
        story5010answer1.discipline = "Grace feline";
        story5010answer2.discipline = "Reflexe eclaire";
        story5010answer3.discipline = "Intimider";
        story5010answer4.discipline = "Reverence";
        story5010answer5.discipline = "Sens accrus";
        story5010answer6.discipline = "Sentir linvisible";
        story5010answer1.description = "Vous bondissez en l�air et retombez gracieusement d�bout sur l��paule d�un soldat. Vous le poussez violemment du pied avant de sauter sur un autre.";
        story5010answer2.description = "Les soldats tirent sur vous et vous ralentissez le temps, esquivant chacune des balles.";
        story5010answer3.description = "Vous vous redressez de toute votre taille et regardez les soldats d�un air mauvais, semant un vent de panique surnaturelle.";
        story5010answer4.description = "Vous rayonnez de majest�, d�gageant une aura quasi divine, for�ant vos ennemis � mettre un genoux � terre.";
        story5010answer5.description = "Vous vous concentrez et parvenez � voir � travers les fumig�nes, vous arrivez m�me � entendre les conversations dans les oreillettes des soldats.";
        story5010answer6.description = "Vous vous concentrez et parvenez � voir quelque chose qui �tait invisible jusqu�ici.";

        AnswerDescription story5010answer1Description1 = new AnswerDescription();
        AnswerDescription story5010answer2Description1 = new AnswerDescription();
        AnswerDescription story5010answer3Description1 = new AnswerDescription();
        AnswerDescription story5010answer4Description1 = new AnswerDescription();
        AnswerDescription story5010answer5Description1 = new AnswerDescription();
        AnswerDescription story5010answer6Description1 = new AnswerDescription();

        story5010answer1Description1.answerDescriptionNumber = 5001;
        story5010answer2Description1.answerDescriptionNumber = 5002;
        story5010answer3Description1.answerDescriptionNumber = 5003;
        story5010answer4Description1.answerDescriptionNumber = 5004;
        story5010answer5Description1.answerDescriptionNumber = 5005;
        story5010answer6Description1.answerDescriptionNumber = 5006;

        story5010answer1Description1.description = "Vous bondissez d�ennemis en ennemis tel un f�lin, posant vos pieds tant�t sur leurs t�tes, tant�t sur leurs �paules et rien ne vient perturber votre �quilibre, vous semez le chaos autours de vous.";
        story5010answer2Description1.description = "Vos ennemis sont sous le choc, et malgr� la fum�e ils ont bien vu qu�aucune balle n�a eu d'impact sur vous, vous entendez leurs jurons retentir alors qu�ils rechargent leurs armes.";
        story5010answer3Description1.description = "A travers les fumig�nes votre aura p�se d�une lourdeur extr�me sur les �paules des soldats, vous-en entendez quelques-un crier et partir en courant. Les autres sont terroris�s et n�osent plus s�approcher.";
        story5010answer4Description1.description = "Les soldats sont �cras�s par la force mystique qui rayonne de votre position. Les plus proches l�chent les armes et tombent � genoux, des larmes pleins les yeux devant votre splendeur. Les autres sont bloqu�s sur place quelques secondes t�tanis�s par la sc�ne.";
        story5010answer5Description1.description = "Vous entendez les consignes de leur chef d�escouade, vous savez ainsi que vous affrontez la team sigma de l�inquisition de Montpellier. Il y a 8 ennemis en face de vous.";
        story5010answer6Description1.description = "Vous voyez pleinement la jeune femme qui vous a transform� cette nuit. Elle est sto�que, immobile et impassible dans un coin de la pi�ce, les soldats semblent incapable de la voir et elle vous regarde dans les yeux. Vous sentez qu�elle est en train de vous juger pour exp�rimenter vos comp�tences face � cette situation.";

        story5010answer1.answerDescriptions.Add(story5010answer1Description1);
        story5010answer2.answerDescriptions.Add(story5010answer2Description1);
        story5010answer3.answerDescriptions.Add(story5010answer3Description1);
        story5010answer4.answerDescriptions.Add(story5010answer4Description1);
        story5010answer5.answerDescriptions.Add(story5010answer5Description1);
        story5010answer6.answerDescriptions.Add(story5010answer6Description1);

        story5010.answers.Add(story5010answer1);
        story5010.answers.Add(story5010answer2);
        story5010.answers.Add(story5010answer3);
        story5010.answers.Add(story5010answer4);
        story5010.answers.Add(story5010answer5);
        story5010.answers.Add(story5010answer6);

        stories.listStory.Add(story5010);

        ////STORY 5020

        Story story5020 = new Story();
        story5020.storyNumber = 5020;

        StoryDescription story5020Description1 = new StoryDescription();
        story5020Description1.answerNumberPreRequise = new List<int>();
        story5020Description1.description = "Dans une derni�re tentative pour r�sister vous voyez un petit groupe de soldat se former en un ultime bastion pr�s de la porte : ";
        story5020.storyDescriptions.Add(story5020Description1);

        Answer story5020answer1 = new Answer();
        Answer story5020answer2 = new Answer();
        Answer story5020answer3 = new Answer();
        Answer story5020answer4 = new Answer();
        Answer story5020answer5 = new Answer();
        Answer story5020answer6 = new Answer();
        Answer story5020answer7 = new Answer();
        Answer story5020answer8 = new Answer();
        Answer story5020answer9 = new Answer();

        story5020answer1.isPreRequiseNotIn = true;
        story5020answer1.answerNumberPreRequise = new List<int>{ 5001 };
        story5020answer2.isPreRequiseNotIn = true;
        story5020answer2.answerNumberPreRequise = new List<int>{ 5002 };
        story5020answer3.isPreRequiseNotIn = true;
        story5020answer3.answerNumberPreRequise = new List<int>{ 5003 };
        story5020answer4.isPreRequiseNotIn = true;
        story5020answer4.answerNumberPreRequise = new List<int>{ 5004 };
        story5020answer5.isPreRequiseNotIn = true;
        story5020answer5.answerNumberPreRequise = new List<int>{ 5005 };
        story5020answer6.isPreRequiseNotIn = true;
        story5020answer6.answerNumberPreRequise = new List<int>{ 5006 };
        story5020answer7.isPreRequiseListAndAndNotOr = false;
        story5020answer7.answerNumberPreRequise = new List<int>{ 5001, 5002 };
        story5020answer8.isPreRequiseListAndAndNotOr = false;
        story5020answer8.answerNumberPreRequise = new List<int>{ 5003, 5004 };
        story5020answer9.isPreRequiseListAndAndNotOr = false;
        story5020answer9.answerNumberPreRequise = new List<int>{ 5005, 5006 };

        story5020answer1.nextStory = 10000;
        story5020answer2.nextStory = 10000;
        story5020answer3.nextStory = 10000;
        story5020answer4.nextStory = 10000;
        story5020answer5.nextStory = 10000;
        story5020answer6.nextStory = 10000;
        story5020answer7.nextStory = 10000;
        story5020answer8.nextStory = 10000;
        story5020answer9.nextStory = 10000;
        story5020answer1.discipline = "Grace feline";
        story5020answer2.discipline = "Reflexe eclaire";
        story5020answer3.discipline = "Intimider";
        story5020answer4.discipline = "Reverence";
        story5020answer5.discipline = "Sens accrus";
        story5020answer6.discipline = "Sentir linvisible";
        story5020answer7.discipline = "Rapidite";
        story5020answer8.discipline = "Baiser persistant";
        story5020answer9.discipline = "Premonition";
        story5020answer1.description = "Vous bondissez en l�air et retombez gracieusement d�bout sur l��paule d�un soldat. Vous le poussez violemment du pied avant de sauter sur un autre.";
        story5020answer2.description = "Les soldats tirent sur vous et vous ralentissez le temps, esquivant chacune des balles.";
        story5020answer3.description = "Vous vous redressez de toute votre taille et regardez les soldats d�un air mauvais, semant un vent de panique surnaturelle.";
        story5020answer4.description = "Vous rayonnez de majest�, d�gageant une aura quasi divine, for�ant vos ennemis � mettre un genoux � terre.";
        story5020answer5.description = "Vous vous concentrez et parvenez � voir � travers les fumig�nes, vous arrivez m�me � entendre les conversations dans les oreillettes des soldats.";
        story5020answer6.description = "Vous vous concentrez et parvenez � voir quelque chose qui �tait invisible jusqu�ici.";
        story5020answer7.description = "Vous sentez le temps se solidifier autour de vous. Toutes vos actions deviennent terriblement plus rapides alors que les soldats se meuvent au ralenti.";
        story5020answer8.description = "Vous plongez vos crocs dans la nuque d�un soldat. Lorsque vous le l�chez, celui-ci est envout� et se retourne contre ses alli�s pour vous prot�ger.";
        story5020answer9.description = "Vos yeux se retournent sous vos paupi�res. Vous avez une pr�monition et brusquement vous prenez conscience d�une v�rit� jusque-l� cach�e.";

        AnswerDescription story5020answer1Description1 = new AnswerDescription();
        AnswerDescription story5020answer2Description1 = new AnswerDescription();
        AnswerDescription story5020answer3Description1 = new AnswerDescription();
        AnswerDescription story5020answer4Description1 = new AnswerDescription();
        AnswerDescription story5020answer5Description1 = new AnswerDescription();
        AnswerDescription story5020answer6Description1 = new AnswerDescription();
        AnswerDescription story5020answer7Description1 = new AnswerDescription();
        AnswerDescription story5020answer8Description1 = new AnswerDescription();
        AnswerDescription story5020answer9Description1 = new AnswerDescription();

        story5020answer1Description1.answerDescriptionNumber = 5001;
        story5020answer2Description1.answerDescriptionNumber = 5002;
        story5020answer3Description1.answerDescriptionNumber = 5003;
        story5020answer4Description1.answerDescriptionNumber = 5004;
        story5020answer5Description1.answerDescriptionNumber = 5005;
        story5020answer6Description1.answerDescriptionNumber = 5006;
        story5020answer7Description1.answerDescriptionNumber = 5021;
        story5020answer8Description1.answerDescriptionNumber = 5022;
        story5020answer9Description1.answerDescriptionNumber = 5023;

        story5020answer1Description1.description = "Vous bondissez d�ennemis en ennemis tel un f�lin, posant vos pieds tant�t sur leurs t�tes, tant�t sur leurs �paules et rien ne vient perturber votre �quilibre, vous semez le chaos autours de vous.";
        story5020answer2Description1.description = "Vos ennemis sont sous le choc, et malgr� la fum�e ils ont bien vu qu�aucune balle n�a eu d'impact sur vous, vous entendez leurs jurons retentir alors qu�ils rechargent leurs armes.";
        story5020answer3Description1.description = "A travers les fumig�nes votre aura p�se d�une lourdeur extr�me sur les �paules des soldats, vous-en entendez quelques-un crier et partir en courant. Les autres sont terroris�s et n�osent plus s�approcher.";
        story5020answer4Description1.description = "Les soldats sont �cras�s par la force mystique qui rayonne de votre position. Les plus proches l�chent les armes et tombent � genoux, des larmes pleins les yeux devant votre splendeur. Les autres sont bloqu�s sur place quelques secondes t�tanis�s par la sc�ne.";
        story5020answer5Description1.description = "Vous entendez les consignes de leur chef d�escouade, vous savez ainsi que vous affrontez la team sigma de l�inquisition de Montpellier. Il y a 8 ennemis en face de vous.";
        story5020answer6Description1.description = "Vous voyez pleinement la jeune femme qui vous a transform� cette nuit. Elle est sto�que, immobile et impassible dans un coin de la pi�ce, les soldats semblent incapable de la voir et elle vous regarde dans les yeux. Vous sentez qu�elle est en train de vous juger pour exp�rimenter vos comp�tences face � cette situation.";
        story5020answer7Description1.description = "Les balles des fusils mitrailleurs filent autour de vous tandis que dans les flashs de lumi�res des canons vous frappez, d�truisant des m�choires et fracassant des nez dans des craquements sourds et cris d�horreurs.";
        story5020answer8Description1.description = "C�est un forcen� que vous venez de lib�rer, le cou encore en sang il beugle que vous �tes un dieu ancien et qu�il vous prot�gera � jamais au p�ril de sa vie. Il d�goupille une grenade et la jette au milieu des soldats qui explosent dans une gicl�e de membres arrach�s.";
        story5020answer9Description1.description = "Une grenade d�goupill�e � roul�e jusqu�� vous, cette v�rit� vous frappe. Vous regardez � vos pied et savez qu�il vous reste 2 secondes pour la renvoyer avant qu�elle ne vous explose dessus. Vous la ramassez et la projetez de toutes vos forces sur vos ennemis. A peine a- t-elle quitt� vos doigts et parcourus 3 m�tres qu�elle d�tonne dans un souffle explosif projetant des membres dans une gerbe de sang.";

        story5020answer1.answerDescriptions.Add(story5020answer1Description1);
        story5020answer2.answerDescriptions.Add(story5020answer2Description1);
        story5020answer3.answerDescriptions.Add(story5020answer3Description1);
        story5020answer4.answerDescriptions.Add(story5020answer4Description1);
        story5020answer5.answerDescriptions.Add(story5020answer5Description1);
        story5020answer6.answerDescriptions.Add(story5020answer6Description1);
        story5020answer7.answerDescriptions.Add(story5020answer7Description1);
        story5020answer8.answerDescriptions.Add(story5020answer8Description1);
        story5020answer9.answerDescriptions.Add(story5020answer9Description1);

        story5020.answers.Add(story5020answer7); //Put the lvl 2 firsts
        story5020.answers.Add(story5020answer8);
        story5020.answers.Add(story5020answer9);
        story5020.answers.Add(story5020answer1);
        story5020.answers.Add(story5020answer2);
        story5020.answers.Add(story5020answer3);
        story5020.answers.Add(story5020answer4);
        story5020.answers.Add(story5020answer5);
        story5020.answers.Add(story5020answer6);

        stories.listStory.Add(story5020);

        ////////////STORY6000

        Story story6000 = new Story();
        story6000.storyNumber = 6000;

        StoryDescription story6000Description1 = new StoryDescription();
        story6000Description1.answerNumberPreRequise = new List<int>();
        story6000Description1.description = "Vous sentez la chaleur dans ses paroles et savez que vous venez de trouver une nouvelle famille en ce monde, m�me si vous ne savez pas encore ce qui vous attend. Tout � coup, la porte de la salle explose, une grenade roule au sol et se met � cracher de la fum�e partout. La jeune femme se precipite dessus, mais aussitot des lasers percent la fum�e et des coups de feu retentissent dans la salle, assourdissants.";
        story6000.storyDescriptions.Add(story6000Description1);

        Answer story6000answer1 = new Answer();
        Answer story6000answer2 = new Answer();
        Answer story6000answer3 = new Answer();
        Answer story6000answer4 = new Answer();
        Answer story6000answer5 = new Answer();
        Answer story6000answer6 = new Answer();


        story6000answer1.isPreRequiseNotIn = true;
        story6000answer1.answerNumberPreRequise = new List<int>();
        story6000answer2.isPreRequiseNotIn = true;
        story6000answer2.answerNumberPreRequise = new List<int>();
        story6000answer3.isPreRequiseNotIn = true;
        story6000answer3.answerNumberPreRequise = new List<int>();
        story6000answer4.isPreRequiseNotIn = true;
        story6000answer4.answerNumberPreRequise = new List<int>();
        story6000answer5.isPreRequiseNotIn = true;
        story6000answer5.answerNumberPreRequise = new List<int>();
        story6000answer6.isPreRequiseNotIn = true;
        story6000answer6.answerNumberPreRequise = new List<int>();

        story6000answer1.nextStory = 6010;
        story6000answer2.nextStory = 6010;
        story6000answer3.nextStory = 6010;
        story6000answer4.nextStory = 6010;
        story6000answer5.nextStory = 6010;
        story6000answer6.nextStory = 6010;
        story6000answer1.discipline = "Sens accrus";
        story6000answer2.discipline = "Sentir linvisible";
        story6000answer3.discipline = "Brouillage memoriel";
        story6000answer4.discipline = "Contrainte";
        story6000answer5.discipline = "Gout du sang";
        story6000answer6.discipline = "Vitae Corrosive";
        story6000answer1.description = "Vous vous concentrez et parvenez � voir � travers les fumig�nes, vous arrivez m�me � entendre les conversations dans les oreillettes des soldats.";
        story6000answer2.description = "Vous vous concentrez et parvenez � voir quelque chose qui �tait invisible jusqu�ici.";
        story6000answer3.description = "Vous tendez votre main en avant et supprimez brutalement les derniers souvenirs des soldats pour semer la confusion.";
        story6000answer4.description = "Vous avisez le premier soldat et lui ordonnez de se retourner en tirant sur ses alli�s.";
        story6000answer5.description = "Vous vous jetez sur le premier soldat et plantez vos dents dans son cou. Le go�t de son sang vous conf�re des informations � son propos.";
        story6000answer6.description = "Vous vous mordez � pleine dents l�avant bras et exp�diez d�un large mouvement votre sang en direction des soldats qui se mettent � hurler � son contact.";

        AnswerDescription story6000answer1Description1 = new AnswerDescription();
        AnswerDescription story6000answer2Description1 = new AnswerDescription();
        AnswerDescription story6000answer3Description1 = new AnswerDescription();
        AnswerDescription story6000answer4Description1 = new AnswerDescription();
        AnswerDescription story6000answer5Description1 = new AnswerDescription();
        AnswerDescription story6000answer6Description1 = new AnswerDescription();

        story6000answer1Description1.answerDescriptionNumber = 6001;
        story6000answer2Description1.answerDescriptionNumber = 6002;
        story6000answer3Description1.answerDescriptionNumber = 6003;
        story6000answer4Description1.answerDescriptionNumber = 6004;
        story6000answer5Description1.answerDescriptionNumber = 6005;
        story6000answer6Description1.answerDescriptionNumber = 6006;

        story6000answer1Description1.description = "Vous entendez les consignes de leur chef d�escouade, vous savez ainsi que vous affrontez la team sigma de l�inquisition de Montpellier. Il y a 8 ennemis en face de vous.";
        story6000answer2Description1.description = "Vous voyez pleinement la jeune femme qui vous a transform� cette nuit. Elle est sto�que, immobile et impassible dans un coin de la pi�ce, les soldats semblent incapable de la voir et elle vous regarde dans les yeux. Vous sentez qu�elle est en train de vous juger pour exp�rimenter vos comp�tences face � cette situation.";
        story6000answer3Description1.description = "Et �a fonctionne � merveille. Vous entendez les soldats hurler confus, se demandant o� est pass� le camion et comment ils sont arriv�s dans cette pi�ce. Ils regardent autour d�eux sans comprendre et perdent de pr�cieuses secondes � prendre connaissance de leur environnement.";
        story6000answer4Description1.description = "Les yeux de ce dernier deviennent vitreux. Il se retourne brusquement en appuyant sur la g�chette et c�est un carnage, il tire dans son groupe pendant plusieurs secondes et des hurlements de douleur retentissent dans les rangs de vos ennemis.";
        story6000answer5Description1.description = "Votre proie est terroris�e. Et � mesure que l�h�moglobine coule dans votre gorge vous avez des flashback des derni�res heures. Il s'appelle Michael, et est le second de cette �quipe de raid sous les ordres de Gellec l'�v�que Auxiliaire de Montpellier. Vous connaissez d�sormais l�emplacement du QG militaire de cette op�ration.";
        story6000answer6Description1.description = "Leurs armes et �quipements se mettent � fondre au contact des gouttelettes dans des nuages de fum�es corrosives. Vous les entendez tousser et vomir pendant qu�ils tentent de retirer en h�te certaines parties de leur �quipement.";

        story6000answer1.answerDescriptions.Add(story6000answer1Description1);
        story6000answer2.answerDescriptions.Add(story6000answer2Description1);
        story6000answer3.answerDescriptions.Add(story6000answer3Description1);
        story6000answer4.answerDescriptions.Add(story6000answer4Description1);
        story6000answer5.answerDescriptions.Add(story6000answer5Description1);
        story6000answer6.answerDescriptions.Add(story6000answer6Description1);

        story6000.answers.Add(story6000answer1);
        story6000.answers.Add(story6000answer2);
        story6000.answers.Add(story6000answer3);
        story6000.answers.Add(story6000answer4);
        story6000.answers.Add(story6000answer5);
        story6000.answers.Add(story6000answer6);

        stories.listStory.Add(story6000);

        //STORY 6010

        Story story6010 = new Story();
        story6010.storyNumber = 6010;

        StoryDescription story6010Description1 = new StoryDescription();
        story6010Description1.answerNumberPreRequise = new List<int>();
        story6010Description1.description = "Certains des hommes arm�s r�sistent et se mettent alors � tirer dans tous les sens, et vous sentez que vous pouvez les contrer : ";
        story6010.storyDescriptions.Add(story6010Description1);

        Answer story6010answer1 = new Answer();
        Answer story6010answer2 = new Answer();
        Answer story6010answer3 = new Answer();
        Answer story6010answer4 = new Answer();
        Answer story6010answer5 = new Answer();
        Answer story6010answer6 = new Answer();


        story6010answer1.isPreRequiseNotIn = true;
        story6010answer1.answerNumberPreRequise = new List<int>{ 6001 };
        story6010answer2.isPreRequiseNotIn = true;
        story6010answer2.answerNumberPreRequise = new List<int>{ 6002 };
        story6010answer3.isPreRequiseNotIn = true;
        story6010answer3.answerNumberPreRequise = new List<int>{ 6003 };
        story6010answer4.isPreRequiseNotIn = true;
        story6010answer4.answerNumberPreRequise = new List<int>{ 6004 };
        story6010answer5.isPreRequiseNotIn = true;
        story6010answer5.answerNumberPreRequise = new List<int>{ 6005 };
        story6010answer6.isPreRequiseNotIn = true;
        story6010answer6.answerNumberPreRequise = new List<int>{ 6006 };

        story6010answer1.nextStory = 6020;
        story6010answer2.nextStory = 6020;
        story6010answer3.nextStory = 6020;
        story6010answer4.nextStory = 6020;
        story6010answer5.nextStory = 6020;
        story6010answer6.nextStory = 6020;
        story6010answer1.discipline = "Sens accrus";
        story6010answer2.discipline = "Sentir linvisible";
        story6010answer3.discipline = "Brouillage memoriel";
        story6010answer4.discipline = "Contrainte";
        story6010answer5.discipline = "Gout du sang";
        story6010answer6.discipline = "Vitae Corrosive";
        story6010answer1.description = "Vous vous concentrez et parvenez � voir � travers les fumig�nes, vous arrivez m�me � entendre les conversations dans les oreillettes des soldats.";
        story6010answer2.description = "Vous vous concentrez et parvenez � voir quelque chose qui �tait invisible jusqu�ici.";
        story6010answer3.description = "Vous tendez votre main en avant et supprimez brutalement les derniers souvenirs des soldats pour semer la confusion.";
        story6010answer4.description = "Vous avisez le premier soldat et lui ordonnez de se retourner en tirant sur ses alli�s.";
        story6010answer5.description = "Vous vous jetez sur le premier soldat et plantez vos dents dans son cou. Le go�t de son sang vous conf�re des informations � son propos.";
        story6010answer6.description = "Vous vous mordez � pleine dents l�avant bras et exp�diez d�un large mouvement votre sang en direction des soldats qui se mettent � hurler � son contact.";

        AnswerDescription story6010answer1Description1 = new AnswerDescription();
        AnswerDescription story6010answer2Description1 = new AnswerDescription();
        AnswerDescription story6010answer3Description1 = new AnswerDescription();
        AnswerDescription story6010answer4Description1 = new AnswerDescription();
        AnswerDescription story6010answer5Description1 = new AnswerDescription();
        AnswerDescription story6010answer6Description1 = new AnswerDescription();

        story6010answer1Description1.answerDescriptionNumber = 6001;
        story6010answer2Description1.answerDescriptionNumber = 6002;
        story6010answer3Description1.answerDescriptionNumber = 6003;
        story6010answer4Description1.answerDescriptionNumber = 6004;
        story6010answer5Description1.answerDescriptionNumber = 6005;
        story6010answer6Description1.answerDescriptionNumber = 6006;

        story6010answer1Description1.description = "Vous entendez les consignes de leur chef d�escouade, vous savez ainsi que vous affrontez la team sigma de l�inquisition de Montpellier. Il y a 8 ennemis en face de vous.";
        story6010answer2Description1.description = "Vous voyez pleinement la jeune femme qui vous a transform� cette nuit. Elle est sto�que, immobile et impassible dans un coin de la pi�ce, les soldats semblent incapable de la voir et elle vous regarde dans les yeux. Vous sentez qu�elle est en train de vous juger pour exp�rimenter vos comp�tences face � cette situation.";
        story6010answer3Description1.description = "Et �a fonctionne � merveille. Vous entendez les soldats hurler confus, se demandant o� est pass� le camion et comment ils sont arriv�s dans cette pi�ce. Ils regardent autour d�eux sans comprendre et perdent de pr�cieuses secondes � prendre connaissance de leur environnement.";
        story6010answer4Description1.description = "Les yeux de ce dernier deviennent vitreux. Il se retourne brusquement en appuyant sur la g�chette et c�est un carnage, il tire dans son groupe pendant plusieurs secondes et des hurlements de douleur retentissent dans les rangs de vos ennemis.";
        story6010answer5Description1.description = "Votre proie est terroris�e. Et � mesure que l�h�moglobine coule dans votre gorge vous avez des flashback des derni�res heures. Il s'appelle Michael, et est le second de cette �quipe de raid sous les ordres de Gellec l'�v�que Auxiliaire de Montpellier. Vous connaissez d�sormais l�emplacement du QG militaire de cette op�ration.";
        story6010answer6Description1.description = "Leurs armes et �quipements se mettent � fondre au contact des gouttelettes dans des nuages de fum�es corrosives. Vous les entendez tousser et vomir pendant qu�ils tentent de retirer en h�te certaines parties de leur �quipement.";

        story6010answer1.answerDescriptions.Add(story6010answer1Description1);
        story6010answer2.answerDescriptions.Add(story6010answer2Description1);
        story6010answer3.answerDescriptions.Add(story6010answer3Description1);
        story6010answer4.answerDescriptions.Add(story6010answer4Description1);
        story6010answer5.answerDescriptions.Add(story6010answer5Description1);
        story6010answer6.answerDescriptions.Add(story6010answer6Description1);

        story6010.answers.Add(story6010answer1);
        story6010.answers.Add(story6010answer2);
        story6010.answers.Add(story6010answer3);
        story6010.answers.Add(story6010answer4);
        story6010.answers.Add(story6010answer5);
        story6010.answers.Add(story6010answer6);

        stories.listStory.Add(story6010);

        ////STORY 6020

        Story story6020 = new Story();
        story6020.storyNumber = 6020;

        StoryDescription story6020Description1 = new StoryDescription();
        story6020Description1.answerNumberPreRequise = new List<int>();
        story6020Description1.description = "Dans une derni�re tentative pour r�sister vous voyez un petit groupe de soldat se former en un ultime bastion pr�s de la porte : ";
        story6020.storyDescriptions.Add(story6020Description1);

        Answer story6020answer1 = new Answer();
        Answer story6020answer2 = new Answer();
        Answer story6020answer3 = new Answer();
        Answer story6020answer4 = new Answer();
        Answer story6020answer5 = new Answer();
        Answer story6020answer6 = new Answer();
        Answer story6020answer7 = new Answer();
        Answer story6020answer8 = new Answer();
        Answer story6020answer9 = new Answer();

        story6020answer1.isPreRequiseNotIn = true;
        story6020answer1.answerNumberPreRequise = new List<int>{ 6001 };
        story6020answer2.isPreRequiseNotIn = true;
        story6020answer2.answerNumberPreRequise = new List<int>{ 6002 };
        story6020answer3.isPreRequiseNotIn = true;
        story6020answer3.answerNumberPreRequise = new List<int>{ 6003 };
        story6020answer4.isPreRequiseNotIn = true;
        story6020answer4.answerNumberPreRequise = new List<int>{ 6004 };
        story6020answer5.isPreRequiseNotIn = true;
        story6020answer5.answerNumberPreRequise = new List<int>{ 6005 };
        story6020answer6.isPreRequiseNotIn = true;
        story6020answer6.answerNumberPreRequise = new List<int>{ 6006 };
        story6020answer7.isPreRequiseListAndAndNotOr = false;
        story6020answer7.answerNumberPreRequise = new List<int>{ 6001, 6002 };
        story6020answer8.isPreRequiseListAndAndNotOr = false;
        story6020answer8.answerNumberPreRequise = new List<int>{ 6003, 6004 };
        story6020answer9.isPreRequiseListAndAndNotOr = false;
        story6020answer9.answerNumberPreRequise = new List<int>{ 6005, 6006 };

        story6020answer1.nextStory = 10000;
        story6020answer2.nextStory = 10000;
        story6020answer3.nextStory = 10000;
        story6020answer4.nextStory = 10000;
        story6020answer5.nextStory = 10000;
        story6020answer6.nextStory = 10000;
        story6020answer7.nextStory = 10000;
        story6020answer8.nextStory = 10000;
        story6020answer9.nextStory = 10000;
        story6020answer1.discipline = "Sens accrus";
        story6020answer2.discipline = "Sentir linvisible";
        story6020answer3.discipline = "Brouillage memoriel";
        story6020answer4.discipline = "Contrainte";
        story6020answer5.discipline = "Gout du sang";
        story6020answer6.discipline = "Vitae Corrosive";
        story6020answer7.discipline = "Premonition";
        story6020answer8.discipline = "Hypnose";
        story6020answer9.discipline = "Eteindre la vitae";
        story6020answer1.description = "Vous vous concentrez et parvenez � voir � travers les fumig�nes, vous arrivez m�me � entendre les conversations dans les oreillettes des soldats.";
        story6020answer2.description = "Vous vous concentrez et parvenez � voir quelque chose qui �tait invisible jusqu�ici.";
        story6020answer3.description = "Vous tendez votre main en avant et supprimez brutalement les derniers souvenirs des soldats pour semer la confusion.";
        story6020answer4.description = "Vous avisez le premier soldat et lui ordonnez de se retourner en tirant sur ses alli�s.";
        story6020answer5.description = "Vous vous jetez sur le premier soldat et plantez vos dents dans son cou. Le go�t de son sang vous conf�re des informations � son propos.";
        story6020answer6.description = "Vous vous mordez � pleine dents l�avant bras et exp�diez d�un large mouvement votre sang en direction des soldats qui se mettent � hurler � son contact.";
        story6020answer7.description = "Vos yeux se retournent sous vos paupi�res. Vous avez une pr�monition et brusquement vous prenez conscience d�une v�rit� jusque-l� cach�e.";
        story6020answer8.description = "Vous profitez du chaos pour poser votre main sur les yeux d�un soldat et en une phrase vous l�hypnotisez compl�tement.";
        story6020answer9.description = "Vous vous concentrez et avez de plus en plus soif� La b�te en vous prend le contr�le de votre corps et devient inarr�table.";

        AnswerDescription story6020answer1Description1 = new AnswerDescription();
        AnswerDescription story6020answer2Description1 = new AnswerDescription();
        AnswerDescription story6020answer3Description1 = new AnswerDescription();
        AnswerDescription story6020answer4Description1 = new AnswerDescription();
        AnswerDescription story6020answer5Description1 = new AnswerDescription();
        AnswerDescription story6020answer6Description1 = new AnswerDescription();
        AnswerDescription story6020answer7Description1 = new AnswerDescription();
        AnswerDescription story6020answer8Description1 = new AnswerDescription();
        AnswerDescription story6020answer9Description1 = new AnswerDescription();

        story6020answer1Description1.answerDescriptionNumber = 6001;
        story6020answer2Description1.answerDescriptionNumber = 6002;
        story6020answer3Description1.answerDescriptionNumber = 6003;
        story6020answer4Description1.answerDescriptionNumber = 6004;
        story6020answer5Description1.answerDescriptionNumber = 6005;
        story6020answer6Description1.answerDescriptionNumber = 6006;
        story6020answer7Description1.answerDescriptionNumber = 6021;
        story6020answer8Description1.answerDescriptionNumber = 6022;
        story6020answer9Description1.answerDescriptionNumber = 6023;

        story6020answer1Description1.description = "Vous entendez les consignes de leur chef d�escouade, vous savez ainsi que vous affrontez la team sigma de l�inquisition de Montpellier. Il y a 8 ennemis en face de vous.";
        story6020answer2Description1.description = "Vous voyez pleinement la jeune femme qui vous a transform� cette nuit. Elle est sto�que, immobile et impassible dans un coin de la pi�ce, les soldats semblent incapable de la voir et elle vous regarde dans les yeux. Vous sentez qu�elle est en train de vous juger pour exp�rimenter vos comp�tences face � cette situation.";
        story6020answer3Description1.description = "Et �a fonctionne � merveille. Vous entendez les soldats hurler confus, se demandant o� est pass� le camion et comment ils sont arriv�s dans cette pi�ce. Ils regardent autour d�eux sans comprendre et perdent de pr�cieuses secondes � prendre connaissance de leur environnement.";
        story6020answer4Description1.description = "Les yeux de ce dernier deviennent vitreux. Il se retourne brusquement en appuyant sur la g�chette et c�est un carnage, il tire dans son groupe pendant plusieurs secondes et des hurlements de douleur retentissent dans les rangs de vos ennemis.";
        story6020answer5Description1.description = "Votre proie est terroris�e. Et � mesure que l�h�moglobine coule dans votre gorge vous avez des flashback des derni�res heures. Il s'appelle Michael, et est le second de cette �quipe de raid sous les ordres de Gellec l'�v�que Auxiliaire de Montpellier. Vous connaissez d�sormais l�emplacement du QG militaire de cette op�ration.";
        story6020answer6Description1.description = "Leurs armes et �quipements se mettent � fondre au contact des gouttelettes dans des nuages de fum�es corrosives. Vous les entendez tousser et vomir pendant qu�ils tentent de retirer en h�te certaines parties de leur �quipement.";
        story6020answer7Description1.description = "Une grenade d�goupill�e � roul�e jusqu�� vous, cette v�rit� vous frappe. Vous regardez � vos pied et savez qu�il vous reste 2 secondes pour la renvoyer avant qu�elle ne vous explose dessus. Vous la ramassez et la projetez de toutes vos forces sur vos ennemis. A peine a- t-elle quitt� vos doigts et parcourus 3 m�tres qu�elle d�tonne dans un souffle explosif projetant des membres dans une gerbe de sang.";
        story6020answer8Description1.description = "Il est persuad� d��tre votre petit fr�re, et compte utiliser tout son arsenal d'armes pour vous prot�ger. Vous le voyez sortir une grenade, il vous fait un clin d'�il rassurant et la balance sur vos ennemis. A peine a- t-elle quitt� ses doigts et parcourus 3 m�tres qu�elle d�tonne dans un souffle explosif projetant des membres dans une gerbe de sang.";
        story6020answer9Description1.description = "C�est une boucherie. Vos crocs sortent de votre bouche, votre visage devient animal et vous vous projetez dans le combat en grognant telle une b�te f�roce. Vous ne sentez ni les balles p�n�trer votre corps ni les coups de crosse vains sur votre visage. Il n�y a qu�apr�s avoir arrach� la t�te de plusieurs de vos ennemis et bu leur sang � m�me leur cou d�capit� que vous parvenez � reprendre le contr�le et � vous calmer.";

        story6020answer1.answerDescriptions.Add(story6020answer1Description1);
        story6020answer2.answerDescriptions.Add(story6020answer2Description1);
        story6020answer3.answerDescriptions.Add(story6020answer3Description1);
        story6020answer4.answerDescriptions.Add(story6020answer4Description1);
        story6020answer5.answerDescriptions.Add(story6020answer5Description1);
        story6020answer6.answerDescriptions.Add(story6020answer6Description1);
        story6020answer7.answerDescriptions.Add(story6020answer7Description1);
        story6020answer8.answerDescriptions.Add(story6020answer8Description1);
        story6020answer9.answerDescriptions.Add(story6020answer9Description1);

        story6020.answers.Add(story6020answer7); //Put the lvl 2 firsts
        story6020.answers.Add(story6020answer8);
        story6020.answers.Add(story6020answer9);
        story6020.answers.Add(story6020answer1);
        story6020.answers.Add(story6020answer2);
        story6020.answers.Add(story6020answer3);
        story6020.answers.Add(story6020answer4);
        story6020.answers.Add(story6020answer5);
        story6020.answers.Add(story6020answer6);

        stories.listStory.Add(story6020);



        //////////STORY 7000

        Story story7000 = new Story();
        story7000.storyNumber = 7000;

        StoryDescription story7000Description1 = new StoryDescription();
        story7000Description1.answerNumberPreRequise = new List<int>();
        story7000Description1.description = "Vous sentez la chaleur dans ses paroles et savez que vous venez de trouver une nouvelle famille en ce monde, m�me si vous ne savez pas encore ce qui vous attend. Tout � coup, la porte de la salle explose, une grenade roule au sol et se met � cracher de la fum�e partout. La jeune femme se precipite dessus, mais aussitot des lasers percent la fum�e et des coups de feu retentissent dans la salle, assourdissants.";
        story7000.storyDescriptions.Add(story7000Description1);

        Answer story7000answer1 = new Answer();
        Answer story7000answer2 = new Answer();
        Answer story7000answer3 = new Answer();
        Answer story7000answer4 = new Answer();
        Answer story7000answer5 = new Answer();
        Answer story7000answer6 = new Answer();


        story7000answer1.isPreRequiseNotIn = true;
        story7000answer1.answerNumberPreRequise = new List<int>();
        story7000answer2.isPreRequiseNotIn = true;
        story7000answer2.answerNumberPreRequise = new List<int>();
        story7000answer3.isPreRequiseNotIn = true;
        story7000answer3.answerNumberPreRequise = new List<int>();
        story7000answer4.isPreRequiseNotIn = true;
        story7000answer4.answerNumberPreRequise = new List<int>();
        story7000answer5.isPreRequiseNotIn = true;
        story7000answer5.answerNumberPreRequise = new List<int>();
        story7000answer6.isPreRequiseNotIn = true;
        story7000answer6.answerNumberPreRequise = new List<int>();

        story7000answer1.nextStory = 7010;
        story7000answer2.nextStory = 7010;
        story7000answer3.nextStory = 7010;
        story7000answer4.nextStory = 7010;
        story7000answer5.nextStory = 7010;
        story7000answer6.nextStory = 7010;
        story7000answer1.discipline = "Esprit resolu";
        story7000answer2.discipline = "Resilience";
        story7000answer3.discipline = "Brouillage memoriel";
        story7000answer4.discipline = "Contrainte";
        story7000answer5.discipline = "Intimider";
        story7000answer6.discipline = "Reverence";
        story7000answer1.description = "Vous augmentez votre r�sistance mentale et avancez maintenant sans aucune crainte face aux soldats. La situation ne vous effraie plus du tout.";
        story7000answer2.description = "Vous augmentez votre r�sistance physique au moment o� les soldats vous tirent dessus.";
        story7000answer3.description = "Vous tendez votre main en avant et supprimez brutalement les derniers souvenirs des soldats pour semer la confusion.";
        story7000answer4.description = "Vous avisez le premier soldat et lui ordonnez de se retourner en tirant sur ses alli�s.";
        story7000answer5.description = "Vous vous redressez de toute votre taille et regardez les soldats d�un air mauvais, semant un vent de panique surnaturelle.";
        story7000answer6.description = "Vous rayonnez de majest�, d�gageant une aura quasi divine, for�ant vos ennemis � mettre un genoux � terre.";

        AnswerDescription story7000answer1Description1 = new AnswerDescription();
        AnswerDescription story7000answer2Description1 = new AnswerDescription();
        AnswerDescription story7000answer3Description1 = new AnswerDescription();
        AnswerDescription story7000answer4Description1 = new AnswerDescription();
        AnswerDescription story7000answer5Description1 = new AnswerDescription();
        AnswerDescription story7000answer6Description1 = new AnswerDescription();

        story7000answer1Description1.answerDescriptionNumber = 7001;
        story7000answer2Description1.answerDescriptionNumber = 7002;
        story7000answer3Description1.answerDescriptionNumber = 7003;
        story7000answer4Description1.answerDescriptionNumber = 7004;
        story7000answer5Description1.answerDescriptionNumber = 7005;
        story7000answer6Description1.answerDescriptionNumber = 7006;

        story7000answer1Description1.description = "Et vous sentez que les soldats eux se mettent � h�siter longuement face � votre confiance en vous.";
        story7000answer2Description1.description = "Les balles s�enfoncent dans votre chaire, mais l�impact vous fait � peine souffrir, c�est comme si votre peau �tait devenue aussi solide et �paisse que du cuir.";
        story7000answer3Description1.description = "Et �a fonctionne � merveille. Vous entendez les soldats hurler confus, se demandant o� est pass� le camion et comment ils sont arriv�s dans cette pi�ce. Ils regardent autour d�eux sans comprendre et perdent de pr�cieuses secondes � prendre connaissance de leur environnement.";
        story7000answer4Description1.description = "Les yeux de ce dernier deviennent vitreux. Il se retourne brusquement en appuyant sur la g�chette et c�est un carnage, il tire dans son groupe pendant plusieurs secondes et des hurlements de douleur retentissent dans les rangs de vos ennemis.";
        story7000answer5Description1.description = "A travers les fumig�nes votre aura p�se d�une lourdeur extr�me sur les �paules des soldats, vous-en entendez quelques-un crier et partir en courant. Les autres sont terroris�s et n�osent plus s�approcher.";
        story7000answer6Description1.description = "Les soldats sont �cras�s par la force mystique qui rayonne de votre position. Les plus proches l�chent les armes et tombent � genoux, des larmes pleins les yeux devant votre splendeur. Les autres sont bloqu�s sur place quelques secondes t�tanis�s par la sc�ne.";

        story7000answer1.answerDescriptions.Add(story7000answer1Description1);
        story7000answer2.answerDescriptions.Add(story7000answer2Description1);
        story7000answer3.answerDescriptions.Add(story7000answer3Description1);
        story7000answer4.answerDescriptions.Add(story7000answer4Description1);
        story7000answer5.answerDescriptions.Add(story7000answer5Description1);
        story7000answer6.answerDescriptions.Add(story7000answer6Description1);

        story7000.answers.Add(story7000answer1);
        story7000.answers.Add(story7000answer2);
        story7000.answers.Add(story7000answer3);
        story7000.answers.Add(story7000answer4);
        story7000.answers.Add(story7000answer5);
        story7000.answers.Add(story7000answer6);

        stories.listStory.Add(story7000);

        //STORY 7010

        Story story7010 = new Story();
        story7010.storyNumber = 7010;

        StoryDescription story7010Description1 = new StoryDescription();
        story7010Description1.answerNumberPreRequise = new List<int>();
        story7010Description1.description = "Certains des hommes arm�s r�sistent et se mettent alors � tirer dans tous les sens, et vous sentez que vous pouvez les contrer : ";
        story7010.storyDescriptions.Add(story7010Description1);

        Answer story7010answer1 = new Answer();
        Answer story7010answer2 = new Answer();
        Answer story7010answer3 = new Answer();
        Answer story7010answer4 = new Answer();
        Answer story7010answer5 = new Answer();
        Answer story7010answer6 = new Answer();


        story7010answer1.isPreRequiseNotIn = true;
        story7010answer1.answerNumberPreRequise = new List<int>{ 7001 };
        story7010answer2.isPreRequiseNotIn = true;
        story7010answer2.answerNumberPreRequise = new List<int>{ 7002 };
        story7010answer3.isPreRequiseNotIn = true;
        story7010answer3.answerNumberPreRequise = new List<int>{ 7003 };
        story7010answer4.isPreRequiseNotIn = true;
        story7010answer4.answerNumberPreRequise = new List<int>{ 7004 };
        story7010answer5.isPreRequiseNotIn = true;
        story7010answer5.answerNumberPreRequise = new List<int>{ 7005 };
        story7010answer6.isPreRequiseNotIn = true;
        story7010answer6.answerNumberPreRequise = new List<int>{ 7006 };

        story7010answer1.nextStory = 7020;
        story7010answer2.nextStory = 7020;
        story7010answer3.nextStory = 7020;
        story7010answer4.nextStory = 7020;
        story7010answer5.nextStory = 7020;
        story7010answer6.nextStory = 7020;
        story7010answer1.discipline = "Esprit resolu";
        story7010answer2.discipline = "Resilience";
        story7010answer3.discipline = "Brouillage memoriel";
        story7010answer4.discipline = "Contrainte";
        story7010answer5.discipline = "Intimider";
        story7010answer6.discipline = "Reverence";
        story7010answer1.description = "Vous augmentez votre r�sistance mentale et avancez maintenant sans aucune crainte face aux soldats. La situation ne vous effraie plus du tout.";
        story7010answer2.description = "Vous augmentez votre r�sistance physique au moment o� les soldats vous tirent dessus.";
        story7010answer3.description = "Vous tendez votre main en avant et supprimez brutalement les derniers souvenirs des soldats pour semer la confusion.";
        story7010answer4.description = "Vous avisez le premier soldat et lui ordonnez de se retourner en tirant sur ses alli�s.";
        story7010answer5.description = "Vous vous redressez de toute votre taille et regardez les soldats d�un air mauvais, semant un vent de panique surnaturelle.";
        story7010answer6.description = "Vous rayonnez de majest�, d�gageant une aura quasi divine, for�ant vos ennemis � mettre un genoux � terre.";

        AnswerDescription story7010answer1Description1 = new AnswerDescription();
        AnswerDescription story7010answer2Description1 = new AnswerDescription();
        AnswerDescription story7010answer3Description1 = new AnswerDescription();
        AnswerDescription story7010answer4Description1 = new AnswerDescription();
        AnswerDescription story7010answer5Description1 = new AnswerDescription();
        AnswerDescription story7010answer6Description1 = new AnswerDescription();

        story7010answer1Description1.answerDescriptionNumber = 7001;
        story7010answer2Description1.answerDescriptionNumber = 7002;
        story7010answer3Description1.answerDescriptionNumber = 7003;
        story7010answer4Description1.answerDescriptionNumber = 7004;
        story7010answer5Description1.answerDescriptionNumber = 7005;
        story7010answer6Description1.answerDescriptionNumber = 7006;

        story7010answer1Description1.description = "Et vous sentez que les soldats eux se mettent � h�siter longuement face � votre confiance en vous.";
        story7010answer2Description1.description = "Les balles s�enfoncent dans votre chaire, mais l�impact vous fait � peine souffrir, c�est comme si votre peau �tait devenue aussi solide et �paisse que du cuir.";
        story7010answer3Description1.description = "Et �a fonctionne � merveille. Vous entendez les soldats hurler confus, se demandant o� est pass� le camion et comment ils sont arriv�s dans cette pi�ce. Ils regardent autour d�eux sans comprendre et perdent de pr�cieuses secondes � prendre connaissance de leur environnement.";
        story7010answer4Description1.description = "Les yeux de ce dernier deviennent vitreux. Il se retourne brusquement en appuyant sur la g�chette et c�est un carnage, il tire dans son groupe pendant plusieurs secondes et des hurlements de douleur retentissent dans les rangs de vos ennemis.";
        story7010answer5Description1.description = "A travers les fumig�nes votre aura p�se d�une lourdeur extr�me sur les �paules des soldats, vous-en entendez quelques-un crier et partir en courant. Les autres sont terroris�s et n�osent plus s�approcher.";
        story7010answer6Description1.description = "Les soldats sont �cras�s par la force mystique qui rayonne de votre position. Les plus proches l�chent les armes et tombent � genoux, des larmes pleins les yeux devant votre splendeur. Les autres sont bloqu�s sur place quelques secondes t�tanis�s par la sc�ne.";

        story7010answer1.answerDescriptions.Add(story7010answer1Description1);
        story7010answer2.answerDescriptions.Add(story7010answer2Description1);
        story7010answer3.answerDescriptions.Add(story7010answer3Description1);
        story7010answer4.answerDescriptions.Add(story7010answer4Description1);
        story7010answer5.answerDescriptions.Add(story7010answer5Description1);
        story7010answer6.answerDescriptions.Add(story7010answer6Description1);

        story7010.answers.Add(story7010answer1);
        story7010.answers.Add(story7010answer2);
        story7010.answers.Add(story7010answer3);
        story7010.answers.Add(story7010answer4);
        story7010.answers.Add(story7010answer5);
        story7010.answers.Add(story7010answer6);

        stories.listStory.Add(story7010);

        ////STORY 7020

        Story story7020 = new Story();
        story7020.storyNumber = 7020;

        StoryDescription story7020Description1 = new StoryDescription();
        story7020Description1.answerNumberPreRequise = new List<int>();
        story7020Description1.description = "Dans une derni�re tentative pour r�sister vous voyez un petit groupe de soldat se former en un ultime bastion pr�s de la porte : ";
        story7020.storyDescriptions.Add(story7020Description1);

        Answer story7020answer1 = new Answer();
        Answer story7020answer2 = new Answer();
        Answer story7020answer3 = new Answer();
        Answer story7020answer4 = new Answer();
        Answer story7020answer5 = new Answer();
        Answer story7020answer6 = new Answer();
        Answer story7020answer7 = new Answer();
        Answer story7020answer8 = new Answer();
        Answer story7020answer9 = new Answer();

        story7020answer1.isPreRequiseNotIn = true;
        story7020answer1.answerNumberPreRequise = new List<int>{ 7001 };
        story7020answer2.isPreRequiseNotIn = true;
        story7020answer2.answerNumberPreRequise = new List<int>{ 7002 };
        story7020answer3.isPreRequiseNotIn = true;
        story7020answer3.answerNumberPreRequise = new List<int>{ 7003 };
        story7020answer4.isPreRequiseNotIn = true;
        story7020answer4.answerNumberPreRequise = new List<int>{ 7004 };
        story7020answer5.isPreRequiseNotIn = true;
        story7020answer5.answerNumberPreRequise = new List<int>{ 7005 };
        story7020answer6.isPreRequiseNotIn = true;
        story7020answer6.answerNumberPreRequise = new List<int>{ 7006 };
        story7020answer7.isPreRequiseListAndAndNotOr = false;
        story7020answer7.answerNumberPreRequise = new List<int>{ 7001, 7002 };
        story7020answer8.isPreRequiseListAndAndNotOr = false;
        story7020answer8.answerNumberPreRequise = new List<int>{ 7003, 7004 };
        story7020answer9.isPreRequiseListAndAndNotOr = false;
        story7020answer9.answerNumberPreRequise = new List<int>{ 7005, 7006 };

        story7020answer1.nextStory = 10000;
        story7020answer2.nextStory = 10000;
        story7020answer3.nextStory = 10000;
        story7020answer4.nextStory = 10000;
        story7020answer5.nextStory = 10000;
        story7020answer6.nextStory = 10000;
        story7020answer7.nextStory = 10000;
        story7020answer8.nextStory = 10000;
        story7020answer9.nextStory = 10000;
        story7020answer1.discipline = "Esprit resolu";
        story7020answer2.discipline = "Resilience";
        story7020answer3.discipline = "Brouillage memoriel";
        story7020answer4.discipline = "Contrainte";
        story7020answer5.discipline = "Intimider";
        story7020answer6.discipline = "Reverence";
        story7020answer7.discipline = "Resistance";
        story7020answer8.discipline = "Hypnose";
        story7020answer9.discipline = "Baiser persistant";
        story7020answer1.description = "Vous augmentez votre r�sistance mentale et avancez maintenant sans aucune crainte face aux soldats. La situation ne vous effraie plus du tout.";
        story7020answer2.description = "Vous augmentez votre r�sistance physique au moment o� les soldats vous tirent dessus.";
        story7020answer3.description = "Vous tendez votre main en avant et supprimez brutalement les derniers souvenirs des soldats pour semer la confusion.";
        story7020answer4.description = "Vous avisez le premier soldat et lui ordonnez de se retourner en tirant sur ses alli�s.";
        story7020answer5.description = "Vous vous redressez de toute votre taille et regardez les soldats d�un air mauvais, semant un vent de panique surnaturelle.";
        story7020answer6.description = "Vous rayonnez de majest�, d�gageant une aura quasi divine, for�ant vos ennemis � mettre un genoux � terre.";
        story7020answer7.description = "Votre peau s��paissit et les balles se fichent dans votre �chine sans p�n�trer sa surface.";
        story7020answer8.description = "Vous profitez du chaos pour poser votre main sur les yeux d�un soldat et en une phrase vous l�hypnotisez compl�tement.";
        story7020answer9.description = "Vous plongez vos crocs dans la nuque d�un soldat. Lorsque vous le l�chez, celui-ci est envout� et se retourne contre ses alli�s pour vous prot�ger.";

        AnswerDescription story7020answer1Description1 = new AnswerDescription();
        AnswerDescription story7020answer2Description1 = new AnswerDescription();
        AnswerDescription story7020answer3Description1 = new AnswerDescription();
        AnswerDescription story7020answer4Description1 = new AnswerDescription();
        AnswerDescription story7020answer5Description1 = new AnswerDescription();
        AnswerDescription story7020answer6Description1 = new AnswerDescription();
        AnswerDescription story7020answer7Description1 = new AnswerDescription();
        AnswerDescription story7020answer8Description1 = new AnswerDescription();
        AnswerDescription story7020answer9Description1 = new AnswerDescription();

        story7020answer1Description1.answerDescriptionNumber = 7001;
        story7020answer2Description1.answerDescriptionNumber = 7002;
        story7020answer3Description1.answerDescriptionNumber = 7003;
        story7020answer4Description1.answerDescriptionNumber = 7004;
        story7020answer5Description1.answerDescriptionNumber = 7005;
        story7020answer6Description1.answerDescriptionNumber = 7006;
        story7020answer7Description1.answerDescriptionNumber = 7021;
        story7020answer8Description1.answerDescriptionNumber = 7022;
        story7020answer9Description1.answerDescriptionNumber = 7023;

        story7020answer1Description1.description = "Et vous sentez que les soldats eux se mettent � h�siter longuement face � votre confiance en vous.";
        story7020answer2Description1.description = "Les balles s�enfoncent dans votre chaire, mais l�impact vous fait � peine souffrir, c�est comme si votre peau �tait devenue aussi solide et �paisse que du cuir.";
        story7020answer3Description1.description = "Et �a fonctionne � merveille. Vous entendez les soldats hurler confus, se demandant o� est pass� le camion et comment ils sont arriv�s dans cette pi�ce. Ils regardent autour d�eux sans comprendre et perdent de pr�cieuses secondes � prendre connaissance de leur environnement.";
        story7020answer4Description1.description = "Les yeux de ce dernier deviennent vitreux. Il se retourne brusquement en appuyant sur la g�chette et c�est un carnage, il tire dans son groupe pendant plusieurs secondes et des hurlements de douleur retentissent dans les rangs de vos ennemis.";
        story7020answer5Description1.description = "A travers les fumig�nes votre aura p�se d�une lourdeur extr�me sur les �paules des soldats, vous-en entendez quelques-un crier et partir en courant. Les autres sont terroris�s et n�osent plus s�approcher.";
        story7020answer6Description1.description = "Les soldats sont �cras�s par la force mystique qui rayonne de votre position. Les plus proches l�chent les armes et tombent � genoux, des larmes pleins les yeux devant votre splendeur. Les autres sont bloqu�s sur place quelques secondes t�tanis�s par la sc�ne.";
        story7020answer7Description1.description = "Les balles qui �taient entr�es en vous en ressortent et tombent � terre dans un tintement sonore. Les soldats sont subjugu�s, vous r�sistez aux balles de front et avancez vers eux sans crainte. Ils reculent.";
        story7020answer8Description1.description = "Il est persuad� d��tre votre petit fr�re, et compte utiliser tout son arsenal d'armes pour vous prot�ger. Vous le voyez sortir une grenade, il vous fait un clin d'�il rassurant et la balance sur vos ennemis. A peine a- t-elle quitt� ses doigts et parcourus 3 m�tres qu�elle d�tonne dans un souffle explosif projetant des membres dans une gerbe de sang.";
        story7020answer9Description1.description = "C�est un forcen� que vous venez de lib�rer, le cou encore en sang il beugle que vous �tes un dieu ancien et qu�il vous prot�gera � jamais au p�ril de sa vie. Il d�goupille une grenade et la jette au milieu des soldats qui explosent dans une gicl�e de membres arrach�s";

        story7020answer1.answerDescriptions.Add(story7020answer1Description1);
        story7020answer2.answerDescriptions.Add(story7020answer2Description1);
        story7020answer3.answerDescriptions.Add(story7020answer3Description1);
        story7020answer4.answerDescriptions.Add(story7020answer4Description1);
        story7020answer5.answerDescriptions.Add(story7020answer5Description1);
        story7020answer6.answerDescriptions.Add(story7020answer6Description1);
        story7020answer7.answerDescriptions.Add(story7020answer7Description1);
        story7020answer8.answerDescriptions.Add(story7020answer8Description1);
        story7020answer9.answerDescriptions.Add(story7020answer9Description1);

        story7020.answers.Add(story7020answer7); //Put the lvl 2 firsts
        story7020.answers.Add(story7020answer8);
        story7020.answers.Add(story7020answer9);
        story7020.answers.Add(story7020answer1);
        story7020.answers.Add(story7020answer2);
        story7020.answers.Add(story7020answer3);
        story7020.answers.Add(story7020answer4);
        story7020.answers.Add(story7020answer5);
        story7020.answers.Add(story7020answer6);

        stories.listStory.Add(story7020);


        ///STORY 8000
        ///

        Story story8000 = new Story();
        story8000.storyNumber = 8000;

        StoryDescription story8000Description1 = new StoryDescription();
        story8000Description1.answerNumberPreRequise = new List<int>();
        story8000Description1.description = "Vous sentez la chaleur dans ses paroles et savez que vous venez de trouver une nouvelle famille en ce monde, m�me si vous ne savez pas encore ce qui vous attend. Tout � coup, la porte de la salle explose, une grenade roule au sol et se met � cracher de la fum�e partout. La jeune femme se precipite dessus, mais aussitot des lasers percent la fum�e et des coups de feu retentissent dans la salle, assourdissants.";
        story8000.storyDescriptions.Add(story8000Description1);

        Answer story8000answer1 = new Answer();
        Answer story8000answer2 = new Answer();
        Answer story8000answer3 = new Answer();
        Answer story8000answer4 = new Answer();
        Answer story8000answer5 = new Answer();
        Answer story8000answer6 = new Answer();


        story8000answer1.isPreRequiseNotIn = true;
        story8000answer1.answerNumberPreRequise = new List<int>();
        story8000answer2.isPreRequiseNotIn = true;
        story8000answer2.answerNumberPreRequise = new List<int>();
        story8000answer3.isPreRequiseNotIn = true;
        story8000answer3.answerNumberPreRequise = new List<int>();
        story8000answer4.isPreRequiseNotIn = true;
        story8000answer4.answerNumberPreRequise = new List<int>();
        story8000answer5.isPreRequiseNotIn = true;
        story8000answer5.answerNumberPreRequise = new List<int>();
        story8000answer6.isPreRequiseNotIn = true;
        story8000answer6.answerNumberPreRequise = new List<int>();

        story8000answer1.nextStory = 8010;
        story8000answer2.nextStory = 8010;
        story8000answer3.nextStory = 8010;
        story8000answer4.nextStory = 8010;
        story8000answer5.nextStory = 8010;
        story8000answer6.nextStory = 8010;
        story8000answer1.discipline = "Bond surhumain";
        story8000answer2.discipline = "Corps lethal";
        story8000answer3.discipline = "Brouillage memoriel";
        story8000answer4.discipline = "Contrainte";
        story8000answer5.discipline = "Regard Abyssal";
        story8000answer6.discipline = "Manteau Tenebreux";
        story8000answer1.description = "Vous vous propulsez de toute la puissance de vos jambes � travers la masse de soldats. La violence de l�impact d�boite des �paules et les fait chuter au sol.";
        story8000answer2.description = "Vous foncez sur le premier soldat et envoyez un coup de poing fulgurant pour d�truire son gilet par balle.";
        story8000answer3.description = "Vous tendez votre main en avant et supprimez brutalement les derniers souvenirs des soldats pour semer la confusion.";
        story8000answer4.description = "Vous avisez le premier soldat et lui ordonnez de se retourner en tirant sur ses alli�s.";
        story8000answer5.description = "Vous plissez les yeux et parvenez � identifier chacun de vos ennemis � travers les fumig�nes et l�obscurit� profonde.";
        story8000answer6.description = "Vous appelez � vous les ombres et vous enroulez de fum�e noire terrifiante et insondable, terrorisant les soldats.";

        AnswerDescription story8000answer1Description1 = new AnswerDescription();
        AnswerDescription story8000answer2Description1 = new AnswerDescription();
        AnswerDescription story8000answer3Description1 = new AnswerDescription();
        AnswerDescription story8000answer4Description1 = new AnswerDescription();
        AnswerDescription story8000answer5Description1 = new AnswerDescription();
        AnswerDescription story8000answer6Description1 = new AnswerDescription();

        story8000answer1Description1.answerDescriptionNumber = 8001;
        story8000answer2Description1.answerDescriptionNumber = 8002;
        story8000answer3Description1.answerDescriptionNumber = 8003;
        story8000answer4Description1.answerDescriptionNumber = 8004;
        story8000answer5Description1.answerDescriptionNumber = 8005;
        story8000answer6Description1.answerDescriptionNumber = 8006;

        story8000answer1Description1.description = "Des jurons explosent dans tous les sens au milieu des cris de douleur. Vous avez fonc� � travers vos ennemis tel un boulet de canon, et ceux-ci mirent quelques secondes � se relever.";
        story8000answer2Description1.description = "Le gilet est transperc� dans une gerbe de sang. Vous sentez votre poing s�enfoncer dans les tripes humides, chaudes et gluantes de votre ennemi. Vous retirez brusquement votre main et celui-ci tombe � terre, mort sur le coup.";
        story8000answer3Description1.description = "Et �a fonctionne � merveille. Vous entendez les soldats hurler confus, se demandant o� est pass� le camion et comment ils sont arriv�s dans cette pi�ce. Ils regardent autour d�eux sans comprendre et perdent de pr�cieuses secondes � prendre connaissance de leur environnement.";
        story8000answer4Description1.description = "Les yeux de ce dernier deviennent vitreux. Il se retourne brusquement en appuyant sur la g�chette et c�est un carnage, il tire dans son groupe pendant plusieurs secondes et des hurlements de douleur retentissent dans les rangs de vos ennemis.";
        story8000answer5Description1.description = "Vous avez l�impression que les fumig�nes ont totalement disparu, mais il n�en est rien. C�est votre regard surnaturel qui perce le voile entre les mondes. 8 ennemis se tiennent devant vous, ils semblent coordonn�s et vous parvenez � d�cortiquer chacun de leurs mouvements, le moindre de leurs pas. Vous arrivez presque � pr�voir leurs actions.";
        story8000answer6Description1.description = "Du point de vue de vos ennemis c�est comme si l�air s'engouffrait dans les tr�fonds d�un tunnel emportant un tourbillon d�ombres comme des volumes de fum�e charbonneuse. Vous absorbez toutes les ombres de la pi�ce, et celles-ci tourbillonnent autour de vous, ne laissant luire que vos yeux per�ants et f�roces. Vous entendez quelques soldats crier et partir en courant. Les autres sont terroris�s et n�osent plus s�approcher.";

        story8000answer1.answerDescriptions.Add(story8000answer1Description1);
        story8000answer2.answerDescriptions.Add(story8000answer2Description1);
        story8000answer3.answerDescriptions.Add(story8000answer3Description1);
        story8000answer4.answerDescriptions.Add(story8000answer4Description1);
        story8000answer5.answerDescriptions.Add(story8000answer5Description1);
        story8000answer6.answerDescriptions.Add(story8000answer6Description1);

        story8000.answers.Add(story8000answer1);
        story8000.answers.Add(story8000answer2);
        story8000.answers.Add(story8000answer3);
        story8000.answers.Add(story8000answer4);
        story8000.answers.Add(story8000answer5);
        story8000.answers.Add(story8000answer6);

        stories.listStory.Add(story8000);

        //STORY 8010

        Story story8010 = new Story();
        story8010.storyNumber = 8010;

        StoryDescription story8010Description1 = new StoryDescription();
        story8010Description1.answerNumberPreRequise = new List<int>();
        story8010Description1.description = "Certains des hommes arm�s r�sistent et se mettent alors � tirer dans tous les sens, et vous sentez que vous pouvez les contrer : ";
        story8010.storyDescriptions.Add(story8010Description1);

        Answer story8010answer1 = new Answer();
        Answer story8010answer2 = new Answer();
        Answer story8010answer3 = new Answer();
        Answer story8010answer4 = new Answer();
        Answer story8010answer5 = new Answer();
        Answer story8010answer6 = new Answer();


        story8010answer1.isPreRequiseNotIn = true;
        story8010answer1.answerNumberPreRequise = new List<int> { 8001 };
        story8010answer2.isPreRequiseNotIn = true;
        story8010answer2.answerNumberPreRequise = new List<int> { 8002 };
        story8010answer3.isPreRequiseNotIn = true;
        story8010answer3.answerNumberPreRequise = new List<int> { 8003 };
        story8010answer4.isPreRequiseNotIn = true;
        story8010answer4.answerNumberPreRequise = new List<int> { 8004 };
        story8010answer5.isPreRequiseNotIn = true;
        story8010answer5.answerNumberPreRequise = new List<int> { 8005 };
        story8010answer6.isPreRequiseNotIn = true;
        story8010answer6.answerNumberPreRequise = new List<int> { 8006 };

        story8010answer1.nextStory = 8020;
        story8010answer2.nextStory = 8020;
        story8010answer3.nextStory = 8020;
        story8010answer4.nextStory = 8020;
        story8010answer5.nextStory = 8020;
        story8010answer6.nextStory = 8020;
        story8010answer1.discipline = "Bond surhumain";
        story8010answer2.discipline = "Corps lethal";
        story8010answer3.discipline = "Brouillage memoriel";
        story8010answer4.discipline = "Contrainte";
        story8010answer5.discipline = "Regard Abyssal";
        story8010answer6.discipline = "Manteau Tenebreux";
        story8010answer1.description = "Vous vous propulsez de toute la puissance de vos jambes � travers la masse de soldats. La violence de l�impact d�boite des �paules et les fait chuter au sol.";
        story8010answer2.description = "Vous foncez sur le premier soldat et envoyez un coup de poing fulgurant pour d�truire son gilet par balle.";
        story8010answer3.description = "Vous tendez votre main en avant et supprimez brutalement les derniers souvenirs des soldats pour semer la confusion.";
        story8010answer4.description = "Vous avisez le premier soldat et lui ordonnez de se retourner en tirant sur ses alli�s.";
        story8010answer5.description = "Vous plissez les yeux et parvenez � identifier chacun de vos ennemis � travers les fumig�nes et l�obscurit� profonde.";
        story8010answer6.description = "Vous appelez � vous les ombres et vous enroulez de fum�e noire terrifiante et insondable, terrorisant les soldats.";

        AnswerDescription story8010answer1Description1 = new AnswerDescription();
        AnswerDescription story8010answer2Description1 = new AnswerDescription();
        AnswerDescription story8010answer3Description1 = new AnswerDescription();
        AnswerDescription story8010answer4Description1 = new AnswerDescription();
        AnswerDescription story8010answer5Description1 = new AnswerDescription();
        AnswerDescription story8010answer6Description1 = new AnswerDescription();

        story8010answer1Description1.answerDescriptionNumber = 8001;
        story8010answer2Description1.answerDescriptionNumber = 8002;
        story8010answer3Description1.answerDescriptionNumber = 8003;
        story8010answer4Description1.answerDescriptionNumber = 8004;
        story8010answer5Description1.answerDescriptionNumber = 8005;
        story8010answer6Description1.answerDescriptionNumber = 8006;

        story8010answer1Description1.description = "Des jurons explosent dans tous les sens au milieu des cris de douleur. Vous avez fonc� � travers vos ennemis tel un boulet de canon, et ceux-ci mirent quelques secondes � se relever.";
        story8010answer2Description1.description = "Le gilet est transperc� dans une gerbe de sang. Vous sentez votre poing s�enfoncer dans les tripes humides, chaudes et gluantes de votre ennemi. Vous retirez brusquement votre main et celui-ci tombe � terre, mort sur le coup.";
        story8010answer3Description1.description = "Et �a fonctionne � merveille. Vous entendez les soldats hurler confus, se demandant o� est pass� le camion et comment ils sont arriv�s dans cette pi�ce. Ils regardent autour d�eux sans comprendre et perdent de pr�cieuses secondes � prendre connaissance de leur environnement.";
        story8010answer4Description1.description = "Les yeux de ce dernier deviennent vitreux. Il se retourne brusquement en appuyant sur la g�chette et c�est un carnage, il tire dans son groupe pendant plusieurs secondes et des hurlements de douleur retentissent dans les rangs de vos ennemis.";
        story8010answer5Description1.description = "Vous avez l�impression que les fumig�nes ont totalement disparu, mais il n�en est rien. C�est votre regard surnaturel qui perce le voile entre les mondes. 8 ennemis se tiennent devant vous, ils semblent coordonn�s et vous parvenez � d�cortiquer chacun de leurs mouvements, le moindre de leurs pas. Vous arrivez presque � pr�voir leurs actions.";
        story8010answer6Description1.description = "Du point de vue de vos ennemis c�est comme si l�air s'engouffrait dans les tr�fonds d�un tunnel emportant un tourbillon d�ombres comme des volumes de fum�e charbonneuse. Vous absorbez toutes les ombres de la pi�ce, et celles-ci tourbillonnent autour de vous, ne laissant luire que vos yeux per�ants et f�roces. Vous entendez quelques soldats crier et partir en courant. Les autres sont terroris�s et n�osent plus s�approcher.";

        story8010answer1.answerDescriptions.Add(story8010answer1Description1);
        story8010answer2.answerDescriptions.Add(story8010answer2Description1);
        story8010answer3.answerDescriptions.Add(story8010answer3Description1);
        story8010answer4.answerDescriptions.Add(story8010answer4Description1);
        story8010answer5.answerDescriptions.Add(story8010answer5Description1);
        story8010answer6.answerDescriptions.Add(story8010answer6Description1);

        story8010.answers.Add(story8010answer1);
        story8010.answers.Add(story8010answer2);
        story8010.answers.Add(story8010answer3);
        story8010.answers.Add(story8010answer4);
        story8010.answers.Add(story8010answer5);
        story8010.answers.Add(story8010answer6);

        stories.listStory.Add(story8010);

        ////STORY 8020

        Story story8020 = new Story();
        story8020.storyNumber = 8020;

        StoryDescription story8020Description1 = new StoryDescription();
        story8020Description1.answerNumberPreRequise = new List<int>();
        story8020Description1.description = "Dans une derni�re tentative pour r�sister vous voyez un petit groupe de soldat se former en un ultime bastion pr�s de la porte : ";
        story8020.storyDescriptions.Add(story8020Description1);

        Answer story8020answer1 = new Answer();
        Answer story8020answer2 = new Answer();
        Answer story8020answer3 = new Answer();
        Answer story8020answer4 = new Answer();
        Answer story8020answer5 = new Answer();
        Answer story8020answer6 = new Answer();
        Answer story8020answer7 = new Answer();
        Answer story8020answer8 = new Answer();
        Answer story8020answer9 = new Answer();
        Answer story8020answer10 = new Answer();
        Answer story8020answer11 = new Answer();
        Answer story8020answer12 = new Answer();
        Answer story8020answer13 = new Answer();

        story8020answer1.isPreRequiseNotIn = true;
        story8020answer1.answerNumberPreRequise = new List<int> { 8001 };
        story8020answer2.isPreRequiseNotIn = true;
        story8020answer2.answerNumberPreRequise = new List<int> { 8002 };
        story8020answer3.isPreRequiseNotIn = true;
        story8020answer3.answerNumberPreRequise = new List<int> { 8003 };
        story8020answer4.isPreRequiseNotIn = true;
        story8020answer4.answerNumberPreRequise = new List<int> { 8004 };
        story8020answer5.isPreRequiseNotIn = true;
        story8020answer5.answerNumberPreRequise = new List<int> { 8005 };
        story8020answer6.isPreRequiseNotIn = true;
        story8020answer6.answerNumberPreRequise = new List<int> { 8006 };
        story8020answer7.isPreRequiseListAndAndNotOr = false;
        story8020answer7.answerNumberPreRequise = new List<int> { 8001, 8002 };
        story8020answer8.isPreRequiseListAndAndNotOr = false;
        story8020answer8.answerNumberPreRequise = new List<int> { 8003, 8004 };
        story8020answer9.isPreRequiseListAndAndNotOr = false;
        story8020answer9.answerNumberPreRequise = new List<int> { 8005, 8006 };

        story8020answer10.answerNumberPreRequise = new List<int> { 8005, 8001 };
        story8020answer11.answerNumberPreRequise = new List<int> { 8005, 8002 };
        story8020answer12.answerNumberPreRequise = new List<int> { 8006, 8001 };
        story8020answer13.answerNumberPreRequise = new List<int> { 8006, 8002 };

        story8020answer1.nextStory = 10000;
        story8020answer2.nextStory = 10000;
        story8020answer3.nextStory = 10000;
        story8020answer4.nextStory = 10000;
        story8020answer5.nextStory = 10000;
        story8020answer6.nextStory = 10000;
        story8020answer7.nextStory = 10000;
        story8020answer8.nextStory = 10000;
        story8020answer9.nextStory = 10000;
        story8020answer10.nextStory = 10000;
        story8020answer11.nextStory = 10000;
        story8020answer12.nextStory = 10000;
        story8020answer13.nextStory = 10000;

        story8020answer1.discipline = "Bond surhumain";
        story8020answer2.discipline = "Corps lethal";
        story8020answer3.discipline = "Brouillage memoriel";
        story8020answer4.discipline = "Contrainte";
        story8020answer5.discipline = "Regard Abyssal";
        story8020answer6.discipline = "Manteau Tenebreux";
        story8020answer7.discipline = "Prouesse";
        story8020answer8.discipline = "Hypnose";
        story8020answer9.discipline = "Manipulation dombre";
        story8020answer10.discipline = "Bras darhiman";
        story8020answer11.discipline = "Bras darhiman";
        story8020answer12.discipline = "Bras darhiman";
        story8020answer13.discipline = "Bras darhiman";
        story8020answer1.description = "Vous vous propulsez de toute la puissance de vos jambes � travers la masse de soldats. La violence de l�impact d�boite des �paules et les fait chuter au sol.";
        story8020answer2.description = "Vous foncez sur le premier soldat et envoyez un coup de poing fulgurant pour d�truire son gilet par balle.";
        story8020answer3.description = "Vous tendez votre main en avant et supprimez brutalement les derniers souvenirs des soldats pour semer la confusion.";
        story8020answer4.description = "Vous avisez le premier soldat et lui ordonnez de se retourner en tirant sur ses alli�s.";
        story8020answer5.description = "Vous plissez les yeux et parvenez � identifier chacun de vos ennemis � travers les fumig�nes et l�obscurit� profonde.";
        story8020answer6.description = "Vous appelez � vous les ombres et vous enroulez de fum�e noire terrifiante et insondable, terrorisant les soldats.";
        story8020answer7.description = "Vous sentez vos muscles se gorger de puissance. Toutes vos actions deviennent terriblement plus destructives.";
        story8020answer8.description = "Vous profitez du chaos pour poser votre main sur les yeux d�un soldat et en une phrase vous l�hypnotisez compl�tement.";
        story8020answer9.description = "Vous manipulez maintenant votre ombre et lui faites prendre une forme terrifiante.";
        story8020answer10.description = "Une multitude d�appendices ombreux s��chappent de vous tels des bras t�n�breux sortis des enfers.";
        story8020answer11.description = "Une multitude d�appendices ombreux s��chappent de vous tels des bras t�n�breux sortis des enfers.";
        story8020answer12.description = "Une multitude d�appendices ombreux s��chappent de vous tels des bras t�n�breux sortis des enfers.";
        story8020answer13.description = "Une multitude d�appendices ombreux s��chappent de vous tels des bras t�n�breux sortis des enfers.";

        AnswerDescription story8020answer1Description1 = new AnswerDescription();
        AnswerDescription story8020answer2Description1 = new AnswerDescription();
        AnswerDescription story8020answer3Description1 = new AnswerDescription();
        AnswerDescription story8020answer4Description1 = new AnswerDescription();
        AnswerDescription story8020answer5Description1 = new AnswerDescription();
        AnswerDescription story8020answer6Description1 = new AnswerDescription();
        AnswerDescription story8020answer7Description1 = new AnswerDescription();
        AnswerDescription story8020answer8Description1 = new AnswerDescription();
        AnswerDescription story8020answer9Description1 = new AnswerDescription();
        AnswerDescription story8020answer10Description1 = new AnswerDescription();

        story8020answer1Description1.answerDescriptionNumber = 8001;
        story8020answer2Description1.answerDescriptionNumber = 8002;
        story8020answer3Description1.answerDescriptionNumber = 8003;
        story8020answer4Description1.answerDescriptionNumber = 8004;
        story8020answer5Description1.answerDescriptionNumber = 8005;
        story8020answer6Description1.answerDescriptionNumber = 8006;
        story8020answer7Description1.answerDescriptionNumber = 8021;
        story8020answer8Description1.answerDescriptionNumber = 8022;
        story8020answer9Description1.answerDescriptionNumber = 8023;
        story8020answer10Description1.answerDescriptionNumber = 8024;

        story8020answer1Description1.description = "Des jurons explosent dans tous les sens au milieu des cris de douleur. Vous avez fonc� � travers vos ennemis tel un boulet de canon, et ceux-ci mirent quelques secondes � se relever.";
        story8020answer2Description1.description = "Le gilet est transperc� dans une gerbe de sang. Vous sentez votre poing s�enfoncer dans les tripes humides, chaudes et gluantes de votre ennemi. Vous retirez brusquement votre main et celui-ci tombe � terre, mort sur le coup.";
        story8020answer3Description1.description = "Et �a fonctionne � merveille. Vous entendez les soldats hurler confus, se demandant o� est pass� le camion et comment ils sont arriv�s dans cette pi�ce. Ils regardent autour d�eux sans comprendre et perdent de pr�cieuses secondes � prendre connaissance de leur environnement.";
        story8020answer4Description1.description = "Les yeux de ce dernier deviennent vitreux. Il se retourne brusquement en appuyant sur la g�chette et c�est un carnage, il tire dans son groupe pendant plusieurs secondes et des hurlements de douleur retentissent dans les rangs de vos ennemis.";
        story8020answer5Description1.description = "Vous avez l�impression que les fumig�nes ont totalement disparu, mais il n�en est rien. C�est votre regard surnaturel qui perce le voile entre les mondes. 8 ennemis se tiennent devant vous, ils semblent coordonn�s et vous parvenez � d�cortiquer chacun de leurs mouvements, le moindre de leurs pas. Vous arrivez presque � pr�voir leurs actions.";
        story8020answer6Description1.description = "Du point de vue de vos ennemis c�est comme si l�air s'engouffrait dans les tr�fonds d�un tunnel emportant un tourbillon d�ombres comme des volumes de fum�e charbonneuse. Vous absorbez toutes les ombres de la pi�ce, et celles-ci tourbillonnent autour de vous, ne laissant luire que vos yeux per�ants et f�roces. Vous entendez quelques soldats crier et partir en courant. Les autres sont terroris�s et n�osent plus s�approcher.";
        story8020answer7Description1.description = "Vous agrippez le soldat le plus proche par la figure, et votre seule force de pr�hension suffit � faire exploser son visage sous vos doigts. Vous balancez son corps sur ses alli�s terroris�s.";
        story8020answer8Description1.description = "Il est persuad� d��tre votre petit fr�re, et compte utiliser tout son arsenal d'armes pour vous prot�ger. Vous le voyez sortir une grenade, il vous fait un clin d'�il rassurant et la balance sur vos ennemis. A peine a- t-elle quitt� ses doigts et parcourus 3 m�tres qu�elle d�tonne dans un souffle explosif projetant des membres dans une gerbe de sang.";
        story8020answer9Description1.description = "L�obscurit� envahit la pi�ce alors que vous projetez votre ombre autours de vous. Elle se r�pand comme une tra�n�e de feu, enveloppant les soldats terroris�s. Recouvert totalement et plong�s dans l�obscurit� la plus totale, les soldats vivent leurs pire cauchemar.";
        story8020answer10Description1.description = "Ils viennent s�enrouler autour des armes des soldats pour leur arracher des mains et les saisir � la gorge. Certains tentent de lutter contre, sans succ�s. Vous soulevez un couteau � l�aide d�une main de t�n�bre et commencez � tailler m�thodiquement le visage de  vos victimes immobilis�es.";


        story8020answer1.answerDescriptions.Add(story8020answer1Description1);
        story8020answer2.answerDescriptions.Add(story8020answer2Description1);
        story8020answer3.answerDescriptions.Add(story8020answer3Description1);
        story8020answer4.answerDescriptions.Add(story8020answer4Description1);
        story8020answer5.answerDescriptions.Add(story8020answer5Description1);
        story8020answer6.answerDescriptions.Add(story8020answer6Description1);
        story8020answer7.answerDescriptions.Add(story8020answer7Description1);
        story8020answer8.answerDescriptions.Add(story8020answer8Description1);
        story8020answer9.answerDescriptions.Add(story8020answer9Description1);
        story8020answer10.answerDescriptions.Add(story8020answer10Description1);
        story8020answer11.answerDescriptions.Add(story8020answer10Description1);
        story8020answer12.answerDescriptions.Add(story8020answer10Description1);
        story8020answer13.answerDescriptions.Add(story8020answer10Description1);

        story8020.answers.Add(story8020answer7); //Put the lvl 2 firsts
        story8020.answers.Add(story8020answer8);
        story8020.answers.Add(story8020answer9);
        story8020.answers.Add(story8020answer10);
        story8020.answers.Add(story8020answer11);
        story8020.answers.Add(story8020answer12);
        story8020.answers.Add(story8020answer13);
        story8020.answers.Add(story8020answer1);
        story8020.answers.Add(story8020answer2);
        story8020.answers.Add(story8020answer3);
        story8020.answers.Add(story8020answer4);
        story8020.answers.Add(story8020answer5);
        story8020.answers.Add(story8020answer6);

        stories.listStory.Add(story8020);

        /////STORY 9000

        Story story9000 = new Story();
        story9000.storyNumber = 9000;

        StoryDescription story9000Description1 = new StoryDescription();
        story9000Description1.answerNumberPreRequise = new List<int>();
        story9000Description1.description = "Vous sentez la chaleur dans ses paroles et savez que vous venez de trouver une nouvelle famille en ce monde, m�me si vous ne savez pas encore ce qui vous attend. Tout � coup, la porte de la salle explose, une grenade roule au sol et se met � cracher de la fum�e partout. La jeune femme se precipite dessus, mais aussitot des lasers percent la fum�e et des coups de feu retentissent dans la salle, assourdissants.";
        story9000.storyDescriptions.Add(story9000Description1);

        Answer story9000answer1 = new Answer();
        Answer story9000answer2 = new Answer();
        Answer story9000answer3 = new Answer();
        Answer story9000answer4 = new Answer();
        Answer story9000answer5 = new Answer();

        story9000answer1.isPreRequiseNotIn = true;
        story9000answer1.answerNumberPreRequise = new List<int>();
        story9000answer2.isPreRequiseNotIn = true;
        story9000answer2.answerNumberPreRequise = new List<int>();
        story9000answer3.isPreRequiseNotIn = true;
        story9000answer3.answerNumberPreRequise = new List<int>();
        story9000answer4.isPreRequiseNotIn = true;
        story9000answer4.answerNumberPreRequise = new List<int>();
        story9000answer5.isPreRequiseNotIn = true;
        story9000answer5.answerNumberPreRequise = new List<int>();

        story9000answer1.nextStory = 9010;
        story9000answer2.nextStory = 9010;
        story9000answer3.nextStory = 9010;
        story9000answer4.nextStory = 9010;
        story9000answer5.nextStory = 9010;
        story9000answer1.discipline = "Lier le famulus";
        story9000answer2.discipline = "Poid plume";
        story9000answer3.discipline = "Yeux de la bete";
        story9000answer4.discipline = "Brouillage memoriel";
        story9000answer5.discipline = "Contrainte";
        story9000answer1.description = "Vous vous concentrez sur le gros chat et vous liez � lui pour qu�il attaque f�rocement les soldats";
        story9000answer2.description = "Vous sautez vers les soldats et votre poids diminue jusqu�� ce que vous ne pesiez plus rien. Vous vous accrochez au plafond pour les surprendre, la gravit� n�a plus d�effet sur vous.";
        story9000answer3.description = "Votre regard devient rouge �carlate et vous permet d�identifier vos cibles � travers les fumig�nes. Les soldats les voient briller comme deux brasiers et sont terrifi�s.";
        story9000answer4.description = "Vous tendez votre main en avant et supprimez brutalement les derniers souvenirs des soldats pour semer la confusion.";
        story9000answer5.description = "Vous avisez le premier soldat et lui ordonnez de se retourner en tirant sur ses alli�s.";

        AnswerDescription story9000answer1Description1 = new AnswerDescription();
        AnswerDescription story9000answer2Description1 = new AnswerDescription();
        AnswerDescription story9000answer3Description1 = new AnswerDescription();
        AnswerDescription story9000answer4Description1 = new AnswerDescription();
        AnswerDescription story9000answer5Description1 = new AnswerDescription();

        story9000answer1Description1.answerDescriptionNumber = 9001;
        story9000answer2Description1.answerDescriptionNumber = 9002;
        story9000answer3Description1.answerDescriptionNumber = 9003;
        story9000answer4Description1.answerDescriptionNumber = 9004;
        story9000answer5Description1.answerDescriptionNumber = 9005;

        story9000answer1Description1.description = "Les pupilles de l�animal se dilatent comme s�il comprenait votre volont�. Vous ne le commandez pas, mais il vous rend cela comme un service. Il bondit, toutes griffes dehors sur un soldat proche de vous et lui cr�ve les yeux d�un coup de pattes dans un feulement terrifiant. Vous le voyez alors bondir sur le prochain ennemi alors que sa victime se roule par terre en hurlant, aveugle � jamais.";
        story9000answer2Description1.description = "Et vous vous tenez l�, la t�te � l�envers dans un coin du plafond pendant que les soldats finissent d�entrer dans la pi�ce, confus de n�y trouver personne.";
        story9000answer3Description1.description = "Vous entendez quelques-un des soldats crier et partir en courant. Les autres mettent un moment avant d�oser avancer dans votre direction.";
        story9000answer4Description1.description = "Et �a fonctionne � merveille. Vous entendez les soldats hurler confus, se demandant o� est pass� le camion et comment ils sont arriv�s dans cette pi�ce. Ils regardent autour d�eux sans comprendre et perdent de pr�cieuses secondes � prendre connaissance de leur environnement.";
        story9000answer5Description1.description = "Les yeux de ce dernier deviennent vitreux. Il se retourne brusquement en appuyant sur la g�chette et c�est un carnage, il tire dans son groupe pendant plusieurs secondes et des hurlements de douleur retentissent dans les rangs de vos ennemis.";

        story9000answer1.answerDescriptions.Add(story9000answer1Description1);
        story9000answer2.answerDescriptions.Add(story9000answer2Description1);
        story9000answer3.answerDescriptions.Add(story9000answer3Description1);
        story9000answer4.answerDescriptions.Add(story9000answer4Description1);
        story9000answer5.answerDescriptions.Add(story9000answer5Description1);

        story9000.answers.Add(story9000answer1);
        story9000.answers.Add(story9000answer2);
        story9000.answers.Add(story9000answer3);
        story9000.answers.Add(story9000answer4);
        story9000.answers.Add(story9000answer5);

        stories.listStory.Add(story9000);

        //STORY 9010

        Story story9010 = new Story();
        story9010.storyNumber = 9010;

        StoryDescription story9010Description1 = new StoryDescription();
        story9010Description1.answerNumberPreRequise = new List<int>();
        story9010Description1.description = "Certains des hommes arm�s r�sistent et se mettent alors � tirer dans tous les sens, et vous sentez que vous pouvez les contrer : ";
        story9010.storyDescriptions.Add(story9010Description1);

        Answer story9010answer1 = new Answer();
        Answer story9010answer2 = new Answer();
        Answer story9010answer3 = new Answer();
        Answer story9010answer4 = new Answer();
        Answer story9010answer5 = new Answer();

        story9010answer1.isPreRequiseNotIn = true;
        story9010answer1.answerNumberPreRequise = new List<int> { 9001 };
        story9010answer2.isPreRequiseNotIn = true;
        story9010answer2.answerNumberPreRequise = new List<int> { 9002 };
        story9010answer3.isPreRequiseNotIn = true;
        story9010answer3.answerNumberPreRequise = new List<int> { 9003 };
        story9010answer4.isPreRequiseNotIn = true;
        story9010answer4.answerNumberPreRequise = new List<int> { 9004 };
        story9010answer5.isPreRequiseNotIn = true;
        story9010answer5.answerNumberPreRequise = new List<int> { 9005 };

        story9010answer1.nextStory = 9020;
        story9010answer2.nextStory = 9020;
        story9010answer3.nextStory = 9020;
        story9010answer4.nextStory = 9020;
        story9010answer5.nextStory = 9020;
        story9010answer1.discipline = "Lier le famulus";
        story9010answer2.discipline = "Poid plume";
        story9010answer3.discipline = "Yeux de la bete";
        story9010answer4.discipline = "Brouillage memoriel";
        story9010answer5.discipline = "Contrainte";
        story9010answer1.description = "Vous vous concentrez sur le gros chat et vous liez � lui pour qu�il attaque f�rocement les soldats";
        story9010answer2.description = "Vous sautez vers les soldats et votre poids diminue jusqu�� ce que vous ne pesiez plus rien. Vous vous accrochez au plafond pour les surprendre, la gravit� n�a plus d�effet sur vous.";
        story9010answer3.description = "Votre regard devient rouge �carlate et vous permet d�identifier vos cibles � travers les fumig�nes. Les soldats les voient briller comme deux brasiers et sont terrifi�s.";
        story9010answer4.description = "Vous tendez votre main en avant et supprimez brutalement les derniers souvenirs des soldats pour semer la confusion.";
        story9010answer5.description = "Vous avisez le premier soldat et lui ordonnez de se retourner en tirant sur ses alli�s.";

        AnswerDescription story9010answer1Description1 = new AnswerDescription();
        AnswerDescription story9010answer2Description1 = new AnswerDescription();
        AnswerDescription story9010answer3Description1 = new AnswerDescription();
        AnswerDescription story9010answer4Description1 = new AnswerDescription();
        AnswerDescription story9010answer5Description1 = new AnswerDescription();

        story9010answer1Description1.answerDescriptionNumber = 9001;
        story9010answer2Description1.answerDescriptionNumber = 9002;
        story9010answer3Description1.answerDescriptionNumber = 9003;
        story9010answer4Description1.answerDescriptionNumber = 9004;
        story9010answer5Description1.answerDescriptionNumber = 9005;

        story9010answer1Description1.description = "Les pupilles de l�animal se dilatent comme s�il comprenait votre volont�. Vous ne le commandez pas, mais il vous rend cela comme un service. Il bondit, toutes griffes dehors sur un soldat proche de vous et lui cr�ve les yeux d�un coup de pattes dans un feulement terrifiant. Vous le voyez alors bondir sur le prochain ennemi alors que sa victime se roule par terre en hurlant, aveugle � jamais.";
        story9010answer2Description1.description = "Et vous vous tenez l�, la t�te � l�envers dans un coin du plafond pendant que les soldats finissent d�entrer dans la pi�ce, confus de n�y trouver personne.";
        story9010answer3Description1.description = "Vous entendez quelques-un des soldats crier et partir en courant. Les autres mettent un moment avant d�oser avancer dans votre direction.";
        story9010answer4Description1.description = "Et �a fonctionne � merveille. Vous entendez les soldats hurler confus, se demandant o� est pass� le camion et comment ils sont arriv�s dans cette pi�ce. Ils regardent autour d�eux sans comprendre et perdent de pr�cieuses secondes � prendre connaissance de leur environnement.";
        story9010answer5Description1.description = "Les yeux de ce dernier deviennent vitreux. Il se retourne brusquement en appuyant sur la g�chette et c�est un carnage, il tire dans son groupe pendant plusieurs secondes et des hurlements de douleur retentissent dans les rangs de vos ennemis.";

        story9010answer1.answerDescriptions.Add(story9010answer1Description1);
        story9010answer2.answerDescriptions.Add(story9010answer2Description1);
        story9010answer3.answerDescriptions.Add(story9010answer3Description1);
        story9010answer4.answerDescriptions.Add(story9010answer4Description1);
        story9010answer5.answerDescriptions.Add(story9010answer5Description1);

        story9010.answers.Add(story9010answer1);
        story9010.answers.Add(story9010answer2);
        story9010.answers.Add(story9010answer3);
        story9010.answers.Add(story9010answer4);
        story9010.answers.Add(story9010answer5);

        stories.listStory.Add(story9010);

        ////STORY 9020

        Story story9020 = new Story();
        story9020.storyNumber = 9020;

        StoryDescription story9020Description1 = new StoryDescription();
        story9020Description1.answerNumberPreRequise = new List<int>();
        story9020Description1.description = "Dans une derni�re tentative pour r�sister vous voyez un petit groupe de soldat se former en un ultime bastion pr�s de la porte : ";
        story9020.storyDescriptions.Add(story9020Description1);

        Answer story9020answer1 = new Answer();
        Answer story9020answer2 = new Answer();
        Answer story9020answer3 = new Answer();
        Answer story9020answer4 = new Answer();
        Answer story9020answer5 = new Answer();
        Answer story9020answer6 = new Answer();
        Answer story9020answer7 = new Answer();
        Answer story9020answer8 = new Answer();
        Answer story9020answer9 = new Answer();
        Answer story9020answer10 = new Answer();
        Answer story9020answer11 = new Answer();
        Answer story9020answer12 = new Answer();

        story9020answer1.isPreRequiseNotIn = true;
        story9020answer1.answerNumberPreRequise = new List<int> { 9001 };
        story9020answer2.isPreRequiseNotIn = true;
        story9020answer2.answerNumberPreRequise = new List<int> { 9002 };
        story9020answer3.isPreRequiseNotIn = true;
        story9020answer3.answerNumberPreRequise = new List<int> { 9003 };
        story9020answer4.isPreRequiseNotIn = true;
        story9020answer4.answerNumberPreRequise = new List<int> { 9004 };
        story9020answer5.isPreRequiseNotIn = true;
        story9020answer5.answerNumberPreRequise = new List<int> { 9005 };

        story9020answer6.answerNumberPreRequise = new List<int> { 9001 };
        story9020answer7.isPreRequiseListAndAndNotOr = false;
        story9020answer7.answerNumberPreRequise = new List<int> { 9002, 9003 };

        story9020answer8.answerNumberPreRequise = new List<int> { 9002,9004 };
        story9020answer9.answerNumberPreRequise = new List<int> { 9002, 9005 };
        story9020answer10.answerNumberPreRequise = new List<int> { 9003, 9004 };
        story9020answer11.answerNumberPreRequise = new List<int> { 9003, 9005 };
        story9020answer12.isPreRequiseListAndAndNotOr = false;
        story9020answer12.answerNumberPreRequise = new List<int> { 9004, 9005 };


        story9020answer1.nextStory = 10000;
        story9020answer2.nextStory = 10000;
        story9020answer3.nextStory = 10000;
        story9020answer4.nextStory = 10000;
        story9020answer5.nextStory = 10000;
        story9020answer6.nextStory = 10000;
        story9020answer7.nextStory = 10000;
        story9020answer8.nextStory = 10000;
        story9020answer9.nextStory = 10000;
        story9020answer10.nextStory = 10000;
        story9020answer11.nextStory = 10000;
        story9020answer12.nextStory = 10000;

        story9020answer1.discipline = "Lier le famulus";
        story9020answer2.discipline = "Poid plume";
        story9020answer3.discipline = "Yeux de la bete";
        story9020answer4.discipline = "Brouillage memoriel";
        story9020answer5.discipline = "Contrainte";
        story9020answer6.discipline = "Murmure sauvage";
        story9020answer7.discipline = "Armes bestiales";
        story9020answer8.discipline = "Vicissitudes";
        story9020answer9.discipline = "Vicissitudes";
        story9020answer10.discipline = "Vicissitudes";
        story9020answer11.discipline = "Vicissitudes";
        story9020answer12.discipline = "Hypnose";
        story9020answer1.description = "Vous vous concentrez sur le gros chat et vous liez � lui pour qu�il attaque f�rocement les soldats";
        story9020answer2.description = "Vous sautez vers les soldats et votre poids diminue jusqu�� ce que vous ne pesiez plus rien. Vous vous accrochez au plafond pour les surprendre, la gravit� n�a plus d�effet sur vous.";
        story9020answer3.description = "Votre regard devient rouge �carlate et vous permet d�identifier vos cibles � travers les fumig�nes. Les soldats les voient briller comme deux brasiers et sont terrifi�s.";
        story9020answer4.description = "Vous tendez votre main en avant et supprimez brutalement les derniers souvenirs des soldats pour semer la confusion.";
        story9020answer5.description = "Vous avisez le premier soldat et lui ordonnez de se retourner en tirant sur ses alli�s.";
        story9020answer6.description = "Vous avez maintenant un total contr�le sur toutes les cr�atures animales. Le gros chat blanc est desormais couvert de sang et vous lui ordonnez de redoubler de violence.";
        story9020answer7.description = "Vos ongles s�allongent et prennent la forme de griffes tranchantes. Elles d�coupent indistinctement la chaire et l�acier.";
        story9020answer8.description = "Vous manipulez la chair comme de la glaise, vous faites sortir un de vos os de votre bras et embrochez vos ennemis avec.";
        story9020answer9.description = "Vous manipulez la chair comme de la glaise, vous faites sortir un de vos os de votre bras et embrochez vos ennemis avec.";
        story9020answer10.description = "Vous manipulez la chair comme de la glaise, vous faites sortir un de vos os de votre bras et embrochez vos ennemis avec.";
        story9020answer11.description = "Vous manipulez la chair comme de la glaise, vous faites sortir un de vos os de votre bras et embrochez vos ennemis avec.";
        story9020answer12.description = "Vous profitez du chaos pour poser votre main sur les yeux d�un soldat et en une phrase vous l�hypnotisez compl�tement.";

        AnswerDescription story9020answer1Description1 = new AnswerDescription();
        AnswerDescription story9020answer2Description1 = new AnswerDescription();
        AnswerDescription story9020answer3Description1 = new AnswerDescription();
        AnswerDescription story9020answer4Description1 = new AnswerDescription();
        AnswerDescription story9020answer5Description1 = new AnswerDescription();
        AnswerDescription story9020answer6Description1 = new AnswerDescription();
        AnswerDescription story9020answer7Description1 = new AnswerDescription();
        AnswerDescription story9020answer8Description1 = new AnswerDescription();
        AnswerDescription story9020answer9Description1 = new AnswerDescription();

        story9020answer1Description1.answerDescriptionNumber = 9001;
        story9020answer2Description1.answerDescriptionNumber = 9002;
        story9020answer3Description1.answerDescriptionNumber = 9003;
        story9020answer4Description1.answerDescriptionNumber = 9004;
        story9020answer5Description1.answerDescriptionNumber = 9005;
        story9020answer6Description1.answerDescriptionNumber = 9021;
        story9020answer7Description1.answerDescriptionNumber = 9022;
        story9020answer8Description1.answerDescriptionNumber = 9023;
        story9020answer9Description1.answerDescriptionNumber = 9024;

        story9020answer1Description1.description = "Les pupilles de l�animal se dilatent comme s�il comprenait votre volont�. Vous ne le commandez pas, mais il vous rend cela comme un service. Il bondit, toutes griffes dehors sur un soldat proche de vous et lui cr�ve les yeux d�un coup de pattes dans un feulement terrifiant. Vous le voyez alors bondir sur le prochain ennemi alors que sa victime se roule par terre en hurlant, aveugle � jamais.";
        story9020answer2Description1.description = "Et vous vous tenez l�, la t�te � l�envers dans un coin du plafond pendant que les soldats finissent d�entrer dans la pi�ce, confus de n�y trouver personne.";
        story9020answer3Description1.description = "Vous entendez quelques-un des soldats crier et partir en courant. Les autres mettent un moment avant d�oser avancer dans votre direction.";
        story9020answer4Description1.description = "Et �a fonctionne � merveille. Vous entendez les soldats hurler confus, se demandant o� est pass� le camion et comment ils sont arriv�s dans cette pi�ce. Ils regardent autour d�eux sans comprendre et perdent de pr�cieuses secondes � prendre connaissance de leur environnement.";
        story9020answer5Description1.description = "Les yeux de ce dernier deviennent vitreux. Il se retourne brusquement en appuyant sur la g�chette et c�est un carnage, il tire dans son groupe pendant plusieurs secondes et des hurlements de douleur retentissent dans les rangs de vos ennemis.";
        story9020answer6Description1.description = "Celui ci ne peut pas resister � votre appel mental et est totalement sous votre controle. Les millions d�ann�e d'�volution de son esp�ce remonte au grand jour et il d�chiqu�te avec all�gresse tous les soldats � sa port�e.";
        story9020answer7Description1.description = "Vous vous propulsez ainsi dans la m�l�e et d�coupez un canon en train de tirer. Vous griffez ensuite le visage d�un soldat qui tente vainement de se d�fendre en levant des mains devant son visage. Et ses mains tombent au sol, d�coup�es dans une gerbe de sang.";
        story9020answer8Description1.description = "Et c�est naturel pour vous d�abord vous empalez un ennemi avec l�os sorti de votre avant bras, puis vous d�cidez de vous amuser et collez la paume de votre main sur le visage d�un autre soldat.  Vous manipulez alors la chair de son visage pour faire dispara�tre son nez et sa bouche. Celui-ci �carquille les yeux d�horreur griffant sa face vainement, tentant de respirer sans trou pour le faire sous le regard terrifi� de ses camarades. Finalement il s'effondre au sol, mort dans un ultime soubresaut.";
        story9020answer9Description1.description = " Il est persuad� d��tre votre petit fr�re, et compte utiliser tout son arsenal d'armes pour vous prot�ger. Vous le voyez sortir une grenade, il vous fait un clin d'�il rassurant et la balance sur vos ennemis. A peine a- t-elle quitt� ses doigts et parcourus 3 m�tres qu�elle d�tonne dans un souffle explosif projetant des membres dans une gerbe de sang.";

        story9020answer1.answerDescriptions.Add(story9020answer1Description1);
        story9020answer2.answerDescriptions.Add(story9020answer2Description1);
        story9020answer3.answerDescriptions.Add(story9020answer3Description1);
        story9020answer4.answerDescriptions.Add(story9020answer4Description1);
        story9020answer5.answerDescriptions.Add(story9020answer5Description1);
        story9020answer6.answerDescriptions.Add(story9020answer6Description1);
        story9020answer7.answerDescriptions.Add(story9020answer7Description1);
        story9020answer8.answerDescriptions.Add(story9020answer8Description1);
        story9020answer9.answerDescriptions.Add(story9020answer8Description1);
        story9020answer10.answerDescriptions.Add(story9020answer8Description1);
        story9020answer11.answerDescriptions.Add(story9020answer8Description1);
        story9020answer12.answerDescriptions.Add(story9020answer9Description1);

        story9020.answers.Add(story9020answer6);
        story9020.answers.Add(story9020answer7); //Put the lvl 2 firsts
        story9020.answers.Add(story9020answer8);
        story9020.answers.Add(story9020answer9);
        story9020.answers.Add(story9020answer10);
        story9020.answers.Add(story9020answer11);
        story9020.answers.Add(story9020answer12);
        story9020.answers.Add(story9020answer1);
        story9020.answers.Add(story9020answer2);
        story9020.answers.Add(story9020answer3);
        story9020.answers.Add(story9020answer4);
        story9020.answers.Add(story9020answer5);

        stories.listStory.Add(story9020);


        /////STORY 10000
        ///

        Story story10000 = new Story();
        story10000.storyNumber = 10000;

        StoryDescription story10000Description1 = new StoryDescription();
        story10000Description1.answerNumberPreRequise = new List<int>();
        story10000Description1.description = "La jeune femme apparait alors � nouveau devant vous et vous regarde en souriant, vous avez bien fait usage de vos comp�tences et cela lui a plu. Elle se charge alors de se d�barrasser des soldats et absolument aucun ne parviennent � �chapper au massacre. Elle se tourne alors vers vous et vous comprenez que vous �tes devenu un Vampire, dot� de capacit� surhumaines.";
        story10000.storyDescriptions.Add(story10000Description1);
        Answer story10000answer1 = new Answer();
        story10000answer1.attributsBoost = new List<string>();
        story10000answer1.specialiteBoost = new List<string>();
        story10000answer1.nextStory = 99999;
        story10000answer1.description = "J�ai compris, et ceci signe la fin de cette cr�ation de personnage";
        AnswerDescription story10000answer1Description1 = new AnswerDescription();
        story10000answer1Description1.answerDescriptionNumber = 10001;
        story10000answer1Description1.answerNumberPreRequise = new List<int>();
        story10000answer1Description1.description = "";
        story10000answer1.answerDescriptions.Add(story10000answer1Description1);
        story10000.answers.Add(story10000answer1);
        stories.listStory.Add(story10000);

        /////STORY 99999
        ///
        Story story99999 = new Story();
        story99999.storyNumber = 99999;

        StoryDescription story99999Description1 = new StoryDescription();
        story99999Description1.answerNumberPreRequise = new List<int>();
        story99999Description1.description = "Vous �tes arriv�s � la fin du sc�nario ! Nous vous invitons � cliquer sur Character Sheet, pour r�cup�rer les informations de votre personnage. Vous serez en mesure de changer les statistiques � souhait et compl�ter les informations avant de sortir la fiche de personnage.";
        story99999.storyDescriptions.Add(story99999Description1);
        Answer story99999answer1 = new Answer();
        story99999answer1.attributsBoost = new List<string>();
        story99999answer1.specialiteBoost = new List<string>();
        story99999answer1.nextStory = 0;
        story99999answer1.description = "J�ai compris.";
        AnswerDescription story99999answer1Description1 = new AnswerDescription();
        story99999answer1Description1.answerDescriptionNumber = 99999;
        story99999answer1Description1.answerNumberPreRequise = new List<int>();
        story99999answer1Description1.description = "";
        story99999answer1.answerDescriptions.Add(story99999answer1Description1);
        story99999.answers.Add(story99999answer1);
        stories.listStory.Add(story99999);

        ///////////////////////JSON






        Debug.Log(SaveToString(stories));


    }

    public string SaveToString(Stories stories)
    {
        return JsonUtility.ToJson(stories);
    }
}
