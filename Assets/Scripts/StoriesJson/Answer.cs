using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Answer 
{
    public int nextStory = 0;
    public int answerNumber = 0;
    //Si on veut afficher seulement la premi�re Answer Description avec requirement qui match
    public bool isOnlyOneAnswerDescription = false;
    //What is on the answer button
    public string description = "";
    //What will appear on the next main text if the player pick up this answer. One from the list.
    public List<AnswerDescription> answerDescriptions = new List<AnswerDescription>();
    public List<string> attributsBoost = new List<string>();
    public List<string> specialiteBoost = new List<string>();
    public string clan = "";
    public string discipline = "";
    public bool isPreRequiseListAndAndNotOr = true;
    public bool isPreRequiseNotIn = false;
    public List<int> answerNumberPreRequise = new List<int>();
    public int answerDescriptionNumber = 0;
}
