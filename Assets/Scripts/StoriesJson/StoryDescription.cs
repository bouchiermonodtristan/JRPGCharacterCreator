using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class StoryDescription 
{
    //used to know if it's 15 && 16 or 15 || 16
    public bool isPreRequiseListAndAndNotOr = true;
    //Is !in list
    public bool isPreRequiseNotIn = false;
    public int storyDescriptionNumber = 0;
    public List<int> answerNumberPreRequise =  new List<int>();
    public string description = "";
}
