using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class AnswerDescription
{
    public bool isPreRequiseListAndAndNotOr = true;
    public bool isPreRequiseNotIn = false;
    public int answerDescriptionNumber = 0;
    public List<int> answerNumberPreRequise = new List<int>();
    public string description = "";
}
