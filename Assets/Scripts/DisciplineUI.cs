using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Michsky.UI.ModernUIPack;
public class DisciplineUI : MonoBehaviour
{
    public DotsUI dotsUI;
    public CustomInputField DisciplineTitle;
    public List<CustomInputField> DisciplinePowerSlot;
    public string discipline = "";

    public void setDiscipline(string localDiscipline)
    {
        discipline = localDiscipline;
        DisciplineTitle.inputText.text = localDiscipline;
        DisciplineTitle.UpdateState();
    }

    public void clean()
    {
        discipline = "";
        DisciplineTitle.inputText.text = "";
        DisciplineTitle.UpdateState();
        foreach (CustomInputField power in DisciplinePowerSlot)
        {
            power.inputText.text = "";
            power.UpdateState();
        }
        dotsUI.deactivateAllToggles();
    }

    public void setDisciplineValue(int value)
    {
        dotsUI.activateToggles(value);
    }

    public void addNewPower(string powerName)
    {
        foreach(CustomInputField field in DisciplinePowerSlot)
        {
            if(field.inputText.text == "")
            {
                field.inputText.text = powerName;
                field.UpdateState();
                break;
            }
        }
    }
}
