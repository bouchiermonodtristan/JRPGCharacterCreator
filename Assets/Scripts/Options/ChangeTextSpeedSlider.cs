using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Slider))]
public class ChangeTextSpeedSlider : MonoBehaviour
{

    public void changeTextSpeed()
    {
        VampireUIManager.current.textSpeed = 0.3f / (GetComponent<Slider>().value * 6);
    }
}
